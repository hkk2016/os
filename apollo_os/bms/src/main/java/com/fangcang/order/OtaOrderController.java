package com.fangcang.order;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.constant.InitData;
import com.fangcang.common.controller.BaseController;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.util.DateUtil;
import com.fangcang.order.domain.OrderDO;
import com.fangcang.order.request.AddOrderReqRequestDTO;
import com.fangcang.order.request.ChangeChannelOrderCodeRequestDTO;
import com.fangcang.order.request.CreateOrderRequestDTO;
import com.fangcang.order.request.OrderCancelRequestDTO;
import com.fangcang.order.request.OrderDetailRequestDTO;
import com.fangcang.order.response.OrderDetailResponseDTO;
import com.fangcang.order.response.SupplyProductPriceResponseDTO;
import com.fangcang.order.service.OrderDetailService;
import com.fangcang.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

/**
 * @author : zhanwang
 * @date : 2018/5/21
 */
@RestController
@Slf4j
@RequestMapping("/otaOrder")
public class OtaOrderController extends BaseController {

    @Resource
    private OrderService orderService;
    @Resource
    private OrderDetailService orderDetailService;

    @RequestMapping(value = "/preBooking/feizhu", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<Collection> preBookingForFeizhu(@RequestBody @Valid CreateOrderRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            responseDTO = orderService.preBookingForFeizhu(requestDTO);
        } catch (Exception e) {
            log.error("preBookingForFeizhu error:{}", JSON.toJSONString(requestDTO),e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/queryOrder/feizhu", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO queryOrderForFeizhu(@RequestBody @Valid OrderDetailRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            responseDTO = orderDetailService.queryOrderByCustomerOrderCode(requestDTO);
        } catch (Exception e) {
            log.error("preBookingForFeizhu error:{}", JSON.toJSONString(requestDTO),e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/preBooking", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<Collection<SupplyProductPriceResponseDTO>> preBooking(@RequestBody @Valid CreateOrderRequestDTO requestDTO) {
        ResponseDTO<Collection<SupplyProductPriceResponseDTO>> responseDTO = new ResponseDTO<>();
        try {
            responseDTO = orderService.preBooking(requestDTO);
        } catch (Exception e) {
            log.error("preBooking error:{}", JSON.toJSONString(requestDTO),e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 生成手工单
     * @param requestDTO
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO create(@RequestBody @Valid CreateOrderRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.setCreateTime(new Date());
            requestDTO.setMerchantName(InitData.MERCHANT_CODE_NAME_MAP.get(requestDTO.getMerchantCode()));
            responseDTO = orderService.addManualOrder(requestDTO);
        } catch (Exception e) {
            log.error("otaOrder create error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 正常下单，包含试预定流程
     * @param requestDTO
     * @return
     */
    @RequestMapping(value = "/booking", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO booking(@RequestBody @Valid CreateOrderRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.setCreateTime(new Date());
            requestDTO.setMerchantName(InitData.MERCHANT_CODE_NAME_MAP.get(requestDTO.getMerchantCode()));
            responseDTO = orderService.create(requestDTO);
        } catch (Exception e) {
            log.error("otaOrder create error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO cancel(@RequestBody @Valid OrderCancelRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.setModifyTime(new Date());
            responseDTO = orderService.cancelOrder(requestDTO);
        } catch (Exception e) {
            log.error("otaOrder cancel error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/orderInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<OrderDetailResponseDTO> orderInfo(@RequestBody @Valid OrderDetailRequestDTO requestDTO) {
        ResponseDTO<OrderDetailResponseDTO> responseDTO = new ResponseDTO<>();
        try {
            responseDTO = orderDetailService.orderInfo(requestDTO);
        } catch (Exception e) {
            log.error("otaOrderInfo error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/queryOrderByCustomerOrderCode", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<OrderDO> queryOrderByCustomerOrderCode(@RequestBody @Valid OrderDetailRequestDTO requestDTO) {
        ResponseDTO<OrderDO> responseDTO = new ResponseDTO<>();
        try {
            responseDTO = orderDetailService.queryOrderByCustomerOrderCode(requestDTO);
        } catch (Exception e) {
            log.error("queryOrderByCustomerOrderCode error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/addOrderRequest", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO addOrderRequest(@RequestBody @Valid AddOrderReqRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.setCreateTime(DateUtil.dateToStringWithHms(new Date()));
            responseDTO = orderService.addOrderRequest(requestDTO);
        } catch (Exception e) {
            log.error("addOrderRequest error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/changeChannelOrderCode", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO changeChannelOrderCode(@RequestBody @Valid ChangeChannelOrderCodeRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.setModifyTime(new Date());
            responseDTO = orderService.changeChannelOrderCode(requestDTO);
        } catch (Exception e) {
            log.error("changeChannelOrderCode error:", e);
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }
}
