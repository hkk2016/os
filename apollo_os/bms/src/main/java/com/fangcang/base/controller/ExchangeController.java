package com.fangcang.base.controller;

import com.fangcang.base.dto.CurrencyDTO;
import com.fangcang.base.request.QueryExchangeDTO;
import com.fangcang.base.request.UpdateExchangeDTO;
import com.fangcang.base.response.ExchangeDTO;
import com.fangcang.base.response.ExchangeLogDTO;
import com.fangcang.base.service.ExchangeService;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.controller.BaseController;
import com.fangcang.common.enums.CurrencyEnum;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(("/base/exchange"))
public class ExchangeController extends BaseController {

    @Autowired
    private ExchangeService exchangeService;

    @RequestMapping(value = "/queryExchange", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public ResponseDTO queryExchange(@RequestBody QueryExchangeDTO requestDTO) {
        ResponseDTO responseDTO;
        try {
            requestDTO.setMerchantCode(super.getCacheUser().getMerchantCode());
            List<ExchangeDTO> exchangeDTOS = exchangeService.queryExchange(requestDTO);
            responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
            responseDTO.setModel(exchangeDTOS);
        } catch (Exception e) {
            log.error("exchangeService.queryExchange 异常", e);
            responseDTO = new ResponseDTO(ResultCodeEnum.FAILURE.code, ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode, ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }


    @RequestMapping(value = "/queryExchangeLog", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public ResponseDTO queryExchangeLog(@RequestBody QueryExchangeDTO requestDTO) {
        ResponseDTO responseDTO;
        try {
            requestDTO.setMerchantCode(super.getCacheUser().getMerchantCode());
            List<ExchangeLogDTO> exchangeLogDTOS = exchangeService.queryExchangeLog(requestDTO);
            responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
            responseDTO.setModel(exchangeLogDTOS);
        } catch (Exception e) {
            log.error("exchangeService.queryExchangeLog 异常", e);
            responseDTO = new ResponseDTO(ResultCodeEnum.FAILURE.code, ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode, ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/saveOrUpdateExchange", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public ResponseDTO saveOrUpdateExchange(@RequestBody @Valid UpdateExchangeDTO requestDTO) {
        ResponseDTO responseDTO;
        try {
            requestDTO.setMerchantCode(super.getCacheUser().getMerchantCode());
            requestDTO.setOperator(getFullName());
            responseDTO = exchangeService.saveOrUpdateExchange(requestDTO);
        } catch (Exception e) {
            log.error("exchangeService.saveOrUpdateExchange 异常", e);
            responseDTO = new ResponseDTO(ResultCodeEnum.FAILURE.code, ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode, ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    @RequestMapping(value = "/currencyList", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public ResponseDTO currencyList() {
        ResponseDTO responseDTO;
        try {
            responseDTO = new ResponseDTO(1);
            List<CurrencyDTO> currencyDTOS = new ArrayList<>();
            CurrencyEnum[] currencyEnums = CurrencyEnum.values();
            for (CurrencyEnum currencyEnum : currencyEnums) {
                CurrencyDTO currencyDTO = new CurrencyDTO();
                currencyDTO.setKey(currencyEnum.getKey());
                currencyDTO.setValue(currencyEnum.getValue());
                currencyDTO.setDesc(currencyEnum.getDesc());
                currencyDTOS.add(currencyDTO);
            }
            responseDTO.setModel(currencyDTOS);
        } catch (Exception e) {
            log.error("currencyList 异常", e);
            responseDTO = new ResponseDTO(ResultCodeEnum.FAILURE.code, ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode, ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

}
