package com.fangcang.directlink;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.directlink.dto.SyncRatesAndRoomsRequestDTO;
import com.fangcang.directlink.service.SyncRatePlanService;
import com.fangcang.product.domain.PricePlanDO;
import com.fangcang.product.request.PricePlanRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

@RestController
@Slf4j
@RequestMapping(("/supply/ratePlan"))
public class SyncRatePlanController {

    @Autowired
    private SyncRatePlanService syncRatePlanService;

    @RequestMapping(value = "/syncPriceAndRooms", method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO syncPriceAndRooms(HttpServletRequest servletRequest) {
        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        SyncRatesAndRoomsRequestDTO syncRatesAndRoomsRequestDTO = null;
        try {
            syncRatesAndRoomsRequestDTO = this.getRequestDTO(servletRequest, SyncRatesAndRoomsRequestDTO.class);
            responseDTO = syncRatePlanService.saveRatesAndRooms(syncRatesAndRoomsRequestDTO);
        } catch (IOException e) {
            log.error("读取供应商数据异常",e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
            responseDTO.setFailReason("IO异常");
        } catch (Exception e){
            log.error("/supply/sync/ratesAndRooms 同步数据异常,请求参数：{}",JSON.toJSONString(syncRatesAndRoomsRequestDTO),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("同步房价房态，返回：{}",JSON.toJSONString(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO createRatePlan(HttpServletRequest servletRequest){
        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        PricePlanRequestDTO pricePlanRequestDTO = null;
        try {
            pricePlanRequestDTO = this.getRequestDTO(servletRequest, PricePlanRequestDTO.class);
            responseDTO = syncRatePlanService.createRatePlan(pricePlanRequestDTO);
        } catch (IOException e) {
            log.error("读取供应商数据异常",e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
            responseDTO.setFailReason("IO异常");
        } catch (Exception e){
            log.error("/supply/sync/create 新增价格计划异常,请求参数：{}",JSON.toJSONString(pricePlanRequestDTO),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }

        return responseDTO;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO deleteRatePlan(HttpServletRequest servletRequest){
        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        PricePlanDO pricePlanDO = null;
        try {
            pricePlanDO = this.getRequestDTO(servletRequest, PricePlanDO.class);
            responseDTO = syncRatePlanService.deleteRatePlan(pricePlanDO);
        } catch (IOException e) {
            log.error("读取供应商数据异常",e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
            responseDTO.setFailReason("IO异常");
        } catch (Exception e){
            log.error("/supply/sync/delete 删除价格计划异常,请求参数：{}",JSON.toJSONString(pricePlanDO),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }

        return responseDTO;
    }


    private <T> T getRequestDTO(HttpServletRequest request, Class<T> clazz) throws IOException {

        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = request.getReader();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }finally {
            if (null != br){
                br.close();
            }
        }

        log.debug("{}请求数据：{}",request.getServletPath(),sb.toString());
        T requestDTO = JSON.parseObject(sb.toString(), clazz);
        return requestDTO;
    }
}
