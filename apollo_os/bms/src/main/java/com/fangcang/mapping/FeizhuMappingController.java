package com.fangcang.mapping;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.controller.BaseController;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.mapping.request.feizhu.*;
import com.fangcang.mapping.service.FeizhuMappingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 飞猪酒店映射
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:35
 */
@RestController
@Slf4j
@RequestMapping("/feizhu")
public class FeizhuMappingController extends BaseController {

    @Autowired
    private FeizhuMappingService feizhuMappingService;

    @RequestMapping(value = "/shop/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryFeizhuShop() {

        ResponseDTO responseDTO = new ResponseDTO();
        try{
            responseDTO = feizhuMappingService.queryFeizhuShop(super.getMerchantCode());
        }catch (Exception e){
            log.error("/feizhu/shop/list异常,请求参数", e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }

        log.info("/feizhu/shop/list请求完成，返回：{}", JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryFeizhuHotelMappingList(@RequestBody QueryFeizhuHotelMappingRequest queryHotelMappingRequest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try{
            queryHotelMappingRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.queryFeizhuHotelListForPage(queryHotelMappingRequest);
        }catch (Exception e){
            log.error("/feizhu/hotel/list异常,请求参数：{}", JSON.toJSONString(queryHotelMappingRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/hotel/list请求完成，请求参数：{}。返回：{}", JSON.toJSONString(queryHotelMappingRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/add", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO addHotelToFeizhu(@RequestBody AddHotelToFeizhuRequest addHotelToFeizhuRequest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            addHotelToFeizhuRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.addHotelToFeizhu(addHotelToFeizhuRequest);
        }catch (Exception e){
            log.error("feizhu/hotel/add异常,请求参数：{}", JSON.toJSONString(addHotelToFeizhuRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/hotel/add，请求参数：{}。返回：{}", JSON.toJSONString(addHotelToFeizhuRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/get", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO getFeizhuHotel(@RequestBody GetFeizhuHotelRequest getFeizhuHotelRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            getFeizhuHotelRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.getFeizhuHotel(getFeizhuHotelRequest);
        }catch (Exception e){
            log.error("feizhu/hotel/get异常,请求参数：{}", JSON.toJSONString(getFeizhuHotelRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/hotel/get，请求参数：{}。返回：{}", JSON.toJSONString(getFeizhuHotelRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/roomtype/add", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO addRoomTypeToFeizhu(@RequestBody AddRoomTypeToFeizhuRequest addRoomTypeToFeizhuRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            addRoomTypeToFeizhuRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.addRoomTypeToFeizhu(addRoomTypeToFeizhuRequest);
        }catch (Exception e){
            log.error("feizhu/roomtype/add请求异常,请求参数：{}", JSON.toJSONString(addRoomTypeToFeizhuRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/roomtype/add，请求参数：{}。返回：{}", JSON.toJSONString(addRoomTypeToFeizhuRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/roomtype/get", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO getFeizhuRoomType(@RequestBody GetFeizhuRoomTypeRequest getFeizhuRoomTypeRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            getFeizhuRoomTypeRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.getFeizhuRoomType(getFeizhuRoomTypeRequest);
        }catch (Exception e){
            log.error("feizhu/roomtype/get请求异常,请求参数：{}", JSON.toJSONString(getFeizhuRoomTypeRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/roomtype/get，请求参数：{}。返回：{}", JSON.toJSONString(getFeizhuRoomTypeRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/rateplan/add", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO addRatePlanToFeizhu(@RequestBody AddRatePlanToFeizhuResquest addRatePlanToFeizhuResquest) {
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            addRatePlanToFeizhuResquest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.addRatePlanToFeizhu(addRatePlanToFeizhuResquest);
        }catch (Exception e){
            log.error("feizhu/rateplan/add请求异常,请求参数：{}", JSON.toJSONString(addRatePlanToFeizhuResquest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/rateplan/get，请求参数：{}。返回：{}", JSON.toJSONString(addRatePlanToFeizhuResquest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/rateplan/del", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO delFeizhuRatePlan(@RequestBody DelRatePlanToFeizhuResquest delRatePlanToFeizhuResquest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            delRatePlanToFeizhuResquest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.delRatePlanToFeizhu(delRatePlanToFeizhuResquest);
        }catch (Exception e){
            log.error("feizhu/rateplan/del请求异常,请求参数：{}", JSON.toJSONString(delRatePlanToFeizhuResquest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/rateplan/del，请求参数：{}。返回：{}", JSON.toJSONString(delRatePlanToFeizhuResquest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/sync/pricestatus", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO syncPriceAndRoomStatus(@RequestBody SyncRateAndQuotaToFeizhuResquest syncRateAndQuotaToFeizhuResquest){

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            syncRateAndQuotaToFeizhuResquest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.syncRateAndQuotaToFeizhu(syncRateAndQuotaToFeizhuResquest);
        }catch (Exception e){
            log.error("feizhu/sync/pricestatus请求异常,请求参数：{}", JSON.toJSONString(syncRateAndQuotaToFeizhuResquest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/sync/pricestatus，请求参数：{}。返回：{}", JSON.toJSONString(syncRateAndQuotaToFeizhuResquest), JSON.toJSON(responseDTO));
        return responseDTO;

    }

    /**
     * 房型和价格计划的列表页
     * @param queryRoomTypeAndRpListRequest
     * @return
     */
    @RequestMapping(value = "/roomtype/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryFeizhuRoomTypeAndRatePlanList(@RequestBody QueryRoomTypeAndRpListRequest queryRoomTypeAndRpListRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            queryRoomTypeAndRpListRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = feizhuMappingService.queryRoomTypeAndPricePlanList(queryRoomTypeAndRpListRequest);
        }catch (Exception e){
            log.error("feizhu/roomtype/list 请求异常,请求参数：{}", JSON.toJSONString(queryRoomTypeAndRpListRequest),e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/feizhu/roomtype/list，请求参数：{}。返回：{}", JSON.toJSONString(queryRoomTypeAndRpListRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }
}
