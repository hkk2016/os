package com.fangcang.mapping;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.controller.BaseController;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.mapping.request.ctripos.BatchSyncCtripRequestCtripos;
import com.fangcang.mapping.request.ctripos.PushPriceAndQuotaToCtripRequest;
import com.fangcang.mapping.request.ctripos.QueryHotelListRequest;
import com.fangcang.mapping.request.ctripos.QueryRoomOrRateListRequest;
import com.fangcang.mapping.service.CtriposMappingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 携程酒店映射
 */
@RestController
@Slf4j
@RequestMapping("/ctripos")
public class CtriposMappingController extends BaseController {

    @Autowired
    private CtriposMappingService ctriposMappingService;

    @RequestMapping(value = "/shop/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryCtriposShop() {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            responseDTO = ctriposMappingService.queryShopList(super.getMerchantCode());
        } catch (Exception e) {
            log.error("/ctripos/shop/list异常,请求参数", e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }

        log.info("/ctripos/shop/list请求完成，返回：{}", JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryCtriposHotelMappingList(@RequestBody QueryHotelListRequest queryHotelMappingRequest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            queryHotelMappingRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = ctriposMappingService.queryHotelList(queryHotelMappingRequest);
        } catch (Exception e) {
            log.error("/ctripos/hotel/list异常,请求参数：{}", JSON.toJSONString(queryHotelMappingRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/ctripos/hotel/list请求完成，请求参数：{}。返回：{}", JSON.toJSONString(queryHotelMappingRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    /**
     * 房型和价格计划的列表页
     *
     * @param queryRoomOrRateListRequest
     * @return
     */
    @RequestMapping(value = "/roomtype/list", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO queryCtriposRoomTypeAndRatePlanList(@RequestBody QueryRoomOrRateListRequest queryRoomOrRateListRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            queryRoomOrRateListRequest.setMerchantCode(super.getMerchantCode());
            responseDTO = ctriposMappingService.queryRoomOrRateList(queryRoomOrRateListRequest);
        } catch (Exception e) {
            log.error("ctripos/roomtype/list 请求异常,请求参数：{}", JSON.toJSONString(queryRoomOrRateListRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/ctripos/roomtype/list，请求参数：{}。返回：{}", JSON.toJSONString(queryRoomOrRateListRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/batchPushHotelInfoToCtrip", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO batchPushHotelInfoToCtrip(@RequestBody BatchSyncCtripRequestCtripos batchSyncCtripRequest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            batchSyncCtripRequest.setMerchantCode(super.getMerchantCode());
            batchSyncCtripRequest.setCreator(super.getFullName());
            responseDTO = ctriposMappingService.batchPushHotelInfoToCtrip(batchSyncCtripRequest);
        } catch (Exception e) {
            log.error("ctripos/hotel/batchPushHotelInfoToCtrip异常,请求参数：{}", JSON.toJSONString(batchSyncCtripRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/ctripos/hotel/batchPushHotelInfoToCtrip，请求参数：{}。返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/batchSyncHotelInfoFromCtrip", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO batchSyncHotelInfoFromCtrip(@RequestBody BatchSyncCtripRequestCtripos batchSyncCtripRequest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            batchSyncCtripRequest.setMerchantCode(super.getMerchantCode());
            batchSyncCtripRequest.setCreator(super.getFullName());
            responseDTO = ctriposMappingService.batchSyncHotelInfoFromCtrip(batchSyncCtripRequest);
        } catch (Exception e) {
            log.error("ctripos/hotel/batchSyncHotelInfoFromCtrip异常,请求参数：{}", JSON.toJSONString(batchSyncCtripRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/ctripos/hotel/batchSyncHotelInfoFromCtrip，请求参数：{}。返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSON(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/sync/pricestatus", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO syncPriceAndRoomStatus(@RequestBody PushPriceAndQuotaToCtripRequest syncRateAndQuotaToCtriposResquest) {

        ResponseDTO responseDTO = new ResponseDTO();
        try {
            syncRateAndQuotaToCtriposResquest.setMerchantCode(super.getMerchantCode());
            syncRateAndQuotaToCtriposResquest.setCreator(super.getFullName());
            responseDTO = ctriposMappingService.pushPriceAndQuotaToCtrip(syncRateAndQuotaToCtriposResquest);
        } catch (Exception e) {
            log.error("ctripos/sync/pricestatus请求异常,请求参数：{}", JSON.toJSONString(syncRateAndQuotaToCtriposResquest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/ctripos/sync/pricestatus，请求参数：{}。返回：{}", JSON.toJSONString(syncRateAndQuotaToCtriposResquest), JSON.toJSON(responseDTO));
        return responseDTO;
    }
}
