package com.fangcang.b2b.wxpay.mapper;

import com.fangcang.b2b.wxpay.domain.WxPayRecordDO;
import com.fangcang.common.MyMapper;

public interface WxPayRecordMapper extends MyMapper<WxPayRecordDO> {
}
