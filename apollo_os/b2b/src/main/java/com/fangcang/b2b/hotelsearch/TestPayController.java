package com.fangcang.b2b.hotelsearch;

import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.request.SaveSearchHistoryDTO;
import com.fangcang.b2b.response.GetOfflinePayResponseDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.merchant.dto.BankCardDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 2018/6/30.
 */
@RestController
@RequestMapping("/test/b2b/hotel")
public class TestPayController extends BaseController {

    /**
     * 获取线下支付
     * @return
     */
    @RequestMapping(value = "/getOfflinePay",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<GetOfflinePayResponseDTO> getOfflinePay(){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<BankCardDTO> bankCardDTOList = new ArrayList<>();
        for(int i = 0;i < 5;i ++){
            BankCardDTO bankCardDTO = new BankCardDTO();
            bankCardDTO.setOpeningBank("中国银行" + i);
            bankCardDTO.setAccountName("张三" + i);
            bankCardDTO.setAccountNumber("8723642649234" + i);
            bankCardDTOList.add(bankCardDTO);
        }
        GetOfflinePayResponseDTO getOfflinePayResponseDTO = new GetOfflinePayResponseDTO();
        getOfflinePayResponseDTO.setBankCardList(bankCardDTOList);
        getOfflinePayResponseDTO.setAlipayUrl("http://image.fangcang.com/upload/HFP/finance_M10000001_1499234253025.png");
        getOfflinePayResponseDTO.setWechatPayUrl("http://image.fangcang.com/upload/HFP/finance_M10000001_1499234253025.png");

        responseDTO.setModel(getOfflinePayResponseDTO);
        return responseDTO;
    }
}
