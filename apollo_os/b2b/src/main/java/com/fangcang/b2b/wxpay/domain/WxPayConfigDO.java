package com.fangcang.b2b.wxpay.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name="t_weixin_pay_config")
public class WxPayConfigDO implements Serializable {
    private static final long serialVersionUID = -9158958518006423192L;

    /**
     * 商家支付配置ID
     */
    @Id
    @Column(name="payConfigId")
    private String payConfigId;

    /**
     * 类型
     */
    @Column(name="configType")
    private Integer configType;

    /**
     * 商户号
     */
    @Column(name="accountNo")
    private String accountNo;

    /**
     * api密钥
     */
    @Column(name="apiKey")
    private String apiKey;

    /**
     * paySignKey
     */
    @Column(name="paySignKey")
    private String paySignKey;

    /**
     * 微信商户证书
     */
    @Column(name="wechatCertificateUrl")
    private String wechatCertificateUrl;

    /**
     * 公钥
     */
    @Column(name="publicKey")
    private String publicKey;

    /**
     * 私钥
     */
    @Column(name="privateKey")
    private String privateKey;

    /**
     * signType
     */
    @Column(name="signType")
    private String signType;

    /**
     * 扩展字段1 <br>
     * 微信商户收款(configType=1) 情况下：该字段表示 是否有H5支付权限 (取值：1：申请中，2：申请拒绝，3：已开通H5支付)
     */
    @Column(name="feature_1")
    private String feature1;
    /**
     * 扩展字段2
     */
    @Column(name="feature_2")
    private String feature2;
    /**
     * 扩展字段3
     */
    @Column(name="feature_3")
    private String feature3;
    /**
     * 扩展字段4
     */
    @Column(name="feature_4")
    private String feature4;
    /**
     * 扩展字段5
     */
    @Column(name="feature_5")
    private String feature5;

    /**
     * 创建人
     */
    @Column(name="creator")
    private String creator;

    /**
     * 创建时间
     */
    @Column(name="createTime")
    private Date createTime;

    /**
     * 修改人
     */
    @Column(name="lastModifier")
    private String lastModifier;

    /**
     * 修改时间
     */
    @Column(name="lastModifyTime")
    private Date lastModifyTime;
}
