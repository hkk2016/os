package com.fangcang.b2b.wxpay.mapper;

import com.fangcang.b2b.wxpay.domain.WxPayNotifyDO;
import com.fangcang.common.MyMapper;

public interface WxPayNotifyMapper extends MyMapper<WxPayNotifyDO> {
}
