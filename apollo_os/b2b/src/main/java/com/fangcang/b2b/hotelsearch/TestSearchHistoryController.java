package com.fangcang.b2b.hotelsearch;

import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.dto.KeyWordDTO;
import com.fangcang.b2b.request.SaveSearchHistoryDTO;
import com.fangcang.b2b.request.SearchHistoryQueryDTO;
import com.fangcang.b2b.response.ContactUsResponseDTO;
import com.fangcang.b2b.response.SearchHistoryResponseDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 2018/6/30.
 */
@RestController
@RequestMapping("/test/b2b/hotel")
public class TestSearchHistoryController extends BaseController {

    /**
     * 保存搜索历史
     * @param saveSearchHistoryDTO
     * @return
     */
    @RequestMapping(value = "/saveSearchHistory",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO saveSearchHistory(@RequestBody @Valid SaveSearchHistoryDTO saveSearchHistoryDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    /**
     * 查询搜索历史
     * @param searchHistoryQueryDTO
     * @return
     */
    @RequestMapping(value = "/querySearchHistory",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<SearchHistoryResponseDTO> querySearchHistory(@RequestBody @Valid SearchHistoryQueryDTO searchHistoryQueryDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<KeyWordDTO> keyWordDTOS = new ArrayList<>();
        for(int i = 0;i < 10;i++){
            KeyWordDTO keyWordDTO = new KeyWordDTO();
            keyWordDTO.setKeyWord("武汉光谷太子大酒店" + i);
            keyWordDTOS.add(keyWordDTO);
        }
        SearchHistoryResponseDTO searchHistoryResponseDTO  = new SearchHistoryResponseDTO();
        searchHistoryResponseDTO.setKeyWords(keyWordDTOS);
        responseDTO.setModel(searchHistoryResponseDTO);
        return responseDTO;
    }

    /**
     * 联系我们
     * @return
     */
    @RequestMapping(value = "/contactUs",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<ContactUsResponseDTO> contactUs(){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        ContactUsResponseDTO contactUsResponseDTO = new ContactUsResponseDTO();
        contactUsResponseDTO.setMerchantBMName("张三");
        contactUsResponseDTO.setMerchantBMPhone("1232432432423");
        responseDTO.setModel(contactUsResponseDTO);
        return responseDTO;
    }
}
