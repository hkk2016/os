package com.fangcang.b2b.wxpay.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name="t_weixin_pay_record")
public class WxPayRecordDO implements Serializable {
    private static final long serialVersionUID = -9158958518006423192L;

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    /**
     * 订单编码
     */
    private String orderCode;
    /**
     * 支付金额
     */
    private BigDecimal payAmount;
    /**
     * 客户单号(格式：订单号+下划线+随机数)
     */
    private String weixinOutTradeNo;
    /**
     * 微信侧预付单ID
     */
    private String prepayId;
    /**
     * 状态(0：新建：1.已支付，2.已作废)
     */
    private Integer status;
    /**
     * 定时器批量处理状态（是否已处理）(1-已处理、0-未处理)
     */
    private Integer batchStatus;
    /**
     * 批量处理结果描述
     */
    private String batchDesc;
    /**
     * 发送异常支付报警短信次数
     */
    private Integer sysalarmCnt;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后修改人
     */
    private String lastmodifier;
    /**
     * 最后修改时间
     */
    private Date lastmodifyTime;
}
