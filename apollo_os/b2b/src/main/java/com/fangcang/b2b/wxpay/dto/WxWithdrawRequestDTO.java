package com.fangcang.b2b.wxpay.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class WxWithdrawRequestDTO implements Serializable {
    private static final long serialVersionUID = -4347841619314646846L;

    /**
     * 公众账号appid
     */
    private String mchAppid;

    /**
     * 商户号
     */
    private String mchid;

    /**
     * 用户openid
     */
    private String openid;

    /**
     * 收款用户姓名
     */
    private String reUserName;

    /**
     * 支付金额
     */
    private BigDecimal amount;

    /**
     * 秘钥
     */
    private String apiKey;

    public String getMchAppid() {
        return mchAppid;
    }

    public void setMchAppid(String mchAppid) {
        this.mchAppid = mchAppid;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getReUserName() {
        return reUserName;
    }

    public void setReUserName(String reUserName) {
        this.reUserName = reUserName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
