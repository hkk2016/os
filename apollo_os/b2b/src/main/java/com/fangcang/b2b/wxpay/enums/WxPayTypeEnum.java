package com.fangcang.b2b.wxpay.enums;

/**
 * 微信支付类型
 */
public enum WxPayTypeEnum {

	JSAPI("JSAPI", "微信公众号支付，微信内打开支付"),
	MWEB("MWEB", "微信H5支付，微信外打开支付");

	private String code;
	private String name;

	private WxPayTypeEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
}
