package com.fangcang.b2b.wxpay.mapper;

import com.fangcang.b2b.wxpay.domain.WxPayConfigDO;
import com.fangcang.common.MyMapper;

public interface WxPayConfigMapper extends MyMapper<WxPayConfigDO> {
}
