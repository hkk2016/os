package com.fangcang.b2b.wxpay.dto;

import com.fangcang.b2b.wxpay.enums.WxPayTypeEnum;
import com.fangcang.common.BaseDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 创建微信预付单Request
 */
@Data
public class WxPrepayRequestDTO extends BaseDTO implements Serializable {
	private static final long serialVersionUID = -4347841619314646846L;

	/**
	 * 公众账号ID
	 */
	private String appId;

	/**
	 * 微信openid
	 */
	private String openId;

	/**
	 * 订单编码
	 */
	private String orderCode;

	/**
	 * 商品名称
	 */
	private String productName;

	/**
	 * 订单总额
	 */
	private BigDecimal orderAmount;
	
	/**
	 * 微信支付方式，默认为微信公众号支付
	 */
	private WxPayTypeEnum wechatPayType = WxPayTypeEnum.JSAPI;

	/**
	 * 回调地址
	 */
	private String notifyUrl;

	/**
	 * 客户端Ip
	 */
	private String clientIp;

	/**
	 * 应用URL
	 */
	private String wapUrl;

	/**
	 * 应用名称
	 */
	private String wapName;
}
