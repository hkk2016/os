package com.fangcang.b2b.wxpay.enums;

public enum WxPayRecordStatusEnum {

    NEW(0,"新建"),
    PAY(1,"已支付"),
    CANCEL(2,"已作废");

    public int code;
    public String desc;

    private WxPayRecordStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getValueBykey(int code){
        String value = "其他";
        for(WxPayRecordStatusEnum wxPayRecordStatusEnum : WxPayRecordStatusEnum.values()) {
            if(wxPayRecordStatusEnum.code == code) {
                value = wxPayRecordStatusEnum.desc;
                break;
            }
        }
        return value;
    }
}
