package com.fangcang.b2b.wxpay.dto;

public class WxOrderQueryResponseDTO {

	private static final long serialVersionUID = -3468138284175027442L;

	/**
	 * 交易状态 (REFUND—转入退款, NOTPAY—未支付, CLOSED—已关闭, REVOKED—已撤销（刷卡支付）, USERPAYING--用户支付中, PAYERROR--支付失败(其他原因，如银行返回失败)) 
	 */
	private String trade_state;

	public String getTrade_state() {
		return trade_state;
	}

	public void setTrade_state(String trade_state) {
		this.trade_state = trade_state;
	}
	
	
}
