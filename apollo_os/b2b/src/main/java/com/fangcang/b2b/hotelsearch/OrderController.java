package com.fangcang.b2b.hotelsearch;

import com.fangcang.agent.dto.AgentUserDTO;
import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.request.ApplyCancelOrderRequestDTO;
import com.fangcang.b2b.request.BookOrderRequestDTO;
import com.fangcang.b2b.request.HotelChangeGuideRequestDTO;
import com.fangcang.b2b.request.HotelOrderDetailRequestDTO;
import com.fangcang.b2b.request.HotelOrderListRequestDTO;
import com.fangcang.b2b.request.PreBookRequestDTO;
import com.fangcang.b2b.response.BookOrderResponseDTO;
import com.fangcang.b2b.response.HotelOrderDetailResponseDTO;
import com.fangcang.b2b.response.OrderListResponseDTO;
import com.fangcang.b2b.response.PreBookResponseDTO;
import com.fangcang.b2b.service.HotelOrderService;
import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

/**
 * Created by ASUS on 2018/6/30.
 */
@RestController
@RequestMapping("/b2b/order")
public class OrderController extends BaseController {

    @Autowired
    private HotelOrderService hotelOrderService;
    /**
     * 申请取消订单
     * @param applyCancelOrderRequestDTO
     * @return
     */
    @RequestMapping(value = "/applyCancelOrder",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO applyCancelOrder(@RequestBody @Valid ApplyCancelOrderRequestDTO applyCancelOrderRequestDTO){
        applyCancelOrderRequestDTO.setCreator(super.getOperator());
        return hotelOrderService.applyCancelOrder(applyCancelOrderRequestDTO);
    }

    /**
     * 查询订单列表
     * @param orderListRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryOrderList",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<PaginationSupportDTO<OrderListResponseDTO>> queryOrderList(@RequestBody HotelOrderListRequestDTO orderListRequestDTO){
        AgentUserDTO agentUserDTO = super.getCacheUser();
        return hotelOrderService.queryOrderList(orderListRequestDTO,agentUserDTO);
    }

    /**
     * 保存修改向导
     * @param hotelChangeGuideRequestDTO
     * @return
     */
    @RequestMapping(value = "/changeGuide",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO changeGuide(@RequestBody @Valid HotelChangeGuideRequestDTO hotelChangeGuideRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        hotelChangeGuideRequestDTO.setCreator(super.getOperator());
        hotelChangeGuideRequestDTO.setCreateTime(new Date());
        hotelChangeGuideRequestDTO.setModifier(super.getOperator());
        hotelChangeGuideRequestDTO.setModifyTime(new Date());
        return hotelOrderService.changeGuide(hotelChangeGuideRequestDTO);
    }

    /**
     * 订单详细
     * @param hotelOrderDetailRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryOrderDetail",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<HotelOrderDetailResponseDTO> queryOrderDetail(@RequestBody @Valid HotelOrderDetailRequestDTO hotelOrderDetailRequestDTO){
        AgentUserDTO agentUserDTO = super.getCacheUser();
        return hotelOrderService.queryOrderDetail(hotelOrderDetailRequestDTO,agentUserDTO);
    }

    /**
     * 试预订
     * @param preBookRequestDTO
     * @return
     */
    @RequestMapping(value = "/prebook",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<PreBookResponseDTO> prebook(@RequestBody @Valid PreBookRequestDTO preBookRequestDTO){
        AgentUserDTO agentUserDTO = super.getCacheUser();
        return hotelOrderService.prebook(preBookRequestDTO,agentUserDTO);
    }

    /**
     * 预定
     * @param bookOrderRequestDTO
     * @return
     */
    @RequestMapping(value = "/book",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<BookOrderResponseDTO> book(@RequestBody @Valid BookOrderRequestDTO bookOrderRequestDTO){
        AgentUserDTO agentUserDTO = super.getCacheUser();
        bookOrderRequestDTO.setCreator(super.getOperator());
        return hotelOrderService.book(bookOrderRequestDTO,agentUserDTO);
    }
}
