package com.fangcang.b2b.hotelsearch;

import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.dto.ProductDetailsDTO;
import com.fangcang.b2b.dto.RoomTypeDetailsDTO;
import com.fangcang.b2b.dto.SaleItemDetailDTO;
import com.fangcang.b2b.request.BusinessZoneRequestDTO;
import com.fangcang.b2b.request.GetHotelDetailRequestDTO;
import com.fangcang.b2b.request.QueryHotelListRequestDTO;
import com.fangcang.b2b.response.BusinessZoneResponseDTO;
import com.fangcang.b2b.response.GetHotelDetailResponseDTO;
import com.fangcang.b2b.response.HotelBaseInfoWithImagesResponseDTO;
import com.fangcang.b2b.response.QueryHotelListRsponseDTO;
import com.fangcang.b2b.response.QueryMerchantCommonCityResponseDTO;
import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.B2B.BookTypeEnum;
import com.fangcang.common.enums.B2B.FreeRoomPolicyEnum;
import com.fangcang.common.enums.BedTypeEnum;
import com.fangcang.common.enums.BreakFastTypeEnum;
import com.fangcang.common.enums.CurrencyEnum;
import com.fangcang.common.enums.PayMethodEnum;
import com.fangcang.common.enums.ProductTypeEnum;
import com.fangcang.common.enums.QuotaTypeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.enums.SupplyWayEnum;
import com.fangcang.common.enums.order.SupplyTypeEnum;
import com.fangcang.hotelinfo.dto.HotelAdditionalDTO;
import com.fangcang.hotelinfo.dto.HotelFacilityDTO;
import com.fangcang.hotelinfo.dto.ImageInfoDTO;
import com.fangcang.hotelinfo.dto.ImageTypeDTO;
import com.fangcang.hotelinfo.dto.RoomTypeImageDTO;
import com.fangcang.hotelinfo.request.HotelBaseInfoRequestDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Auther: yanming.li@fangcang.com
 * @Date: 2018/6/30 11:01
 * @Description:
 */
@RequestMapping("/test/b2b/hotel")
@RestController
public class TestHotelSearchController extends BaseController {


    /**
     * 获取商业区
     *
     * @return
     */
    @RequestMapping(value = "/getBusinessZone", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<BusinessZoneResponseDTO> getBusinessZone(@RequestBody @Valid BusinessZoneRequestDTO businessZoneRequestDTO) {

        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);

        List<BusinessZoneResponseDTO> businessList = new ArrayList<>();
        BusinessZoneResponseDTO businessDTO = null;
        for (int i = 0; i < 5; i++) {
            businessDTO = new BusinessZoneResponseDTO();
            businessDTO.setBusinessCode("B1001" + i);
            businessDTO.setBusinessName("测试商圈0" + i);
            businessList.add(businessDTO);
        }
        responseDTO.setModel(businessList);

        return responseDTO;
    }

    /**
     * 获取酒店列表
     *
     * @param queryHotelListRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryHotelList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<PaginationSupportDTO<QueryHotelListRsponseDTO>> queryHotelList(@RequestBody @Valid QueryHotelListRequestDTO queryHotelListRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        PaginationSupportDTO paginationSupportDTO = new PaginationSupportDTO();
        List<QueryHotelListRsponseDTO> hotelListRsponseDTOS = new ArrayList<>();
        QueryHotelListRsponseDTO queryHotelListRsponseDTO = null;
        for (int i = 0; i < 5; i++) {
            queryHotelListRsponseDTO = new QueryHotelListRsponseDTO();
            queryHotelListRsponseDTO.setHotelId(1L);
            queryHotelListRsponseDTO.setChineseName("测试酒店" + i);
            queryHotelListRsponseDTO.setEnglishName("TestHotel" + i);
            queryHotelListRsponseDTO.setChineseAddress("武汉" + i);
            queryHotelListRsponseDTO.setEnglishAddress("WuHan" + i);
            queryHotelListRsponseDTO.setHotelStar(Integer.valueOf("10" + 5 * i));
            queryHotelListRsponseDTO.setStartPrice(new BigDecimal(200).add(new BigDecimal(i * 50)).setScale(2));
            queryHotelListRsponseDTO.setShowCurrency("CNY");
            queryHotelListRsponseDTO.setImgUrl("http://b.hiphotos.baidu.com/image/pic/item/0ff41bd5ad6eddc4f8daa30935dbb6fd52663306.jpg");
            hotelListRsponseDTOS.add(queryHotelListRsponseDTO);
        }
        paginationSupportDTO.setItemList(hotelListRsponseDTOS);
        paginationSupportDTO.setTotalCount(5L);
        paginationSupportDTO.setCurrentPage(1);
        paginationSupportDTO.setPageSize(5);
        paginationSupportDTO.setTotalPage(1);
        responseDTO.setModel(paginationSupportDTO);
        return responseDTO;
    }

    /**
     * 获取酒店详情
     *
     * @param getHotelDetailRequestDTO
     * @return
     */
    @RequestMapping(value = "/getHotelDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<GetHotelDetailResponseDTO> getHotelDetail(@RequestBody @Valid GetHotelDetailRequestDTO getHotelDetailRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        /**
         * getHotelDetailResponseDTO结构
         * hotelid
         * roomTypeDetails
         *     **
         *     productDetails
         *         **
         *         saleItemDetails
         *             **
         */
        GetHotelDetailResponseDTO getHotelDetailResponseDTO = new GetHotelDetailResponseDTO();
        getHotelDetailResponseDTO.setHotelId(120L);
        //
        List<RoomTypeDetailsDTO> roomTypeDetails = new ArrayList<>();
        RoomTypeDetailsDTO roomTypeDetailsDTO = null;
        List<ProductDetailsDTO> productDetails = new ArrayList<>();
        ProductDetailsDTO productDetailsDTO = null;
        List<SaleItemDetailDTO> saleItemDetails = new ArrayList<>();
        SaleItemDetailDTO saleItemDetailDTO = null;
        int i;//循环变量
        for (i = 0; i < 5; i++) {
            saleItemDetailDTO = new SaleItemDetailDTO();
            saleItemDetailDTO.setSaleDate(new Date());
            saleItemDetailDTO.setBasePrice(new BigDecimal(200).add(new BigDecimal(i * 50)).setScale(2));
            saleItemDetailDTO.setB2bSalePrice(new BigDecimal(300 + i * 50).setScale(2));
            saleItemDetailDTO.setGroupBasePrice(new BigDecimal(150 + i * 40).setScale(2));
            saleItemDetailDTO.setGroupSalePrice(new BigDecimal(200 + i * 40).setScale(2));
            saleItemDetailDTO.setQuotaNum(10 + i * 3);
            saleItemDetailDTO.setOverDraft(0);
            saleItemDetailDTO.setQuotaState(1);
            saleItemDetailDTO.setShowCurrency("CNY");
            saleItemDetails.add(saleItemDetailDTO);
        }
        for (i = 0; i < 5; i++) {
            productDetailsDTO = new ProductDetailsDTO();
            productDetailsDTO.setHotelId(120L);
            productDetailsDTO.setRoomTypeId(1 + i);
            productDetailsDTO.setPricePlanId(1 + i);
            productDetailsDTO.setPricePlanName("价格计划" + i);
            productDetailsDTO.setSupplyName("测试供应商" + i);
            productDetailsDTO.setSupplyCode("S100028" + i);
            productDetailsDTO.setBreakFastType(BreakFastTypeEnum.TWO.key);
            productDetailsDTO.setQuotaType(QuotaTypeEnum.CONTRACT_ROOM.key);
            productDetailsDTO.setIsAdditional(0);
            productDetailsDTO.setCancelPolicy("取消条款");
            productDetailsDTO.setPayMethod(PayMethodEnum.PAY_NOCOMISSION.key);
            productDetailsDTO.setSupplyWay(SupplyWayEnum.SupplyWayEnum.key);
            productDetailsDTO.setMerchantCode(this.getMerchantCode());
            productDetailsDTO.setProductType(ProductTypeEnum.SCATTERED_ROOM.key);
            String [] freeRoomPolicy = new String[]{String.valueOf(FreeRoomPolicyEnum.OVERFIVE.key)};
            productDetailsDTO.setFreeRoomPolicy(freeRoomPolicy);
            productDetailsDTO.setBedType(BedTypeEnum.KING.value);
            productDetailsDTO.setFirstDayPrice(new BigDecimal(230 + i * 50).setScale(2));
            productDetailsDTO.setShowCurrency("CNY");
            productDetailsDTO.setBookType(BookTypeEnum.RESERVATION.value);
            productDetailsDTO.setProductDailyList(saleItemDetails);
            productDetails.add(productDetailsDTO);
        }
        for (i = 0; i < 5; i++) {
            roomTypeDetailsDTO = new RoomTypeDetailsDTO();
            roomTypeDetailsDTO.setHotelId(120L);
            roomTypeDetailsDTO.setRoomTypeId(Long.valueOf(1 + i));
            roomTypeDetailsDTO.setRoomTypeName("测试房型"+i);
            roomTypeDetailsDTO.setFloor("0"+i);
            roomTypeDetailsDTO.setArea(40+i*5+"");
            roomTypeDetailsDTO.setBedDescription("这是一张很好的床"+i);
            roomTypeDetailsDTO.setStartPrice(new BigDecimal(100+i*35).setScale(2));
            roomTypeDetailsDTO.setShowCurrency(CurrencyEnum.CNY.value);
            roomTypeDetailsDTO.setImageUrl("http://b.hiphotos.baidu.com/image/pic/item/0ff41bd5ad6eddc4f8daa30935dbb6fd52663306.jpg");
            roomTypeDetailsDTO.setProductDetails(productDetails);
            roomTypeDetails.add(roomTypeDetailsDTO);
        }

        getHotelDetailResponseDTO.setRoomTypeDetails(roomTypeDetails);
        responseDTO.setModel(getHotelDetailResponseDTO);

        return responseDTO;
    }

    /**
     * 查询常用城市
     * @return
     */
    @RequestMapping(value = "/queryCommonCity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<QueryMerchantCommonCityResponseDTO> queryCommonCity(){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        //常用城市需要根据商家code查
        String merchantCode = this.getCacheUser().getMerchantCode();

        List<QueryMerchantCommonCityResponseDTO> cityList = new ArrayList<>();
        QueryMerchantCommonCityResponseDTO commonCityResponseDTO = null;
        for (int i = 1; i < 6; i++) {
            commonCityResponseDTO = new QueryMerchantCommonCityResponseDTO();
            commonCityResponseDTO.setCityCode("000"+i);
            commonCityResponseDTO.setCityName("测试城市00"+i);
            cityList.add(commonCityResponseDTO);
        }
        responseDTO.setModel(cityList);
        return responseDTO;
    }

    /**
     * 查询酒店基本信息
     * @return
     */
    @RequestMapping(value = "/queryHotelBaseInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<HotelBaseInfoWithImagesResponseDTO> queryHotelBaseInfo(@RequestBody @Valid HotelBaseInfoRequestDTO hotelBaseInfoRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        /**
         * HotelBaseInfoWithImagesResponseDTO结构
         *      **
         *      facilityList
         *          **
         *      additionalList
         *          **
         *      imageTypeList
         *          **
         *          imageList
         *              **
         *      roomTypeImageList
         *          **
         *          imageList
         */

        HotelBaseInfoWithImagesResponseDTO hotelBaseInfoWithImagesResponseDTO = new HotelBaseInfoWithImagesResponseDTO();
        //酒店设施
        List<HotelFacilityDTO> facilityList = new ArrayList<>();
        HotelFacilityDTO hotelFacilityDTO = null;
        //酒店附加项
        List<HotelAdditionalDTO> additionalList = new ArrayList<>();
        HotelAdditionalDTO hotelAdditionalDTO = null;
        //酒店图片类型列表
        List<ImageTypeDTO> imageTypeList = new ArrayList<>();
        ImageTypeDTO imageTypeDTO = null;
        //房型图片类型列表
        List<RoomTypeImageDTO> roomTypeImageList = new ArrayList<>();
        RoomTypeImageDTO roomTypeImageDTO = null;
        //图片列表
        List<ImageInfoDTO> imageList = new ArrayList<>();
        ImageInfoDTO imageInfoDTO = null;

        int i = 0;//循环变量
        for (i = 0; i < 5; i++) {
            imageInfoDTO = new ImageInfoDTO();
            imageInfoDTO.setImageId(1000L+i);
            imageInfoDTO.setHotelId(1000L);
            imageInfoDTO.setImageType(1);
            imageInfoDTO.setExtId(1000+i);
            imageInfoDTO.setIsMainImage(0);
            imageInfoDTO.setImageUrl( "http://fcimage.fangcang.com/test02images/hotels/873/195873/201709271506477176812.jpg");
            imageInfoDTO.setRealPath("~/download/pic");
            imageList.add(imageInfoDTO);
        }

        for (i = 0; i < 5; i++) {
            roomTypeImageDTO = new RoomTypeImageDTO();
            roomTypeImageDTO.setRoomTypeId(100L+i);
            roomTypeImageDTO.setRoomTypeName("测试房型0"+i);
            roomTypeImageDTO.setImageList(imageList);
            roomTypeImageList.add(roomTypeImageDTO);
        }

        for (i = 0; i < 5; i++) {
            imageTypeDTO = new ImageTypeDTO();
            imageTypeDTO.setImageType(1);
            imageTypeDTO.setImageTypeName("外观图");
            imageTypeDTO.setImageList(imageList);
            imageTypeList.add(imageTypeDTO);
        }

        for (i = 0; i < 3; i++) {
            hotelAdditionalDTO = new HotelAdditionalDTO();
            hotelAdditionalDTO.setAdditionalType(1);
            hotelAdditionalDTO.setAdditionalName("加早");
            hotelAdditionalDTO.setAdditionalPrice(new BigDecimal(30+i*20).setScale(2));
            additionalList.add(hotelAdditionalDTO);
        }

        for (i = 0; i < 3; i++) {
            hotelFacilityDTO = new HotelFacilityDTO();
            hotelFacilityDTO.setFacilityType(1);
            hotelFacilityDTO.setFacilityName("宽带网络");
            facilityList.add(hotelFacilityDTO);
        }

        hotelBaseInfoWithImagesResponseDTO.setHotelId(1000L);
        hotelBaseInfoWithImagesResponseDTO.setHotelName("测试酒店");
        hotelBaseInfoWithImagesResponseDTO.setCityCode("SZX");
        hotelBaseInfoWithImagesResponseDTO.setCityName("深圳");
        hotelBaseInfoWithImagesResponseDTO.setEngHotelName("Test Hotel");
        hotelBaseInfoWithImagesResponseDTO.setHotelAddress("中国广东深圳");
        hotelBaseInfoWithImagesResponseDTO.setEngHotelAddress("ChinaGuangDongShenZhen");
        hotelBaseInfoWithImagesResponseDTO.setHotelStar(4);
        hotelBaseInfoWithImagesResponseDTO.setCheckInTime(new Date().toString());
        hotelBaseInfoWithImagesResponseDTO.setCheckOutTime(new Date().toString());
        hotelBaseInfoWithImagesResponseDTO.setCreditCard(new String[] {"1","2","3"});
        hotelBaseInfoWithImagesResponseDTO.setIntroduction("这是一个测试酒店");
        hotelBaseInfoWithImagesResponseDTO.setPhone("110");
        hotelBaseInfoWithImagesResponseDTO.setPet(1);
        hotelBaseInfoWithImagesResponseDTO.setImageUrl("http://fcimage.fangcang.com/test02images/hotels/873/195873/201709271506477176812.jpg");
        hotelBaseInfoWithImagesResponseDTO.setAdditionalList(additionalList);
        hotelBaseInfoWithImagesResponseDTO.setImageTypeList(imageTypeList);
        hotelBaseInfoWithImagesResponseDTO.setFacilityList(facilityList);
        hotelBaseInfoWithImagesResponseDTO.setRoomTypeImageList(roomTypeImageList);

        responseDTO.setModel(hotelBaseInfoWithImagesResponseDTO);

        return responseDTO;
    }


}
