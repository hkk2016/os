package com.fangcang.b2b.wxpay.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="t_weixin_pay_notify")
public class WxPayNotifyDO implements Serializable {
    private static final long serialVersionUID = -9158958518006423192L;

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private String id;
    /**
     * 通知文本
     */
    private String noticeComment;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 微信侧的客户单号
     */
    private String weixinOutTradeNo;
    /**
     * 微信支付订单号
     */
    private String weixinTransactionId;
    /**
     * 本次通知处理状态(详见LvXinWxPayNoticeStatusEnum)
     */
    private Integer status;
    /**
     * 处理结果描述
     */
    private String resultDesc;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后修改人
     */
    private String lastmodifier;
    /**
     * 最后修改时间
     */
    private Date lastmodifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNoticeComment() {
        return noticeComment;
    }

    public void setNoticeComment(String noticeComment) {
        this.noticeComment = noticeComment;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getWeixinOutTradeNo() {
        return weixinOutTradeNo;
    }

    public void setWeixinOutTradeNo(String weixinOutTradeNo) {
        this.weixinOutTradeNo = weixinOutTradeNo;
    }

    public String getWeixinTransactionId() {
        return weixinTransactionId;
    }

    public void setWeixinTransactionId(String weixinTransactionId) {
        this.weixinTransactionId = weixinTransactionId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLastmodifier() {
        return lastmodifier;
    }

    public void setLastmodifier(String lastmodifier) {
        this.lastmodifier = lastmodifier;
    }

    public Date getLastmodifyTime() {
        return lastmodifyTime;
    }

    public void setLastmodifyTime(Date lastmodifyTime) {
        this.lastmodifyTime = lastmodifyTime;
    }
}
