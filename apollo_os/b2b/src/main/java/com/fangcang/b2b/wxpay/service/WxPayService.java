package com.fangcang.b2b.wxpay.service;

import com.fangcang.b2b.wxpay.domain.WxPayConfigDO;
import com.fangcang.b2b.wxpay.domain.WxPayNotifyDO;
import com.fangcang.b2b.wxpay.domain.WxPayRecordDO;
import com.fangcang.b2b.wxpay.dto.WxOrderQueryRequestDTO;
import com.fangcang.b2b.wxpay.dto.WxOrderQueryResponseDTO;
import com.fangcang.b2b.wxpay.dto.WxPrepayRequestDTO;
import com.fangcang.b2b.wxpay.dto.WxPrepayResponseDTO;

import java.util.List;

public interface WxPayService {

    /**
     * 下微信预付单
     */
    public WxPrepayResponseDTO createWechatPrepayInfo(WxPrepayRequestDTO request);

    /**
     * 查询微信订单接口，主动查询订单状态
     * @param request
     * @return
     */
    public WxOrderQueryResponseDTO queryOrderInfo(WxOrderQueryRequestDTO request);

    /**
     * 保存微信支付记录
     * @param wxPayRecordDO
     */
    public void saveWxPayRecord(WxPayRecordDO wxPayRecordDO);

    public void updateWxPayRecordStatus(String weixinOutTradeNo, Integer status, String desc);

    public List<WxPayRecordDO> queryWxPayRecord(String orderCode, Integer status);

    /**
     * 保存微信支付回调记录
     * @param wxPayNotifyDO
     */
    public void saveWxPayNotify(WxPayNotifyDO wxPayNotifyDO);

    public void updateWxPayNotifyStatus(String weixinOutTradeNo, Integer status, String resultDesc);

    /**
     * 查询微信支付配置
     * @return
     */
    public WxPayConfigDO queryWxPayConfig();
}
