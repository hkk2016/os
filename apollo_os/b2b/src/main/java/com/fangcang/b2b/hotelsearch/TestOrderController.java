package com.fangcang.b2b.hotelsearch;

import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.dto.AdditionChargeDTO;
import com.fangcang.b2b.dto.DailyDTO;
import com.fangcang.b2b.dto.RatePricePlanDTO;
import com.fangcang.b2b.request.ApplyCancelOrderRequestDTO;
import com.fangcang.b2b.request.BookOrderRequestDTO;
import com.fangcang.b2b.request.HotelChangeGuideRequestDTO;
import com.fangcang.b2b.request.HotelOrderDetailRequestDTO;
import com.fangcang.b2b.request.HotelOrderListRequestDTO;
import com.fangcang.b2b.request.PreBookRequestDTO;
import com.fangcang.b2b.response.BookOrderResponseDTO;
import com.fangcang.b2b.response.HotelOrderDetailResponseDTO;
import com.fangcang.b2b.response.OrderListResponseDTO;
import com.fangcang.b2b.response.PreBookResponseDTO;
import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.BalanceMethodEnum;
import com.fangcang.common.enums.ChannelTypeEnum;
import com.fangcang.common.enums.CurrencyEnum;
import com.fangcang.common.enums.QuotaTypeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.enums.order.OrderStatusEnum;
import com.fangcang.common.enums.order.PayMethodEnum;
import com.fangcang.common.enums.order.PayStatusEnum;
import com.fangcang.common.util.DateUtil;
import com.fangcang.common.util.PropertyCopyUtil;
import com.fangcang.hotelinfo.dto.HotelAdditionalDTO;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ASUS on 2018/6/30.
 */
@RestController
@RequestMapping("/test/b2b/order")
public class TestOrderController extends BaseController {

    /**
     * 申请取消订单
     * @param applyCancelOrderRequestDTO
     * @return
     */
    @RequestMapping(value = "/applyCancelOrder",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO applyCancelOrder(@RequestBody @Valid ApplyCancelOrderRequestDTO applyCancelOrderRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    /**
     * 查询订单列表
     * @param orderListRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryOrderList",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<PaginationSupportDTO<OrderListResponseDTO>> queryOrderList(@RequestBody HotelOrderListRequestDTO orderListRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<OrderListResponseDTO> orderListResponseDTOS = new ArrayList<>();
        for(int i = 0;i < 10;i ++){
            OrderListResponseDTO orderListResponseDTO = new OrderListResponseDTO();
            orderListResponseDTO.setOrderId(Long.valueOf(1000 + i));
            orderListResponseDTO.setOrderCode("H1000978834" + i);
            orderListResponseDTO.setCreateTime(new Date());
            orderListResponseDTO.setHotelName("武汉光谷国际酒店");
            orderListResponseDTO.setRoomTypeNames("豪华套房");
            orderListResponseDTO.setGuestNames("张三" + i);
            orderListResponseDTO.setCheckinDate(DateUtil.dateToString(new Date()));
            orderListResponseDTO.setCheckoutDate(DateUtil.dateToString(DateUtil.getDate(new Date(),4,0,0)));
            orderListResponseDTO.setRoomNum( i  + 1);
            orderListResponseDTO.setOrderSum(new BigDecimal(100));
            if(i % 1 == 0){
                orderListResponseDTO.setPayStatus(PayStatusEnum.UN_PAID.key);
            }else if(i % 2 == 0){
                orderListResponseDTO.setPayStatus(PayStatusEnum.PAID.key);
            }else if(i % 3 == 0){
                orderListResponseDTO.setPayStatus(PayStatusEnum.CREDIT.key);
            }else if(i % 4 == 0){
                orderListResponseDTO.setPayStatus(PayStatusEnum.UN_CREDIT.key);
            }

            if(i % 1 == 0){
                orderListResponseDTO.setOrderStatus(OrderStatusEnum.NEWORDER.key);
            }else if(i % 2 == 0){
                orderListResponseDTO.setOrderStatus(OrderStatusEnum.PROCESSING.key);
            }else if(i % 3 == 0){
                orderListResponseDTO.setOrderStatus(OrderStatusEnum.TRADED.key);
            }else if(i % 4 == 0){
                orderListResponseDTO.setOrderStatus(OrderStatusEnum.CANCELED.key);
            }
            orderListResponseDTO.setChannelCode(ChannelTypeEnum.B2B.key);
            orderListResponseDTO.setChannelName(ChannelTypeEnum.B2B.value);

            if(i % 1 == 0){
                orderListResponseDTO.setIsGroupRoom(1);
            }else{
                orderListResponseDTO.setIsGroupRoom(0);
            }
            orderListResponseDTO.setConfirmNo("23243" + i);
            orderListResponseDTO.setRateplanName("无早" + i);
            orderListResponseDTO.setNight(4L);
            orderListResponseDTO.setSaleCurrency(CurrencyEnum.CNY.value);
            orderListResponseDTOS.add(orderListResponseDTO);
        }
        PaginationSupportDTO paginationSupportDTO = new PaginationSupportDTO();
        paginationSupportDTO.setTotalCount(10);
        paginationSupportDTO.setTotalPage(1);
        paginationSupportDTO.setCurrentPage(1);
        paginationSupportDTO.setPageSize(10);
        paginationSupportDTO.setItemList(orderListResponseDTOS);
        responseDTO.setModel(paginationSupportDTO);
        return responseDTO;
    }

    /**
     * 保存修改向导
     * @param hotelChangeGuideRequestDTO
     * @return
     */
    @RequestMapping(value = "/changeGuide",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO changeGuide(@RequestBody @Valid HotelChangeGuideRequestDTO hotelChangeGuideRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    /**
     * 订单详细
     * @param hotelOrderDetailRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryOrderDetail",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<HotelOrderDetailResponseDTO> queryOrderDetail(@RequestBody @Valid HotelOrderDetailRequestDTO hotelOrderDetailRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        HotelOrderDetailResponseDTO orderDetail = new HotelOrderDetailResponseDTO();
        orderDetail.setOrderId(hotelOrderDetailRequestDTO.getOrderId());
        orderDetail.setOrderCode("H" + orderDetail.getOrderId());
        orderDetail.setCreator("张三");
        orderDetail.setCreateTime(new Date());
        orderDetail.setHotelId(2000);
        orderDetail.setHotelName("深圳国际小酒店");
        orderDetail.setGuestNames("张三，李四，王五");
        orderDetail.setOrderSum(new BigDecimal(699));
        orderDetail.setPayStatus(PayStatusEnum.UN_PAID.key);
        orderDetail.setOrderStatus(OrderStatusEnum.NEWORDER.key);

        orderDetail.setChannelName("B2B");
        orderDetail.setChannelCode("B2B");
        orderDetail.setIsGroupRoom(0);
        orderDetail.setConfirmNo("123456，78945");
        orderDetail.setPayMethod(PayMethodEnum.PRE_PAY.key);
        orderDetail.setSaleCurrency("CNY");
        orderDetail.setBalanceMethod(BalanceMethodEnum.MONTH.key);
        orderDetail.setReceivableAmount(new BigDecimal(699));
        orderDetail.setPaidInAmount(BigDecimal.ZERO);
        orderDetail.setAgentCode("F000000");
        orderDetail.setAgentName("月结分销商");
        orderDetail.setSpecialRequest("无烟处理");
        orderDetail.setContractName("张三");
        orderDetail.setContractPhone("18600000000");
        orderDetail.setShowCancel(1);
        orderDetail.setGroupNo(null);
        orderDetail.setBeforeOrderSum(new BigDecimal(699));

        orderDetail.setImageUrl("http://fcimage.fangcang.com/test02images/hotels/873/195873/201709271506477176812.jpg");
        orderDetail.setHotelAddress("温泉镇温泉政府向东400米");
        orderDetail.setCancelPolicy("一经预定不可取消");
        orderDetail.setBelongUser("18600000000");
        orderDetail.setBelongName("小明");
        orderDetail.setGuide("张三");
        orderDetail.setGuidePhone("18600000000");

        List<RatePricePlanDTO> pricePlanDTOS = new ArrayList<>();
        RatePricePlanDTO ratePricePlanDTO = new RatePricePlanDTO();
        ratePricePlanDTO.setSupplyCode("S000000");
        ratePricePlanDTO.setSupplyName("月结供应商");
        ratePricePlanDTO.setRoomTypeId(1000);
        ratePricePlanDTO.setRoomTypeName("大床房");
        ratePricePlanDTO.setRateplanId(1000);
        ratePricePlanDTO.setRateplanName("优惠早餐");
        ratePricePlanDTO.setQuotaType(QuotaTypeEnum.QUOTA_ROOM.key);
        ratePricePlanDTO.setBedtype("1");
        ratePricePlanDTO.setBreakfastType(1);
        ratePricePlanDTO.setCheckinDate(DateUtil.dateToString(new Date()));
        ratePricePlanDTO.setCheckoutDate(DateUtil.dateToString(new Date()));
        ratePricePlanDTO.setRoomNum(1);

        List<DailyDTO> dailyDTOS = new ArrayList<>();
        DailyDTO dailyDTO = new DailyDTO();
        dailyDTO.setSaleDate(DateUtil.dateToString(new Date()));
        dailyDTO.setSalePrice(new BigDecimal(699));
        dailyDTOS.add(dailyDTO);

        ratePricePlanDTO.setProductPriceDTOList(dailyDTOS);
        pricePlanDTOS.add(ratePricePlanDTO);
        orderDetail.setRatePlanList(pricePlanDTOS);

        responseDTO.setModel(orderDetail);
        return responseDTO;
    }

    /**
     * 试预订
     * @param preBookRequestDTO
     * @return
     */
    @RequestMapping(value = "/prebook",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<PreBookResponseDTO> prebook(@RequestBody @Valid PreBookRequestDTO preBookRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);

        PreBookResponseDTO preBookResponseDTO = PropertyCopyUtil.transfer(preBookRequestDTO,PreBookResponseDTO.class);
        if(!CollectionUtils.isEmpty(preBookRequestDTO.getAdditionalList())){
            List<AdditionChargeDTO> additionChargeDTOS = new ArrayList<>();
            for(HotelAdditionalDTO hotelAdditionalDTO : preBookRequestDTO.getAdditionalList()){
                AdditionChargeDTO additionChargeDTO = new AdditionChargeDTO();
                additionChargeDTO.setType(hotelAdditionalDTO.getAdditionalType());
                additionChargeDTO.setName(hotelAdditionalDTO.getAdditionalName());
                additionChargeDTO.setSalePrice(hotelAdditionalDTO.getAdditionalPrice());
                additionChargeDTOS.add(additionChargeDTO);
            }
            preBookResponseDTO.setAdditionChargeList(additionChargeDTOS);
        }
        preBookResponseDTO.setContractName("张三");
        preBookResponseDTO.setContractPhone("185662535218");
        responseDTO.setModel(preBookResponseDTO);
        return responseDTO;
    }

    /**
     * 预定
     * @param bookOrderRequestDTO
     * @return
     */
    @RequestMapping(value = "/book",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public ResponseDTO<BookOrderResponseDTO> book(@RequestBody @Valid BookOrderRequestDTO bookOrderRequestDTO){
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        BookOrderResponseDTO bookOrderResponseDTO = new BookOrderResponseDTO();
        bookOrderResponseDTO.setOrderId(10000L);
        responseDTO.setModel(bookOrderResponseDTO);
        return responseDTO;
    }
}
