package com.fangcang.b2b.common.controller;

import com.fangcang.agent.dto.AgentUserDTO;
import com.fangcang.b2b.common.constant.CommonConstant;
import com.fangcang.common.constant.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * @Auther: yanming.li@fangcang.com
 * @Date: 2018/6/30 09:16
 * @Description:
 */
public class BaseController implements Serializable {
    private static final long serialVersionUID = -8694817912391665597L;

    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    public HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 获取已登录用户的会员信息
     */
    public AgentUserDTO getCacheUser() {
        if (getSession()==null || getSession().getAttribute(CommonConstant.SESSION_SYSUSER)==null){
            return null;
        }
        return (AgentUserDTO) getSession().getAttribute(CommonConstant.SESSION_SYSUSER);
//        AgentUserDTO agentUserDTO = new AgentUserDTO();
//        agentUserDTO.setMerchantCode("M10000003");
//        agentUserDTO.setMerchantName("test");
//        agentUserDTO.setMerchantId(4L);
//        agentUserDTO.setAgentCode("A100053");
//        agentUserDTO.setAgentId(116L);
//        agentUserDTO.setAgentUserId(164L);
//        agentUserDTO.setUserName("13888888888");
//        agentUserDTO.setRealName("张三");
//        agentUserDTO.setAgentName("架构师分销商");
//        agentUserDTO.setFinanceCurrency("CNY");
//        return agentUserDTO;
    }

    public String getOperator(){
        AgentUserDTO agentUserDTO = getCacheUser();
        if(null != agentUserDTO){
            return agentUserDTO.getRealName() + "(" + agentUserDTO.getUserName() + ")" ;
        }
        return "";
    }

    public String getMerchantCode(){
        AgentUserDTO cacheUser = getCacheUser();
        if(null != cacheUser){
            return cacheUser.getMerchantCode();
        }
        return "";
    }

    public String getAgentCode(){
        AgentUserDTO cacheUser = getCacheUser();
        if(null != cacheUser){
            return cacheUser.getAgentCode();
        }
        return "";
    }

    /**
     * 获取当前访问者的微信openid
     * @return
     */
    public String getCacheVisitWxOpenId(){
        return getSession()==null?null:(String)getSession().getAttribute(CommonConstant.SESSION_VISIT_WX_OPENID);
    }

    /**
     * 获取微信自动登录访问源URL（微信自动登录跳转用）
     */
    public String getWxSrcVisitUrl(){
        return (String)getSession().getAttribute(CommonConstant.SESSION_WX_SRC_VISIT_URL);
    }

    /**
     * 设置微信自动登录访问源URL（微信自动登录跳转用）
     */
    public void setWxSrcVisitUrl(String wxSrcVisitUrl){
        if(StringUtils.isBlank(wxSrcVisitUrl)){
            getSession().removeAttribute(CommonConstant.SESSION_WX_SRC_VISIT_URL);
        }else{
            getSession().setAttribute(CommonConstant.SESSION_WX_SRC_VISIT_URL, wxSrcVisitUrl);
        }
    }
}
