package com.fangcang.b2b.wxpay.dto;

import java.io.Serializable;

public class WxOrderQueryRequestDTO implements Serializable{
	private static final long serialVersionUID = 8538765781180271328L;

	/**
	 * 客户单号(格式：订单号+下划线+随机数)
	 */
	private String weixinOutTradeNo;
	
	/**
	 * 公众账号ID
	 */
	private String appId;

	public String getWeixinOutTradeNo() {
		return weixinOutTradeNo;
	}

	public void setWeixinOutTradeNo(String weixinOutTradeNo) {
		this.weixinOutTradeNo = weixinOutTradeNo;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
	
}
