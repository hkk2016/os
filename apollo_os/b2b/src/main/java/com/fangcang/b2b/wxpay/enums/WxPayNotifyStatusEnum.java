package com.fangcang.b2b.wxpay.enums;

public enum WxPayNotifyStatusEnum {

    SUCCESS(1,"成功"),

    FAILURE(0,"失败");

    public int code;
    public String desc;

    private WxPayNotifyStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getValueBykey(int code){
        String value = "其他";
        for(WxPayNotifyStatusEnum wxPayNotifyStatusEnum : WxPayNotifyStatusEnum.values()) {
            if(wxPayNotifyStatusEnum.code == code) {
                value = wxPayNotifyStatusEnum.desc;
                break;
            }
        }
        return value;
    }
}
