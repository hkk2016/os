package com.fangcang.b2b.hotelsearch;

import com.fangcang.agent.dto.AgentUserDTO;
import com.fangcang.b2b.common.controller.BaseController;
import com.fangcang.b2b.dto.HotelSimpleDTO;
import com.fangcang.b2b.dto.WeChatUserInfoDTO;
import com.fangcang.b2b.request.BusinessZoneRequestDTO;
import com.fangcang.b2b.request.GetHotelDetailRequestDTO;
import com.fangcang.b2b.request.QueryHotelListRequestDTO;
import com.fangcang.b2b.request.QueryMerchantAllCityDTO;
import com.fangcang.b2b.request.SaveSearchHistoryDTO;
import com.fangcang.b2b.request.SearchHistoryQueryDTO;
import com.fangcang.b2b.response.BusinessZoneResponseDTO;
import com.fangcang.b2b.response.ContactUsResponseDTO;
import com.fangcang.b2b.response.GetHotelDetailResponseDTO;
import com.fangcang.b2b.response.GetOfflinePayResponseDTO;
import com.fangcang.b2b.response.HotelBaseInfoWithImagesResponseDTO;
import com.fangcang.b2b.response.QueryHotelListRsponseDTO;
import com.fangcang.b2b.response.QueryMerchantCommonCityResponseDTO;
import com.fangcang.b2b.response.SearchHistoryResponseDTO;
import com.fangcang.b2b.service.B2BHistoryService;
import com.fangcang.b2b.service.HotelOrderService;
import com.fangcang.b2b.service.HotelSearchService;
import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.hotelinfo.request.HotelBaseInfoRequestDTO;
import com.fangcang.hotelinfo.request.HotelListQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/b2b/hotel")
@RestController
public class HotelSearchController extends BaseController {

    @Autowired
    private HotelSearchService hotelSearchService;


    @Autowired
    private B2BHistoryService b2BHistoryService;

    @Autowired
    private HotelOrderService hotelOrderService;

    /**
     * 获取商业区
     *
     * @return
     */
    @RequestMapping(value = "/getBusinessZone", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<BusinessZoneResponseDTO> getBusinessZone(@RequestBody @Valid BusinessZoneRequestDTO businessZoneRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    /**
     * 获取酒店列表
     *
     * @param queryHotelListRequestDTO
     * @return
     */
    @RequestMapping(value = "/queryHotelList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<PaginationSupportDTO<QueryHotelListRsponseDTO>> queryHotelList(@RequestBody @Valid QueryHotelListRequestDTO queryHotelListRequestDTO) {
        AgentUserDTO agentUserDTO = super.getCacheUser();
        queryHotelListRequestDTO.setMerchantCode(agentUserDTO.getMerchantCode());
        return hotelSearchService.queryHotelList(queryHotelListRequestDTO, agentUserDTO);
    }

    /**
     * 获取酒店详情
     *
     * @param getHotelDetailRequestDTO
     * @return
     */
    @RequestMapping(value = "/getHotelDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<GetHotelDetailResponseDTO> getHotelDetail(@RequestBody @Valid GetHotelDetailRequestDTO getHotelDetailRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        AgentUserDTO agentUserDTO = super.getCacheUser();
        return hotelSearchService.getHotelDetail(getHotelDetailRequestDTO, agentUserDTO);
    }

    /**
     * 查询常用城市
     *
     * @return
     */
    @RequestMapping(value = "/queryCommonCity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<QueryMerchantCommonCityResponseDTO> queryCommonCity() {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            String merchantCode = this.getCacheUser().getMerchantCode();
            responseDTO = hotelSearchService.queryCommonCity(merchantCode);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 模糊查询商家城市
     *
     * @return
     */
    @RequestMapping(value = "/fuzzyQueryMerchantCity", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<QueryMerchantCommonCityResponseDTO> fuzzyQueryMerchantCity(@RequestBody QueryMerchantAllCityDTO queryMerchantAllCityDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            if (null != this.getCacheUser() && null != this.getCacheUser().getMerchantCode()) {
                queryMerchantAllCityDTO.setMerchantCode(this.getCacheUser().getMerchantCode());
                responseDTO = hotelSearchService.fuzzyQueryMerchantCity(queryMerchantAllCityDTO);
            } else {
                responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            }
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }

        return responseDTO;
    }

    /**
     * 查询酒店基本信息
     *
     * @return
     */
    @RequestMapping(value = "/queryHotelBaseInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<HotelBaseInfoWithImagesResponseDTO> queryHotelBaseInfo(@RequestBody @Valid HotelBaseInfoRequestDTO hotelBaseInfoRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            hotelBaseInfoRequestDTO.setMerchantCode(this.getCacheUser().getMerchantCode());
            responseDTO = hotelSearchService.queryHotelBaseInfo(hotelBaseInfoRequestDTO);
            responseDTO.setResult(ResultCodeEnum.SUCCESS.code);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 保存搜索历史
     *
     * @param saveSearchHistoryDTO
     * @return
     */
    @RequestMapping(value = "/saveSearchHistory", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO saveSearchHistory(@RequestBody @Valid SaveSearchHistoryDTO saveSearchHistoryDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        try {
            //从session中获取登录的分销商用户id
            saveSearchHistoryDTO.setAgentUserId(this.getCacheUser().getAgentUserId());
            responseDTO = b2BHistoryService.saveSearchHistory(saveSearchHistoryDTO);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 查询搜索历史
     *
     * @param searchHistoryQueryDTO
     * @return
     */
    @RequestMapping(value = "/querySearchHistory", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<SearchHistoryResponseDTO> querySearchHistory(@RequestBody @Valid SearchHistoryQueryDTO searchHistoryQueryDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        try {
            //从session中获取登录的分销商用户id
            searchHistoryQueryDTO.setAgentUserId(this.getCacheUser().getAgentUserId());
            responseDTO = b2BHistoryService.querySearchHistory(searchHistoryQueryDTO);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 删除搜索历史
     * @param searchHistoryQueryDTO
     * @return
     */
    @RequestMapping(value = "/deleteSearchHistory", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO deleteSearchHistory(@RequestBody @Valid SearchHistoryQueryDTO searchHistoryQueryDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        try {
            searchHistoryQueryDTO.setAgentUserId(this.getCacheUser().getAgentUserId());
            responseDTO = b2BHistoryService.deleteSearchHistory(searchHistoryQueryDTO);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 联系我们
     *
     * @return
     */
    @RequestMapping(value = "/contactUs", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<ContactUsResponseDTO> contactUs() {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        try {
            AgentUserDTO agentUserDTO = this.getCacheUser();
            //从缓存中获取商家id和分销商编码
            Long merchantId = agentUserDTO.getMerchantId();
            String agentCode = agentUserDTO.getAgentCode();
            responseDTO = b2BHistoryService.contactUs(merchantId, agentCode);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 获取线下支付
     *
     * @return
     */
    @RequestMapping(value = "/getOfflinePay", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<GetOfflinePayResponseDTO> getOfflinePay() {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            Long merchantId = this.getCacheUser().getMerchantId();
            responseDTO = hotelOrderService.getOfflinePay(merchantId);
        } catch (Exception e) {
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }

    /**
     * 获取微信头像地址
     * @return
     */
    @RequestMapping(value = "/getUserWeChatInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<WeChatUserInfoDTO> getUserWeChatInfo() {
        ResponseDTO responseDTO = new ResponseDTO();
        AgentUserDTO cacheUser = super.getCacheUser();
        if(null == cacheUser){
            responseDTO.setResult(ResultCodeEnum.FAILURE.code);
            responseDTO.setFailCode(ErrorCodeEnum.SYSTEM_EXCEPTION.errorCode);
            responseDTO.setFailReason(ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }else{
            WeChatUserInfoDTO weChatUserInfoDTO = new WeChatUserInfoDTO();
            weChatUserInfoDTO.setWxName(cacheUser.getWxName());
            weChatUserInfoDTO.setWxHead(cacheUser.getWxHead());
            responseDTO.setResult(ResultCodeEnum.SUCCESS.code);
            responseDTO.setModel(weChatUserInfoDTO);
        }
        return responseDTO;
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/associateHotelInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseDTO<List<HotelSimpleDTO>> associateHotelInfo(@RequestBody HotelListQueryDTO hotelListQueryDTO) {
        final String merchantCode = super.getMerchantCode();
        hotelListQueryDTO.setMerchantCode(merchantCode);
        return hotelSearchService.associateHotelInfo(hotelListQueryDTO);
    }
}
