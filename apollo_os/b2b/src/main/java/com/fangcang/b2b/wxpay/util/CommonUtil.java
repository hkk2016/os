package com.fangcang.b2b.wxpay.util;

import com.fangcang.b2b.wxpay.dto.AccountPayResponseDTO;
import com.fangcang.b2b.wxpay.dto.OrderQueryResponseDTO;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URL;

public class CommonUtil {
	private static Logger log = LoggerFactory.getLogger(CommonUtil.class);
	/**
	 * 发送https请求
	 * @param requestUrl 请求地址
	 * @param requestMethod 请求方式（GET、POST）
	 * @param outputStr 提交的数据
	 * @return 返回微信服务器响应的信息
	 */
	public static String httpsRequest(String requestUrl, String requestMethod, String outputStr) {
		log.info("----------------->向微信发送https请求，提交的数据:"+outputStr);
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			// 从上述SSLContext对象中得到SSLSocketFactory对象
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			URL url = new URL(requestUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setSSLSocketFactory(ssf);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			conn.setRequestMethod(requestMethod);
			conn.setRequestProperty("content-type", "application/x-www-form-urlencoded"); 
			// 当outputStr不为null时向输出流写数据
			if (null != outputStr) {
				OutputStream outputStream = conn.getOutputStream();
				// 注意编码格式
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}
			// 从输入流读取返回内容
			InputStream inputStream = conn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String str = null;
			StringBuffer buffer = new StringBuffer();
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			// 释放资源
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
			inputStream = null;
			conn.disconnect();
			log.info("返回结果："+buffer.toString());
			return buffer.toString();
		} catch (ConnectException ce) {
			log.error("连接超时：{}", ce);
		} catch (Exception e) {
			log.error("https请求异常：{}", e);
		}
		return null;
	}

	public static String urlEncodeUTF8(String source){
		String result = source;
		try {
			result = java.net.URLEncoder.encode(source,"utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 分转成元
	 */
	public static String fenToYuan(double money){
		double moneyDouble=money*100;
		return moneyDouble+"";
	}
    
    /**
     * 将微信返回的string格式的xml封装到类
     * @param result
     * @return
     */
    public static Object getClsFromWxResponse(String result,String clsName){ 
    	XStream xs = XStreamFactory.init(false);
      	xs.ignoreUnknownElements();
      	if("AccountPayResponseDTO".equals(clsName)){
      		xs.alias("xml", AccountPayResponseDTO.class);
      	}else if("OrderQueryResponseDTO".equals(clsName)){
      		xs.alias("xml", OrderQueryResponseDTO.class);
      	}
      	Object obj =  xs.fromXML(result);
      	return obj;
    }
    
    /**
     * 根据货币类型简写字母获取简称（如：CNY:人民币）
     * @param fee_type
     * @return
     */
//    public static CurrencyEnum getFeeTypeStr(String fee_type){
//    	CurrencyEnum fee_typeEnum=CurrencyEnum.CNY;
//    	if(fee_type.equalsIgnoreCase("CNY")){
//    		fee_typeEnum=CurrencyEnum.CNY;
//    	}
//    	return fee_typeEnum;
//    }
    
    /**
     * 获取浏览器类型
     * @param request
     * @return
     */
    public static String getUserAgent(HttpServletRequest request){
    	String ua = request.getHeader("user-agent");
		if(ua == null){
			ua = request.getHeader("User-Agent");
		}
		if(ua != null){
			ua = ua.toLowerCase();
		}
		return ua;
    }
    
}