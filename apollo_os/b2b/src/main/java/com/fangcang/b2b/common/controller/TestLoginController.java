package com.fangcang.b2b.common.controller;

import com.fangcang.agent.dto.AgentUserDTO;
import com.fangcang.b2b.request.AgentLoginDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

/**
 * @Auther: yanming.li@fangcang.com
 * @Date: 2018/6/30 10:30
 * @Description:
 */
@Controller
@Slf4j
@RequestMapping("/test//b2b/home/")
public class TestLoginController extends BaseController{

    /**
     * 登录
     * @param agentLoginDTO
     * @return
     */
    @RequestMapping(value = "/login" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO<AgentUserDTO> login(@RequestBody AgentLoginDTO agentLoginDTO){
        ResponseDTO responseDTO = new ResponseDTO();
        AgentUserDTO agentUserDTO = new AgentUserDTO();
        agentUserDTO.setUserName(agentLoginDTO.getLoginName());
        /** 前端传的key是loginName而不是userName */
        agentUserDTO.setUserName("测试账号");
        agentUserDTO.setRealName("测试001");
        agentUserDTO.setAgentUserId(1L);
        agentUserDTO.setAgentId(110L);
        agentUserDTO.setAgentName("");
        agentUserDTO.setAgentCode("A100051");
        agentUserDTO.setCityName("北京");
        agentUserDTO.setCityCode("PEK");
        agentUserDTO.setAmount(new BigDecimal(123456.78).setScale(2,BigDecimal.ROUND_HALF_UP));
        agentUserDTO.setBillingMethod(1);
        agentUserDTO.setFinanceCurrency("CNY");
        agentUserDTO.setMerchantId(22L);
        agentUserDTO.setMerchantCode("M10000008");
        responseDTO.setResult(ResultCodeEnum.SUCCESS.code);
        responseDTO.setModel(agentUserDTO);
        return responseDTO;
    }

}
