package com.fangcang.b2b.wxpay.dto;

import lombok.Data;

/**
 * 创建微信预付单Response
 */
@Data
public class WxPrepayResponseDTO {

	private static final long serialVersionUID = -4319649296719058630L;

	/**
	 * 订单号
	 */
	private String orderCode;

	/**
	 * 客户单号
	 */
	private String weixinOutTradeNo;

	/**
	 * 微信预付单ID
	 */
	private String perpayId;

	/**
	 * 微信H5支付参数
	 */
	private WxH5PayResponseDTO weiXinH5PayVo;

	/**
	 * 支付跳转链接(微信H5支付需要)
	 */
	private String mwebUrl;
}
