package com.fangcang.b2b.common.constant;

public interface CommonConstant {

    /**
     * session key：系统用户
     */
    public String SESSION_SYSUSER = "sysUser";

    /**
     * session key：当前访问者微信openid
     */
    public String SESSION_VISIT_WX_OPENID = "visitWxOpenId";

    /**
     * session key：微信自动登录访问源URL
     */
    public String SESSION_WX_SRC_VISIT_URL = "wxSrcVisitUrl";
}
