package com.fangcang.ebk.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class EbkUserDTO implements Serializable {
    private static final long serialVersionUID = 6899493106722037546L;

    private String userName;

    private String realName;

    private String phone;

    private Date createTime;

    private String supplyCode;

    private String supplyName;

    private String merchantCode;

    private String merchantName;

    private String systemName;

    private String ebkName;

    /**
     * 产品经理账号ID
     */
    private MerchantUserDTO merchantPM;

    /**
     * 业务经理账号ID
     */
    private MerchantUserDTO merchantBM;

    /**
     * 跟单员账号ID
     */
    private List<MerchantUserDTO> merchantOPList;

    /**
     * 财务员账号ID
     */
    private MerchantUserDTO merchantFinancer;
}
