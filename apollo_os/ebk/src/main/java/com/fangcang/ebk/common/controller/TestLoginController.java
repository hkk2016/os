package com.fangcang.ebk.common.controller;

import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.ebk.common.constant.CommonConstant;
import com.fangcang.ebk.common.dto.EbkUserDTO;
import com.fangcang.merchant.dto.QueryUserConditionDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@RestController
@Slf4j
@RequestMapping(("/test/ebk"))
public class TestLoginController extends BaseController{

    @RequestMapping(value = "/login" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO queryUser(@RequestBody QueryUserConditionDTO requestDTO){
        ResponseDTO responseDTO=new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        EbkUserDTO supplyUserDTO=new EbkUserDTO();
        supplyUserDTO.setUserName("longkangfu");
        supplyUserDTO.setRealName("龙康甫");
        supplyUserDTO.setPhone("13570830210");
        supplyUserDTO.setCreateTime(new Date());
        supplyUserDTO.setSupplyCode("S3232323232");
        supplyUserDTO.setSupplyName("港澳国旅");
        responseDTO.setModel(supplyUserDTO);
        if(responseDTO.getResult()==ResultCodeEnum.SUCCESS.code){
            super.getSession().setAttribute(CommonConstant.SESSION_SYSUSER,responseDTO.getModel());
        }
        return responseDTO;
    }

    @RequestMapping(value = "/logout" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO logout(){
        super.getSession().setAttribute(CommonConstant.SESSION_SYSUSER,null);
        return new ResponseDTO(ResultCodeEnum.SUCCESS.code);
    }

    @RequestMapping(value = "/checkLogin" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO checkLogin(){
        ResponseDTO response=null;
        EbkUserDTO sysUser=super.getCacheUser();
        if (sysUser==null){
            response=new ResponseDTO(ResultCodeEnum.FAILURE.code);
        }else{
            response=new ResponseDTO(ResultCodeEnum.SUCCESS.code);
            response.setModel(sysUser);
        }
        return response;
    }

    /**
     * 微信回调地址，根据微信回传code获取微信openID
     */
    @RequestMapping(value = { "/wxBusDispatcher.shtml" })
    public ModelAndView wxBusDispatcher(String code, HttpServletRequest request, HttpServletResponse response) throws IOException {
        return null;
    }
}
