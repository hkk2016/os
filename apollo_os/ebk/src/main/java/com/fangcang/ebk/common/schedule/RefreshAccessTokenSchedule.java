package com.fangcang.ebk.common.schedule;

import com.fangcang.message.weixin.service.WxAccessTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RefreshAccessTokenSchedule {

    @Autowired
    private WxAccessTokenService wxAccessTokenService;

    @Scheduled(cron = "0 0/30 * * * ?")
    public void refreshAccessToken(){
        wxAccessTokenService.resetAccessToken();
    }
}
