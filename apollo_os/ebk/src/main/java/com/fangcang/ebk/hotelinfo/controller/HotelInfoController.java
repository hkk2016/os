package com.fangcang.ebk.hotelinfo.controller;

import com.fangcang.common.ResponseDTO;
import com.fangcang.ebk.common.controller.BaseController;
import com.fangcang.ebk.request.QuerySupplyHotelDTO;
import com.fangcang.ebk.response.SupplyHotelResponseDTO;
import com.fangcang.ebk.service.CheckInReportService;
import com.fangcang.hotelinfo.request.HotelBaseInfoRequestDTO;
import com.fangcang.hotelinfo.response.HotelBaseInfoRsponseDTO;
import com.fangcang.hotelinfo.service.HotelInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping(("/ebk/hotelinfo"))
public class HotelInfoController extends BaseController {

    @Autowired
    private HotelInfoService hotelInfoService;

    @Autowired
    private CheckInReportService checkInReportService;

    /**
     * 查询酒店基本信息
     */
    @RequestMapping(value = "/queryHotelInfoByHotelId",method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<HotelBaseInfoRsponseDTO> queryHotelInfoByHotelId(@RequestBody @Valid HotelBaseInfoRequestDTO hotelBaseInfoRequestDTO){
        hotelBaseInfoRequestDTO.setMerchantCode(super.getCacheUser().getMerchantCode());
        return hotelInfoService.queryHotelInfoByHotelId(hotelBaseInfoRequestDTO);
    }

    /**
     * 查询商家常用酒店
     * @param queryCommonHotelDTO
     * @return
     */
    /*@RequestMapping(value = "/queryCommonUsedHotel",method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<CommonUsedHotelResponseDTO> queryCommonUsedHotel(@RequestBody @Valid CommonUsedHotelRequestDTO queryCommonHotelDTO){
        queryCommonHotelDTO.setMerchantCode(super.getCacheUser().getMerchantCode());
        return hotelInfoService.queryCommonUsedHotel(queryCommonHotelDTO);
    }*/

    /**
     * 查询供应商有价格计划的酒店
     */
    @RequestMapping(value = "/queryCommonUsedHotel",method = RequestMethod.POST,produces = {"application/json;charset=UTF-8"})
    public ResponseDTO<SupplyHotelResponseDTO> queryCommonUsedHotel(@RequestBody QuerySupplyHotelDTO requestDTO){
        requestDTO.setSupplyCode(super.getCacheUser().getSupplyCode());
        return checkInReportService.querySupplyHotel(requestDTO);
    }
}
