package com.fangcang.ebk.base.controller;

import com.fangcang.base.dto.CommonCityDTO;
import com.fangcang.base.service.CommonCityService;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ErrorCodeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.ebk.common.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(("/ebk/base/commoncity"))
public class CommonCityController extends BaseController {

    @Autowired
    private CommonCityService commonCityService;

    @RequestMapping(value = "/querycommoncity" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO querycommoncity() {
        ResponseDTO responseDTO=null;
        try{
            List<CommonCityDTO> list=commonCityService.queryCommonCity(super.getCacheUser().getMerchantCode());
            responseDTO=new ResponseDTO(ResultCodeEnum.SUCCESS.code);
            responseDTO.setModel(list);
        }catch (Exception e){
            log.error("commonCityService.queryCommonCity 异常",e);
            responseDTO=new ResponseDTO(ResultCodeEnum.FAILURE.code,null, ErrorCodeEnum.SYSTEM_EXCEPTION.errorDesc);
        }
        return responseDTO;
    }
}
