package com.fangcang.ebk.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MerchantUserDTO implements Serializable {
    private static final long serialVersionUID = 6899493106722037546L;

    private Long userId;

    private String userName;

    private String phone;

    private String qq;

    private String email;

    private String landlineTelephone;
}
