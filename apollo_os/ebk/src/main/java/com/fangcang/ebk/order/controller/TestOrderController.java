package com.fangcang.ebk.order.controller;

import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.util.DateUtil;
import com.fangcang.common.util.excel.ExcelUtils;
import com.fangcang.ebk.common.controller.BaseController;
import com.fangcang.ebk.request.QueryCheckInReportDTO;
import com.fangcang.ebk.request.QueryEbkOrderDTO;
import com.fangcang.ebk.request.UpdateConfirmNoDTO;
import com.fangcang.ebk.request.UpdateConfirmResultDTO;
import com.fangcang.ebk.response.CheckInReportDTO;
import com.fangcang.ebk.response.CheckInReportSummaryDTO;
import com.fangcang.ebk.response.EbkNotifyDTO;
import com.fangcang.ebk.response.EbkOrderSimpleDTO;
import com.fangcang.ebk.response.EbkOrderStatisticsDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(("/test/ebk/order"))
public class TestOrderController extends BaseController {

    @RequestMapping(value = "/orderlist" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO orderlist(@RequestBody QueryEbkOrderDTO requestDTO){
        ResponseDTO responseDTO=new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<EbkOrderSimpleDTO> ebkOrderSimpleList=new ArrayList<>();
        for (int i=0;i < 20; i++){
            EbkOrderSimpleDTO ebkOrderSimple=new EbkOrderSimpleDTO(i+1L,"S321332312323"+i,
                    i%5==0?1:i%5,i%2==0?1:i%2,BigDecimal.valueOf(10*i),"CNY","深圳喜马拉雅大酒店","大床房",
                    "限时优惠促销","2016/03/06","2016/03/08",2,1,"李雷、韩梅梅",
                    "李雷","李雷",i+1L,"434233232","李晓晓","159996059906",DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
            ebkOrderSimpleList.add(ebkOrderSimple);

            i++;
            EbkOrderSimpleDTO ebkOrderSimple2=new EbkOrderSimpleDTO();
            ebkOrderSimple2.setId(i+1L);
            ebkOrderSimple2.setSupplyOrderCode("S321332312323"+i);
            ebkOrderSimple2.setOrderStatus(i%5==0?1:i%5);
            ebkOrderSimple2.setIsGroupon(i%2==0?1:i%2);
            ebkOrderSimple2.setOrderSum(BigDecimal.valueOf(10*i));
            ebkOrderSimple2.setCurrency("CNY");
            ebkOrderSimple2.setHotelName("深圳大梅沙京基喜来登酒店");
            ebkOrderSimple2.setRoomtypeName("高级房");
            ebkOrderSimple2.setRateplanName("双早");
            ebkOrderSimple2.setCheckInDate("2016/03/07");
            ebkOrderSimple2.setCheckInDate("2016/03/09");
            ebkOrderSimple2.setNightNum(1);
            ebkOrderSimple2.setRoomNum(3);
            ebkOrderSimple2.setGuest("王大明");
            ebkOrderSimple2.setBelongName("李雷");
            ebkOrderSimple2.setLockName("李雷");
            ebkOrderSimple2.setRequestId(i+1L);
            ebkOrderSimple2.setConfirmNo("");
            ebkOrderSimple2.setGuide("李晓晓");
            ebkOrderSimple2.setGuidePhone("159996059906");
            ebkOrderSimple2.setCreateTime(DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
            ebkOrderSimpleList.add(ebkOrderSimple);
        }
        PaginationSupportDTO<EbkOrderSimpleDTO> paginationSupportDTO=new PaginationSupportDTO();
        paginationSupportDTO.setTotalCount(20);
        paginationSupportDTO.setTotalPage(4);
        paginationSupportDTO.setCurrentPage(1);
        paginationSupportDTO.setPageSize(5);
        paginationSupportDTO.setItemList(ebkOrderSimpleList);
        responseDTO.setModel(paginationSupportDTO);
        return responseDTO;
    }

    @RequestMapping(value = "/orderlistexport")
    public void orderlistexport(QueryEbkOrderDTO requestDTO,HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<EbkOrderSimpleDTO> ebkOrderSimpleList = new ArrayList<>();
        for (int i=0;i < 20; i++){
            EbkOrderSimpleDTO ebkOrderSimple=new EbkOrderSimpleDTO(i+1L,"S321332312323"+i,
                    i%5==0?1:i%5,i%2==0?1:i%2,BigDecimal.valueOf(10*i+5),"CNY","深圳喜马拉雅大酒店","大床房",
                    "限时优惠促销","2016/03/06","2016/03/08",2,1,"李雷、韩梅梅",
                    "李雷","李雷",i+1L,"434233232","李晓晓","159996059906",DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
            ebkOrderSimpleList.add(ebkOrderSimple);

            i++;
            EbkOrderSimpleDTO ebkOrderSimple2=new EbkOrderSimpleDTO(i+1L,"S321332312323"+i,
                    i%5==0?1:i%5,i%2==0?1:i%2,BigDecimal.valueOf(10*i),"CNY","深圳大梅沙京基喜来登酒店","高级房",
                    "双早","2016/03/07","2016/03/09",2,3,"王大明",
                    "李雷","李雷",i+1L,"434233232","李晓晓","159996059906",DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
            ebkOrderSimpleList.add(ebkOrderSimple2);
        }

        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("EBK订单报表","UTF-8") + ".xls");
        //编码
        response.setCharacterEncoding("UTF-8");
        ExcelUtils.exportExcel(response.getOutputStream(),EbkOrderSimpleDTO.class, ebkOrderSimpleList);
    }

    @RequestMapping(value = "/orderstatistics" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO orderstatistics(){
        ResponseDTO responseDTO=new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        EbkOrderStatisticsDTO ebkOrderStatisticsDTO=new EbkOrderStatisticsDTO(10,8);
        responseDTO.setModel(ebkOrderStatisticsDTO);
        return responseDTO;
    }

    /*@RequestMapping(value = "/orderdetail" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO orderdetail(@RequestBody QueryOrderDetailDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        EbkOrderDTO orderDTO=new EbkOrderDTO(1L,"S3213323123231",1,1,321312L,"深圳大梅沙京基喜来登酒店",
                3232L,"高级房",32323L,"双早","2016/03/07","2016/03/09",1,2,"王大明","",
                BigDecimal.valueOf(100),BigDecimal.valueOf(154),
                "CNY","","S342132332","畅游假期","M10000001","深圳新天地假期","",1,"李晓晓","159996059906",1L,"3232323",
                null,null,null,null,null,
                "longkangfu",DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"),1,null,null,null,null,null);
        orderDTO.setProductList(new ArrayList<>());
        EbkOrderProductDTO productDTO=new EbkOrderProductDTO(1L,1,"客房",323L,"高级房",434L,"双早","1",1,
                "2016/03/07","2016/03/09",2,1,BigDecimal.valueOf(100),BigDecimal.valueOf(200),null);
        orderDTO.getProductList().add(productDTO);
        EbkOrderProductDTO productDTO1=new EbkOrderProductDTO(2L,1,"客房",323L,"大床房",434L,"无早","1",1,
                "2016/04/07","2016/04/09",2,1,BigDecimal.valueOf(200),BigDecimal.valueOf(300),null);
        orderDTO.getProductList().add(productDTO);
        EbkOrderProductDTO productDTO2=new EbkOrderProductDTO(3L,1,"客房",323L,"豪华房",434L,"单早","1",1,
                "2016/04/11","2016/03/13",2,1,BigDecimal.valueOf(130),BigDecimal.valueOf(260),null);
        orderDTO.getProductList().add(productDTO);

        orderDTO.setAdditionList(new ArrayList<>());
        EbkOrderProductDTO additionDTO=new EbkOrderProductDTO(1L,2,"加早",null,null,null,null,null,null,
                null,null,null,1,BigDecimal.valueOf(100),BigDecimal.valueOf(200),null);
        orderDTO.getAdditionList().add(additionDTO);

        orderDTO.setDerateList(new ArrayList<>());
        EbkOrderProductDTO derateDTO=new EbkOrderProductDTO(1L,3,"全陪免半",null,null,null,null,null,null,
                null,null,null,null,BigDecimal.valueOf(54),BigDecimal.valueOf(54),null);
        orderDTO.getDerateList().add(derateDTO);

        orderDTO.setLogList(new ArrayList<>());
        EbkOrderLogDTO logDTO=new EbkOrderLogDTO(1L,"同意了商家预订申请","李雷",
                DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
        orderDTO.getLogList().add(logDTO);

        orderDTO.setUntreatedRequest(new EbkRequestDTO(1L,2,0,null,null,null,"longkangfu",
                DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss")));
        responseDTO.setModel(orderDTO);
        return responseDTO;
    }*/

    @RequestMapping(value = "/confirm" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO confirm(@RequestBody UpdateConfirmResultDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    @RequestMapping(value = "/modifyconfirmnum" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO modifyconfirmnum(@RequestBody UpdateConfirmNoDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    @RequestMapping(value = "/unlock" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO unlock() {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        return responseDTO;
    }

    @RequestMapping(value = "/checkinreport" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO checkinreport(@RequestBody QueryCheckInReportDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<CheckInReportDTO> list=new ArrayList<>();
        for (int i=1;i<16;i++){
            CheckInReportDTO reportDTO=new CheckInReportDTO("2016-10-0"+i,i*10,i*6,i*4,
                    BigDecimal.valueOf(3300),"格林联盟酒店（珠海吉大九洲港店）");
            list.add(reportDTO);
        }
        CheckInReportSummaryDTO summaryDTO=new CheckInReportSummaryDTO(500,300,200,BigDecimal.valueOf(2340080));
        summaryDTO.setTotalCount(15);
        summaryDTO.setTotalPage(3);
        summaryDTO.setCurrentPage(1);
        summaryDTO.setPageSize(5);
        summaryDTO.setItemList(list);
        responseDTO.setModel(summaryDTO);
        return responseDTO;
    }

    @RequestMapping(value = "/checkinreportexport")
    public void checkinreportexport(QueryCheckInReportDTO requestDTO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<CheckInReportDTO> list=new ArrayList<>();
        for (int i=1;i<16;i++){
            CheckInReportDTO reportDTO=new CheckInReportDTO("2016-10-0"+i,i*10,i*6,i*4,
                    BigDecimal.valueOf(3300),"格林联盟酒店（珠海吉大九洲港店）");
            list.add(reportDTO);
        }

        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("在住间夜报表","UTF-8") + ".xls");
        //编码
        response.setCharacterEncoding("UTF-8");
        ExcelUtils.exportExcel(response.getOutputStream(),CheckInReportDTO.class, list);
    }

    @RequestMapping(value = "/querynotify" , method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
    @ResponseBody
    public ResponseDTO querynotify() {
        ResponseDTO responseDTO = new ResponseDTO(ResultCodeEnum.SUCCESS.code);
        List<EbkNotifyDTO> list=new ArrayList<>();
        EbkNotifyDTO notifyDTO=new EbkNotifyDTO("供货单 S3213323123231 通过 EBK发送的预订单，请及时跟进。",
                "S3213323123231");
        list.add(notifyDTO);
        EbkNotifyDTO notifyDTO2=new EbkNotifyDTO("供货单 S3213323345231 通过 EBK发送的预订单，请及时跟进。",
                "S3213323345231");
        list.add(notifyDTO2);
        responseDTO.setModel(list);
        return responseDTO;
    }
}
