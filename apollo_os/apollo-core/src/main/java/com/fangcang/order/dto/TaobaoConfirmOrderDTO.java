package com.fangcang.order.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chihping
 * @Description TODO
 * @date 19-3-1 上午10:01
 */
@Data
public class TaobaoConfirmOrderDTO implements Serializable {

    private String merchantCode;

    private String channelCode;

    private String orderCode;

    private String shopId;

    /**
     * 淘宝订单号
     */
    private String customerOrderCode;

    /**
     * 操作的类型：1.确认无房（取消预订，710发送短信提醒买家申请退款）,2.确认预订
     */
    private String confirmResult;
}
