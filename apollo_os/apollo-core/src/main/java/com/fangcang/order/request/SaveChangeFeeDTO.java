package com.fangcang.order.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by on 2018/12/25.
 */
@Data
public class SaveChangeFeeDTO implements Serializable {
    private static final long serialVersionUID = -8979719692757984700L;

    private Integer orderId;

    /**
     * 退改费
     */
    private BigDecimal changeFee;

    /**
     * 操作人
     */
    private String operator;
}
