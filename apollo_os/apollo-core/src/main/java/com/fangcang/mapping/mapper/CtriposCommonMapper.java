package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.CtriposShopInfoDO;
import com.fangcang.mapping.dto.ctripos.CtriposMapHotelDTO;
import com.fangcang.mapping.request.ctripos.QueryHotelListRequest;
import com.fangcang.mapping.request.ctripos.QueryRoomOrRateListRequest;

import java.util.List;

public interface CtriposCommonMapper extends MyMapper<CtriposShopInfoDO> {

    /**
     * 查询酒店列表
     *
     * @param request
     * @return
     */
    List<CtriposMapHotelDTO> queryHotelList(QueryHotelListRequest request);

    /**
     * 查单酒店下房型及价格计划列表
     *
     * @param request
     * @return
     */
    List<CtriposMapHotelDTO> queryRoomOrRateList(QueryRoomOrRateListRequest request);

    /**
     * 查多酒店下房型及价格计划列表
     *
     * @param request
     * @return
     */
    List<CtriposMapHotelDTO> queryHotelInfoByIds(QueryRoomOrRateListRequest request);
}