package com.fangcang.mapping.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午6:04
 */
@Data
public class QueryRoomTypeAndRpListResponse implements Serializable {

    private Integer pricePlanId;
    private Integer hotelId;
    private Integer roomTypeId;
    private Integer bedType;
    private Integer quotaaccountId;
    private String pricePlanName;
    private Integer payMethod;
    private String baseCurrency;
    private Integer isActive;
    private Integer breakfastType;
    private String supplyCode;
    private String merchantCode;
    private String roomTypeName;
    private String hotelName;
    private String hid;
    private String rid;
    private String feizhuRoomName;
    private String roomStatus;
    private String roomMappingStatus;
    private String rpid;
}
