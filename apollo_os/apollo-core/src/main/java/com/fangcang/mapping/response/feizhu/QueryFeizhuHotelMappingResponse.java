package com.fangcang.mapping.response.feizhu;

import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:59
 */
@Data
public class QueryFeizhuHotelMappingResponse {

    private Long hotelId;
    private String hotelName;
    private String phone;
    private String address;
    private String cityCode;

    //淘宝酒店ID
    private String hid;
    //淘宝酒店名称
    private String name;
    //淘宝酒店状态：0: 正常;-2:停售；-1：删除
    private String status;
    //匹配状态
    private String mappingStatus;
}
