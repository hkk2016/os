package com.fangcang.mapping.service;

import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.mapping.request.feizhu.*;
import com.fangcang.mapping.response.feizhu.*;

import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:41
 */
public interface FeizhuMappingService {

    /**
     * 查询店铺列表
     * @return
     */
    ResponseDTO<List<ShopInfo>>queryFeizhuShop(String merchantCode);

    /**
     * 加载shop到内存，初始化
     * InitData.merchantShopMap
     * InitData.merchantShopChanneCodeMap
     */
    void initFeizhuShopInfo();

    /**queryRoomTypeAndPricePlanList
     * 分页查询飞猪的酒店映射列表
     * @param queryFeizhuHotelMappingRequest
     * @return
     */
    ResponseDTO<PaginationSupportDTO<QueryFeizhuHotelMappingResponse>> queryFeizhuHotelListForPage(QueryFeizhuHotelMappingRequest queryFeizhuHotelMappingRequest);

    /**
     * 查询相关信息，调用飞猪的酒店新增接口
     * @param addHotelToFeizhuRequest
     * @return
     */
    ResponseDTO<AddHotelToFeizhuResponse> addHotelToFeizhu(AddHotelToFeizhuRequest addHotelToFeizhuRequest);


    ResponseDTO<GetFeizhuHotelResponse> getFeizhuHotel(GetFeizhuHotelRequest getFeizhuHotelRequest) ;

    /**
     * 房型和价格计划列表页面
     * @param queryRoomTypeAndRpListRequest
     * @return
     */
    ResponseDTO<FeizhuHotelResponse> queryRoomTypeAndPricePlanList(QueryRoomTypeAndRpListRequest queryRoomTypeAndRpListRequest);

    /**
     * 推送房型到飞猪
     * @param addRoomTypeToFeizhuRequesst
     * @return
     */
    ResponseDTO<AddRoomTypeToFeizhuResponse> addRoomTypeToFeizhu(AddRoomTypeToFeizhuRequest addRoomTypeToFeizhuRequesst);


    /**
     * 查询飞猪的房型匹配状态
     * @param getFeizhuRoomTypeRequest
     * @return
     */
    ResponseDTO<GetFeizhuRoomTypeResponse> getFeizhuRoomType(GetFeizhuRoomTypeRequest getFeizhuRoomTypeRequest) ;

    /**
     * 推送价格计划到飞猪
     * @param addRatePlanToFeizhuResquest
     * @return
     */
    ResponseDTO<AddRatePlanToFeizhuResponse> addRatePlanToFeizhu(AddRatePlanToFeizhuResquest addRatePlanToFeizhuResquest);

    /**
     * 删除飞猪价格计划
     * @param delRatePlanToFeizhuResquest
     * @return
     */
    ResponseDTO<DelRatePlanToFeizhuResponse> delRatePlanToFeizhu(DelRatePlanToFeizhuResquest delRatePlanToFeizhuResquest);

    /**
     * 推送房价房态到飞猪
     * @param syncRateAndQuotaToFeizhuResquest
     * @return
     */
    ResponseDTO<SyncRateAndQuotaToFeizhuResponse> syncRateAndQuotaToFeizhu(SyncRateAndQuotaToFeizhuResquest syncRateAndQuotaToFeizhuResquest);
}
