package com.fangcang.mapping.request.feizhu;

import com.fangcang.common.BaseQueryConditionDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:52
 */
@Data
public class QueryRoomTypeAndRpListRequest extends BaseQueryConditionDTO {

    private Integer hotelId;
    private String shopId;
    private String merchantCode;

    private List<Integer> roomTypeIdList = new ArrayList<>();

    private List<Integer> pricePlanIdList = new ArrayList<>();
}
