package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.RatePlanMappingDO;
import com.fangcang.mapping.dto.Mapping;
import com.fangcang.mapping.request.feizhu.QueryFeizhuHotelMappingRequest;
import com.fangcang.mapping.request.feizhu.QueryRoomTypeAndRpListRequest;
import com.fangcang.mapping.response.QueryRoomTypeAndRpListResponse;
import com.fangcang.mapping.response.feizhu.QueryFeizhuHotelMappingResponse;
import com.fangcang.mapping.response.feizhu.ShopInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Owen on 2018/6/8.
 */
public interface RatePlanMappingMapper extends MyMapper<RatePlanMappingDO> {

    List<Mapping> queryMapping(@Param("merchantCode")String merchantCode, @Param("list")List<Long> hotelIdList);

    List<QueryFeizhuHotelMappingResponse> queryFeizhuHotelListForPage(QueryFeizhuHotelMappingRequest queryFeizhuHotelMappingRequest);

    List<ShopInfo> queryFeizhuShop(ShopInfo shopInfo);

    List<QueryRoomTypeAndRpListResponse> queryRoomTypeAndPricePlanList(QueryRoomTypeAndRpListRequest queryRoomTypeAndRpListRequest);

}
