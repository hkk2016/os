package com.fangcang.mapping.dto.ctripos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class CtriposMapHotelDTO implements Serializable {
    private static final long serialVersionUID = 8361237974804158229L;
    private Integer id;

    /**
     * 房仓酒店编码
     */
    private Integer hotelId;

    /**
     * 房仓酒店中文名称
     */
    private String hotelName;

    /**
     * 房仓酒店英文名称
     */
    private String hotelNameEng;

    /**
     * 国家编码
     */
    private String countryCode;

    /**
     * 国家中文名
     */
    private String countryName;

    /**
     * 国家英文名
     */
    private String countryNameEng;

    /**
     * 省份编码
     */
    private String provinceCode;

    /**
     * 省份中文名
     */
    private String provinceName;

    /**
     * 省份英文名
     */
    private String provinceNameEng;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 城市中文名
     */
    private String cityName;

    /**
     * 城市英文名
     */
    private String cityNameEng;

    /**
     * 携程酒店编码
     */
    private String ctripHotelId;

    /**
     * 携程酒店中文
     */
    private String ctripHotelName;

    /**
     * 携程酒店英文名
     */
    private String ctripHotelNameEng;

    /**
     * 携程城市编码
     */
    private String ctripCityCode;

    /**
     * 携程城市名称
     */
    private String ctripCityName;

    /**
     * 酒店星级
     */
    private Integer hotelStar;

    /**
     * 维度
     */
    private String lat;

    /**
     * 经度
     */
    private String lon;

    /**
     * 酒店长简介中文
     */
    private String description;

    /**
     * 酒店长简介英文
     */
    private String descriptionEng;

    /**
     * 开业时间
     */
    private Date openYear;

    /**
     * 装修时间
     */
    private Date fitmentYear;

    /**
     * 传真号码
     */
    private String fax;

    /**
     * 房间数量
     */
    private Integer roomQuantity;

    /**
     * 电话号码
     */
    private String telephone;

    /**
     * 酒店地址
     */
    private String address;

    /**
     * 酒店英文地址
     */
    private String addressEng;

    /**
     * 酒店品牌
     */
    private String hotelBrand;

    /**
     * 商家主题
     */
    private String hotelTheme;

    /**
     * 商家编码
     */
    private String merchantCode;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 更新时间
     */
    private Date modifiedTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    private Integer mapStatus;

    /**
     * 错误信息
     */
    private String errorInfo;

    /**
     * 房型列表
     */
    private List<CtriposMapRoomtypeDTO> roomtypeDTOList;

}