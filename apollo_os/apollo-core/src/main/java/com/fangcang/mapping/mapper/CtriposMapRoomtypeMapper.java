package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.CtriposMapRoomtypeDO;

public interface CtriposMapRoomtypeMapper extends MyMapper<CtriposMapRoomtypeDO> {
}