package com.fangcang.mapping.request.feizhu;

import com.fangcang.common.BaseDTO;
import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:45
 */
@Data
public class FeizhuBaseRequest extends BaseDTO {

    private String merchantCode;
    private String shopId;
}
