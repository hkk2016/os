package com.fangcang.mapping.dto.ctripos;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class CtriposMapRoomtypeDTO implements Serializable {
    private static final long serialVersionUID = 838212753503404099L;
    private Integer id;

    /**
     * 酒店编码
     */
    private Integer hotelId;

    /**
     * 酒店中文名
     */
    private String hotelName;

    /**
     * 酒店英文名
     */
    private String hotelNameEng;

    /**
     * 房型编码
     */
    private Integer roomTypeId;

    /**
     * 房型中文名
     */
    private String roomName;

    /**
     * 房型英文名
     */
    private String roomNameEng;

    /**
     * 携程房型编码
     */
    private String ctripRoomtypeId;

    /**
     * 携程房型名称
     */
    private String ctripRoomtypeName;

    /**
     * 携程房型英文名称
     */
    private String ctripRoomtypeNameEng;

    /**
     * 商家编码
     */
    private String merchantCode;

    /**
     * 房型中文描述
     */
    private String roomDesc;

    /**
     * 房型英文描述
     */
    private String roomDescEng;

    /**
     * 最大成人入住人数
     */
    private Integer maxperson;

    /**
     * 楼层
     */
    private String floor;

    /**
     * 床型
     */
    private String bedType;

    /**
     * 床宽
     */
    private String bedWidth;

    /**
     * 是/否可加床
     */
    private Integer extraBed;

    /**
     * 加床费用
     */
    private BigDecimal extraBedFee;

    /**
     * 房间面积
     */
    private BigDecimal roomArea;

    /**
     * 宽带
     */
    private Integer broadnet;

    /**
     * 有窗/无窗/部分有窗
     */
    private Integer hasWindow;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    private Integer mapStatus;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 错误信息
     */
    private String errorInfo;

    /**
     * 价格计划列表
     */
    private List<CtriposMapRateplanDTO> rateplanDTOList;

}