package com.fangcang.mapping.request.feizhu;

import com.fangcang.common.BaseQueryConditionDTO;
import lombok.Data;

import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:50
 */
@Data
public class QueryFeizhuHotelMappingRequest extends BaseQueryConditionDTO {

    private String merchantCode;

    private String cityCode;

    private Long hotelId;

    private String shopId;

    private Integer mappingStatus;

    private List<String> mappingStatusList;
}
