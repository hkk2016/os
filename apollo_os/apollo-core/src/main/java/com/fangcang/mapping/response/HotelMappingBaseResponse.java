package com.fangcang.mapping.response;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午6:01
 */
@Data
public class HotelMappingBaseResponse <T extends RoomTypeMappingBaseResponse> implements Serializable {

    private Integer hotelId;
    private String hotelName;
    private List<T> roomTypeList = new ArrayList<>();
}
