package com.fangcang.mapping.request.ctripos;

import java.io.Serializable;
import java.util.List;

public class SyncCtripRoomTypeRequest implements Serializable{
	
	private static final long serialVersionUID = 7960546187914587635L;
	
	/*
	 */
	private Long roomTypeId;
	/**
	 * 房型名称
	 */
	private String roomTypeName;
	
	private List<SyncCtripCommodityRequest> commodityVOs;

	public Long getRoomTypeId() {
		return roomTypeId;
	}

	public void setRoomTypeId(Long roomTypeId) {
		this.roomTypeId = roomTypeId;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public List<SyncCtripCommodityRequest> getCommodityVOs() {
		return commodityVOs;
	}

	public void setCommodityVOs(List<SyncCtripCommodityRequest> commodityVOs) {
		this.commodityVOs = commodityVOs;
	}
	
	
}
