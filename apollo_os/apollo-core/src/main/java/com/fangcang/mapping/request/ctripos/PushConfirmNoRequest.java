package com.fangcang.mapping.request.ctripos;

import lombok.Data;

/**
 * @author zhanwang
 */
@Data
public class PushConfirmNoRequest extends CtriposBaseRequest {
    private static final long serialVersionUID = -277022013728290303L;
    /**
     * 订单确认号（冗余字段）
     */
    private String confirmNo;

    /**
     * 订单编码
     */
    private String orderCode;

    /**
     * 携程订单编码
     */
    private String ctripOrderCode;
}
