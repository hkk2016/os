package com.fangcang.mapping.request.feizhu;

import com.fangcang.common.IncrementDTO;
import com.fangcang.common.IncrementType;
import lombok.Data;

import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:53
 */
@Data
public class SyncRateAndQuotaToFeizhuResquest extends FeizhuBaseRequest {

    private Long pricePlanId;

    private List<IncrementDTO> incrementDTOList ;
    /**
     * 本次增量类型
     */
    private IncrementType incrementType;
}
