package com.fangcang.mapping.request.ctripos;

import com.fangcang.common.BaseQueryConditionDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QueryRoomOrRateListRequest extends BaseQueryConditionDTO {

    private Integer hotelId;
    private String shopId;
    private String merchantCode;

    private List<Integer> hotelIdList = new ArrayList<>();
}
