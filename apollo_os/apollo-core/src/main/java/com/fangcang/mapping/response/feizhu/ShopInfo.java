package com.fangcang.mapping.response.feizhu;

import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:59
 */
@Data
public class ShopInfo {

    private String merchantCode;

    private String shopId;

    private String shopName;

    private String channelCode;
}
