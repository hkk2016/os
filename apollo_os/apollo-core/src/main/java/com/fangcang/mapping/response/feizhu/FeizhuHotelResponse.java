package com.fangcang.mapping.response.feizhu;

import com.fangcang.mapping.response.HotelMappingBaseResponse;
import lombok.Data;

import java.io.Serializable;

/**
 * TODO FeizhuHotelResponse 、 FeizhuRoomTypeResponse、FeizhuRatePlanResponse
 * TODO 这三个类的定义可以用泛型，
 * TODO 这样后续添加渠道只需要添加继承对应的DTO即可
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:56
 */
@Data
public class FeizhuHotelResponse <FeizhuRoomAndRpListResponse> extends HotelMappingBaseResponse implements Serializable {
    private String hotelName;
    private Integer hotelId;
}
