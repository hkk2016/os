package com.fangcang.mapping.request.feizhu;

import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:48
 */
@Data
public class GetFeizhuHotelRequest extends FeizhuBaseRequest {

    private Integer hotelId;
}
