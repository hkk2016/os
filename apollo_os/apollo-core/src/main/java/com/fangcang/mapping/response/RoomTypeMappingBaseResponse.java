package com.fangcang.mapping.response;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午6:01
 */
@Data
public class RoomTypeMappingBaseResponse <T extends PricePlanMappingBaseResponse> implements Serializable {

    private Integer roomTypeId;
    private String roomTypeName;
    private List<T> pricePlanList = new ArrayList<>();
}
