package com.fangcang.mapping.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_ctripos_map_hotel")
public class CtriposMapHotelDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 房仓酒店编码
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 房仓酒店中文名称
     */
    @Column(name = "hotel_name")
    private String hotelName;

    /**
     * 房仓酒店英文名称
     */
    @Column(name = "hotel_name_eng")
    private String hotelNameEng;

    /**
     * 国家编码
     */
    @Column(name = "country_code")
    private String countryCode;

    /**
     * 国家中文名
     */
    @Column(name = "country_name")
    private String countryName;

    /**
     * 国家英文名
     */
    @Column(name = "country_name_eng")
    private String countryNameEng;

    /**
     * 省份编码
     */
    @Column(name = "province_code")
    private String provinceCode;

    /**
     * 省份中文名
     */
    @Column(name = "province_name")
    private String provinceName;

    /**
     * 省份英文名
     */
    @Column(name = "province_name_eng")
    private String provinceNameEng;

    /**
     * 城市编码
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * 城市中文名
     */
    @Column(name = "city_name")
    private String cityName;

    /**
     * 城市英文名
     */
    @Column(name = "city_name_eng")
    private String cityNameEng;

    /**
     * 携程酒店编码
     */
    @Column(name = "ctrip_hotel_id")
    private String ctripHotelId;

    /**
     * 携程酒店中文
     */
    @Column(name = "ctrip_hotel_name")
    private String ctripHotelName;

    /**
     * 携程酒店英文名
     */
    @Column(name = "ctrip_hotel_name_eng")
    private String ctripHotelNameEng;

    /**
     * 携程城市编码
     */
    @Column(name = "ctrip_city_code")
    private String ctripCityCode;

    /**
     * 携程城市名称
     */
    @Column(name = "ctrip_city_name")
    private String ctripCityName;

    /**
     * 酒店星级
     */
    @Column(name = "hotel_star")
    private Integer hotelStar;

    /**
     * 维度
     */
    private String lat;

    /**
     * 经度
     */
    private String lon;

    /**
     * 酒店长简介中文
     */
    private String description;

    /**
     * 酒店长简介英文
     */
    @Column(name = "description_eng")
    private String descriptionEng;

    /**
     * 开业时间
     */
    @Column(name = "open_year")
    private Date openYear;

    /**
     * 装修时间
     */
    @Column(name = "fitment_year")
    private Date fitmentYear;

    /**
     * 传真号码
     */
    private String fax;

    /**
     * 房间数量
     */
    @Column(name = "room_quantity")
    private Integer roomQuantity;

    /**
     * 电话号码
     */
    private String telephone;

    /**
     * 酒店地址
     */
    private String address;

    /**
     * 酒店英文地址
     */
    @Column(name = "address_eng")
    private String addressEng;

    /**
     * 酒店品牌
     */
    @Column(name = "hotel_brand")
    private String hotelBrand;

    /**
     * 商家主题
     */
    @Column(name = "hotel_theme")
    private String hotelTheme;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 更新时间
     */
    @Column(name = "modified_time")
    private Date modifiedTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    @Column(name = "map_status")
    private Integer mapStatus;

    /**
     * 错误信息
     */
    @Column(name = "error_info")
    private String errorInfo;

    /**
     * 店铺id
     */
    @Column(name = "shop_id")
    private Integer shopId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取房仓酒店编码
     *
     * @return hotel_id - 房仓酒店编码
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置房仓酒店编码
     *
     * @param hotelId 房仓酒店编码
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取房仓酒店中文名称
     *
     * @return hotel_name - 房仓酒店中文名称
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * 设置房仓酒店中文名称
     *
     * @param hotelName 房仓酒店中文名称
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     * 获取房仓酒店英文名称
     *
     * @return hotel_name_eng - 房仓酒店英文名称
     */
    public String getHotelNameEng() {
        return hotelNameEng;
    }

    /**
     * 设置房仓酒店英文名称
     *
     * @param hotelNameEng 房仓酒店英文名称
     */
    public void setHotelNameEng(String hotelNameEng) {
        this.hotelNameEng = hotelNameEng;
    }

    /**
     * 获取国家编码
     *
     * @return country_code - 国家编码
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * 设置国家编码
     *
     * @param countryCode 国家编码
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * 获取国家中文名
     *
     * @return country_name - 国家中文名
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * 设置国家中文名
     *
     * @param countryName 国家中文名
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * 获取国家英文名
     *
     * @return country_name_eng - 国家英文名
     */
    public String getCountryNameEng() {
        return countryNameEng;
    }

    /**
     * 设置国家英文名
     *
     * @param countryNameEng 国家英文名
     */
    public void setCountryNameEng(String countryNameEng) {
        this.countryNameEng = countryNameEng;
    }

    /**
     * 获取省份编码
     *
     * @return province_code - 省份编码
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * 设置省份编码
     *
     * @param provinceCode 省份编码
     */
    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    /**
     * 获取省份中文名
     *
     * @return province_name - 省份中文名
     */
    public String getProvinceName() {
        return provinceName;
    }

    /**
     * 设置省份中文名
     *
     * @param provinceName 省份中文名
     */
    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    /**
     * 获取省份英文名
     *
     * @return province_name_eng - 省份英文名
     */
    public String getProvinceNameEng() {
        return provinceNameEng;
    }

    /**
     * 设置省份英文名
     *
     * @param provinceNameEng 省份英文名
     */
    public void setProvinceNameEng(String provinceNameEng) {
        this.provinceNameEng = provinceNameEng;
    }

    /**
     * 获取城市编码
     *
     * @return city_code - 城市编码
     */
    public String getCityCode() {
        return cityCode;
    }

    /**
     * 设置城市编码
     *
     * @param cityCode 城市编码
     */
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    /**
     * 获取城市中文名
     *
     * @return city_name - 城市中文名
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 设置城市中文名
     *
     * @param cityName 城市中文名
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 获取城市英文名
     *
     * @return city_name_eng - 城市英文名
     */
    public String getCityNameEng() {
        return cityNameEng;
    }

    /**
     * 设置城市英文名
     *
     * @param cityNameEng 城市英文名
     */
    public void setCityNameEng(String cityNameEng) {
        this.cityNameEng = cityNameEng;
    }

    /**
     * 获取携程酒店编码
     *
     * @return ctrip_hotel_id - 携程酒店编码
     */
    public String getCtripHotelId() {
        return ctripHotelId;
    }

    /**
     * 设置携程酒店编码
     *
     * @param ctripHotelId 携程酒店编码
     */
    public void setCtripHotelId(String ctripHotelId) {
        this.ctripHotelId = ctripHotelId;
    }

    /**
     * 获取携程酒店中文
     *
     * @return ctrip_hotel_name - 携程酒店中文
     */
    public String getCtripHotelName() {
        return ctripHotelName;
    }

    /**
     * 设置携程酒店中文
     *
     * @param ctripHotelName 携程酒店中文
     */
    public void setCtripHotelName(String ctripHotelName) {
        this.ctripHotelName = ctripHotelName;
    }

    /**
     * 获取携程酒店英文名
     *
     * @return ctrip_hotel_name_eng - 携程酒店英文名
     */
    public String getCtripHotelNameEng() {
        return ctripHotelNameEng;
    }

    /**
     * 设置携程酒店英文名
     *
     * @param ctripHotelNameEng 携程酒店英文名
     */
    public void setCtripHotelNameEng(String ctripHotelNameEng) {
        this.ctripHotelNameEng = ctripHotelNameEng;
    }

    /**
     * 获取携程城市编码
     *
     * @return ctrip_city_code - 携程城市编码
     */
    public String getCtripCityCode() {
        return ctripCityCode;
    }

    /**
     * 设置携程城市编码
     *
     * @param ctripCityCode 携程城市编码
     */
    public void setCtripCityCode(String ctripCityCode) {
        this.ctripCityCode = ctripCityCode;
    }

    /**
     * 获取携程城市名称
     *
     * @return ctrip_city_name - 携程城市名称
     */
    public String getCtripCityName() {
        return ctripCityName;
    }

    /**
     * 设置携程城市名称
     *
     * @param ctripCityName 携程城市名称
     */
    public void setCtripCityName(String ctripCityName) {
        this.ctripCityName = ctripCityName;
    }

    /**
     * 获取酒店星级
     *
     * @return hotel_star - 酒店星级
     */
    public Integer getHotelStar() {
        return hotelStar;
    }

    /**
     * 设置酒店星级
     *
     * @param hotelStar 酒店星级
     */
    public void setHotelStar(Integer hotelStar) {
        this.hotelStar = hotelStar;
    }

    /**
     * 获取维度
     *
     * @return lat - 维度
     */
    public String getLat() {
        return lat;
    }

    /**
     * 设置维度
     *
     * @param lat 维度
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 获取经度
     *
     * @return lon - 经度
     */
    public String getLon() {
        return lon;
    }

    /**
     * 设置经度
     *
     * @param lon 经度
     */
    public void setLon(String lon) {
        this.lon = lon;
    }

    /**
     * 获取酒店长简介中文
     *
     * @return description - 酒店长简介中文
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置酒店长简介中文
     *
     * @param description 酒店长简介中文
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取酒店长简介英文
     *
     * @return description_eng - 酒店长简介英文
     */
    public String getDescriptionEng() {
        return descriptionEng;
    }

    /**
     * 设置酒店长简介英文
     *
     * @param descriptionEng 酒店长简介英文
     */
    public void setDescriptionEng(String descriptionEng) {
        this.descriptionEng = descriptionEng;
    }

    /**
     * 获取开业时间
     *
     * @return open_year - 开业时间
     */
    public Date getOpenYear() {
        return openYear;
    }

    /**
     * 设置开业时间
     *
     * @param openYear 开业时间
     */
    public void setOpenYear(Date openYear) {
        this.openYear = openYear;
    }

    /**
     * 获取装修时间
     *
     * @return fitment_year - 装修时间
     */
    public Date getFitmentYear() {
        return fitmentYear;
    }

    /**
     * 设置装修时间
     *
     * @param fitmentYear 装修时间
     */
    public void setFitmentYear(Date fitmentYear) {
        this.fitmentYear = fitmentYear;
    }

    /**
     * 获取传真号码
     *
     * @return fax - 传真号码
     */
    public String getFax() {
        return fax;
    }

    /**
     * 设置传真号码
     *
     * @param fax 传真号码
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * 获取房间数量
     *
     * @return room_quantity - 房间数量
     */
    public Integer getRoomQuantity() {
        return roomQuantity;
    }

    /**
     * 设置房间数量
     *
     * @param roomQuantity 房间数量
     */
    public void setRoomQuantity(Integer roomQuantity) {
        this.roomQuantity = roomQuantity;
    }

    /**
     * 获取电话号码
     *
     * @return telephone - 电话号码
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * 设置电话号码
     *
     * @param telephone 电话号码
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 获取酒店地址
     *
     * @return address - 酒店地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置酒店地址
     *
     * @param address 酒店地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取酒店英文地址
     *
     * @return address_eng - 酒店英文地址
     */
    public String getAddressEng() {
        return addressEng;
    }

    /**
     * 设置酒店英文地址
     *
     * @param addressEng 酒店英文地址
     */
    public void setAddressEng(String addressEng) {
        this.addressEng = addressEng;
    }

    /**
     * 获取酒店品牌
     *
     * @return hotel_brand - 酒店品牌
     */
    public String getHotelBrand() {
        return hotelBrand;
    }

    /**
     * 设置酒店品牌
     *
     * @param hotelBrand 酒店品牌
     */
    public void setHotelBrand(String hotelBrand) {
        this.hotelBrand = hotelBrand;
    }

    /**
     * 获取商家主题
     *
     * @return hotel_theme - 商家主题
     */
    public String getHotelTheme() {
        return hotelTheme;
    }

    /**
     * 设置商家主题
     *
     * @param hotelTheme 商家主题
     */
    public void setHotelTheme(String hotelTheme) {
        this.hotelTheme = hotelTheme;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取创建时间
     *
     * @return created_time - 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 设置创建时间
     *
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 获取创建人
     *
     * @return creater - 创建人
     */
    public String getCreater() {
        return creater;
    }

    /**
     * 设置创建人
     *
     * @param creater 创建人
     */
    public void setCreater(String creater) {
        this.creater = creater;
    }

    /**
     * 获取更新时间
     *
     * @return modified_time - 更新时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 设置更新时间
     *
     * @param modifiedTime 更新时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 获取更新人
     *
     * @return modifier - 更新人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置更新人
     *
     * @param modifier 更新人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @return map_status - 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public Integer getMapStatus() {
        return mapStatus;
    }

    /**
     * 设置映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @param mapStatus 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public void setMapStatus(Integer mapStatus) {
        this.mapStatus = mapStatus;
    }

    /**
     * 获取错误信息
     *
     * @return error_info - 错误信息
     */
    public String getErrorInfo() {
        return errorInfo;
    }

    /**
     * 设置错误信息
     *
     * @param errorInfo 错误信息
     */
    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    /**
     * 获取店铺id
     *
     * @return shop_id - 店铺id
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 设置店铺id
     *
     * @param shopId 店铺id
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}