package com.fangcang.mapping.request.feizhu;

import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:44
 */
@Data
public class AddHotelToFeizhuRequest extends FeizhuBaseRequest {

    /**
     * 酒店映射表的主键
     */
    private Long id;
    private Integer hotelId;
    private String name;
    private String cityCode;
    private String cityName;
    private String address;
    //0086#010#12345678
    private String tel;
}
