package com.fangcang.mapping.response.feizhu;

import com.fangcang.mapping.response.PricePlanMappingBaseResponse;
import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:57
 */
@Data
public class FeizhuRatePlanResponse extends PricePlanMappingBaseResponse {

    private String rpid;
}
