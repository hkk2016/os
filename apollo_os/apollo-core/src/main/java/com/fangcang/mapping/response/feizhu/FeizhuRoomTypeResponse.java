package com.fangcang.mapping.response.feizhu;

import com.fangcang.mapping.response.RoomTypeMappingBaseResponse;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:58
 */
@Data
public class FeizhuRoomTypeResponse <FeizhuRatePLanResponse> extends RoomTypeMappingBaseResponse implements Serializable {

    //飞猪房型ID
    private String rid;
    //飞猪房型名称
    private String name;
}
