package com.fangcang.mapping.request.ctripos;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BatchSyncCtripRequestCtripos extends CtriposBaseRequest implements Serializable {

    private static final long serialVersionUID = -3988688040266516338L;

    private List<SyncCtripHotelRequest> hotelVOs;
}
