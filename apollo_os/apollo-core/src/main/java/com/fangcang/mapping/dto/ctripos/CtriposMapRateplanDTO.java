package com.fangcang.mapping.dto.ctripos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CtriposMapRateplanDTO implements Serializable {
    private Integer id;

    /**
     * 酒店编码
     */
    private Integer hotelId;

    /**
     * 酒店中文名
     */
    private String hotelName;

    /**
     * 酒店英文名
     */
    private String hotelNameEng;

    /**
     * 房型编码
     */
    private Integer roomTypeId;

    /**
     * 房型中文名
     */
    private String roomTypeName;

    /**
     * 房型英文名
     */
    private String roomTypeNameEng;

    /**
     * 商品编码
     */
    private Integer commodityId;

    /**
     * 商品中文名
     */
    private String commodityName;

    /**
     * 商品英文名
     */
    private String commodityNameEng;

    /**
     * 携程子房型id
     */
    private String ctripRateplanId;

    /**
     * 携程子房型中文名
     */
    private String ctripRateplanName;

    /**
     * 携程子房型英文名
     */
    private String ctripRateplanNameEng;

    /**
     * 商家编码
     */
    private String merchantCode;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    private Integer mapStatus;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 更新时间
     */
    private Date modifiedTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 错误信息
     */
    private String errorInfo;

    /**
     * 是否上架：1已上架，0未上架
     */
    private Integer isOnsale;

}