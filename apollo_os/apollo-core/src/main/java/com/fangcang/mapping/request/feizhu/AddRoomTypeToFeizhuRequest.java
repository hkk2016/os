package com.fangcang.mapping.request.feizhu;

import lombok.Data;

import java.util.List;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:47
 */
@Data
public class AddRoomTypeToFeizhuRequest extends FeizhuBaseRequest {

    /**
     * 系统的酒店ID
     */
    private Long hotelId;

    /**
     * 系统的房型ID
     */
    private Long roomTypeId;

    private String name;

    /**
     * 这里是系统存储的床型，最终要转换成http://open.taobao.com/docs/doc.htm?&docType=1&articleId=105610
     */
    private List<Integer> bedTypeList;
}
