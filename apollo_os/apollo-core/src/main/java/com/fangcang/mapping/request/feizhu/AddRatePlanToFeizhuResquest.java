package com.fangcang.mapping.request.feizhu;

import com.fangcang.product.dto.RestrictDTO;
import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:46
 */
@Data
public class AddRatePlanToFeizhuResquest extends FeizhuBaseRequest {

    /**
     * 酒店id
     */
    private Long hotelId;

    /**
     * 房型id
     */
    private Long roomTypeId;

    /**
     * 价格计划id
     */
    private Long pricePlanId;

    /**
     * 飞猪酒店id
     */
    private Long hid;

    /**
     * 飞猪房型id
     */
    private Long rid;

    /**
     * 飞猪价格计划id
     */
    private Long rpid;

    /**
     * 1.表示任意退{"cancelPolicyType":1};
     * 2.表示不能退{"cancelPolicyType":2};
     * 4.从入住当天24点往前推X小时前取消收取Y%手续费，否则不可取消{"cancelPolicyType":4,"policyInfo":{"48":10,"24":20}}表示，从入住日24点往前推提前至少48小时取消，收取10%的手续费，从入住日24点往前推提前至少24小时取消，收取20%的手续费;
     * 5.从24点往前推多少小时可退{"cancelPolicyType":5,"policyInfo":{"timeBefore":6}}表示从入住日24点往前推至少6个小时即入住日18点前可免费取消;6.从入住日24点往前推，至少提前小时数扣取首晚房费{"cancelPolicyType":
     * 6,"policyInfo":{"14":1}}表示入住日24点往前推14小时，即入住日10点前取消收取首晚房费
     */
    private RestrictDTO restrictDTO;

    /**
     * 价格计划名称
     */
    private String pricePlanName;

    /**
     * 飞猪早餐
     * -1：状态早餐,有具体几人价有关系，几人价是几份早餐;
     * 0：不含早
     * 1：含单早
     * 2：含双早
     * N：含N早（-1-99可选）
     */
    /**
     * 早餐类型
     * com.fangcang.common.enums.BreakFastTypeEnum
     *
     */
    private Integer breakFastType;

    /**
     * 支付方式
     * com.fangcang.common.enums.PayMethodEnum
     *
     * 飞猪的支付类型，只支持：1：预付5：现付6: 信用住7:预付在线预约8:信用住在线预约。其中5,6,7,8四种类型需要申请权限
     */
    private Integer payMethod;
}
