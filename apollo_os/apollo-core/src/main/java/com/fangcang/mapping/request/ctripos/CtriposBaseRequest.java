package com.fangcang.mapping.request.ctripos;

import com.fangcang.common.BaseDTO;
import lombok.Data;


@Data
public class CtriposBaseRequest extends BaseDTO {

    private String merchantCode;
    private String shopId;
    private String channelCode;
}
