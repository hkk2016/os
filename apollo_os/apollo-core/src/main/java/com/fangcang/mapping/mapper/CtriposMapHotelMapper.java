package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.CtriposMapHotelDO;

public interface CtriposMapHotelMapper extends MyMapper<CtriposMapHotelDO> {
}