package com.fangcang.mapping.service;

import com.fangcang.common.PaginationSupportDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.mapping.dto.ctripos.CtriposMapHotelDTO;
import com.fangcang.mapping.dto.ctripos.CtriposShopInfoDTO;
import com.fangcang.mapping.request.ctripos.BatchSyncCtripRequestCtripos;
import com.fangcang.mapping.request.ctripos.PushConfirmNoRequest;
import com.fangcang.mapping.request.ctripos.PushPriceAndQuotaToCtripRequest;
import com.fangcang.mapping.request.ctripos.QueryHotelListRequest;
import com.fangcang.mapping.request.ctripos.QueryRoomOrRateListRequest;

import java.util.List;

/**
 * @author zhanwang
 */
public interface CtriposMappingService {

    /**
     * 查询店铺列表
     *
     * @param merchantCode
     * @return
     */
    ResponseDTO<List<CtriposShopInfoDTO>> queryShopList(String merchantCode);

    /**
     * 加载shop到内存，初始化
     * InitData.merchantShopMap
     * InitData.merchantShopChanneCodeMap
     */
    void initCtriposShopInfo();

    /**
     * 分页查询携程的酒店映射列表
     *
     * @param request
     * @return
     */
    ResponseDTO<PaginationSupportDTO<CtriposMapHotelDTO>> queryHotelList(QueryHotelListRequest request);

    /**
     * 房型和价格计划列表页面
     *
     * @param request
     * @return
     */
    ResponseDTO<CtriposMapHotelDTO> queryRoomOrRateList(QueryRoomOrRateListRequest request);

    /**
     * 批量推送酒店基本信息到携程
     *
     * @param request
     * @return
     */
    public ResponseDTO batchPushHotelInfoToCtrip(BatchSyncCtripRequestCtripos request);

    /**
     * 批量更新关联状态
     *
     * @param request
     * @return
     */
    public ResponseDTO batchSyncHotelInfoFromCtrip(BatchSyncCtripRequestCtripos request);

    /**
     * 推送价格和房态到携程
     *
     * @param request
     * @return
     */
    ResponseDTO pushPriceAndQuotaToCtrip(PushPriceAndQuotaToCtripRequest request);


    /**
     * 推送确认号到携程
     * @param request
     * @return
     */
    ResponseDTO pushConfirmNoToOta(PushConfirmNoRequest request);
}
