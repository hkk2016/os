package com.fangcang.mapping.request.ctripos;

import com.fangcang.common.BaseQueryConditionDTO;
import lombok.Data;

import java.util.List;

@Data
public class QueryHotelListRequest extends BaseQueryConditionDTO {

    private String merchantCode;

    private Long hotelId;

    private String shopId;

    private String cityCode;

    private Integer mapStatus;

    private List<String> mapStatusList;
}
