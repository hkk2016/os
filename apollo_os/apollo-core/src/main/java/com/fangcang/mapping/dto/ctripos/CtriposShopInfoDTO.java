package com.fangcang.mapping.dto.ctripos;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

@Data
public class CtriposShopInfoDTO implements Serializable {
    private static final long serialVersionUID = -486010470932647729L;
    private Integer id;

    /**
     * 账户id
     */
    private String apiId;

    /**
     * 账户密码
     */
    private String messagePassword;

    /**
     * 账户类型
     */
    private String type;

    /**
     * 组织类别
     */
    private String code;

    /**
     * 合作伙伴标示
     */
    private String codeContext;

    /**
     * 酒店基础信息映射url
     */
    private String staticinfoUrl;

    /**
     * 价格房态订单等推送url
     */
    private String dynamicinfoUrl;

    /**
     * 确认号url
     */
    private String confirmNumUrl;

    /**
     * 商家编码
     */
    private String merchantCode;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 渠道编码
     */
    private String channelCode;

}