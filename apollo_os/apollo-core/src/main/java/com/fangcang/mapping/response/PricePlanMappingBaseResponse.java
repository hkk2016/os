package com.fangcang.mapping.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午6:02
 */
@Data
public class PricePlanMappingBaseResponse implements Serializable {

    private Integer pricePlanId;
    private String pricePlanName;
}
