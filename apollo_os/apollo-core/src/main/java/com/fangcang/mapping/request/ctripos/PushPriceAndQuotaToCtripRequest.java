package com.fangcang.mapping.request.ctripos;

import com.fangcang.common.IncrementDTO;
import com.fangcang.common.IncrementType;
import lombok.Data;

import java.util.List;

@Data
public class PushPriceAndQuotaToCtripRequest extends CtriposBaseRequest {

    private Long pricePlanId;
    private Long hotelId;
    private Long roomTypeId;




    private List<IncrementDTO> incrementDTOList ;
    /**
     * 本次增量类型
     */
    private IncrementType incrementType;
}
