package com.fangcang.mapping.request.ctripos;

import java.io.Serializable;

public class SyncCtripCommodityRequest implements Serializable{

	private static final long serialVersionUID = -2633345236854877111L;
	
	/**
	 * 商品ID
	 */
	private Long commodityId;
	
	/**
	 * 商品名称
	 */
	private String commodityName;

	public Long getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(Long commodityId) {
		this.commodityId = commodityId;
	}

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}
	
	

}
