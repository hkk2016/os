package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.CtriposShopInfoDO;

public interface CtriposShopInfoMapper extends MyMapper<CtriposShopInfoDO> {
}