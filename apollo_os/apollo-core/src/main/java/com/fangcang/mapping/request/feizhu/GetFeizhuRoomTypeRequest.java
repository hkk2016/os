package com.fangcang.mapping.request.feizhu;

import lombok.Data;

/**
 * @author chihping
 * @Description TODO
 * @date 19-2-19 下午5:49
 */
@Data
public class GetFeizhuRoomTypeRequest extends FeizhuBaseRequest {

    private Integer roomTypeId;
}
