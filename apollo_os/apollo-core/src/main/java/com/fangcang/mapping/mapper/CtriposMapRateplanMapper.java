package com.fangcang.mapping.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.mapping.domain.CtriposMapRateplanDO;

public interface CtriposMapRateplanMapper extends MyMapper<CtriposMapRateplanDO> {
}