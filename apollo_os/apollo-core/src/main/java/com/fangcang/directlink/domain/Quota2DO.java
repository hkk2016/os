package com.fangcang.directlink.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlpro_quota")
public class Quota2DO {
    /**
     * 配额主键ID
     */
    @Id
    @Column(name = "quota_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer quotaId;

    /**
     * 配额账户ID
     */
    @Column(name = "quotaaccount_id")
    private Integer quotaaccountId;

    /**
     * 售卖日期
     */
    @Column(name = "sale_date")
    private Date saleDate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    private String modifier;

    /**
     * 配额数量
     */
    @Column(name = "quota_num")
    private Integer quotaNum;

    /**
     * 已用配额数量
     */
    @Column(name = "used_quota_num")
    private Integer usedQuotaNum;

    /**
     * 配额总数
     */
    @Column(name = "all_quota_num")
    private Integer allQuotaNum;

    /**
     * 是否可超(1可超  0不可超)
     */
    @Column(name = "over_draft")
    private Integer overDraft;

    /**
     * 房态(0关房  1开房)
     */
    @Column(name = "quota_state")
    private Integer quotaState;

    /**
     * 获取配额主键ID
     *
     * @return quota_id - 配额主键ID
     */
    public Integer getQuotaId() {
        return quotaId;
    }

    /**
     * 设置配额主键ID
     *
     * @param quotaId 配额主键ID
     */
    public void setQuotaId(Integer quotaId) {
        this.quotaId = quotaId;
    }

    /**
     * 获取配额账户ID
     *
     * @return quotaaccount_id - 配额账户ID
     */
    public Integer getQuotaaccountId() {
        return quotaaccountId;
    }

    /**
     * 设置配额账户ID
     *
     * @param quotaaccountId 配额账户ID
     */
    public void setQuotaaccountId(Integer quotaaccountId) {
        this.quotaaccountId = quotaaccountId;
    }

    /**
     * 获取售卖日期
     *
     * @return sale_date - 售卖日期
     */
    public Date getSaleDate() {
        return saleDate;
    }

    /**
     * 设置售卖日期
     *
     * @param saleDate 售卖日期
     */
    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * @return modifier
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * @param modifier
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取配额数量
     *
     * @return quota_num - 配额数量
     */
    public Integer getQuotaNum() {
        return quotaNum;
    }

    /**
     * 设置配额数量
     *
     * @param quotaNum 配额数量
     */
    public void setQuotaNum(Integer quotaNum) {
        this.quotaNum = quotaNum;
    }

    /**
     * 获取已用配额数量
     *
     * @return used_quota_num - 已用配额数量
     */
    public Integer getUsedQuotaNum() {
        return usedQuotaNum;
    }

    /**
     * 设置已用配额数量
     *
     * @param usedQuotaNum 已用配额数量
     */
    public void setUsedQuotaNum(Integer usedQuotaNum) {
        this.usedQuotaNum = usedQuotaNum;
    }

    /**
     * 获取配额总数
     *
     * @return all_quota_num - 配额总数
     */
    public Integer getAllQuotaNum() {
        return allQuotaNum;
    }

    /**
     * 设置配额总数
     *
     * @param allQuotaNum 配额总数
     */
    public void setAllQuotaNum(Integer allQuotaNum) {
        this.allQuotaNum = allQuotaNum;
    }

    /**
     * 获取是否可超(1可超  0不可超)
     *
     * @return over_draft - 是否可超(1可超  0不可超)
     */
    public Integer getOverDraft() {
        return overDraft;
    }

    /**
     * 设置是否可超(1可超  0不可超)
     *
     * @param overDraft 是否可超(1可超  0不可超)
     */
    public void setOverDraft(Integer overDraft) {
        this.overDraft = overDraft;
    }

    /**
     * 获取房态(0关房  1开房)
     *
     * @return quota_state - 房态(0关房  1开房)
     */
    public Integer getQuotaState() {
        return quotaState;
    }

    /**
     * 设置房态(0关房  1开房)
     *
     * @param quotaState 房态(0关房  1开房)
     */
    public void setQuotaState(Integer quotaState) {
        this.quotaState = quotaState;
    }
}