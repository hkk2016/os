package com.fangcang.directlink.service;

import com.fangcang.product.domain.IncreaseDO;
import com.fangcang.product.dto.IncreaseDTO;

import java.util.Date;
import java.util.List;

public interface SyncRedisService {


    Integer getPriceId(Integer pricePlanId, Date saleDate);

    /**
     * 获取pricePlanId在渠道channelCode所有的加幅，然后根据日期找到对应的加幅
     * @param pricePlanId
     * @param saleDate
     * @param channelCode
     * @return
     */
    IncreaseDTO getIncreaseByChannel(Integer pricePlanId, Date saleDate, String channelCode);

    Integer getQuotaAccountId(Integer pricePlanId);

    Integer getQuotaId(Integer pricePlanId, Date saleDate);

    Integer getRestrictId(Integer pricePlanId);

    void saveIncrease(List<IncreaseDO> increaseList);
}
