package com.fangcang.directlink.dto;

import com.fangcang.product.dto.RestrictDTO;
import lombok.Data;

@Data
public class SyncRestrictDTO extends RestrictDTO {

    private Integer pricePlanId;
}
