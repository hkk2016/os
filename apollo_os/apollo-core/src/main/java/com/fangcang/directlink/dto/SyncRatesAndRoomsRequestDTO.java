package com.fangcang.directlink.dto;

import lombok.Data;

import java.util.List;

@Data
public class SyncRatesAndRoomsRequestDTO {

    private Integer pricePlanId;


    private String supplyCode;

    /**
     * 价格
     */
    private List<SyncPriceInfoDTO> priceInfoList;

    /**
     * 房态
     */
    private List<SyncQuotaStateDTO> roomStatusList;

    /**
     * 条款
     */
    private List<SyncRestrictDTO> restrictDTOList;


}
