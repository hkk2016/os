package com.fangcang.directlink.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SyncPriceInfoDTO implements Serializable {

    /**
     * 价格计划ID
     */
    private Long pricePlanId;

    /**
     * 售卖日期
     */
    private Date saleDate;

    /**
     * 散房底价:单位分
     */
    private Integer basePrice;
}
