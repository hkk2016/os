package com.fangcang.directlink.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.directlink.domain.Quota2DO;

public interface Quota2Mapper extends MyMapper<Quota2DO> {
}