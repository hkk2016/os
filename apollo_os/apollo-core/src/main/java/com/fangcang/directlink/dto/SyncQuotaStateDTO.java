package com.fangcang.directlink.dto;

import com.fangcang.product.dto.QuotaStateDTO;
import lombok.Data;

@Data
public class SyncQuotaStateDTO extends QuotaStateDTO {
    private Integer pricePlanId;

}
