package com.fangcang.directlink.service;

import com.fangcang.common.ResponseDTO;
import com.fangcang.directlink.dto.SyncRatesAndRoomsRequestDTO;
import com.fangcang.product.domain.PricePlanDO;
import com.fangcang.product.dto.PricePlanDTO;
import com.fangcang.product.request.PricePlanRequestDTO;

public interface SyncRatePlanService {

    /**
     * 同步价格房态条款（新增或者更新）
     * @param syncRatesAndRoomsRequestDTO
     * @return
     */
    ResponseDTO saveRatesAndRooms(SyncRatesAndRoomsRequestDTO syncRatesAndRoomsRequestDTO);

    /**
     * 创建价格计划
     * @return
     */
    ResponseDTO createRatePlan(PricePlanRequestDTO pricePlanRequestDTO);

    /**
     * 删除价格计划
     * @param pricePlanDO
     * @return
     */
    ResponseDTO deleteRatePlan(PricePlanDO pricePlanDO);

}
