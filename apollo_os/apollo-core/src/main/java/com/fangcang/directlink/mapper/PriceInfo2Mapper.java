package com.fangcang.directlink.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.directlink.domain.PriceInfo2DO;

public interface PriceInfo2Mapper extends MyMapper<PriceInfo2DO> {
}