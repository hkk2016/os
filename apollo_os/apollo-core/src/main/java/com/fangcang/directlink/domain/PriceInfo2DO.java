package com.fangcang.directlink.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_htlpro_priceinfo")
public class PriceInfo2DO {
    /**
     * 价格主键ID
     */
    @Id
    @Column(name = "priceinfo_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer priceinfoId;

    /**
     * 价格计划ID
     */
    @Column(name = "priceplan_id")
    private Integer priceplanId;

    /**
     * 售卖日期
     */
    @Column(name = "sale_date")
    private Date saleDate;

    /**
     * 底价
     */
    @Column(name = "base_price")
    private BigDecimal basePrice;

    /**
     * 团房底价

     */
    @Column(name = "group_baseprice")
    private BigDecimal groupBaseprice;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    private String modifier;

    /**
     * B2B售价
     */
    @Column(name = "b2b_saleprice")
    private BigDecimal b2bSaleprice;

    /**
     * 团房售价
     */
    @Column(name = "group_saleprice")
    private BigDecimal groupSaleprice;

    /**
     * 携程售价
     */
    @Column(name = "ctrip_saleprice")
    private BigDecimal ctripSaleprice;

    /**
     * 去哪儿TTS售价
     */
    @Column(name = "qunar_saleprice")
    private BigDecimal qunarSaleprice;

    /**
     * 艺龙售价
     */
    @Column(name = "elong_saleprice")
    private BigDecimal elongSaleprice;

    /**
     * 同程售价
     */
    @Column(name = "tongcheng_saleprice")
    private BigDecimal tongchengSaleprice;

    /**
     * 途牛售价
     */
    @Column(name = "tuniu_saleprice")
    private BigDecimal tuniuSaleprice;

    /**
     * 新美大售卖价格
     */
    @Column(name = "xmd_saleprice")
    private BigDecimal xmdSaleprice;

    /**
     * 京东售卖价格
     */
    @Column(name = "jd_saleprice")
    private BigDecimal jdSaleprice;

    /**
     * 淘宝售价
     */
    @Column(name = "taobao_saleprice")
    private BigDecimal taobaoSaleprice;

    /**
     * 去哪儿大B售价
     */
    @Column(name = "qunar_b2b_saleprice")
    private BigDecimal qunarB2bSaleprice;

    /**
     * 去哪儿夜宵售价
     */
    @Column(name = "qunar_ngt_saleprice")
    private BigDecimal qunarNgtSaleprice;

    /**
     * 去哪儿美元售价
     */
    @Column(name = "qunar_usd_saleprice")
    private BigDecimal qunarUsdSaleprice;

    /**
     * 去哪儿子渠道售价
     */
    @Column(name = "qunar_son_saleprice")
    private BigDecimal qunarSonSaleprice;

    /**
     * 携程海外渠道售价
     */
    @Column(name = "ctripos_saleprice")
    private BigDecimal ctriposSaleprice;

    /**
     * 获取价格主键ID
     *
     * @return priceinfo_id - 价格主键ID
     */
    public Integer getPriceinfoId() {
        return priceinfoId;
    }

    /**
     * 设置价格主键ID
     *
     * @param priceinfoId 价格主键ID
     */
    public void setPriceinfoId(Integer priceinfoId) {
        this.priceinfoId = priceinfoId;
    }

    /**
     * 获取价格计划ID
     *
     * @return priceplan_id - 价格计划ID
     */
    public Integer getPriceplanId() {
        return priceplanId;
    }

    /**
     * 设置价格计划ID
     *
     * @param priceplanId 价格计划ID
     */
    public void setPriceplanId(Integer priceplanId) {
        this.priceplanId = priceplanId;
    }

    /**
     * 获取售卖日期
     *
     * @return sale_date - 售卖日期
     */
    public Date getSaleDate() {
        return saleDate;
    }

    /**
     * 设置售卖日期
     *
     * @param saleDate 售卖日期
     */
    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    /**
     * 获取底价
     *
     * @return base_price - 底价
     */
    public BigDecimal getBasePrice() {
        return basePrice;
    }

    /**
     * 设置底价
     *
     * @param basePrice 底价
     */
    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * 获取团房底价

     *
     * @return group_baseprice - 团房底价

     */
    public BigDecimal getGroupBaseprice() {
        return groupBaseprice;
    }

    /**
     * 设置团房底价

     *
     * @param groupBaseprice 团房底价

     */
    public void setGroupBaseprice(BigDecimal groupBaseprice) {
        this.groupBaseprice = groupBaseprice;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return creator - 创建者
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建者
     *
     * @param creator 创建者
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * @return modifier
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * @param modifier
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取B2B售价
     *
     * @return b2b_saleprice - B2B售价
     */
    public BigDecimal getB2bSaleprice() {
        return b2bSaleprice;
    }

    /**
     * 设置B2B售价
     *
     * @param b2bSaleprice B2B售价
     */
    public void setB2bSaleprice(BigDecimal b2bSaleprice) {
        this.b2bSaleprice = b2bSaleprice;
    }

    /**
     * 获取团房售价
     *
     * @return group_saleprice - 团房售价
     */
    public BigDecimal getGroupSaleprice() {
        return groupSaleprice;
    }

    /**
     * 设置团房售价
     *
     * @param groupSaleprice 团房售价
     */
    public void setGroupSaleprice(BigDecimal groupSaleprice) {
        this.groupSaleprice = groupSaleprice;
    }

    /**
     * 获取携程售价
     *
     * @return ctrip_saleprice - 携程售价
     */
    public BigDecimal getCtripSaleprice() {
        return ctripSaleprice;
    }

    /**
     * 设置携程售价
     *
     * @param ctripSaleprice 携程售价
     */
    public void setCtripSaleprice(BigDecimal ctripSaleprice) {
        this.ctripSaleprice = ctripSaleprice;
    }

    /**
     * 获取去哪儿TTS售价
     *
     * @return qunar_saleprice - 去哪儿TTS售价
     */
    public BigDecimal getQunarSaleprice() {
        return qunarSaleprice;
    }

    /**
     * 设置去哪儿TTS售价
     *
     * @param qunarSaleprice 去哪儿TTS售价
     */
    public void setQunarSaleprice(BigDecimal qunarSaleprice) {
        this.qunarSaleprice = qunarSaleprice;
    }

    /**
     * 获取艺龙售价
     *
     * @return elong_saleprice - 艺龙售价
     */
    public BigDecimal getElongSaleprice() {
        return elongSaleprice;
    }

    /**
     * 设置艺龙售价
     *
     * @param elongSaleprice 艺龙售价
     */
    public void setElongSaleprice(BigDecimal elongSaleprice) {
        this.elongSaleprice = elongSaleprice;
    }

    /**
     * 获取同程售价
     *
     * @return tongcheng_saleprice - 同程售价
     */
    public BigDecimal getTongchengSaleprice() {
        return tongchengSaleprice;
    }

    /**
     * 设置同程售价
     *
     * @param tongchengSaleprice 同程售价
     */
    public void setTongchengSaleprice(BigDecimal tongchengSaleprice) {
        this.tongchengSaleprice = tongchengSaleprice;
    }

    /**
     * 获取途牛售价
     *
     * @return tuniu_saleprice - 途牛售价
     */
    public BigDecimal getTuniuSaleprice() {
        return tuniuSaleprice;
    }

    /**
     * 设置途牛售价
     *
     * @param tuniuSaleprice 途牛售价
     */
    public void setTuniuSaleprice(BigDecimal tuniuSaleprice) {
        this.tuniuSaleprice = tuniuSaleprice;
    }

    /**
     * 获取新美大售卖价格
     *
     * @return xmd_saleprice - 新美大售卖价格
     */
    public BigDecimal getXmdSaleprice() {
        return xmdSaleprice;
    }

    /**
     * 设置新美大售卖价格
     *
     * @param xmdSaleprice 新美大售卖价格
     */
    public void setXmdSaleprice(BigDecimal xmdSaleprice) {
        this.xmdSaleprice = xmdSaleprice;
    }

    /**
     * 获取京东售卖价格
     *
     * @return jd_saleprice - 京东售卖价格
     */
    public BigDecimal getJdSaleprice() {
        return jdSaleprice;
    }

    /**
     * 设置京东售卖价格
     *
     * @param jdSaleprice 京东售卖价格
     */
    public void setJdSaleprice(BigDecimal jdSaleprice) {
        this.jdSaleprice = jdSaleprice;
    }

    /**
     * 获取淘宝售价
     *
     * @return taobao_saleprice - 淘宝售价
     */
    public BigDecimal getTaobaoSaleprice() {
        return taobaoSaleprice;
    }

    /**
     * 设置淘宝售价
     *
     * @param taobaoSaleprice 淘宝售价
     */
    public void setTaobaoSaleprice(BigDecimal taobaoSaleprice) {
        this.taobaoSaleprice = taobaoSaleprice;
    }

    /**
     * 获取去哪儿大B售价
     *
     * @return qunar_b2b_saleprice - 去哪儿大B售价
     */
    public BigDecimal getQunarB2bSaleprice() {
        return qunarB2bSaleprice;
    }

    /**
     * 设置去哪儿大B售价
     *
     * @param qunarB2bSaleprice 去哪儿大B售价
     */
    public void setQunarB2bSaleprice(BigDecimal qunarB2bSaleprice) {
        this.qunarB2bSaleprice = qunarB2bSaleprice;
    }

    /**
     * 获取去哪儿夜宵售价
     *
     * @return qunar_ngt_saleprice - 去哪儿夜宵售价
     */
    public BigDecimal getQunarNgtSaleprice() {
        return qunarNgtSaleprice;
    }

    /**
     * 设置去哪儿夜宵售价
     *
     * @param qunarNgtSaleprice 去哪儿夜宵售价
     */
    public void setQunarNgtSaleprice(BigDecimal qunarNgtSaleprice) {
        this.qunarNgtSaleprice = qunarNgtSaleprice;
    }

    /**
     * 获取去哪儿美元售价
     *
     * @return qunar_usd_saleprice - 去哪儿美元售价
     */
    public BigDecimal getQunarUsdSaleprice() {
        return qunarUsdSaleprice;
    }

    /**
     * 设置去哪儿美元售价
     *
     * @param qunarUsdSaleprice 去哪儿美元售价
     */
    public void setQunarUsdSaleprice(BigDecimal qunarUsdSaleprice) {
        this.qunarUsdSaleprice = qunarUsdSaleprice;
    }

    /**
     * 获取去哪儿子渠道售价
     *
     * @return qunar_son_saleprice - 去哪儿子渠道售价
     */
    public BigDecimal getQunarSonSaleprice() {
        return qunarSonSaleprice;
    }

    /**
     * 设置去哪儿子渠道售价
     *
     * @param qunarSonSaleprice 去哪儿子渠道售价
     */
    public void setQunarSonSaleprice(BigDecimal qunarSonSaleprice) {
        this.qunarSonSaleprice = qunarSonSaleprice;
    }

    public BigDecimal getCtriposSaleprice() {
        return ctriposSaleprice;
    }

    public void setCtriposSaleprice(BigDecimal ctriposSaleprice) {
        this.ctriposSaleprice = ctriposSaleprice;
    }
}