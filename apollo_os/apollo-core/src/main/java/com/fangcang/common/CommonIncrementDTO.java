package com.fangcang.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 402931207@qq.com on 2018/12/12.
 */
@Data
public class CommonIncrementDTO implements Serializable {

    private String merchantCode;

    private String shopId;

    /**
     * 增量数据
     */
    private List<IncrementDTO> incrementDTOList;

    private IncrementType incrementType;
}
