package com.fangcang.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

public class PropertyCopyUtil {
	
	public static <T,S> S transfer(T t,Class<S> s){
		if (t == null) {
			return null;
		}
		return (S) JSONObject.parseObject(JSONObject.toJSONString(t),s);
	}

	public static <T> List<T>  transferArray(Object object,Class<T> t){
		if (object == null) {
			return null;
		}
		return JSON.parseArray(JSON.toJSONString(object),t);
	}

}
