package com.fangcang.common.enums;

public enum IncreaseTypeEnum {

    PLUS(1,"加法"),MINUS(2,"减法"),MULTIPLY(3,"乘法")
    ;

    IncreaseTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer key;

    public String value;

    public static IncreaseTypeEnum getEnumByKey(Integer key){
        IncreaseTypeEnum increaseTypeEnum = null;
        for (IncreaseTypeEnum typeEnum : IncreaseTypeEnum.values()){
            if (key.intValue() == typeEnum.key.intValue()){
                increaseTypeEnum = typeEnum;
            }
        }

        return increaseTypeEnum;
    }
}
