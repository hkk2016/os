package com.fangcang.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 402931207@qq.com on 2018/12/12.
 */
@Component
@ConfigurationProperties(prefix = "ota")
@Data
public class OtaConfig {

    /**
     * 增量推送或订单确认的URL，只需要定义到应用层，使用时：用value+incrementPush拼接成完整的URL<br/>
     * key = channelCode,value=url,如：http;//192.168.10.201:8085/fliggy/
     */
    private Map<String,String> urlMap = new HashMap<>();

    /**
     * 增量推送的方法
     */
    private String incrementPush = "";

    /**
     * 同步价格和房态
     */
    private String priceAndRoomStatus = "";

    //-------------------------飞猪渠道相关变量
    /**
     * 确认订单的方法
     */
    private String orderOperate = "";

    /**
     * 推送酒店到飞猪
     */
    private String addHotel = "";
    /**
     * 查询飞猪酒店匹配
     */
    private String getHotel = "";
    /**
     * 推送房型到飞猪
     */
    private String addRoomType = "";
    /**
     * 查询飞猪房型匹配
     */
    private String getRoomType = "";
    /**
     * 推送价格计划到飞猪
     */
    private String addRatePlan = "";
    /**
     * 删除飞猪价格计划
     */
    private String delRatePlan = "";
    /**
     * 确认订单
     */
    private String orderConfirm = "";


    //-------------------------携程猪渠道相关变量
    /**
     * 推送酒店到携程海外
     */
    private String pushCtriposHotel = "";

    /**
     * 从携程海外同步映射状态
     */
    private String syncCtriposHotel = "";

    /**
     * 推送确认号到携程
     */
    private String pushConfirmNo = "";
}
