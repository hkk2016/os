package com.fangcang.common.util;

import com.fangcang.common.constant.InitData;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * Created by ASUS on 2018/5/24.
 */
public class RateUtil {

    /**
     * 获取兑换汇率
     * @param merchantCode
     * @param sourceCurrency
     * @param targetCurrency
     * @return
     */
    public static BigDecimal getRate(String merchantCode, String sourceCurrency, String targetCurrency) {
        if (StringUtils.isEmpty(merchantCode)
                || StringUtils.isEmpty(sourceCurrency)
                || StringUtils.isEmpty(targetCurrency)) {
            return null;
        }
        if (sourceCurrency.equals(targetCurrency)) {
            return BigDecimal.ONE;
        }
        return InitData.merchantExchangeMap.get(merchantCode + "-" + sourceCurrency + "-" + targetCurrency);
    }
}
