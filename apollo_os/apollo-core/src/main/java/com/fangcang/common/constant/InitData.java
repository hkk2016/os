package com.fangcang.common.constant;

import com.fangcang.mapping.dto.ctripos.CtriposShopInfoDTO;
import com.fangcang.mapping.response.feizhu.ShopInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vinney on 2018/6/21.
 */
public class InitData {
    public static List<String> NEED_CHECK_URL_LIST = new ArrayList<>();
    /**
     * key:merchantCode
     * value:merchantName;
     */
    public static Map<String,String> MERCHANT_CODE_NAME_MAP = new HashMap<>();

    /**
     * 初始化店铺信息
     * key:merchantCode-shopId
     * value:channelCode
     */
    public static Map<String,String> merchantShopChanneCodeMap = new HashMap<>();

    /**
     * 商家飞猪店铺
     * key: merchantCode
     * value:List<shopId>
     */
    public static Map<String ,List<ShopInfo>> merchantShopMap = new HashMap<>();

    /**
     * 商家携程海外店铺
     * key: merchantCode
     * value:List<shopId>
     */
    public static Map<String ,List<CtriposShopInfoDTO>> merchantCtriposShopMap = new HashMap<>();

    /**
     * 初始化店铺信息
     * key:merchantCode-channelCode
     * value:shopId
     */
    public static Map<String,String> merchantChannelCodeShopMap = new HashMap<>();

    /**
     * 初始化汇率信息
     * key:merchantCode-sourceCurrency-targetCurrency
     * value:rate
     */
    public static Map<String,BigDecimal> merchantExchangeMap = new HashMap<>();
}
