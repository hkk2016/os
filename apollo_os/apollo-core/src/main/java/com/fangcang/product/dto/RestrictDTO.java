package com.fangcang.product.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class RestrictDTO implements Serializable{
    private Integer id;

    private Long ratePlanId;

    /**
     * 提前预定天
     */
    private Integer advanceBookingDays;

    /**
     * 提前预定小时
     */
    private Integer advanceBookingHours;

    /**
     * 连住X天
     */
    private Integer occupancyOfDays;

    /**
     * 至少预定间数
     */
    private Integer numberOfBooking;

    /**
     * 1-一经预定不可取消；2-提前取消
     */
    private Integer cancelType;

    /**
     * 提前取消天数
     */
    private Integer cancelDays;

    /**
     * 提前取消小时
     */
    private String cancelHours;

    private Integer lastConfirmDays;

    private Integer lastConfirmHours;

    private String creator;

    private Date createTime;

    private String modifier;

    private Date modifyDate;
}