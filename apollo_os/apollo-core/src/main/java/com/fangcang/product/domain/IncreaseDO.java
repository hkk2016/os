package com.fangcang.product.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_htlpro_increase")
public class IncreaseDO {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 价格计划id
     */
    @Column(name = "priceplan_id")
    private Integer priceplanId;

    /**
     * 渠道编码
     */
    @Column(name = "channel_code")
    private String channelCode;

    /**
     * 加幅类型
     */
    @Column(name = "increase_type")
    private Integer increaseType;

    /**
     * 加幅金额
     */
    @Column(name = "increase_amount")
    private BigDecimal increaseAmount;

    /**
     * 开始日期
     */
    @Column(name = "start_date")
    private Date startDate;

    /**
     * 结束日期
     */
    @Column(name = "end_date")
    private Date endDate;

    /**
     * 星期
     */
    private String week;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "modify_time")
    private Date modifyTime;

    private String creator;

    private String modifier;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取价格计划id
     *
     * @return priceplan_id - 价格计划id
     */
    public Integer getPriceplanId() {
        return priceplanId;
    }

    /**
     * 设置价格计划id
     *
     * @param priceplanId 价格计划id
     */
    public void setPriceplanId(Integer priceplanId) {
        this.priceplanId = priceplanId;
    }

    /**
     * 获取渠道编码
     *
     * @return channel_code - 渠道编码
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * 设置渠道编码
     *
     * @param channelCode 渠道编码
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    /**
     * 获取加幅类型
     *
     * @return increase_type - 加幅类型
     */
    public Integer getIncreaseType() {
        return increaseType;
    }

    /**
     * 设置加幅类型
     *
     * @param increaseType 加幅类型
     */
    public void setIncreaseType(Integer increaseType) {
        this.increaseType = increaseType;
    }

    /**
     * 获取加幅金额
     *
     * @return increase_amount - 加幅金额
     */
    public BigDecimal getIncreaseAmount() {
        return increaseAmount;
    }

    /**
     * 设置加幅金额
     *
     * @param increaseAmount 加幅金额
     */
    public void setIncreaseAmount(BigDecimal increaseAmount) {
        this.increaseAmount = increaseAmount;
    }

    /**
     * 获取开始日期
     *
     * @return start_date - 开始日期
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * 设置开始日期
     *
     * @param startDate 开始日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 获取结束日期
     *
     * @return end_date - 结束日期
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * 设置结束日期
     *
     * @param endDate 结束日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取星期
     *
     * @return week - 星期
     */
    public String getWeek() {
        return week;
    }

    /**
     * 设置星期
     *
     * @param week 星期
     */
    public void setWeek(String week) {
        this.week = week;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return modify_time
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * @param modifyTime
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * @return creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return modifier
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * @param modifier
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}