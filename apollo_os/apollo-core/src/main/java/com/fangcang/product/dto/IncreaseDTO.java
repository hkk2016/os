package com.fangcang.product.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class IncreaseDTO implements Serializable {

    private Integer id;

    private Long pricePlanId;

    private Date startDate;

    private Date endDate;

    private String channelCode;

    /***
     * 1-在底价基础上增加数额（+）
     * 2-在底价基础上减少（-）
     * 3-在底价基础上乘以（*）
     */
    private Integer increaseType;

    /**
     * 加幅额度
     * + ----> 10
     * - ----> 10
     * * ----> 1.1
     */
    private Double increaseAmount;

    private String week;
}
