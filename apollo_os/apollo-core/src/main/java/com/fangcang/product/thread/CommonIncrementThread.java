package com.fangcang.product.thread;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.CommonIncrementDTO;
import com.fangcang.common.IncrementRetryDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.util.HttpClientUtil;
import com.fangcang.common.util.IncrementConfig;
import com.fangcang.product.service.IncrementService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 402931207@qq.com on 2018/12/11.
 */
@Slf4j
public class CommonIncrementThread extends Thread {

    private CommonIncrementDTO commonIncrementDTO = new CommonIncrementDTO();

    private Map<String,String> urlMap = new HashMap<>();

    private String methodName = "";

    private IncrementService incrementService;


    public CommonIncrementThread(CommonIncrementDTO commonIncrementDTO, Map<String, String> urlMap, String methodName, IncrementService incrementService) {
        this.commonIncrementDTO = commonIncrementDTO;
        this.urlMap = urlMap;
        this.methodName = methodName;
        this.incrementService = incrementService;
    }

    @Override
    public void run() {
        if (CollectionUtils.isEmpty(urlMap)){
            log.error("没有获取到需要推送的URL:{}", JSON.toJSONString(urlMap));
            return ;
        }

        if (StringUtils.isBlank(methodName)){
            log.error("没有找到要调用的方法：{}",JSON.toJSONString(methodName));
            return ;
        }

        try {
            Thread.sleep(500l);
        } catch (InterruptedException e) {
            log.error("推送增量的线程睡眠时报错",e);
        }

        for (Map.Entry urlEntry : urlMap.entrySet()){
            String url = urlEntry.getValue() + methodName;

            Boolean result = false;
            String message = "";
            try {
                message = JSON.toJSONString(commonIncrementDTO);
                String responseStr = HttpClientUtil.postJson(url, message);
                ResponseDTO responseDTO = JSON.parseObject(responseStr, ResponseDTO.class);

                result = (null != responseDTO && ResultCodeEnum.SUCCESS.code == responseDTO.getResult()) ? true : false;

                log.info("增量推送完成,推送内容:{},请求URL：{}，返回结果：{}，result={}",message,url,JSON.toJSONString(responseDTO),result);
            } catch (IOException e) {
                String msg = "url:"+url+",message:"+ message +",result:"+result;
                log.error("增量推送IO异常.{}",msg ,e);
                result = false;
            }

            if(!result){
                //创建重试对象，推送失败则放入队列中
                IncrementRetryDTO incrementRetryDTO = new IncrementRetryDTO();
                incrementRetryDTO.setUrl(url);
                incrementRetryDTO.setIncrementType(commonIncrementDTO.getIncrementType());
                incrementRetryDTO.setIncrementDTOList(commonIncrementDTO.getIncrementDTOList());
                incrementRetryDTO.setMerchantCode(commonIncrementDTO.getMerchantCode());
                incrementRetryDTO.setRetryNum(0);
                incrementRetryDTO.setTime(System.currentTimeMillis() + IncrementConfig.DELAY_TIME);
                incrementService.offer(incrementRetryDTO);
            }
        }
    }
}
