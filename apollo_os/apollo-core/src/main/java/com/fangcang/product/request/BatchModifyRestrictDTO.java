package com.fangcang.product.request;

import com.fangcang.product.dto.RestrictDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by on 2018/12/25.
 */
@Data
public class BatchModifyRestrictDTO implements Serializable{

    private List<RestrictDTO> restrictDTOList;

    private String merchantCode;

    private String operator;
}
