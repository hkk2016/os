package com.fangcang.product.service;

import com.fangcang.common.ResponseDTO;
import com.fangcang.product.dto.RestrictDTO;
import com.fangcang.product.request.BatchModifyRestrictDTO;

/**
 * Created by Vinney on 2018/10/17.
 */
public interface RestrictService {

    /**
     * 保存条款
     * 先删除，再插入。新增和编辑页都可以用这个。
     * @param restrictDTO
     */
    void saveRestrictByRatePlanId(RestrictDTO restrictDTO);

    /**
     * 批量修改条款
     * @return
     */
    ResponseDTO batchModifyRestrict(BatchModifyRestrictDTO requestDTO);

    /**
     *
     * @param restrictDTO
     * @return
     */
    RestrictDTO queryRestrictByRatePlanId(RestrictDTO restrictDTO);

    /**
     *
     */
    void deleteRestrictByRatePlanId(Long ratePlanId);

}
