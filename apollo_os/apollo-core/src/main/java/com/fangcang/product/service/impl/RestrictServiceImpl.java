package com.fangcang.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.fangcang.common.IncrementDTO;
import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.util.IncrementConfig;
import com.fangcang.common.util.PropertyCopyUtil;
import com.fangcang.common.util.URLSplitUtil;
import com.fangcang.product.domain.PricePlanDO;
import com.fangcang.product.domain.RestrictDO;
import com.fangcang.product.dto.RestrictDTO;
import com.fangcang.product.mapper.PricePlanMapper;
import com.fangcang.product.mapper.RestrictMapper;
import com.fangcang.product.request.BatchModifyRestrictDTO;
import com.fangcang.product.request.DynamicPricePlanQueryDTO;
import com.fangcang.product.service.IncrementService;
import com.fangcang.product.service.RestrictService;
import com.fangcang.product.thread.IncrementThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Vinney on 2018/10/17.
 */
@Slf4j
@Service
public class RestrictServiceImpl implements RestrictService {

    @Autowired
    private RestrictMapper restrictMapper;

    @Autowired
    private PricePlanMapper pricePlanMapper;

    @Autowired
    private IncrementConfig incrementConfig;

    @Resource(name = "incrementExecutor")
    private ThreadPoolTaskExecutor incrementExecutor;

    @Autowired
    private IncrementService incrementService;

    @Override
    public void saveRestrictByRatePlanId(RestrictDTO restrictDTO) {

        if (null == restrictDTO.getRatePlanId()){
            log.error("保存条款失败，价格计划ID为空。{}", JSON.toJSONString(restrictDTO));
            return ;
        }

        Example selectExample = new Example(RestrictDO.class);
        Example.Criteria deleteCriteria = selectExample.createCriteria();
        deleteCriteria.andEqualTo("ratePlanId",restrictDTO.getRatePlanId());
        List<RestrictDO> existRestrictDOList = restrictMapper.selectByExample(selectExample);

        RestrictDO restrictDO = PropertyCopyUtil.transfer(restrictDTO,RestrictDO.class);
        if (!CollectionUtils.isEmpty(existRestrictDOList) && existRestrictDOList.size() > 0){
            restrictDO.setCreator(null);
            restrictDO.setModifyDate(new Date());
            Example updateExample = new Example(RestrictDO.class);
            Example.Criteria updateCriteria = updateExample.createCriteria();
            updateCriteria.andEqualTo("ratePlanId",restrictDTO.getRatePlanId());
            restrictMapper.updateByExampleSelective(restrictDO,updateExample);
        } else{
            restrictDO.setModifier(null);
            restrictDO.setCreateTime(new Date());
            restrictMapper.insert(restrictDO);
        }
    }

    @Override
    @Transactional
    public ResponseDTO batchModifyRestrict(BatchModifyRestrictDTO requestDTO){
        if (requestDTO.getRestrictDTOList()!=null && requestDTO.getRestrictDTOList().size()>0){
            List<IncrementDTO> incrementDTOList = new ArrayList<>();
            Set<Long> hotelIdSet=new HashSet<>();
            for (RestrictDTO restrictDTO:requestDTO.getRestrictDTOList()){
                restrictDTO.setCreator(requestDTO.getOperator());
                restrictDTO.setModifier(requestDTO.getOperator());
                saveRestrictByRatePlanId(restrictDTO);

                DynamicPricePlanQueryDTO dynamicPricePlanQueryDTO=new DynamicPricePlanQueryDTO();
                dynamicPricePlanQueryDTO.setPricePlanId(restrictDTO.getRatePlanId());
                List<PricePlanDO> pricePlanDOList=pricePlanMapper.dynamicQueryPricePlanList(dynamicPricePlanQueryDTO);
                IncrementDTO incrementDTO = new IncrementDTO();
                incrementDTO.setMerchantCode(requestDTO.getMerchantCode());
                incrementDTO.setMHotelId(pricePlanDOList.get(0).getHotelId());
                incrementDTO.setMRoomTypeId(pricePlanDOList.get(0).getRoomTypeId());
                incrementDTO.setMRatePlanId(pricePlanDOList.get(0).getPricePlanId());
                incrementDTOList.add(incrementDTO);
                hotelIdSet.add(pricePlanDOList.get(0).getHotelId());
            }
            List<IncrementDTO> fiterAfterList = new ArrayList<>();
            if (incrementDTOList.size()>6){
                for (Long hotelId:hotelIdSet){
                    IncrementDTO incrementDTO = new IncrementDTO();
                    incrementDTO.setMerchantCode(requestDTO.getMerchantCode());
                    incrementDTO.setMHotelId(hotelId);
                    fiterAfterList.add(incrementDTO);
                }
            }
            String url = URLSplitUtil.getUrl(incrementConfig);
            IncrementThread incrementThread = new IncrementThread(fiterAfterList,url,incrementService);
            incrementExecutor.execute(incrementThread);
        }
        return new ResponseDTO(ResultCodeEnum.SUCCESS.code);
    }

    @Override
    public RestrictDTO queryRestrictByRatePlanId(RestrictDTO restrictDTO) {
        RestrictDTO resultDTO = new RestrictDTO();
        Example selectExample = new Example(RestrictDO.class);
        Example.Criteria selectCriteria = selectExample.createCriteria();
        selectCriteria.andEqualTo("ratePlanId",restrictDTO.getRatePlanId());
        List<RestrictDO> existRestrictDOList = restrictMapper.selectByExample(selectExample);
        if (!CollectionUtils.isEmpty(existRestrictDOList)){
            resultDTO = PropertyCopyUtil.transfer(existRestrictDOList.get(0),RestrictDTO.class);
        }
        return resultDTO;
    }

    @Override
    public void deleteRestrictByRatePlanId(Long ratePlanId) {
        if (null == ratePlanId){
            log.error("删除条款错误:价格计划为空。{}",JSON.toJSONString(ratePlanId));
            return ;
        }

        Example deleteExample = new Example(RestrictDO.class);
        Example.Criteria deleteCriteria = deleteExample.createCriteria();
        deleteCriteria.andEqualTo("ratePlanId",ratePlanId);
        restrictMapper.deleteByExample(deleteExample);
    }
}
