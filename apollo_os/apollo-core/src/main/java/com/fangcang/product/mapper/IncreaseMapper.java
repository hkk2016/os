package com.fangcang.product.mapper;

import com.fangcang.common.MyMapper;
import com.fangcang.product.domain.IncreaseDO;

public interface IncreaseMapper extends MyMapper<IncreaseDO> {
}