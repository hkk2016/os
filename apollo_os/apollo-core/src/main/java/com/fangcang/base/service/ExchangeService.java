package com.fangcang.base.service;

import com.fangcang.base.request.QueryExchangeDTO;
import com.fangcang.base.request.UpdateExchangeDTO;
import com.fangcang.base.response.ExchangeDTO;
import com.fangcang.base.response.ExchangeLogDTO;
import com.fangcang.common.ResponseDTO;

import java.util.List;

public interface ExchangeService {

    /**
     * 初始化汇率信息
     */
    void initExchange();

    /**
     * 根据条件查询
     */
    public List<ExchangeDTO> queryExchange(QueryExchangeDTO requestDTO);

    /**
     * 保存/更新汇率
     * @param updateExchangeDTO
     */
    public ResponseDTO saveOrUpdateExchange(UpdateExchangeDTO updateExchangeDTO);

    /**
     * 查询汇率修改日志
     */
    public List<ExchangeLogDTO> queryExchangeLog(QueryExchangeDTO requestDTO);
}
