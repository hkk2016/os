package com.fangcang.base.domain;

import com.fangcang.common.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_areadata")
@Data
@EqualsAndHashCode(callSuper = false)
public class AreaDataDO extends BaseDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "type")
    private Integer type;

    @Column(name = "data_code")
    private String dataCode;

    @Column(name = "data_name")
    private String dataName;

    @Column(name = "pinyin")
    private String pinYin;

    @Column(name = "acronympinyin")
    private String acronymPinYin;

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "eng_dataname")
    private String engDataname;

    @Column(name = "country_code")
    private String countryCode;

    /**
     * 洲编码
     */
    @Column(name = "continent_code")
    private String continentCode;

    /**
     * 洲名称
     */
    @Column(name = "continent_name")
    private String continentName;
}
