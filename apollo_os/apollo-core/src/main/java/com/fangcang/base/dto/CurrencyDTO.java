package com.fangcang.base.dto;

import lombok.Data;

/**
 * @author ivan
 * 提供币种常量和币种常用的方法
 */
@Data
public class CurrencyDTO {
    public int key;
    public String value;
    public String desc;

}
