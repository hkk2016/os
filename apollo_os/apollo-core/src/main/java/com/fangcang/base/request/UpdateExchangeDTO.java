package com.fangcang.base.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UpdateExchangeDTO implements Serializable {
    private static final long serialVersionUID = -6147279044482949578L;

    private Long id;

    private String merchantCode;

    @NotEmpty
    private String sourceCurrency;

    @NotEmpty
    private String targetCurrency;

    @NotNull
    private BigDecimal rate;

    private String operator;
}
