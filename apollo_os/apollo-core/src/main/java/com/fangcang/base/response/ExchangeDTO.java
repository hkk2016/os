package com.fangcang.base.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ExchangeDTO implements Serializable {
    private static final long serialVersionUID = -6147279044482949578L;

    private Long id;

    private String merchantCode;

    private String sourceCurrency;
    private String sourceCurrencyName;

    private String targetCurrency;
    private String targetCurrencyName;

    private BigDecimal rate;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    private String modifyTime;
}
