package com.channel.connector.ctripos.controller;

import com.alibaba.fastjson.JSON;
import com.channel.connector.ctripos.common.util.StringUtil;
import com.channel.connector.ctripos.dto.CtriposBaseRequestDTO;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

public class BaseController {

    /**
     * 得到请求对象
     *
     * @param request
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    protected  <T> T getRequestDTO(HttpServletRequest request, Class<T> clazz) throws IOException {

        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = request.getReader();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            if (null != br) {
                br.close();
            }
        }

        T requestDTO = JSON.parseObject(sb.toString(), clazz);
        return requestDTO;
    }

    /**
     * 验证shopId是否有效
     *
     * @param ctriposBaseRequestDTO
     * @return
     */
    protected boolean invalidShopId(CtriposBaseRequestDTO ctriposBaseRequestDTO) {

        if (null == ctriposBaseRequestDTO) {
            return true;
        }

        if (!StringUtil.isValidString(ctriposBaseRequestDTO.getShopId())) {
            return true;
        }

        return false;
    }
}
