package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "POS", namespace = Namespace.NS)
public class Pos {

	@XmlElement(name = "Source", namespace = Namespace.NS)
	private Source source;

	public void setSource(Source source) {
		this.source = source;
	}

	public Source getSource() {
		return source;
	}

}
