package com.channel.connector.product.service.impl;

import com.channel.connector.ctripos.common.util.PropertyCopyUtil;
import com.channel.connector.ctripos.common.util.StringUtil;
import com.channel.connector.ctripos.dto.request.IncrementDTO;
import com.channel.connector.ctripos.enums.ChannelTypeEnum;
import com.channel.connector.product.domain.PriceAndQuotaDO;
import com.channel.connector.product.domain.PricePlanDO;
import com.channel.connector.product.domain.SaleStateDO;
import com.channel.connector.product.dto.QueryPriceAndQuotaRequestDTO;
import com.channel.connector.product.mapper.PriceAndQuotaMapper;
import com.channel.connector.product.mapper.PricePlanMapper;
import com.channel.connector.product.mapper.SaleStateMapper;
import com.channel.connector.product.service.PricePlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Created by 402931207@qq.com on 2018/12/14.
 */
@Service
@Slf4j
public class PricePlanServiceImpl implements PricePlanService {

    @Autowired
    private PricePlanMapper pricePlanMapper;

    @Autowired
    private PriceAndQuotaMapper priceAndQuotaMapper;

    @Autowired
    private SaleStateMapper saleStateMapper;

    @Override
    public List<PricePlanDO> queryByIds(List<Long> pricePlanIdList) {
        Example queryExample = new Example(PricePlanDO.class);
        Example.Criteria queryCriteria = queryExample.createCriteria();
        queryCriteria.andIn("priceplanId",pricePlanIdList);
        return  pricePlanMapper.selectByExample(queryExample);
    }

    @Override
    public List<PriceAndQuotaDO> queryPriceAndQuota(IncrementDTO incrementDTO, String merchantCode) {
        QueryPriceAndQuotaRequestDTO queryPriceAndQuotaRequestDTO = PropertyCopyUtil.transfer(incrementDTO,QueryPriceAndQuotaRequestDTO.class);
        queryPriceAndQuotaRequestDTO.setMerchantCode(merchantCode);
        List<PriceAndQuotaDO> priceAndQuotaDOList = priceAndQuotaMapper.queryPriceAndQuota(queryPriceAndQuotaRequestDTO);
        return priceAndQuotaDOList;
    }

    @Override
    public List<SaleStateDO> queryOnsale(List<Long> pricePlanIdList, String channelCode, Integer saleState) {
        Example queryExample = new Example(SaleStateDO.class);
        Example.Criteria queryCriteria = queryExample.createCriteria();
        queryCriteria.andIn("priceplanId",pricePlanIdList);
        if (StringUtil.isValidString(channelCode) && ChannelTypeEnum.CTRIPOS.key.equals(channelCode)){
            queryCriteria.andEqualTo("ctriposSaleState",saleState);
        }
        List<SaleStateDO> saleStateDOList = saleStateMapper.selectByExample(queryExample);

        return saleStateDOList;
    }
}
