/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:48:06
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "StayDateRange", namespace = Namespace.NS)
public class StayDateRange {

	@XmlAttribute(name = "End")
	private String end;

	@XmlAttribute(name = "Start")
	private String start;

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
}
