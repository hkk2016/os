package com.channel.connector.ctripos.webservice.order.request;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.common.HotelReservation;
import com.channel.connector.ctripos.webservice.order.request.base.RQHeader;
import com.channel.connector.ctripos.webservice.order.request.dto.Pos;
import com.channel.connector.ctripos.webservice.order.response.dto.UniqueID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_HotelResRQ", namespace = Namespace.NS)
public class OTAHotelResRQ extends RQHeader {

	@XmlElement(name="POS", namespace = Namespace.NS)
	private Pos pos;

	@XmlElement(name = "UniqueID", namespace = Namespace.NS)
	private UniqueID uniqueID;

	@XmlElementWrapper(name = "HotelReservations", namespace = Namespace.NS)
	@XmlElement(name = "HotelReservation", namespace = Namespace.NS)
	private List<HotelReservation> hotelReservations;

	public Pos getPos() {
		return pos;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public UniqueID getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(UniqueID uniqueID) {
		this.uniqueID = uniqueID;
	}

	public List<HotelReservation> getHotelReservations() {
		if (null == hotelReservations) {
			hotelReservations = new ArrayList<>();
		}
		return hotelReservations;
	}

	public void setHotelReservations(List<HotelReservation> hotelReservations) {
		this.hotelReservations = hotelReservations;
	}

}
