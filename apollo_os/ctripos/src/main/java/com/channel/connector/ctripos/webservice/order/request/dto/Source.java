package com.channel.connector.ctripos.webservice.order.request.dto;


import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Source", namespace = Namespace.NS)
public class Source {
	
	@XmlElement(name="RequestorID", namespace = Namespace.NS)
	private RequestorID requestorId;

	public void setRequestorId(RequestorID requestorId) {
		this.requestorId = requestorId;
	}

	public RequestorID getRequestorId() {
		return requestorId;
	} 

}
