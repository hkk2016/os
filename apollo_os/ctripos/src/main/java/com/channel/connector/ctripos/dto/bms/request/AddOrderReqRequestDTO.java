package com.channel.connector.ctripos.dto.bms.request;

import com.channel.connector.ctripos.dto.bms.OrderRequestDTO;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhanwang
 */
@Data
public class AddOrderReqRequestDTO extends OrderRequestDTO implements Serializable {

    private static final long serialVersionUID = 5194113278198788553L;

}