package com.channel.connector.ctripos.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.ctripos.domain.CtriposMapHotelDO;

public interface CtriposMapHotelMapper extends BaseMapper<CtriposMapHotelDO> {
}