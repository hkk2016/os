
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>BookingRulesType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="BookingRulesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookingRule" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CancelPenalties" type="{http://www.opentravel.org/OTA/2003/05}CancelPenaltiesType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="MaxAdvancedBookingOffset" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="MinAdvancedBookingOffset" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="MaxContiguousBookings" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="MaxTotalOccupancy" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="BookingOffsetUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingRulesType", propOrder = {
    "bookingRule"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.HotelRatePlanType.BookingRules.class
})
public class BookingRulesType {

    @XmlElement(name = "BookingRule")
    protected List<BookingRule> bookingRule;

    /**
     * Gets the value of the bookingRule property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingRule property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingRule().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookingRulesType.BookingRule }
     *
     *
     */
    public List<BookingRule> getBookingRule() {
        if (bookingRule == null) {
            bookingRule = new ArrayList<BookingRule>();
        }
        return this.bookingRule;
    }

    public void setBookingRule(List<BookingRule> bookingRule) {
        this.bookingRule = bookingRule;
    }

    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CancelPenalties" type="{http://www.opentravel.org/OTA/2003/05}CancelPenaltiesType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="MaxAdvancedBookingOffset" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="MinAdvancedBookingOffset" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="MaxContiguousBookings" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="MaxTotalOccupancy" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="BookingOffsetUnit" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cancelPenalties"
    })
    public static class BookingRule {

        @XmlElement(name = "CancelPenalties")
        protected CancelPenaltiesType cancelPenalties;

        /**
         * 最大提前预订天数
         */
        @XmlAttribute(name = "MaxAdvancedBookingOffset")
        protected Integer maxAdvancedBookingOffset;

        /**
         * 最小提前预订天数
         */
        @XmlAttribute(name = "MinAdvancedBookingOffset")
        protected Integer minAdvancedBookingOffset;

        /**
         * 最大预订间数-
         */
        @XmlAttribute(name = "MaxContiguousBookings")
        protected Integer maxContiguousBookings;

        /**
         * 最大可入住人数
         */
        @XmlAttribute(name = "MaxTotalOccupancy")
        protected Integer maxTotalOccupancy;

        /**
         * Hour、Day，默认为Day
         */
        @XmlAttribute(name = "BookingOffsetUnit")
        protected TimeUnitType bookingOffsetUnit;

        /**
         * 获取cancelPenalties属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CancelPenaltiesType }
         *     
         */
        public CancelPenaltiesType getCancelPenalties() {
            return cancelPenalties;
        }

        /**
         * 设置cancelPenalties属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CancelPenaltiesType }
         *     
         */
        public void setCancelPenalties(CancelPenaltiesType value) {
            this.cancelPenalties = value;
        }

        /**
         * 获取maxAdvancedBookingOffset属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxAdvancedBookingOffset() {
            return maxAdvancedBookingOffset;
        }

        /**
         * 设置maxAdvancedBookingOffset属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxAdvancedBookingOffset(Integer value) {
            this.maxAdvancedBookingOffset = value;
        }

        /**
         * 获取minAdvancedBookingOffset属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMinAdvancedBookingOffset() {
            return minAdvancedBookingOffset;
        }

        /**
         * 设置minAdvancedBookingOffset属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMinAdvancedBookingOffset(Integer value) {
            this.minAdvancedBookingOffset = value;
        }

        /**
         * 获取maxContiguousBookings属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxContiguousBookings() {
            return maxContiguousBookings;
        }

        /**
         * 设置maxContiguousBookings属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxContiguousBookings(Integer value) {
            this.maxContiguousBookings = value;
        }

        /**
         * 获取maxTotalOccupancy属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxTotalOccupancy() {
            return maxTotalOccupancy;
        }

        /**
         * 设置maxTotalOccupancy属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxTotalOccupancy(Integer value) {
            this.maxTotalOccupancy = value;
        }

        /**
         * 获取bookingOffsetUnit属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TimeUnitType }
         *     
         */
        public TimeUnitType getBookingOffsetUnit() {
            return bookingOffsetUnit;
        }

        /**
         * 设置bookingOffsetUnit属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TimeUnitType }
         *     
         */
        public void setBookingOffsetUnit(TimeUnitType value) {
            this.bookingOffsetUnit = value;
        }

    }

}
