/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:16:10
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Total", namespace = Namespace.NS)
public class Total {

	@XmlAttribute(name = "AmountBeforeTax")
	private Double amountBeforeTax;

	@XmlAttribute(name = "AmountAfterTax")
	private Double amountAfterTax;

	@XmlAttribute(name = "CurrencyCode")
	private String currencyCode;

	public Double getAmountBeforeTax() {
		return amountBeforeTax;
	}

	public void setAmountBeforeTax(Double amountBeforeTax) {
		this.amountBeforeTax = amountBeforeTax;
	}

	public Double getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
}
