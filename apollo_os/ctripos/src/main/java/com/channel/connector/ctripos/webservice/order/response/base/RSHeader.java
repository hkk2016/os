package com.channel.connector.ctripos.webservice.order.response.base;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.Warning;
import com.channel.connector.ctripos.webservice.order.response.dto.Error;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class RSHeader {
	@XmlElement(name = "Success", namespace = Namespace.NS)
	private String success;

	@XmlElementWrapper(name = "Warnings", namespace = Namespace.NS)
	@XmlElement(name = "Warning", namespace = Namespace.NS)
	private List<Warning> Warnings;

	@XmlElementWrapper(name = "Errors", namespace = Namespace.NS)
	@XmlElement(name = "Error", namespace = Namespace.NS)
	private List<Error> Errors;

	@XmlAttribute(name = "Version")
	private String version;

	@XmlAttribute(name = "EchoToken")
	private String echoToken;

	@XmlAttribute(name = "PrimaryLangID")
	private String primaryLangID;
	
	@XmlAttribute(name = "TimeStamp")
	private XMLGregorianCalendar timeStamp;

//	@XmlAttribute(name = "xmlns")
//	private String xmlns;
//
//	@XmlAttribute(name = "xmlns:xsi")
//	private String xmlns_xsi = Namespace.NS_XSI;


	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public List<Warning> getWarnings() {
		return Warnings;
	}

	public void setWarnings(List<Warning> warnings) {
		Warnings = warnings;
	}

	public List<Error> getErrors() {
		return Errors;
	}

	public void setErrors(List<Error> errors) {
		Errors = errors;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getEchoToken() {
		return echoToken;
	}

	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}

//	public String getXmlns() {
//		return xmlns;
//	}
//
//	public void setXmlns(String xmlns) {
//		this.xmlns = xmlns;
//	}
//
//	public String getXmlns_xsi() {
//		return xmlns_xsi;
//	}
//
//	public void setXmlns_xsi(String xmlns_xsi) {
//		this.xmlns_xsi = xmlns_xsi;
//	}

	public String getPrimaryLangID() {
		return primaryLangID;
	}

	public void setPrimaryLangID(String primaryLangID) {
		this.primaryLangID = primaryLangID;
	}

	public XMLGregorianCalendar getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(XMLGregorianCalendar timeStamp) {
		this.timeStamp = timeStamp;
	}

}
