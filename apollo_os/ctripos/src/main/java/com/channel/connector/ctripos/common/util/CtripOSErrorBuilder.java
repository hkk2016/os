package com.channel.connector.ctripos.common.util;

import com.channel.connector.ctripos.enums.CtripOSErrorEnum;
import com.channel.connector.ctripos.enums.CtripOSTypeEnum;
import org.apache.commons.lang3.StringUtils;
import com.channel.connector.ctripos.webservice.order.response.dto.Error;

import java.util.ArrayList;
import java.util.List;

public class CtripOSErrorBuilder {

	public static List<Error> buildErrors(CtripOSTypeEnum typeE, CtripOSErrorEnum e, String fcErrorName) {
		Error error = new Error();
		error.setCode(e.getCode());
		error.setShortText(e.getName() + (StringUtils.isEmpty(fcErrorName) ? "" : ", " + fcErrorName));
		error.setType(typeE.getType());
		List<Error> errors = new ArrayList<>();
		errors.add(error);
		return errors;
	}
}
