package com.channel.connector.ctripos.webservice.order.request;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.request.base.RQHeader;
import com.channel.connector.ctripos.webservice.order.request.dto.Pos;
import com.channel.connector.ctripos.webservice.order.request.dto.ReadRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_ReadRQ", namespace = Namespace.NS)
public class OTAReadRQ extends RQHeader {
    @XmlElement(name = "POS", namespace = Namespace.NS)
    private Pos pos;

    @XmlElementWrapper(name = "ReadRequests", namespace = Namespace.NS)
    @XmlElement(name = "ReadRequest", namespace = Namespace.NS)
    private List<ReadRequest> readRequests;

    public Pos getPos() {
        return pos;
    }

    public void setPos(Pos pos) {
        this.pos = pos;
    }

    public List<ReadRequest> getReadRequests() {
        return readRequests;
    }

    public void setReadRequests(List<ReadRequest> readRequests) {
        if (null == readRequests) {
            readRequests = new ArrayList<>();
        }
        this.readRequests = readRequests;
    }
}
