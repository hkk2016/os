/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:06:24
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.TPAExtension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RoomStay", namespace = Namespace.NS)
public class RoomStay {

	@XmlElementWrapper(name = "RoomTypes", namespace = Namespace.NS)
	@XmlElement(name = "RoomType", namespace = Namespace.NS)
	private List<RoomType> roomTypes;

	@XmlElementWrapper(name = "RatePlans", namespace = Namespace.NS)
	@XmlElement(name = "RatePlan", namespace = Namespace.NS)
	private List<RatePlan> ratePlans;

	@XmlElementWrapper(name = "RoomRates", namespace = Namespace.NS)
	@XmlElement(name = "RoomRate", namespace = Namespace.NS)
	private List<RoomRate> roomRates;

	@XmlElementWrapper(name = "GuestCounts", namespace = Namespace.NS)
	@XmlElement(name = "GuestCount", namespace = Namespace.NS)
	private List<GuestCount> guestCounts;

	@XmlElement(name = "TPA_Extensions", namespace = Namespace.NS)
	private TPAExtension tpaExtension;

	@XmlElement(name = "BasicPropertyInfo", namespace = Namespace.NS)
	private BasicPropertyInfo basicPropertyInfo;



	public List<RoomType> getRoomTypes() {
		if (null == roomTypes) {
			roomTypes = new ArrayList<>();
		}
		return roomTypes;
	}

	public void setRoomTypes(List<RoomType> roomTypes) {
		this.roomTypes = roomTypes;
	}

	public List<RatePlan> getRatePlans() {
		if (null == ratePlans) {
			ratePlans = new ArrayList<RatePlan>();
		}
		return ratePlans;
	}

	public void setRatePlans(List<RatePlan> ratePlans) {
		this.ratePlans = ratePlans;
	}

	public List<RoomRate> getRoomRates() {
		if (null == roomRates) {
			roomRates = new ArrayList<RoomRate>();
		}
		return roomRates;
	}

	public void setRoomRates(List<RoomRate> roomRates) {
		this.roomRates = roomRates;
	}

	public List<GuestCount> getGuestCounts() {
		if (null == guestCounts) {
			guestCounts = new ArrayList<GuestCount>();
		}
		return guestCounts;
	}

	public void setGuestCounts(List<GuestCount> guestCounts) {
		this.guestCounts = guestCounts;
	}

	public TPAExtension getTpaExtension() {
		return tpaExtension;
	}

	public void setTpaExtension(TPAExtension tpaExtension) {
		this.tpaExtension = tpaExtension;
	}

	public BasicPropertyInfo getBasicPropertyInfo() {
		return basicPropertyInfo;
	}

	public void setBasicPropertyInfo(BasicPropertyInfo basicPropertyInfo) {
		this.basicPropertyInfo = basicPropertyInfo;
	}
}
