package com.channel.connector.ctripos.controller;

import com.alibaba.fastjson.JSON;
import com.channel.connector.ctripos.dto.ResponseDTO;
import com.channel.connector.ctripos.dto.request.CommonIncrementDTO;
import com.channel.connector.ctripos.enums.ErrorCodeEnum;
import com.channel.connector.ctripos.service.CtriposProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by 402931207@qq.com on 2018/12/13.
 */
@RestController
@Slf4j
@RequestMapping("/increment")
public class IncrementController extends BaseController {

    private boolean isPush = true;

    @Autowired
    private CtriposProductService productService;

    @RequestMapping(value = "/push", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO push(HttpServletRequest request) {

        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);

        if (!isPush) {
            log.info("同步开关当前状态为：不需要同步.");
            return responseDTO;
        }

        CommonIncrementDTO commonIncrementDTO = null;
        try {
            commonIncrementDTO = getRequestDTO(request, CommonIncrementDTO.class);
        } catch (IOException e) {
            log.error("读取请求参数IO异常", e);
            responseDTO.setErrorCode(ErrorCodeEnum.IO_EXCEPTION);
            return responseDTO;
        }

        log.info("/increment/push 请求参数：{}", JSON.toJSONString(commonIncrementDTO));

        if (StringUtils.isEmpty(commonIncrementDTO.getMerchantCode())) {
            log.error("推送增量参数校验不通过：商家编码为空！");
            responseDTO.setErrorCode(ErrorCodeEnum.INVALID_INPUTPARAM);
            return responseDTO;
        }

        try {
            responseDTO = productService.push(commonIncrementDTO);
        } catch (Exception e) {
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
            log.error("推送异常：{}", JSON.toJSONString(commonIncrementDTO), e);
        }

        return responseDTO;
    }

    @RequestMapping(value = "/on", method = RequestMethod.GET)
    @ResponseBody
    public ResponseDTO on() {
        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        isPush = true;
        responseDTO.setModel(isPush);
        return responseDTO;
    }

    @RequestMapping(value = "/off", method = RequestMethod.GET)
    @ResponseBody
    public ResponseDTO off() {
        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        isPush = false;
        responseDTO.setModel(isPush);
        return responseDTO;
    }
}
