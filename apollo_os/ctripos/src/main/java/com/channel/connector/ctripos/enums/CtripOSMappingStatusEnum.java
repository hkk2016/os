package com.channel.connector.ctripos.enums;

import java.io.Serializable;

/**
 * 携程酒店映射状态
 * @author ASUS
 *
 */
public enum CtripOSMappingStatusEnum implements Serializable {

	
	Pending(1,"映射中","酒店或房型信息已经成功接收到，但还未同步到携程系统"),
	Active(2,"映射成功","酒店或房型信息已经成功在携程系统生成"),
	Failed(3,"映射失败","同步失败，生成失败，下载失败等各种错误"),
	Deactivated(4,"映射已关闭","酒店或房型被关闭");

	public int key;
	public String value;
	
	public String message;

	private CtripOSMappingStatusEnum(int key, String value, String message) {
		this.key = key;
		this.value = value;
		this.message = message;
	}
	
	public static String getValueByKey(int key) {
		String value = null;
		for(CtripOSMappingStatusEnum ctripOSMappingStatusEnum : CtripOSMappingStatusEnum.values()) {
			if(ctripOSMappingStatusEnum.key == key) {
				value = ctripOSMappingStatusEnum.value;
				break;
			}
		}
		return value;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
