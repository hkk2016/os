package com.channel.connector.ctripos.dto.request;

import java.io.Serializable;
import java.util.List;

public class SyncCtripHotelRequest implements Serializable{

	private static final long serialVersionUID = -8057178739350948798L;
	
	/**
	 * 酒店ID
	 */
	private Long hotelId;

	
	private List<SyncCtripRoomTypeRequest> roomTypeVOs;

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public List<SyncCtripRoomTypeRequest> getRoomTypeVOs() {
		return roomTypeVOs;
	}

	public void setRoomTypeVOs(List<SyncCtripRoomTypeRequest> roomTypeVOs) {
		this.roomTypeVOs = roomTypeVOs;
	}
	
	
}
