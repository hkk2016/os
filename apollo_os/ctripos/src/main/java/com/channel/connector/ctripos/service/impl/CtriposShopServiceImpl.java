package com.channel.connector.ctripos.service.impl;

import com.channel.connector.ctripos.common.constant.InitData;
import com.channel.connector.ctripos.common.util.PropertyCopyUtil;
import com.channel.connector.ctripos.domain.CtriposShopInfoDO;
import com.channel.connector.ctripos.mapper.CtriposShopInfoMapper;
import com.channel.connector.ctripos.service.CtriposShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: zhanwang
 * @create: 2019-03-13 19:38
 **/
@Service
public class CtriposShopServiceImpl implements CtriposShopService {
    @Autowired
    private CtriposShopInfoMapper shopInfoMapper;

    @Override
    public void initShopInfoToMap() {
        Map<String, List<CtriposShopInfoDO>> merchantShopMap = new HashMap<>();
        Map<String, CtriposShopInfoDO> shopIdMap = new HashMap<>();
        Map<String, CtriposShopInfoDO> userPwdShopMap = new HashMap<>();
        List<CtriposShopInfoDO> shopInfoDTOList = this.queryAll();
        for (CtriposShopInfoDO shopInfoDTO : shopInfoDTOList) {
            String merchantCode = shopInfoDTO.getMerchantCode();
            if (merchantShopMap.containsKey(merchantCode)) {
                merchantShopMap.get(merchantCode).add(shopInfoDTO);
            } else {
                List<CtriposShopInfoDO> tempCtriposShopInfoDOList = new ArrayList<>();
                tempCtriposShopInfoDOList.add(shopInfoDTO);
                merchantShopMap.put(merchantCode, tempCtriposShopInfoDOList);
            }
            shopIdMap.put(shopInfoDTO.getId().toString(), shopInfoDTO);
            userPwdShopMap.put(shopInfoDTO.getApiId() + "-" + shopInfoDTO.getMessagePassword(), shopInfoDTO);
        }

        InitData.merchantShopMap = merchantShopMap;
        InitData.shopIdShopMap = shopIdMap;
        InitData.userPwdShopMap = userPwdShopMap;
    }

    public List<CtriposShopInfoDO> queryAll() {
        List<CtriposShopInfoDO> shopInfoDOList = shopInfoMapper.selectAll();
        return PropertyCopyUtil.transferArray(shopInfoDOList, CtriposShopInfoDO.class);
    }
}
