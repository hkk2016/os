package com.channel.connector.ctripos.dto.request;

import java.io.Serializable;
import java.util.List;

public class SyncCtripRoomTypeRequest implements Serializable{
	
	private static final long serialVersionUID = 7960546187914587635L;
	
	/*
	 */
	private Long roomTypeId;

	private List<SyncCtripCommodityRequest> commodityVOs;

	public Long getRoomTypeId() {
		return roomTypeId;
	}

	public void setRoomTypeId(Long roomTypeId) {
		this.roomTypeId = roomTypeId;
	}

	public List<SyncCtripCommodityRequest> getCommodityVOs() {
		return commodityVOs;
	}

	public void setCommodityVOs(List<SyncCtripCommodityRequest> commodityVOs) {
		this.commodityVOs = commodityVOs;
	}
	
	
}
