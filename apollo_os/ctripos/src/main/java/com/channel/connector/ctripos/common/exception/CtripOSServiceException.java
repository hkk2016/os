package com.channel.connector.ctripos.common.exception;

/**
 * @Description : 
 * @author : Zhiping Sun
 * @date : 2018年12月4日 上午9:52:37
 */
public class CtripOSServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CtripOSServiceException(String message) {
		super(message);
    }
	
	public CtripOSServiceException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
