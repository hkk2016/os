package com.channel.connector.ctripos.webservice.order.request.dto;


import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RequestorID", namespace = Namespace.NS)
public class RequestorID {
	
	@XmlAttribute(name = "ID")
	private String id;

	@XmlAttribute(name = "Type")
	private String type;
	
	@XmlAttribute(name = "MessagePassword")
	private String messagePassword;
	
	@XmlElement(name="CompanyName", namespace = Namespace.NS)
	private CompanyName companyName;
	
	
	
	public CompanyName getCompanyName() {
		return companyName;
	}

	public void setCompanyName(CompanyName companyName) {
		this.companyName = companyName;
	}

	public String getMessagePassword() {
		return messagePassword;
	}

	public void setMessagePassword(String messagePassword) {
		this.messagePassword = messagePassword;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
