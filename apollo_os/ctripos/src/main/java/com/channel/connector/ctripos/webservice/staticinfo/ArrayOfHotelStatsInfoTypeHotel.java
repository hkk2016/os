
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfHotelStatsInfoTypeHotel complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfHotelStatsInfoTypeHotel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Hotel" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BasicRooms" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfBasicRoomTypeBasicRoom" minOccurs="0"/>
 *                   &lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfErrorType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="HotelCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="CtripHotelCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfHotelStatsInfoTypeHotel", propOrder = {
    "hotel"
})
public class ArrayOfHotelStatsInfoTypeHotel {

    @XmlElement(name = "Hotel")
    protected List<Hotel> hotel;

    /**
     * Gets the value of the hotel property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotel property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotel().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfHotelStatsInfoTypeHotel.Hotel }
     *
     *
     */
    public List<Hotel> getHotel() {
        if (hotel == null) {
            hotel = new ArrayList<Hotel>();
        }
        return this.hotel;
    }

    public void setHotel(List<Hotel> hotel) {
        this.hotel = hotel;
    }

    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BasicRooms" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfBasicRoomTypeBasicRoom" minOccurs="0"/>
     *         &lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfErrorType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="HotelCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="CtripHotelCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Status" use="required" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basicRooms",
        "errors"
    })
    public static class Hotel {

        @XmlElement(name = "BasicRooms")
        protected ArrayOfBasicRoomTypeBasicRoom basicRooms;
        @XmlElement(name = "Errors")
        protected ArrayOfErrorType errors;
        @XmlAttribute(name = "HotelCode")
        protected String hotelCode;
        @XmlAttribute(name = "CtripHotelCode")
        protected String ctripHotelCode;
        @XmlAttribute(name = "Status", required = true)
        protected StatusEnum status;

        /**
         * 获取basicRooms属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfBasicRoomTypeBasicRoom }
         *     
         */
        public ArrayOfBasicRoomTypeBasicRoom getBasicRooms() {
            return basicRooms;
        }

        /**
         * 设置basicRooms属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfBasicRoomTypeBasicRoom }
         *     
         */
        public void setBasicRooms(ArrayOfBasicRoomTypeBasicRoom value) {
            this.basicRooms = value;
        }

        /**
         * 获取errors属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfErrorType }
         *     
         */
        public ArrayOfErrorType getErrors() {
            return errors;
        }

        /**
         * 设置errors属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfErrorType }
         *     
         */
        public void setErrors(ArrayOfErrorType value) {
            this.errors = value;
        }

        /**
         * 获取hotelCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCode() {
            return hotelCode;
        }

        /**
         * 设置hotelCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCode(String value) {
            this.hotelCode = value;
        }

        /**
         * 获取ctripHotelCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCtripHotelCode() {
            return ctripHotelCode;
        }

        /**
         * 设置ctripHotelCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCtripHotelCode(String value) {
            this.ctripHotelCode = value;
        }

        /**
         * 获取status属性的值。
         * 
         * @return
         *     possible object is
         *     {@link StatusEnum }
         *     
         */
        public StatusEnum getStatus() {
            return status;
        }

        /**
         * 设置status属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link StatusEnum }
         *     
         */
        public void setStatus(StatusEnum value) {
            this.status = value;
        }

    }

}
