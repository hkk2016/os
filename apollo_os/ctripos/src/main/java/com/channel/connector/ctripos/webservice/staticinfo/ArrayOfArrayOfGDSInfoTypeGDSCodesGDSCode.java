
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfArrayOfGDS_InfoTypeGDS_CodesGDS_Code complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfArrayOfGDS_InfoTypeGDS_CodesGDS_Code">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GDS_Codes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfGDS_InfoTypeGDS_CodesGDS_Code" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfArrayOfGDS_InfoTypeGDS_CodesGDS_Code", propOrder = {
    "gdsCodes"
})
public class ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode {

    @XmlElement(name = "GDS_Codes")
    protected List<ArrayOfGDSInfoTypeGDSCodesGDSCode> gdsCodes;

    /**
     * Gets the value of the gdsCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gdsCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGDSCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfGDSInfoTypeGDSCodesGDSCode }
     * 
     * 
     */
    public List<ArrayOfGDSInfoTypeGDSCodesGDSCode> getGDSCodes() {
        if (gdsCodes == null) {
            gdsCodes = new ArrayList<ArrayOfGDSInfoTypeGDSCodesGDSCode>();
        }
        return this.gdsCodes;
    }

}
