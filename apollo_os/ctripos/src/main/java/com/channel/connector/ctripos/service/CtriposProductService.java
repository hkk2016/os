package com.channel.connector.ctripos.service;

import com.channel.connector.ctripos.dto.ResponseDTO;
import com.channel.connector.ctripos.dto.request.CommonIncrementDTO;


public interface CtriposProductService {

    /**
     * 这里是个分发器，将增量的通知，分发到不同的service去。调用携程不同的接口进行推送。</br>
     * 各个service内部再查出实时的数据进行推送。
     *
     * @param commonIncrementDTO
     * @return
     */
    ResponseDTO push(CommonIncrementDTO commonIncrementDTO);

    /**
     * 推送价格库存到携程
     *
     * @param commonIncrementDTO
     * @return
     */
    ResponseDTO pushPriceAndRoomStatus(CommonIncrementDTO commonIncrementDTO);

}
