/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:32:12
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.Fee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Rate", namespace = Namespace.NS)
public class Rate {

	@XmlAttribute(name = "EffectiveDate")
	private String effectiveDate;
	
	@XmlAttribute(name = "ExpireDate")
	private String expireDate;
	
	@XmlElement(name = "Base", namespace = Namespace.NS)
	private Base base;
	
	@XmlElementWrapper(name = "Fees", namespace = Namespace.NS)
	@XmlElement(name = "Fee", namespace = Namespace.NS)
	private List<Fee> fees;

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public Base getBase() {
		return base;
	}

	public void setBase(Base base) {
		this.base = base;
	}

	public List<Fee> getFees() {
		return fees;
	}

	public void setFees(List<Fee> fees) {
		this.fees = fees;
	}

}
