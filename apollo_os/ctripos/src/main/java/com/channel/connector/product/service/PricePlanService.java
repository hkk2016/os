package com.channel.connector.product.service;

import com.channel.connector.ctripos.dto.request.IncrementDTO;
import com.channel.connector.product.domain.PriceAndQuotaDO;
import com.channel.connector.product.domain.PricePlanDO;
import com.channel.connector.product.domain.SaleStateDO;

import java.util.List;

/**
 * Created by 402931207@qq.com on 2018/12/14.
 */
public interface PricePlanService {

    /**
     * 根据价格计划ID查询价格计划基本信息
     *
     * @param pricePlanIdList
     * @return
     */
    List<PricePlanDO> queryByIds(List<Long> pricePlanIdList);

    /**
     * @param incrementDTO
     * @param merchantCode
     * @return
     */
    List<PriceAndQuotaDO> queryPriceAndQuota(IncrementDTO incrementDTO, String merchantCode);

    /**
     * 查询channelCode渠道上下架列表
     *
     * @param pricePlanIdList
     * @return
     */
    List<SaleStateDO> queryOnsale(List<Long> pricePlanIdList, String channelCode, Integer saleState);

}
