package com.channel.connector.ctripos.common.util;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 * Created by 402931207@qq.com on 2018/12/24.
 */
@Slf4j
public class CurrencyUtil {

    private static final int YUAN_TO_FEN_UNIT = 100;

    public static Integer conversionYuanToFen(BigDecimal yuan){
        log.info("enter 单元换算,入参{}",yuan);
        Integer tempYuan = 0;
        if (null != yuan){
            tempYuan = yuan.intValue();
            log.info("单位换算，传入{}元",tempYuan);
        }
        return YUAN_TO_FEN_UNIT * tempYuan;
    }
}
