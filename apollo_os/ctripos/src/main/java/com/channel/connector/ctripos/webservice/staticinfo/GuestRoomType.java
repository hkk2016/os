
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>GuestRoomType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="GuestRoomType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Quantities" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="MaxRollaways" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="MaxCribs" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Occupancy" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="MaxOccupancy" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="AgeQualifyingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Room" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="RoomTypeCode" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="RoomViewCode" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="NonSmoking" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="Floor" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="SizeMeasurement" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="RoomGender">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="Male"/>
 *                       &lt;enumeration value="Female"/>
 *                       &lt;enumeration value="MaleAndFemale"/>
 *                       &lt;enumeration value="Unknown"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RoomLevelFees" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Fee" type="{http://www.opentravel.org/OTA/2003/05}RoomFeeType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Amenities" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfGuestRoomTypeAmenity" minOccurs="0"/>
 *         &lt;element name="TPA_Extensions" type="{http://www.opentravel.org/OTA/2003/05}RoomInfoType" minOccurs="0"/>
 *         &lt;element name="Currency" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfParagraphTypeText" minOccurs="0"/>
 *         &lt;element name="LongDescription" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfParagraphTypeText" minOccurs="0"/>
 *         &lt;element name="ImageItems" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfImageItemsTypeImageItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GuestRoomType", propOrder = {
    "quantities",
    "occupancy",
    "room",
    "roomLevelFees",
    "amenities",
    "tpaExtensions",
    "currency",
    "description",
    "longDescription",
    "imageItems"
})
public class GuestRoomType {

    @XmlElement(name = "Quantities")
    protected GuestRoomType.Quantities quantities;
    @XmlElement(name = "Occupancy")
    protected List<Occupancy> occupancy;
    @XmlElement(name = "Room")
    protected GuestRoomType.Room room;
    @XmlElement(name = "RoomLevelFees")
    protected GuestRoomType.RoomLevelFees roomLevelFees;
    @XmlElement(name = "Amenities")
    protected ArrayOfGuestRoomTypeAmenity amenities;
    @XmlElement(name = "TPA_Extensions")
    protected RoomInfoType tpaExtensions;
    @XmlElement(name = "Currency")
    protected GuestRoomType.Currency currency;
    @XmlElement(name = "Description")
    protected ArrayOfParagraphTypeText description;
    @XmlElement(name = "LongDescription")
    protected ArrayOfParagraphTypeText longDescription;
    @XmlElement(name = "ImageItems")
    protected ArrayOfImageItemsTypeImageItem imageItems;

    /**
     * 获取quantities属性的值。
     *
     * @return
     *     possible object is
     *     {@link GuestRoomType.Quantities }
     *
     */
    public GuestRoomType.Quantities getQuantities() {
        return quantities;
    }

    /**
     * 设置quantities属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link GuestRoomType.Quantities }
     *
     */
    public void setQuantities(GuestRoomType.Quantities value) {
        this.quantities = value;
    }

    /**
     * Gets the value of the occupancy property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the occupancy property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOccupancy().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GuestRoomType.Occupancy }
     *
     *
     */
    public List<Occupancy> getOccupancy() {
        if (occupancy == null) {
            occupancy = new ArrayList<Occupancy>();
        }
        return this.occupancy;
    }

    public void setOccupancy(List<Occupancy> occupancy) {
        this.occupancy = occupancy;
    }

    /**
     * 获取room属性的值。
     *
     * @return
     *     possible object is
     *     {@link GuestRoomType.Room }
     *
     */
    public GuestRoomType.Room getRoom() {
        return room;
    }

    /**
     * 设置room属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link GuestRoomType.Room }
     *
     */
    public void setRoom(GuestRoomType.Room value) {
        this.room = value;
    }

    /**
     * 获取roomLevelFees属性的值。
     *
     * @return
     *     possible object is
     *     {@link GuestRoomType.RoomLevelFees }
     *
     */
    public GuestRoomType.RoomLevelFees getRoomLevelFees() {
        return roomLevelFees;
    }

    /**
     * 设置roomLevelFees属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link GuestRoomType.RoomLevelFees }
     *
     */
    public void setRoomLevelFees(GuestRoomType.RoomLevelFees value) {
        this.roomLevelFees = value;
    }

    /**
     * 获取amenities属性的值。
     *
     * @return
     *     possible object is
     *     {@link ArrayOfGuestRoomTypeAmenity }
     *
     */
    public ArrayOfGuestRoomTypeAmenity getAmenities() {
        return amenities;
    }

    /**
     * 设置amenities属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfGuestRoomTypeAmenity }
     *
     */
    public void setAmenities(ArrayOfGuestRoomTypeAmenity value) {
        this.amenities = value;
    }

    /**
     * 获取tpaExtensions属性的值。
     *
     * @return
     *     possible object is
     *     {@link RoomInfoType }
     *
     */
    public RoomInfoType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * 设置tpaExtensions属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link RoomInfoType }
     *
     */
    public void setTPAExtensions(RoomInfoType value) {
        this.tpaExtensions = value;
    }

    /**
     * 获取currency属性的值。
     *
     * @return
     *     possible object is
     *     {@link GuestRoomType.Currency }
     *
     */
    public GuestRoomType.Currency getCurrency() {
        return currency;
    }

    /**
     * 设置currency属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link GuestRoomType.Currency }
     *
     */
    public void setCurrency(GuestRoomType.Currency value) {
        this.currency = value;
    }

    /**
     * 获取description属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public ArrayOfParagraphTypeText getDescription() {
        return description;
    }

    /**
     * 设置description属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public void setDescription(ArrayOfParagraphTypeText value) {
        this.description = value;
    }

    /**
     * 获取longDescription属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public ArrayOfParagraphTypeText getLongDescription() {
        return longDescription;
    }

    /**
     * 设置longDescription属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public void setLongDescription(ArrayOfParagraphTypeText value) {
        this.longDescription = value;
    }

    /**
     * 获取imageItems属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfImageItemsTypeImageItem }
     *     
     */
    public ArrayOfImageItemsTypeImageItem getImageItems() {
        return imageItems;
    }

    /**
     * 设置imageItems属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfImageItemsTypeImageItem }
     *     
     */
    public void setImageItems(ArrayOfImageItemsTypeImageItem value) {
        this.imageItems = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Currency {

        @XmlAttribute(name = "Code")
        protected String code;

        /**
         * 获取code属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * 设置code属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="MaxOccupancy" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="AgeQualifyingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Occupancy {

        /**
         * 最小入住人数
         */
        @XmlAttribute(name = "MinOccupancy")
        protected Integer minOccupancy;

        /**
         * 最大入住人数
         */
        @XmlAttribute(name = "MaxOccupancy", required = true)
        protected int maxOccupancy;

        /**
         * 入住是否成人
         */
        @XmlAttribute(name = "AgeQualifyingCode")
        protected String ageQualifyingCode;

        /**
         * 获取minOccupancy属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMinOccupancy() {
            return minOccupancy;
        }

        /**
         * 设置minOccupancy属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMinOccupancy(Integer value) {
            this.minOccupancy = value;
        }

        /**
         * 获取maxOccupancy属性的值。
         * 
         */
        public int getMaxOccupancy() {
            return maxOccupancy;
        }

        /**
         * 设置maxOccupancy属性的值。
         * 
         */
        public void setMaxOccupancy(int value) {
            this.maxOccupancy = value;
        }

        /**
         * 获取ageQualifyingCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAgeQualifyingCode() {
            return ageQualifyingCode;
        }

        /**
         * 设置ageQualifyingCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAgeQualifyingCode(String value) {
            this.ageQualifyingCode = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="MaxRollaways" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="MaxCribs" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Quantities {
        /**
         * 可加移动床数量
         */
        @XmlAttribute(name = "MaxRollaways")
        protected Integer maxRollaways;

        /**
         * 可加婴儿床数量
         */
        @XmlAttribute(name = "MaxCribs")
        protected Integer maxCribs;

        /**
         * 获取maxRollaways属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxRollaways() {
            return maxRollaways;
        }

        /**
         * 设置maxRollaways属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxRollaways(Integer value) {
            this.maxRollaways = value;
        }

        /**
         * 获取maxCribs属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxCribs() {
            return maxCribs;
        }

        /**
         * 设置maxCribs属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxCribs(Integer value) {
            this.maxCribs = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="RoomTypeCode" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="RoomViewCode" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="NonSmoking" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="Floor" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="SizeMeasurement" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="RoomGender">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="Male"/>
     *             &lt;enumeration value="Female"/>
     *             &lt;enumeration value="MaleAndFemale"/>
     *             &lt;enumeration value="Unknown"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Room {

        @XmlAttribute(name = "RoomTypeCode")
        protected Integer roomTypeCode;

        /**
         * 房间景观类型
         */
        @XmlAttribute(name = "RoomViewCode")
        protected Integer roomViewCode;

        /**
         * 房间数量
         */
        @XmlAttribute(name = "Quantity")
        protected Integer quantity;

        /**
         * 是否禁烟
         */
        @XmlAttribute(name = "NonSmoking")
        protected Boolean nonSmoking;

        /**
         * 所在楼层
         */
        @XmlAttribute(name = "Floor")
        protected String floor;

        /**
         * 房型面积
         */
        @XmlAttribute(name = "SizeMeasurement")
        protected String sizeMeasurement;

        /**
         * 房型性别限制
         *
         * Male
         * Female
         * MaleAndFemale
         */
        @XmlAttribute(name = "RoomGender")
        protected String roomGender;

        /**
         * 获取roomTypeCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRoomTypeCode() {
            return roomTypeCode;
        }

        /**
         * 设置roomTypeCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRoomTypeCode(Integer value) {
            this.roomTypeCode = value;
        }

        /**
         * 获取roomViewCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRoomViewCode() {
            return roomViewCode;
        }

        /**
         * 设置roomViewCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRoomViewCode(Integer value) {
            this.roomViewCode = value;
        }

        /**
         * 获取quantity属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * 设置quantity属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setQuantity(Integer value) {
            this.quantity = value;
        }

        /**
         * 获取nonSmoking属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonSmoking() {
            return nonSmoking;
        }

        /**
         * 设置nonSmoking属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonSmoking(Boolean value) {
            this.nonSmoking = value;
        }

        /**
         * 获取floor属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFloor() {
            return floor;
        }

        /**
         * 设置floor属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFloor(String value) {
            this.floor = value;
        }

        /**
         * 获取sizeMeasurement属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSizeMeasurement() {
            return sizeMeasurement;
        }

        /**
         * 设置sizeMeasurement属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSizeMeasurement(String value) {
            this.sizeMeasurement = value;
        }

        /**
         * 获取roomGender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoomGender() {
            return roomGender;
        }

        /**
         * 设置roomGender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoomGender(String value) {
            this.roomGender = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Fee" type="{http://www.opentravel.org/OTA/2003/05}RoomFeeType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fee"
    })
    public static class RoomLevelFees {

        @XmlElement(name = "Fee")
        protected RoomFeeType fee;

        /**
         * 获取fee属性的值。
         * 
         * @return
         *     possible object is
         *     {@link RoomFeeType }
         *     
         */
        public RoomFeeType getFee() {
            return fee;
        }

        /**
         * 设置fee属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link RoomFeeType }
         *     
         */
        public void setFee(RoomFeeType value) {
            this.fee = value;
        }

    }

}
