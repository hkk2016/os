
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfBasicRoomTypeBasicRoom complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBasicRoomTypeBasicRoom">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasicRoom" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RatePlans" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanShortTypeRatePlan" minOccurs="0"/>
 *                   &lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfErrorType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="RoomCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="CtripRoomCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBasicRoomTypeBasicRoom", propOrder = {
    "basicRoom"
})
public class ArrayOfBasicRoomTypeBasicRoom {

    @XmlElement(name = "BasicRoom")
    protected List<BasicRoom> basicRoom;

    /**
     * Gets the value of the basicRoom property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the basicRoom property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBasicRoom().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfBasicRoomTypeBasicRoom.BasicRoom }
     *
     *
     */
    public List<BasicRoom> getBasicRoom() {
        if (basicRoom == null) {
            basicRoom = new ArrayList<BasicRoom>();
        }
        return this.basicRoom;
    }

    public void setBasicRoom(List<BasicRoom> basicRoom) {
        this.basicRoom = basicRoom;
    }

    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RatePlans" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanShortTypeRatePlan" minOccurs="0"/>
     *         &lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfErrorType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="RoomCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="CtripRoomCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ratePlans",
        "errors"
    })
    public static class BasicRoom {

        @XmlElement(name = "RatePlans")
        protected ArrayOfRatePlanShortTypeRatePlan ratePlans;
        @XmlElement(name = "Errors")
        protected ArrayOfErrorType errors;
        @XmlAttribute(name = "RoomCode")
        protected String roomCode;
        @XmlAttribute(name = "CtripRoomCode")
        protected String ctripRoomCode;
        @XmlAttribute(name = "Status")
        protected StatusEnum status;

        /**
         * 获取ratePlans属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfRatePlanShortTypeRatePlan }
         *     
         */
        public ArrayOfRatePlanShortTypeRatePlan getRatePlans() {
            return ratePlans;
        }

        /**
         * 设置ratePlans属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfRatePlanShortTypeRatePlan }
         *     
         */
        public void setRatePlans(ArrayOfRatePlanShortTypeRatePlan value) {
            this.ratePlans = value;
        }

        /**
         * 获取errors属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfErrorType }
         *     
         */
        public ArrayOfErrorType getErrors() {
            return errors;
        }

        /**
         * 设置errors属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfErrorType }
         *     
         */
        public void setErrors(ArrayOfErrorType value) {
            this.errors = value;
        }

        /**
         * 获取roomCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoomCode() {
            return roomCode;
        }

        /**
         * 设置roomCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoomCode(String value) {
            this.roomCode = value;
        }

        /**
         * 获取ctripRoomCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCtripRoomCode() {
            return ctripRoomCode;
        }

        /**
         * 设置ctripRoomCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCtripRoomCode(String value) {
            this.ctripRoomCode = value;
        }

        /**
         * 获取status属性的值。
         * 
         * @return
         *     possible object is
         *     {@link StatusEnum }
         *     
         */
        public StatusEnum getStatus() {
            return status;
        }

        /**
         * 设置status属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link StatusEnum }
         *     
         */
        public void setStatus(StatusEnum value) {
            this.status = value;
        }

    }

}
