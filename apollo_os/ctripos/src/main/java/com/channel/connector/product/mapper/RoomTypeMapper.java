package com.channel.connector.product.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.RoomTypeDO;

/**
 * @author ASUS
 */

public interface RoomTypeMapper extends BaseMapper<RoomTypeDO> {

}
