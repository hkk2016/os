/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:04:58
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Tax {

	@XmlAttribute(name = "Type")
	private String type;

	@XmlAttribute(name = "CurrencyCode")
	private String currencyCode;

	@XmlAttribute(name = "Percent")
	private Integer percent;

	@XmlAttribute(name = "ChargeUnit")
	private Integer chargeUnit;

	@XmlAttribute(name = "Code")
	private Integer code;
	
	@XmlElement(name = "TaxDescription", namespace = Namespace.NS)
	private TaxDescription TaxDescription;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getPercent() {
		return percent;
	}

	public void setPercent(Integer percent) {
		this.percent = percent;
	}

	public Integer getChargeUnit() {
		return chargeUnit;
	}

	public void setChargeUnit(Integer chargeUnit) {
		this.chargeUnit = chargeUnit;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public TaxDescription getTaxDescription() {
		return TaxDescription;
	}

	public void setTaxDescription(TaxDescription taxDescription) {
		TaxDescription = taxDescription;
	}
}
