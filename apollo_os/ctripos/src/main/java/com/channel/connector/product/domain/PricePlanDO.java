package com.channel.connector.product.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_htlpro_priceplan")
public class PricePlanDO {
    /**
     * 价格计划ID
     */
    @Id
    @Column(name = "priceplan_id")
    private Integer priceplanId;

    /**
     * 酒店ID
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 房型ID
     */
    @Column(name = "room_type_id")
    private Integer roomTypeId;

    /**
     * 床型ID(0.单床 1.大床  2.双床 3.三床  4.四床)
     */
    private String bedtype;

    /**
     * 配额账户ID
     */
    @Column(name = "quotaaccount_id")
    private Integer quotaaccountId;

    /**
     * 价格计划名称
     */
    @Column(name = "priceplan_name")
    private String priceplanName;

    /**
     * 支付方式(1面付有佣金, 2预付, 3面付无佣金)
     */
    @Column(name = "pay_method")
    private Integer payMethod;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 低价币种
     */
    @Column(name = "base_currency")
    private String baseCurrency;

    /**
     * 是否有效(1有效  0无效)
     */
    @Column(name = "is_active")
    private Integer isActive;

    /**
     * 早餐类型(0无早  1单早   2双早   3 人头早)
     */
    @Column(name = "breakfast_type")
    private Integer breakfastType;

    /**
     * 配额类型(1合约房,2配额房,3包房一,4包房二)
     */
    @Column(name = "quota_type")
    private Integer quotaType;

    /**
     * 供应方式(1 商家自签，2 系统直连 )
     */
    @Column(name = "supply_way")
    private Integer supplyWay;

    /**
     * 供应商编码
     */
    @Column(name = "supply_code")
    private String supplyCode;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 取消政策
     */
    @Column(name = "cancel_policy")
    private String cancelPolicy;

    /**
     * 是否含有附加项(1有  0没有)
     */
    @Column(name = "is_additional")
    private Boolean isAdditional;

    /**
     * 产品类型(1 散房  2 团房)
     */
    @Column(name = "product_type")
    private Integer productType;

    /**
     * 免房政策(1 全陪免半 2 8免半16兔1)
     */
    @Column(name = "free_room_policy")
    private String freeRoomPolicy;

    /**
     * 获取价格计划ID
     *
     * @return priceplan_id - 价格计划ID
     */
    public Integer getPriceplanId() {
        return priceplanId;
    }

    /**
     * 设置价格计划ID
     *
     * @param priceplanId 价格计划ID
     */
    public void setPriceplanId(Integer priceplanId) {
        this.priceplanId = priceplanId;
    }

    /**
     * 获取酒店ID
     *
     * @return hotel_id - 酒店ID
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店ID
     *
     * @param hotelId 酒店ID
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取房型ID
     *
     * @return room_type_id - 房型ID
     */
    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    /**
     * 设置房型ID
     *
     * @param roomTypeId 房型ID
     */
    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    /**
     * 获取床型ID(0.单床 1.大床  2.双床 3.三床  4.四床)
     *
     * @return bedtype - 床型ID(0.单床 1.大床  2.双床 3.三床  4.四床)
     */
    public String getBedtype() {
        return bedtype;
    }

    /**
     * 设置床型ID(0.单床 1.大床  2.双床 3.三床  4.四床)
     *
     * @param bedtype 床型ID(0.单床 1.大床  2.双床 3.三床  4.四床)
     */
    public void setBedtype(String bedtype) {
        this.bedtype = bedtype;
    }

    /**
     * 获取配额账户ID
     *
     * @return quotaaccount_id - 配额账户ID
     */
    public Integer getQuotaaccountId() {
        return quotaaccountId;
    }

    /**
     * 设置配额账户ID
     *
     * @param quotaaccountId 配额账户ID
     */
    public void setQuotaaccountId(Integer quotaaccountId) {
        this.quotaaccountId = quotaaccountId;
    }

    /**
     * 获取价格计划名称
     *
     * @return priceplan_name - 价格计划名称
     */
    public String getPriceplanName() {
        return priceplanName;
    }

    /**
     * 设置价格计划名称
     *
     * @param priceplanName 价格计划名称
     */
    public void setPriceplanName(String priceplanName) {
        this.priceplanName = priceplanName;
    }

    /**
     * 获取支付方式(1面付有佣金, 2预付, 3面付无佣金)
     *
     * @return pay_method - 支付方式(1面付有佣金, 2预付, 3面付无佣金)
     */
    public Integer getPayMethod() {
        return payMethod;
    }

    /**
     * 设置支付方式(1面付有佣金, 2预付, 3面付无佣金)
     *
     * @param payMethod 支付方式(1面付有佣金, 2预付, 3面付无佣金)
     */
    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return creator - 创建者
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建者
     *
     * @param creator 创建者
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取修改人
     *
     * @return modifier - 修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改人
     *
     * @param modifier 修改人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取低价币种
     *
     * @return base_currency - 低价币种
     */
    public String getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * 设置低价币种
     *
     * @param baseCurrency 低价币种
     */
    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    /**
     * 获取是否有效(1有效  0无效)
     *
     * @return is_active - 是否有效(1有效  0无效)
     */
    public Integer getIsActive() {
        return isActive;
    }

    /**
     * 设置是否有效(1有效  0无效)
     *
     * @param isActive 是否有效(1有效  0无效)
     */
    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    /**
     * 获取早餐类型(0无早  1单早   2双早   3 人头早)
     *
     * @return breakfast_type - 早餐类型(0无早  1单早   2双早   3 人头早)
     */
    public Integer getBreakfastType() {
        return breakfastType;
    }

    /**
     * 设置早餐类型(0无早  1单早   2双早   3 人头早)
     *
     * @param breakfastType 早餐类型(0无早  1单早   2双早   3 人头早)
     */
    public void setBreakfastType(Integer breakfastType) {
        this.breakfastType = breakfastType;
    }

    /**
     * 获取配额类型(1合约房,2配额房,3包房一,4包房二)
     *
     * @return quota_type - 配额类型(1合约房,2配额房,3包房一,4包房二)
     */
    public Integer getQuotaType() {
        return quotaType;
    }

    /**
     * 设置配额类型(1合约房,2配额房,3包房一,4包房二)
     *
     * @param quotaType 配额类型(1合约房,2配额房,3包房一,4包房二)
     */
    public void setQuotaType(Integer quotaType) {
        this.quotaType = quotaType;
    }

    /**
     * 获取供应方式(1 商家自签，2 系统直连 )
     *
     * @return supply_way - 供应方式(1 商家自签，2 系统直连 )
     */
    public Integer getSupplyWay() {
        return supplyWay;
    }

    /**
     * 设置供应方式(1 商家自签，2 系统直连 )
     *
     * @param supplyWay 供应方式(1 商家自签，2 系统直连 )
     */
    public void setSupplyWay(Integer supplyWay) {
        this.supplyWay = supplyWay;
    }

    /**
     * 获取供应商编码
     *
     * @return supply_code - 供应商编码
     */
    public String getSupplyCode() {
        return supplyCode;
    }

    /**
     * 设置供应商编码
     *
     * @param supplyCode 供应商编码
     */
    public void setSupplyCode(String supplyCode) {
        this.supplyCode = supplyCode;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取取消政策
     *
     * @return cancel_policy - 取消政策
     */
    public String getCancelPolicy() {
        return cancelPolicy;
    }

    /**
     * 设置取消政策
     *
     * @param cancelPolicy 取消政策
     */
    public void setCancelPolicy(String cancelPolicy) {
        this.cancelPolicy = cancelPolicy;
    }

    /**
     * 获取是否含有附加项(1有  0没有)
     *
     * @return is_additional - 是否含有附加项(1有  0没有)
     */
    public Boolean getIsAdditional() {
        return isAdditional;
    }

    /**
     * 设置是否含有附加项(1有  0没有)
     *
     * @param isAdditional 是否含有附加项(1有  0没有)
     */
    public void setIsAdditional(Boolean isAdditional) {
        this.isAdditional = isAdditional;
    }

    /**
     * 获取产品类型(1 散房  2 团房)
     *
     * @return product_type - 产品类型(1 散房  2 团房)
     */
    public Integer getProductType() {
        return productType;
    }

    /**
     * 设置产品类型(1 散房  2 团房)
     *
     * @param productType 产品类型(1 散房  2 团房)
     */
    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    /**
     * 获取免房政策(1 全陪免半 2 8免半16兔1)
     *
     * @return free_room_policy - 免房政策(1 全陪免半 2 8免半16兔1)
     */
    public String getFreeRoomPolicy() {
        return freeRoomPolicy;
    }

    /**
     * 设置免房政策(1 全陪免半 2 8免半16兔1)
     *
     * @param freeRoomPolicy 免房政策(1 全陪免半 2 8免半16兔1)
     */
    public void setFreeRoomPolicy(String freeRoomPolicy) {
        this.freeRoomPolicy = freeRoomPolicy;
    }
}