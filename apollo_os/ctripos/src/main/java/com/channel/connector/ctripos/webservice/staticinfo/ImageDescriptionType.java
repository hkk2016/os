
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ImageDescriptionType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ImageDescriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImageFormat" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}ImageItemType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfParagraphTypeText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImageDescriptionType", propOrder = {
    "imageFormat",
    "description"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.ArrayOfImageItemsTypeImageItem.ImageItem.class,
    com.channel.connector.ctripos.webservice.staticinfo.ImageItemsType.ImageItem.class
})
public class ImageDescriptionType {

    @XmlElement(name = "ImageFormat")
    protected ImageDescriptionType.ImageFormat imageFormat;
    @XmlElement(name = "Description")
    protected ArrayOfParagraphTypeText description;

    /**
     * 获取imageFormat属性的值。
     *
     * @return
     *     possible object is
     *     {@link ImageDescriptionType.ImageFormat }
     *
     */
    public ImageDescriptionType.ImageFormat getImageFormat() {
        return imageFormat;
    }

    /**
     * 设置imageFormat属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link ImageDescriptionType.ImageFormat }
     *
     */
    public void setImageFormat(ImageDescriptionType.ImageFormat value) {
        this.imageFormat = value;
    }

    /**
     * 获取description属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public ArrayOfParagraphTypeText getDescription() {
        return description;
    }

    /**
     * 设置description属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public void setDescription(ArrayOfParagraphTypeText value) {
        this.description = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}ImageItemType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ImageFormat
        extends ImageItemType
    {


    }

}
