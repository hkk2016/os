
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>HotelDescriptiveContentType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="HotelDescriptiveContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HotelInfo" type="{http://www.opentravel.org/OTA/2003/05}HotelInfoType" minOccurs="0"/>
 *         &lt;element name="Policies" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}PoliciesType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AffiliationInfo" type="{http://www.opentravel.org/OTA/2003/05}AffiliationInfoType" minOccurs="0"/>
 *         &lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/>
 *         &lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfContactInfoRootType" minOccurs="0"/>
 *         &lt;element name="GDS_Info" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfArrayOfGDS_InfoTypeGDS_CodesGDS_Code" minOccurs="0"/>
 *         &lt;element name="TPA_Extensions" type="{http://www.opentravel.org/OTA/2003/05}TPA_ExtensionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelDescriptiveContentType", propOrder = {
    "hotelInfo",
    "policies",
    "affiliationInfo",
    "multimediaDescriptions",
    "contactInfos",
    "gdsInfo",
    "tpaExtensions"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents.HotelDescriptiveContent.class
})
public class HotelDescriptiveContentType {

    @XmlElement(name = "HotelInfo")
    protected HotelInfoType hotelInfo;
    @XmlElement(name = "Policies")
    protected HotelDescriptiveContentType.Policies policies;
    @XmlElement(name = "AffiliationInfo")
    protected AffiliationInfoType affiliationInfo;
    @XmlElement(name = "MultimediaDescriptions")
    protected MultimediaDescriptionsType multimediaDescriptions;
    @XmlElement(name = "ContactInfos")
    protected ArrayOfContactInfoRootType contactInfos;
    @XmlElement(name = "GDS_Info")
    protected ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode gdsInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * 获取hotelInfo属性的值。
     *
     * @return
     *     possible object is
     *     {@link HotelInfoType }
     *
     */
    public HotelInfoType getHotelInfo() {
        return hotelInfo;
    }

    /**
     * 设置hotelInfo属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link HotelInfoType }
     *
     */
    public void setHotelInfo(HotelInfoType value) {
        this.hotelInfo = value;
    }

    /**
     * 获取policies属性的值。
     *
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType.Policies }
     *
     */
    public HotelDescriptiveContentType.Policies getPolicies() {
        return policies;
    }

    /**
     * 设置policies属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType.Policies }
     *
     */
    public void setPolicies(HotelDescriptiveContentType.Policies value) {
        this.policies = value;
    }

    /**
     * 获取affiliationInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AffiliationInfoType }
     *     
     */
    public AffiliationInfoType getAffiliationInfo() {
        return affiliationInfo;
    }

    /**
     * 设置affiliationInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AffiliationInfoType }
     *     
     */
    public void setAffiliationInfo(AffiliationInfoType value) {
        this.affiliationInfo = value;
    }

    /**
     * 获取multimediaDescriptions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public MultimediaDescriptionsType getMultimediaDescriptions() {
        return multimediaDescriptions;
    }

    /**
     * 设置multimediaDescriptions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public void setMultimediaDescriptions(MultimediaDescriptionsType value) {
        this.multimediaDescriptions = value;
    }

    /**
     * 获取contactInfos属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactInfoRootType }
     *     
     */
    public ArrayOfContactInfoRootType getContactInfos() {
        return contactInfos;
    }

    /**
     * 设置contactInfos属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactInfoRootType }
     *     
     */
    public void setContactInfos(ArrayOfContactInfoRootType value) {
        this.contactInfos = value;
    }

    /**
     * 获取gdsInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode }
     *     
     */
    public ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode getGDSInfo() {
        return gdsInfo;
    }

    /**
     * 设置gdsInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode }
     *     
     */
    public void setGDSInfo(ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode value) {
        this.gdsInfo = value;
    }

    /**
     * 获取tpaExtensions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * 设置tpaExtensions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}PoliciesType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Policies
        extends PoliciesType
    {


    }

}
