
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfRoomInfoTypeBedType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRoomInfoTypeBedType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BedType" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="BedTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="Length" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="Width" type="{http://www.w3.org/2001/XMLSchema}double" />
 *                 &lt;attribute name="CategoryCode" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRoomInfoTypeBedType", propOrder = {
    "bedType"
})
public class ArrayOfRoomInfoTypeBedType {

    @XmlElement(name = "BedType")
    protected List<BedType> bedType;

    /**
     * Gets the value of the bedType property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bedType property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBedType().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfRoomInfoTypeBedType.BedType }
     *
     *
     */
    public List<BedType> getBedType() {
        if (bedType == null) {
            bedType = new ArrayList<BedType>();
        }
        return this.bedType;
    }

    public void setBedType(List<BedType> bedType) {
        this.bedType = bedType;
    }

    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="BedTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="Length" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="Width" type="{http://www.w3.org/2001/XMLSchema}double" />
     *       &lt;attribute name="CategoryCode" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BedType {

        @XmlAttribute(name = "BedTypeCode")
        protected String bedTypeCode;
        @XmlAttribute(name = "Quantity")
        protected Integer quantity;
        @XmlAttribute(name = "Length")
        protected Double length;
        @XmlAttribute(name = "Width")
        protected Double width;
        @XmlAttribute(name = "CategoryCode")
        protected Integer categoryCode;

        /**
         * 获取bedTypeCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBedTypeCode() {
            return bedTypeCode;
        }

        /**
         * 设置bedTypeCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBedTypeCode(String value) {
            this.bedTypeCode = value;
        }

        /**
         * 获取quantity属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getQuantity() {
            return quantity;
        }

        /**
         * 设置quantity属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setQuantity(Integer value) {
            this.quantity = value;
        }

        /**
         * 获取length属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Double }
         *     
         */
        public Double getLength() {
            return length;
        }

        /**
         * 设置length属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Double }
         *     
         */
        public void setLength(Double value) {
            this.length = value;
        }

        /**
         * 获取width属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Double }
         *     
         */
        public Double getWidth() {
            return width;
        }

        /**
         * 设置width属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Double }
         *     
         */
        public void setWidth(Double value) {
            this.width = value;
        }

        /**
         * 获取categoryCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCategoryCode() {
            return categoryCode;
        }

        /**
         * 设置categoryCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCategoryCode(Integer value) {
            this.categoryCode = value;
        }

    }

}
