
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ContactInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ContactInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Names" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfContactsTypeName" minOccurs="0"/>
 *         &lt;element name="Addresses" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfAddressesTypeAddress" minOccurs="0"/>
 *         &lt;element name="Phones" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfPhonesTypePhone" minOccurs="0"/>
 *         &lt;element name="Emails" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInfoType", propOrder = {
    "names",
    "addresses",
    "phones",
    "emails"
})
@XmlSeeAlso({
    ContactInfoRootType.class
})
public class ContactInfoType {

    @XmlElement(name = "Names")
    protected ArrayOfContactsTypeName names;

    @XmlElement(name = "Addresses")
    protected ArrayOfAddressesTypeAddress addresses;

    @XmlElement(name = "Phones")
    protected ArrayOfPhonesTypePhone phones;

    @XmlElement(name = "Emails")
    protected ArrayOfString emails;

    /**
     * 获取names属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactsTypeName }
     *     
     */
    public ArrayOfContactsTypeName getNames() {
        return names;
    }

    /**
     * 设置names属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactsTypeName }
     *     
     */
    public void setNames(ArrayOfContactsTypeName value) {
        this.names = value;
    }

    /**
     * 获取addresses属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAddressesTypeAddress }
     *     
     */
    public ArrayOfAddressesTypeAddress getAddresses() {
        return addresses;
    }

    /**
     * 设置addresses属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAddressesTypeAddress }
     *     
     */
    public void setAddresses(ArrayOfAddressesTypeAddress value) {
        this.addresses = value;
    }

    /**
     * 获取phones属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPhonesTypePhone }
     *     
     */
    public ArrayOfPhonesTypePhone getPhones() {
        return phones;
    }

    /**
     * 设置phones属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPhonesTypePhone }
     *     
     */
    public void setPhones(ArrayOfPhonesTypePhone value) {
        this.phones = value;
    }

    /**
     * 获取emails属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getEmails() {
        return emails;
    }

    /**
     * 设置emails属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setEmails(ArrayOfString value) {
        this.emails = value;
    }

}
