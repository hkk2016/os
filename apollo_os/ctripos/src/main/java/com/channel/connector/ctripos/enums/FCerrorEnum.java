package com.channel.connector.ctripos.enums;

/**
 * 房仓错误类型
 */
public enum FCerrorEnum {
	
	FC_ERROR_0("10","","未知"),
	FC_ERROR_1("1","param resolve failure","参数无法解析"),
	FC_ERROR_2("2","property is validate or null","属性未空或者不合法"),
	FC_ERROR_3("3","start、end is invalid","入店离店日期不合法"),
	FC_ERROR_4("4","exception accur in Checking Availability","可定检查过程中发生异常"),
	FC_ERROR_5("5","exception accur in booking","下单过程中发生异常"),
	FC_ERROR_6("6","exception accur in canceling order","取消单过程中发生异常"),
	FC_ERROR_7("7","Mandatory booking details missing","下单必须的字段缺失"),
	FC_ERROR_8("8","No mappinged comodities","没有匹配的价格计划"),
	FC_ERROR_9("9","Currency error.","币种不对"),
	FC_ERROR_10("10","Comodity detail no found","商品信息没找到"),
	FC_ERROR_11("11","PreBooking return null","试预定返回为空"),
	FC_ERROR_12("12","the number of personName  less than numberOfUnits","入住人数量需要大约等于房间数"),
	FC_ERROR_13("13","uniqueID is null","入住人数量需要大约等于房间数"),
	FC_ERROR_14("14","unique type expected 501 or ID is null","订单必须是预付"),
	FC_ERROR_15("15","ResID_Type expected 501 or ResID_Value is null ","订单必须是预付"),
	FC_ERROR_16("16","ResID_Value should equels to unique id","ResID_Value需和unique id保持一致"),
	FC_ERROR_17("17","hotelCode is null","hotelCode不能为空！"),
	FC_ERROR_18("18","unique type is null","unique type不能为空！"),
	FC_ERROR_19("19","EffectiveDate  greater than ExpireDate","EffectiveDate 不能大于ExpireDate"),
	FC_ERROR_20("20","AmountAfterTax is null","AmountAfterTax不能为空"),
	FC_ERROR_21("21","EffectiveDate  or ExpireDate invalid format","EffectiveDate 或 ExpireDate格式不对！"),
	FC_ERROR_22("22","PrepaidIndicator should true","PrepaidIndicator应为true"),
	FC_ERROR_23("23","RatePlanCode is null","RatePlanCode 不能为空"),
	FC_ERROR_24("24","NumberOfUnits is null","NumberOfUnits 不能为空"),
	FC_ERROR_25("25","start,end not match effectiveDate,expireDate！","start,end 与 effectiveDate,expireDate不匹配！"),
	FC_ERROR_26("26","the order has canceled","该订单已被取消，请重新下单！"),
	FC_ERROR_27("27","rate code is not exist","商品id不存在"),
	FC_ERROR_28("28","hotelCode is invalid","酒店code不合法"),
	FC_ERROR_29("29","givenName or serName is not  english","givenName or serName非英文"),
	FC_ERROR_30("30","start greater than end","start应小于end"),
	FC_ERROR_31("31","start and end invalid format  expected yyyy-MM-dd","start and end格式不对"),
	FC_ERROR_32("32","the currency of total is invalid","币种不合法"),
	FC_ERROR_33("33","total AmountAfterTax not equals to the sum of rate AmountAfterTax","总价与割明细价格之和不等"),
	FC_ERROR_34("34","invalid rate or  roomrate","rate或roomrate为空"),
	FC_ERROR_35("35","the uniqueID is null","订单确认码为空"),
	FC_ERROR_36("36","the Reservation is not exist","订单不存在"),
	FC_ERROR_37("37","the start or end invalid fomate","开始或结束日期格式不对"),
	FC_ERROR_38("38","roomtypeCode is null","房型编码为空"),
	FC_ERROR_39("39","contactPerson is null","联系人为空"),
	FC_ERROR_40("40","email is null","联系人邮箱为空"),
	FC_ERROR_41("41","phone number  is null","联系人电话为空"),
	FC_ERROR_42("42","the total currency not agreement with the currency of rate","总额的币种与各价格明细的币种不一致"),
	FC_ERROR_43("43","the parameter resolve failed","参数解析失败"),
	FC_ERROR_44("44","not Immediate check!","非即时确认！"),
	FC_ERROR_45("45","NumberOfUnits is null  or Non numeric","NumberOfUnits无效"),
	FC_ERROR_46("46","givenName  or  surName is null","givenName  or  surName 为空"),
	FC_ERROR_47("47","RatePlanCategory expect 501","RatePlanCategory 应为501"),
	FC_ERROR_48("48","the order can not be canceled!","订单不能被取消"),
	FC_ERROR_49("49","exception accur in query order!","查询订单过程中发生异常");

	private  String code;
	
	private  String name;
	
	private  String desc;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private FCerrorEnum(String code, String name, String desc) {
		this.code = code;
		this.name = name;
		this.desc = desc;
	}
	
	public FCerrorEnum getFCerrorEnumByCode(String code){
		for(FCerrorEnum enum0:FCerrorEnum.values()){
			if(code.equals(enum0.code)){
				return enum0;
			}
		}
		return FC_ERROR_0;
	}
	

}
