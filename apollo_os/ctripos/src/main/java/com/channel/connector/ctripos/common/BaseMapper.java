package com.channel.connector.ctripos.common;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @Auther: Vinney
 * @Date: 2018/11/13 10:26
 * @Description:
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
