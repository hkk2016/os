package com.channel.connector.ctripos.dto.request;

import com.channel.connector.ctripos.dto.CtriposBaseRequestDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by 402931207@qq.com on 2018/12/12.
 */
@Data
public class CommonIncrementDTO extends CtriposBaseRequestDTO implements Serializable {

    /**
     * 增量数据
     */
    private List<IncrementDTO> incrementDTOList;

    /**
     * 本次增量类型
     */
    private IncrementType incrementType;

}
