package com.channel.connector.ctripos.enums;

public enum UniqueIDEnum {
	
	UNIQUEID_501("501","Ctrip Reservation order ID"),
	UNIQUEID_502("502","Hotel Confirmation number"),
	UNIQUEID_504("504","Order reference Number");

	public String key;
	public String value;

	private UniqueIDEnum(String key, String value){
		this.key=key;
		this.value=value;
	}

}
