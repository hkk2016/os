package com.channel.connector.ctripos.common.util;

import com.thoughtworks.xstream.converters.SingleValueConverter;

public class EmptyDoubleConverter implements SingleValueConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class arg0) {
		if(arg0.equals(Double.class)){
			return true;
		}
		return false;
	}

	@Override
	public Object fromString(String arg0) {
		if("".equals(arg0) || null == arg0){
			return null;
		}
		return Double.parseDouble(arg0);
	}

	@Override
	public String toString(Object arg0) {
		return String.valueOf(arg0);
	}



}
