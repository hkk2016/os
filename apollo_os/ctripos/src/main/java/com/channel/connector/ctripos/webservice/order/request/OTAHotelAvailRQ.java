/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:22:26
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.request.base.RQHeader;
import com.channel.connector.ctripos.webservice.order.request.dto.AvailRequestSegment;
import com.channel.connector.ctripos.webservice.order.request.dto.Pos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_HotelAvailRQ", namespace = Namespace.NS)
public class OTAHotelAvailRQ extends RQHeader {

	@XmlElement(name = "POS", namespace = Namespace.NS)
	private Pos pos;

	@XmlElementWrapper(name = "AvailRequestSegments", namespace = Namespace.NS)
	@XmlElement(name = "AvailRequestSegment", namespace = Namespace.NS)
	private List<AvailRequestSegment> availRequestSegments;

	public Pos getPos() {
		return pos;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public List<AvailRequestSegment> getAvailRequestSegments() {
		if (null == availRequestSegments) {
			availRequestSegments = new ArrayList<AvailRequestSegment>();
		}
		return availRequestSegments;
	}

	public void setAvailRequestSegments(
			List<AvailRequestSegment> availRequestSegments) {
		this.availRequestSegments = availRequestSegments;
	}

}
