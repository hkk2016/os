
package com.channel.connector.ctripos.webservice.confirmnum;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "HotelResNumUpdate", targetNamespace = "http://www.opentravel.org/OTA/2003/05", wsdlLocation = "http://vendor-ctrip.fws.ctripqa.com/Hotel/OTAReceive/HotelResNumUpdate.asmx?WSDL")
public class HotelResNumUpdate
    extends Service
{

    private final static URL HOTELRESNUMUPDATE_WSDL_LOCATION;
    private final static WebServiceException HOTELRESNUMUPDATE_EXCEPTION;
    private final static QName HOTELRESNUMUPDATE_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "HotelResNumUpdate");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://vendor-ctrip.fws.ctripqa.com/Hotel/OTAReceive/HotelResNumUpdate.asmx?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        HOTELRESNUMUPDATE_WSDL_LOCATION = url;
        HOTELRESNUMUPDATE_EXCEPTION = e;
    }

    public HotelResNumUpdate() {
        super(__getWsdlLocation(), HOTELRESNUMUPDATE_QNAME);
    }

    public HotelResNumUpdate(WebServiceFeature... features) {
        super(__getWsdlLocation(), HOTELRESNUMUPDATE_QNAME, features);
    }

    public HotelResNumUpdate(URL wsdlLocation) {
        super(wsdlLocation, HOTELRESNUMUPDATE_QNAME);
    }

    public HotelResNumUpdate(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, HOTELRESNUMUPDATE_QNAME, features);
    }

    public HotelResNumUpdate(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public HotelResNumUpdate(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns HotelResNumUpdateSoap
     */
    @WebEndpoint(name = "HotelResNumUpdateSoap")
    public HotelResNumUpdateSoap getHotelResNumUpdateSoap() {
        return super.getPort(new QName("http://www.opentravel.org/OTA/2003/05", "HotelResNumUpdateSoap"), HotelResNumUpdateSoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns HotelResNumUpdateSoap
     */
    @WebEndpoint(name = "HotelResNumUpdateSoap")
    public HotelResNumUpdateSoap getHotelResNumUpdateSoap(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.opentravel.org/OTA/2003/05", "HotelResNumUpdateSoap"), HotelResNumUpdateSoap.class, features);
    }

    /**
     *
     * @return
     *     returns HotelResNumUpdateSoap
     */
    @WebEndpoint(name = "HotelResNumUpdateSoap12")
    public HotelResNumUpdateSoap getHotelResNumUpdateSoap12() {
        return super.getPort(new QName("http://www.opentravel.org/OTA/2003/05", "HotelResNumUpdateSoap12"), HotelResNumUpdateSoap.class);
    }

    /**
     *
     * @param features
     *     A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns HotelResNumUpdateSoap
     */
    @WebEndpoint(name = "HotelResNumUpdateSoap12")
    public HotelResNumUpdateSoap getHotelResNumUpdateSoap12(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.opentravel.org/OTA/2003/05", "HotelResNumUpdateSoap12"), HotelResNumUpdateSoap.class, features);
    }

    private static URL __getWsdlLocation() {
        if (HOTELRESNUMUPDATE_EXCEPTION!= null) {
            throw HOTELRESNUMUPDATE_EXCEPTION;
        }
        return HOTELRESNUMUPDATE_WSDL_LOCATION;
    }

}
