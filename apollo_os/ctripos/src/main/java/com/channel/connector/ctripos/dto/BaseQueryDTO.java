package com.channel.connector.ctripos.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 40293 on 2018/11/29.
 */
@Data
public class BaseQueryDTO implements Serializable {
    private static final long serialVersionUID = 3257503600368684953L;

    /**
     * 当前页 默认1
     */
    private int currentPage = 1;

    /**
     * 页面记录数
     */
    private int pageSize = 10;
}
