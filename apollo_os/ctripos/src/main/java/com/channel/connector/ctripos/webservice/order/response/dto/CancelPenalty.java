/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:21:56
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CancelPenalty", namespace = Namespace.NS)
public class CancelPenalty {

	@XmlAttribute(name = "PolicyCode")
	private String policyCode;
	
	@XmlElement(name = "PenaltyDescription", namespace = Namespace.NS)
	private PenaltyDescription penaltyDescription;
	
	@XmlElement(name = "AmountPercent", namespace = Namespace.NS)
	private AmountPercent AmountPercent;
	
	@XmlElement(name = "Deadline", namespace = Namespace.NS)
	private Deadline Deadline;

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public PenaltyDescription getPenaltyDescription() {
		return penaltyDescription;
	}

	public void setPenaltyDescription(PenaltyDescription penaltyDescription) {
		this.penaltyDescription = penaltyDescription;
	}

	public AmountPercent getAmountPercent() {
		return AmountPercent;
	}

	public void setAmountPercent(AmountPercent amountPercent) {
		AmountPercent = amountPercent;
	}

	public Deadline getDeadline() {
		return Deadline;
	}

	public void setDeadline(Deadline deadline) {
		Deadline = deadline;
	}
	
}
