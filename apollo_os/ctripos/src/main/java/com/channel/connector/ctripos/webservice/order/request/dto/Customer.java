package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Customer", namespace = Namespace.NS)
public class Customer {
	@XmlElement(name = "PersonName", namespace = Namespace.NS)
	private List<PersonName> personNames;
	@XmlElement(name="ContactPerson", namespace = Namespace.NS)
	private ContactPerson contactPerson;
	
	public List<PersonName> getPersonNames() {
		return personNames;
	}
	public void setPersonNames(List<PersonName> personNames) {
		this.personNames = personNames;
	}
	public ContactPerson getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}
	

}
