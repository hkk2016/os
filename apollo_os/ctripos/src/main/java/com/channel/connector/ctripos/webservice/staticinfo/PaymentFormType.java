
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>PaymentFormType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PaymentFormType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentCard" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="CardCode" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentFormType", propOrder = {
    "paymentCard"
})
@XmlSeeAlso({
    PaymentDetailType.class,
    com.channel.connector.ctripos.webservice.staticinfo.ArrayOfAcceptedPaymentsTypeAcceptedPayment.AcceptedPayment.class
})
public class PaymentFormType {

    @XmlElement(name = "PaymentCard")
    protected PaymentFormType.PaymentCard paymentCard;

    /**
     * 获取paymentCard属性的值。
     *
     * @return
     *     possible object is
     *     {@link PaymentFormType.PaymentCard }
     *
     */
    public PaymentFormType.PaymentCard getPaymentCard() {
        return paymentCard;
    }

    /**
     * 设置paymentCard属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link PaymentFormType.PaymentCard }
     *
     */
    public void setPaymentCard(PaymentFormType.PaymentCard value) {
        this.paymentCard = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="CardCode" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PaymentCard {

        /**
         * 酒店支持的信用卡类型
         */
        @XmlAttribute(name = "CardCode", required = true)
        protected int cardCode;

        /**
         * 获取cardCode属性的值。
         * 
         */
        public int getCardCode() {
            return cardCode;
        }

        /**
         * 设置cardCode属性的值。
         * 
         */
        public void setCardCode(int value) {
            this.cardCode = value;
        }

    }

}
