package com.channel.connector.ctripos.controller;

import com.alibaba.fastjson.JSON;
import com.channel.connector.ctripos.dto.ResponseDTO;
import com.channel.connector.ctripos.dto.request.BatchSyncCtripRequest;
import com.channel.connector.ctripos.dto.request.CommonIncrementDTO;
import com.channel.connector.ctripos.dto.request.PushConfirmNoRequest;
import com.channel.connector.ctripos.enums.ErrorCodeEnum;
import com.channel.connector.ctripos.service.CtriposMappingService;
import com.channel.connector.ctripos.service.CtriposProductService;
import com.channel.connector.ctripos.service.OrderSyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 提供给BMS调用
 */
@RestController
@Slf4j
@RequestMapping("/bms")
public class BmsController extends BaseController {

    @Autowired
    private CtriposMappingService ctriposMappingService;

    @Autowired
    private CtriposProductService ctriposProductService;

    @Autowired
    private OrderSyncService orderSyncService;

    @RequestMapping(value = "/hotel/push", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO pushHotel(@RequestBody BatchSyncCtripRequest batchSyncCtripRequest) {

        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        log.info("/bms/hotel/push开始，请求参数：{}，返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSONString(responseDTO));

        try {
            if (invalidShopId(batchSyncCtripRequest)) {
                log.error("/bms/hotel/push无效的店铺,请求参数：{}", JSON.toJSONString(batchSyncCtripRequest));
                responseDTO.setErrorCode(ErrorCodeEnum.INVALID_INPUTPARAM);
                responseDTO.setFailReason("无效的店铺");
                return responseDTO;
            }

            responseDTO = ctriposMappingService.pushHotel(batchSyncCtripRequest);

        } catch (Exception e) {
            log.error("/bms/hotel/push 异常，请求参数{}", JSON.toJSONString(batchSyncCtripRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/bms/hotel/push完成，请求参数：{}，返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSONString(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/hotel/sync", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO syncHotel(@RequestBody BatchSyncCtripRequest batchSyncCtripRequest) {

        ResponseDTO responseDTO = new ResponseDTO(ErrorCodeEnum.SUCCESS);
        log.info("/bms/hotel/sync开始，请求参数：{}，返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSONString(responseDTO));

        try {
            if (invalidShopId(batchSyncCtripRequest)) {
                log.error("/bms/hotel/sync无效的店铺,请求参数：{}", JSON.toJSONString(batchSyncCtripRequest));
                responseDTO.setErrorCode(ErrorCodeEnum.INVALID_INPUTPARAM);
                responseDTO.setFailReason("无效的店铺");
                return responseDTO;
            }

            responseDTO = ctriposMappingService.syncHotel(batchSyncCtripRequest);

        } catch (Exception e) {
            log.error("/bms/hotel/sync 异常，请求参数{}", JSON.toJSONString(batchSyncCtripRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/bms/hotel/sync完成，请求参数：{}，返回：{}", JSON.toJSONString(batchSyncCtripRequest), JSON.toJSONString(responseDTO));
        return responseDTO;
    }

    @RequestMapping(value = "/sync/pricestatus", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO syncPriceAndRoomStatus(HttpServletRequest request) {

        ResponseDTO responseDTO = new ResponseDTO();
        CommonIncrementDTO commonIncrementDTO = null;
        try {

            commonIncrementDTO = getRequestDTO(request, CommonIncrementDTO.class);
            if (invalidShopId(commonIncrementDTO)) {
                log.error("/bms/sync/pricestatus 无效的店铺,请求参数：{}", JSON.toJSONString(commonIncrementDTO));
                responseDTO.setErrorCode(ErrorCodeEnum.INVALID_INPUTPARAM);
                responseDTO.setFailReason("无效的店铺");
                return responseDTO;
            }

            responseDTO = ctriposProductService.pushPriceAndRoomStatus(commonIncrementDTO);

        } catch (IOException e) {
            log.error("{} 读取请求数据时IO异常。", JSON.toJSONString(request.getServletPath()), e);
            responseDTO.setErrorCode(ErrorCodeEnum.IO_EXCEPTION);
        } catch (Exception e) {
            log.error("/bms/sync/pricestatus 异常，请求参数{}", JSON.toJSONString(commonIncrementDTO), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/bms/sync/pricestatus完成，请求参数：{}，返回：{}", JSON.toJSONString(commonIncrementDTO), JSON.toJSONString(responseDTO));
        return responseDTO;

    }

    @RequestMapping(value = "/order/pushConfirmNo", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public ResponseDTO pushConfirmNo(HttpServletRequest request) {

        ResponseDTO responseDTO = new ResponseDTO();
        PushConfirmNoRequest pushConfirmNoRequest = null;
        try {

            pushConfirmNoRequest = getRequestDTO(request, PushConfirmNoRequest.class);
            responseDTO = orderSyncService.pushConfirmNo(pushConfirmNoRequest);

        } catch (IOException e) {
            log.error("{} 读取请求数据时IO异常。", JSON.toJSONString(request.getServletPath()), e);
            responseDTO.setErrorCode(ErrorCodeEnum.IO_EXCEPTION);
        } catch (Exception e) {
            log.error("/bms/order/pushConfirmNo 异常，请求参数{}", JSON.toJSONString(pushConfirmNoRequest), e);
            responseDTO.setErrorCode(ErrorCodeEnum.SYSTEM_EXCEPTION);
        }
        log.info("/bms/order/pushConfirmNo完成，请求参数：{}，返回：{}", JSON.toJSONString(pushConfirmNoRequest), JSON.toJSONString(responseDTO));
        return responseDTO;

    }
}
