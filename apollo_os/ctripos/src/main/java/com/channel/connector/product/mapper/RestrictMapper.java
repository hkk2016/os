package com.channel.connector.product.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.RestrictDO;

public interface RestrictMapper extends BaseMapper<RestrictDO> {
}