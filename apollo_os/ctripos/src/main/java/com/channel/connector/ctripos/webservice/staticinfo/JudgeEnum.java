
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>JudgeEnum的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="JudgeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unkown"/>
 *     &lt;enumeration value="Possible"/>
 *     &lt;enumeration value="Yes"/>
 *     &lt;enumeration value="No"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "JudgeEnum")
@XmlEnum
public enum JudgeEnum {

    @XmlEnumValue("Unkown")
    UNKOWN("Unkown"),
    @XmlEnumValue("Possible")
    POSSIBLE("Possible"),
    @XmlEnumValue("Yes")
    YES("Yes"),
    @XmlEnumValue("No")
    NO("No");
    private final String value;

    JudgeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static JudgeEnum fromValue(String v) {
        for (JudgeEnum c: JudgeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
