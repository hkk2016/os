package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="HotelReservationID", namespace = Namespace.NS)
public class HotelReservationID {

	@XmlAttribute(name="ResID_Value")
	private String resID_Value;
	@XmlAttribute(name="ResID_Type")
	private String resID_Type;
	public String getResID_Value() {
		return resID_Value;
	}
	public void setResID_Value(String resID_Value) {
		this.resID_Value = resID_Value;
	}
	public String getResID_Type() {
		return resID_Type;
	}
	public void setResID_Type(String resID_Type) {
		this.resID_Type = resID_Type;
	}

}
