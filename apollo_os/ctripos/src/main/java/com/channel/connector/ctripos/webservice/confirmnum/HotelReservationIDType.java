
package com.channel.connector.ctripos.webservice.confirmnum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>HotelReservationIDType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="HotelReservationIDType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ResID_Value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ResID_Type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelReservationIDType")
public class HotelReservationIDType {

    @XmlAttribute(name = "ResID_Value")
    protected String resIDValue;
    @XmlAttribute(name = "ResID_Type")
    protected String resIDType;

    /**
     * 获取resIDValue属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResIDValue() {
        return resIDValue;
    }

    /**
     * 设置resIDValue属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResIDValue(String value) {
        this.resIDValue = value;
    }

    /**
     * 获取resIDType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResIDType() {
        return resIDType;
    }

    /**
     * 设置resIDType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResIDType(String value) {
        this.resIDType = value;
    }

}
