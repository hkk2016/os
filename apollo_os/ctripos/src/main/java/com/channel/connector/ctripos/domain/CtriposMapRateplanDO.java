package com.channel.connector.ctripos.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_ctripos_map_rateplan")
public class CtriposMapRateplanDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 酒店编码
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 酒店中文名
     */
    @Column(name = "hotel_name")
    private String hotelName;

    /**
     * 酒店英文名
     */
    @Column(name = "hotel_name_eng")
    private String hotelNameEng;

    /**
     * 房型编码
     */
    @Column(name = "room_type_id")
    private Integer roomTypeId;

    /**
     * 房型中文名
     */
    @Column(name = "room_type_name")
    private String roomTypeName;

    /**
     * 房型英文名
     */
    @Column(name = "room_type_name_eng")
    private String roomTypeNameEng;

    /**
     * 商品编码
     */
    @Column(name = "commodity_id")
    private Integer commodityId;

    /**
     * 商品中文名
     */
    @Column(name = "commodity_name")
    private String commodityName;

    /**
     * 商品英文名
     */
    @Column(name = "commodity_name_eng")
    private String commodityNameEng;

    /**
     * 携程子房型id
     */
    @Column(name = "ctrip_rateplan_id")
    private String ctripRateplanId;

    /**
     * 携程子房型中文名
     */
    @Column(name = "ctrip_rateplan_name")
    private String ctripRateplanName;

    /**
     * 携程子房型英文名
     */
    @Column(name = "ctrip_rateplan_name_eng")
    private String ctripRateplanNameEng;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    @Column(name = "map_status")
    private Integer mapStatus;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 更新时间
     */
    @Column(name = "modified_time")
    private Date modifiedTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 错误信息
     */
    @Column(name = "error_info")
    private String errorInfo;

    /**
     * 是否上架：1已上架，0未上架
     */
    @Column(name = "is_onsale")
    private Integer isOnsale;

    /**
     * 店铺id
     */
    @Column(name = "shop_id")
    private Integer shopId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店编码
     *
     * @return hotel_id - 酒店编码
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店编码
     *
     * @param hotelId 酒店编码
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取酒店中文名
     *
     * @return hotel_name - 酒店中文名
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * 设置酒店中文名
     *
     * @param hotelName 酒店中文名
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     * 获取酒店英文名
     *
     * @return hotel_name_eng - 酒店英文名
     */
    public String getHotelNameEng() {
        return hotelNameEng;
    }

    /**
     * 设置酒店英文名
     *
     * @param hotelNameEng 酒店英文名
     */
    public void setHotelNameEng(String hotelNameEng) {
        this.hotelNameEng = hotelNameEng;
    }

    /**
     * 获取房型编码
     *
     * @return room_type_id - 房型编码
     */
    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    /**
     * 设置房型编码
     *
     * @param roomTypeId 房型编码
     */
    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    /**
     * 获取房型中文名
     *
     * @return room_type_name - 房型中文名
     */
    public String getRoomTypeName() {
        return roomTypeName;
    }

    /**
     * 设置房型中文名
     *
     * @param roomTypeName 房型中文名
     */
    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    /**
     * 获取房型英文名
     *
     * @return room_type_name_eng - 房型英文名
     */
    public String getRoomTypeNameEng() {
        return roomTypeNameEng;
    }

    /**
     * 设置房型英文名
     *
     * @param roomTypeNameEng 房型英文名
     */
    public void setRoomTypeNameEng(String roomTypeNameEng) {
        this.roomTypeNameEng = roomTypeNameEng;
    }

    /**
     * 获取商品编码
     *
     * @return commodity_id - 商品编码
     */
    public Integer getCommodityId() {
        return commodityId;
    }

    /**
     * 设置商品编码
     *
     * @param commodityId 商品编码
     */
    public void setCommodityId(Integer commodityId) {
        this.commodityId = commodityId;
    }

    /**
     * 获取商品中文名
     *
     * @return commodity_name - 商品中文名
     */
    public String getCommodityName() {
        return commodityName;
    }

    /**
     * 设置商品中文名
     *
     * @param commodityName 商品中文名
     */
    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    /**
     * 获取商品英文名
     *
     * @return commodity_name_eng - 商品英文名
     */
    public String getCommodityNameEng() {
        return commodityNameEng;
    }

    /**
     * 设置商品英文名
     *
     * @param commodityNameEng 商品英文名
     */
    public void setCommodityNameEng(String commodityNameEng) {
        this.commodityNameEng = commodityNameEng;
    }

    /**
     * 获取携程子房型id
     *
     * @return ctrip_rateplan_id - 携程子房型id
     */
    public String getCtripRateplanId() {
        return ctripRateplanId;
    }

    /**
     * 设置携程子房型id
     *
     * @param ctripRateplanId 携程子房型id
     */
    public void setCtripRateplanId(String ctripRateplanId) {
        this.ctripRateplanId = ctripRateplanId;
    }

    /**
     * 获取携程子房型中文名
     *
     * @return ctrip_rateplan_name - 携程子房型中文名
     */
    public String getCtripRateplanName() {
        return ctripRateplanName;
    }

    /**
     * 设置携程子房型中文名
     *
     * @param ctripRateplanName 携程子房型中文名
     */
    public void setCtripRateplanName(String ctripRateplanName) {
        this.ctripRateplanName = ctripRateplanName;
    }

    /**
     * 获取携程子房型英文名
     *
     * @return ctrip_rateplan_name_eng - 携程子房型英文名
     */
    public String getCtripRateplanNameEng() {
        return ctripRateplanNameEng;
    }

    /**
     * 设置携程子房型英文名
     *
     * @param ctripRateplanNameEng 携程子房型英文名
     */
    public void setCtripRateplanNameEng(String ctripRateplanNameEng) {
        this.ctripRateplanNameEng = ctripRateplanNameEng;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @return map_status - 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public Integer getMapStatus() {
        return mapStatus;
    }

    /**
     * 设置映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @param mapStatus 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public void setMapStatus(Integer mapStatus) {
        this.mapStatus = mapStatus;
    }

    /**
     * 获取创建时间
     *
     * @return created_time - 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 设置创建时间
     *
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 获取创建人
     *
     * @return creater - 创建人
     */
    public String getCreater() {
        return creater;
    }

    /**
     * 设置创建人
     *
     * @param creater 创建人
     */
    public void setCreater(String creater) {
        this.creater = creater;
    }

    /**
     * 获取更新时间
     *
     * @return modified_time - 更新时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 设置更新时间
     *
     * @param modifiedTime 更新时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 获取更新人
     *
     * @return modifier - 更新人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置更新人
     *
     * @param modifier 更新人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取错误信息
     *
     * @return error_info - 错误信息
     */
    public String getErrorInfo() {
        return errorInfo;
    }

    /**
     * 设置错误信息
     *
     * @param errorInfo 错误信息
     */
    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    /**
     * 获取是否上架：1已上架，0未上架
     *
     * @return is_onsale - 是否上架：1已上架，0未上架
     */
    public Integer getIsOnsale() {
        return isOnsale;
    }

    /**
     * 设置是否上架：1已上架，0未上架
     *
     * @param isOnsale 是否上架：1已上架，0未上架
     */
    public void setIsOnsale(Integer isOnsale) {
        this.isOnsale = isOnsale;
    }

    /**
     * 获取店铺id
     *
     * @return shop_id - 店铺id
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 设置店铺id
     *
     * @param shopId 店铺id
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}