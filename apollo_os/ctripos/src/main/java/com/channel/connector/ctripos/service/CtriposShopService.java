package com.channel.connector.ctripos.service;

public interface CtriposShopService {

    /**
     * 初始化店铺信息
     */
    void initShopInfoToMap();

}
