package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.request.dto.SpecialRequest;
import com.channel.connector.ctripos.webservice.order.request.dto.TimeSpan;
import com.channel.connector.ctripos.webservice.order.request.dto.TpaExtensions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ResGlobalInfo", namespace = Namespace.NS)
public class ResGlobalInfo {
	@XmlElementWrapper(name="HotelReservationIDs", namespace = Namespace.NS)
	@XmlElement(name = "HotelReservationID", namespace = Namespace.NS)
	private List<HotelReservationID> hotelReservationIDs;
	
	@XmlElement(name = "Total", namespace = Namespace.NS)
	private Total total;
	
	@XmlElementWrapper(name="GuestCounts", namespace = Namespace.NS)
	@XmlElement(name = "GuestCount", namespace = Namespace.NS)
	private List<GuestCount> guestCounts;
	
	@XmlElement(name = "TimeSpan", namespace = Namespace.NS)
	private TimeSpan timeSpan;
	
	@XmlElementWrapper(name="SpecialRequests", namespace = Namespace.NS)
	@XmlElement(name = "SpecialRequest", namespace = Namespace.NS)
	private List<SpecialRequest> specialRequests;

	@XmlElement(name = "TPA_Extensions", namespace = Namespace.NS)
	private TpaExtensions tpaExtensions;

	public List<HotelReservationID> getHotelReservationIDs() {
		return hotelReservationIDs;
	}

	public void setHotelReservationIDs(List<HotelReservationID> hotelReservationIDs) {
		this.hotelReservationIDs = hotelReservationIDs;
	}

	public Total getTotal() {
		return total;
	}

	public void setTotal(Total total) {
		this.total = total;
	}

	public List<GuestCount> getGuestCounts() {
		if (null == guestCounts) {
			guestCounts = new ArrayList<GuestCount>();
		}
		return guestCounts;
	}

	public void setGuestCounts(List<GuestCount> guestCounts) {
		this.guestCounts = guestCounts;
	}

	public TimeSpan getTimeSpan() {
		return timeSpan;
	}

	public void setTimeSpan(TimeSpan timeSpan) {
		this.timeSpan = timeSpan;
	}

	public List<SpecialRequest> getSpecialRequests() {
		if (null == specialRequests) {
			specialRequests = new ArrayList<SpecialRequest>();
		}
		return specialRequests;
	}

	public void setSpecialRequests(List<SpecialRequest> specialRequests) {
		this.specialRequests = specialRequests;
	}

	public TpaExtensions getTpaExtensions() {
		return tpaExtensions;
	}

	public void setTpaExtensions(TpaExtensions tpaExtensions) {
		this.tpaExtensions = tpaExtensions;
	}
}
