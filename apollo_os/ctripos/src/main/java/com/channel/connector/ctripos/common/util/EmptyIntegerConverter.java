package com.channel.connector.ctripos.common.util;

import com.thoughtworks.xstream.converters.SingleValueConverter;

public class EmptyIntegerConverter implements SingleValueConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class arg0) {
		if(arg0.equals(Integer.class)){
			return true;
		}
		return false;
	}

	@Override
	public Object fromString(String arg0) {
		if("".equals(arg0) || null == arg0){
			return null;
		}
		return Integer.parseInt(arg0);
	}

	@Override
	public String toString(Object arg0) {
		return String.valueOf(arg0);
	}

}
