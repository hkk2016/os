package com.channel.connector.ctripos.service;

import com.channel.connector.ctripos.dto.ResponseDTO;
import com.channel.connector.ctripos.dto.request.PushConfirmNoRequest;
import com.channel.connector.ctripos.webservice.order.request.OTACancelRQ;
import com.channel.connector.ctripos.webservice.order.request.OTAHotelAvailRQ;
import com.channel.connector.ctripos.webservice.order.request.OTAHotelResRQ;
import com.channel.connector.ctripos.webservice.order.request.OTAReadRQ;
import com.channel.connector.ctripos.webservice.order.response.OTACancelRS;
import com.channel.connector.ctripos.webservice.order.response.OTAHotelAvailRS;
import com.channel.connector.ctripos.webservice.order.response.OTAHotelResRS;
import com.channel.connector.ctripos.webservice.order.response.OTAReadRS;

/**
 * @program: hotel-delivery-feizhu-os-server
 * @description: 订单同步服务（万达提供给携程方调用的接口）
 * @author: zhanwang
 * @create: 2018-11-19 10:41
 **/
public interface OrderSyncService {

    /**
     * 试预定接口
     *
     * @param otaHotelAvailRQ
     * @return
     */
    OTAHotelAvailRS checkAvailability(OTAHotelAvailRQ otaHotelAvailRQ);

    /**
     * 下单接口
     *
     * @param otaHotelResRQ
     * @return
     */
    OTAHotelResRS createReservation(OTAHotelResRQ otaHotelResRQ);

    /**
     * 取消订单接口
     *
     * @param otaCancelRQ
     * @return
     */
    OTACancelRS cancelReservation(OTACancelRQ otaCancelRQ);

    /**
     * 查询订单状态接口
     *
     * @param otaReadRQ
     * @return
     */
    OTAReadRS readReservation(OTAReadRQ otaReadRQ);

    /**
     * 携程海外订单同步入口
     *
     * @param xmlReq
     * @param requestType
     * @return
     */
    String orderRequestEntry(String xmlReq, String requestType);

    /**
     * 推送确认号到携程
     * @param pushConfirmNoRequest
     * @return
     */
    ResponseDTO pushConfirmNo(PushConfirmNoRequest pushConfirmNoRequest);
}
