package com.channel.connector.ctripos.domain;

import javax.persistence.*;

@Table(name = "t_ctripos_shop_info")
public class CtriposShopInfoDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 账户id
     */
    @Column(name = "api_id")
    private String apiId;

    /**
     * 账户密码
     */
    @Column(name = "message_password")
    private String messagePassword;

    /**
     * 账户类型
     */
    private String type;

    /**
     * 组织类别
     */
    private String code;

    /**
     * 合作伙伴标示
     */
    @Column(name = "code_context")
    private String codeContext;

    /**
     * 酒店基础信息映射url
     */
    @Column(name = "staticinfo_url")
    private String staticinfoUrl;

    /**
     * 价格房态订单等推送url
     */
    @Column(name = "dynamicinfo_url")
    private String dynamicinfoUrl;

    /**
     * 确认号url
     */
    @Column(name = "confirm_num_url")
    private String confirmNumUrl;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 店铺名称
     */
    @Column(name = "shop_name")
    private String shopName;

    /**
     * 渠道编码
     */
    @Column(name = "channel_code")
    private String channelCode;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取账户id
     *
     * @return api_id - 账户id
     */
    public String getApiId() {
        return apiId;
    }

    /**
     * 设置账户id
     *
     * @param apiId 账户id
     */
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    /**
     * 获取账户密码
     *
     * @return message_password - 账户密码
     */
    public String getMessagePassword() {
        return messagePassword;
    }

    /**
     * 设置账户密码
     *
     * @param messagePassword 账户密码
     */
    public void setMessagePassword(String messagePassword) {
        this.messagePassword = messagePassword;
    }

    /**
     * 获取账户类型
     *
     * @return type - 账户类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置账户类型
     *
     * @param type 账户类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取组织类别
     *
     * @return code - 组织类别
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置组织类别
     *
     * @param code 组织类别
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取合作伙伴标示
     *
     * @return code_context - 合作伙伴标示
     */
    public String getCodeContext() {
        return codeContext;
    }

    /**
     * 设置合作伙伴标示
     *
     * @param codeContext 合作伙伴标示
     */
    public void setCodeContext(String codeContext) {
        this.codeContext = codeContext;
    }

    /**
     * 获取酒店基础信息映射url
     *
     * @return staticinfo_url - 酒店基础信息映射url
     */
    public String getStaticinfoUrl() {
        return staticinfoUrl;
    }

    /**
     * 设置酒店基础信息映射url
     *
     * @param staticinfoUrl 酒店基础信息映射url
     */
    public void setStaticinfoUrl(String staticinfoUrl) {
        this.staticinfoUrl = staticinfoUrl;
    }

    /**
     * 获取价格房态订单等推送url
     *
     * @return dynamicinfo_url - 价格房态订单等推送url
     */
    public String getDynamicinfoUrl() {
        return dynamicinfoUrl;
    }

    /**
     * 设置价格房态订单等推送url
     *
     * @param dynamicinfoUrl 价格房态订单等推送url
     */
    public void setDynamicinfoUrl(String dynamicinfoUrl) {
        this.dynamicinfoUrl = dynamicinfoUrl;
    }

    /**
     * 获取确认号url
     *
     * @return confirm_num_url - 确认号url
     */
    public String getConfirmNumUrl() {
        return confirmNumUrl;
    }

    /**
     * 设置确认号url
     *
     * @param confirmNumUrl 确认号url
     */
    public void setConfirmNumUrl(String confirmNumUrl) {
        this.confirmNumUrl = confirmNumUrl;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取店铺名称
     *
     * @return shop_name - 店铺名称
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 设置店铺名称
     *
     * @param shopName 店铺名称
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * 获取渠道编码
     *
     * @return channel_code - 渠道编码
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * 设置渠道编码
     *
     * @param channelCode 渠道编码
     */
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
}