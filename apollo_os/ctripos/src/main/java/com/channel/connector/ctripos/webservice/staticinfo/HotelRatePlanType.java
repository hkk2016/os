
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>HotelRatePlanType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="HotelRatePlanType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookingRules" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}BookingRulesType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Nationalities" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfNationalityTypeNationality" minOccurs="0"/>
 *         &lt;element name="SellableProducts" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfSellableProductsTypeSellableProduct" minOccurs="0"/>
 *         &lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfHotelRatePlanTypeRate" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfParagraphTypeText" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="RatePlanCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RatePlanCategory" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="RatePlanStatusType" use="required" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelRatePlanType", propOrder = {
    "bookingRules",
    "nationalities",
    "sellableProducts",
    "rates",
    "description"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.OTAHotelRatePlanNotifRQ.RatePlans.RatePlan.class
})
public class HotelRatePlanType {

    @XmlElement(name = "BookingRules")
    protected HotelRatePlanType.BookingRules bookingRules;
    @XmlElement(name = "Nationalities")
    protected ArrayOfNationalityTypeNationality nationalities;
    @XmlElement(name = "SellableProducts")
    protected ArrayOfSellableProductsTypeSellableProduct sellableProducts;
    @XmlElement(name = "Rates")
    protected ArrayOfHotelRatePlanTypeRate rates;
    @XmlElement(name = "Description")
    protected ArrayOfParagraphTypeText description;

    /**
     * 集团的子房型代码-
     */
    @XmlAttribute(name = "RatePlanCode")
    protected String ratePlanCode;

    /**
     * 16现付，501预付
     */
    @XmlAttribute(name = "RatePlanCategory", required = true)
    protected int ratePlanCategory;

    /**
     * 当前酒店在供应商系统中状态
     */
    @XmlAttribute(name = "RatePlanStatusType", required = true)
    protected StatusEnum ratePlanStatusType;

    /**
     * 获取bookingRules属性的值。
     *
     * @return
     *     possible object is
     *     {@link HotelRatePlanType.BookingRules }
     *
     */
    public HotelRatePlanType.BookingRules getBookingRules() {
        return bookingRules;
    }

    /**
     * 设置bookingRules属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link HotelRatePlanType.BookingRules }
     *
     */
    public void setBookingRules(HotelRatePlanType.BookingRules value) {
        this.bookingRules = value;
    }

    /**
     * 获取nationalities属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfNationalityTypeNationality }
     *     
     */
    public ArrayOfNationalityTypeNationality getNationalities() {
        return nationalities;
    }

    /**
     * 设置nationalities属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfNationalityTypeNationality }
     *     
     */
    public void setNationalities(ArrayOfNationalityTypeNationality value) {
        this.nationalities = value;
    }

    /**
     * 获取sellableProducts属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSellableProductsTypeSellableProduct }
     *     
     */
    public ArrayOfSellableProductsTypeSellableProduct getSellableProducts() {
        return sellableProducts;
    }

    /**
     * 设置sellableProducts属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSellableProductsTypeSellableProduct }
     *     
     */
    public void setSellableProducts(ArrayOfSellableProductsTypeSellableProduct value) {
        this.sellableProducts = value;
    }

    /**
     * 获取rates属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHotelRatePlanTypeRate }
     *     
     */
    public ArrayOfHotelRatePlanTypeRate getRates() {
        return rates;
    }

    /**
     * 设置rates属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelRatePlanTypeRate }
     *     
     */
    public void setRates(ArrayOfHotelRatePlanTypeRate value) {
        this.rates = value;
    }

    /**
     * 获取description属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public ArrayOfParagraphTypeText getDescription() {
        return description;
    }

    /**
     * 设置description属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public void setDescription(ArrayOfParagraphTypeText value) {
        this.description = value;
    }

    /**
     * 获取ratePlanCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatePlanCode() {
        return ratePlanCode;
    }

    /**
     * 设置ratePlanCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatePlanCode(String value) {
        this.ratePlanCode = value;
    }

    /**
     * 获取ratePlanCategory属性的值。
     * 
     */
    public int getRatePlanCategory() {
        return ratePlanCategory;
    }

    /**
     * 设置ratePlanCategory属性的值。
     * 
     */
    public void setRatePlanCategory(int value) {
        this.ratePlanCategory = value;
    }

    /**
     * 获取ratePlanStatusType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link StatusEnum }
     *     
     */
    public StatusEnum getRatePlanStatusType() {
        return ratePlanStatusType;
    }

    /**
     * 设置ratePlanStatusType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link StatusEnum }
     *     
     */
    public void setRatePlanStatusType(StatusEnum value) {
        this.ratePlanStatusType = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}BookingRulesType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BookingRules
        extends BookingRulesType
    {


    }

}
