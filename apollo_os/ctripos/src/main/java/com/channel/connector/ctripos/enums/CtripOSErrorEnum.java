package com.channel.connector.ctripos.enums;

/**
 * 携程支持的错误/警告代码
 */
public enum CtripOSErrorEnum {
    CtripOSError_15("15", "Invalid date"),
    CtripOSError_61("61", "Invalid currency code"),
    CtripOSError_95("95", "Booking already cancelled"),
    CtripOSError_127("127", "Reservation already exists"),
    CtripOSError_147("147", "Taxes incorrect"),
    CtripOSError_161("161", "Search criteria invalid"),
    CtripOSError_240("240", "Credit card has expired"),
    CtripOSError_242("242", "Credit card number is invalid or missing"),
    CtripOSError_245("245", "Invalid confirmation number"),
    CtripOSError_249("249", "Invalid rate code"),
    CtripOSError_264("264", "Reservation cannot be cancelled"),
    CtripOSError_321("321", "Required field missing"),
    CtripOSError_322("322", "No availability"),
    CtripOSError_381("381", "Invalid check-in date"),
    CtripOSError_382("382", "Invalid check-out date"),
    CtripOSError_394("394", "Invalid item"),
    CtripOSError_397("397", "Invalid number of adults"),
    CtripOSError_400("400", "Invalid property code"),
    CtripOSError_402("402", "Invalid room type"),
    CtripOSError_448("448", "System error"),
    CtripOSError_450("450", "Unable to process"),
    CtripOSError_506("506", "Credit card not accepted at property"),
    CtripOSError_784("784", "Time out, please modify your request"),
    CtripOSError_19("19", "Name is missing or incomplete"),
    CtripOSError_88("88", "Cannot cancel - service has started"),
    CtripOSError_99("99", "Booking not owned by requester"),
    CtripOSError_107("107", "Cannot book - too far in advance"),
    CtripOSError_111("111", "Booking invalid"),
    CtripOSError_113("113", "Mandatory booking details missing"),
    CtripOSError_132("132", "Room/unit type - no availability"),
    CtripOSError_181("181", "Invalid country code"),
    CtripOSError_188("188", "Transaction error - please report"),
    CtripOSError_193("193", "Cancellation process failed"),
    CtripOSError_197("197", "Undeter mined error - please report"),
    CtripOSError_241("241", "Expiration date is invalid"),
    CtripOSError_243("243", "Invalid ARC/IATA number"),
    CtripOSError_244("244", "Email address is invalid"),
    CtripOSError_284("284", "No reservations found for search criteria"),
    CtripOSError_290("290", "Invalid state/province/territory code"),
    CtripOSError_291("291", "Invalid zip/postal code"),
    CtripOSError_298("298", "City in address is required"),
    CtripOSError_304("304", "Invalid expiration format MMYY"),
    CtripOSError_306("306", "Invalid association id or promo code"),
    CtripOSError_310("310", "Required data missing: last name"),
    CtripOSError_311("311", "Required data missing: first name"),
    CtripOSError_313("313", "Required data missing: credit card type"),
    CtripOSError_314("314", "Required data missing: country of residence"),
    CtripOSError_315("315", "Required data missing: confirmation number"),
    CtripOSError_316("316", "Required data missing: phone number"),
    CtripOSError_317("317", "Invalid phone number"),
    CtripOSError_320("320", "Invalid value"),
    CtripOSError_346("346", "Closed to arrivals"),
    CtripOSError_352("352", "Invalid credit card type"),
    CtripOSError_353("353", "Departure date is past dated"),
    CtripOSError_361("361", "Invalid hotel"),
    CtripOSError_362("362", "Invalid number of nights"),
    CtripOSError_363("317", "Invalid number of rooms"),
    CtripOSError_364("364", "Error rate range"),
    CtripOSError_365("365", "Error credit card"),
    CtripOSError_375("375", "Hotel not active"),
    CtripOSError_377("377", "Invalid - max number of nights exceeded"),
    CtripOSError_436("436", "Rate does not exist"),
    CtripOSError_459("459", "Invalid request code"),
    CtripOSError_497("497", "Authorization error"),
    CtripOSError_110("110", "Confirm the booking offline"),
    CtripOSError_498("498", "The booking is processing");


    private CtripOSErrorEnum(String code, String name) {
        this.name = name;
        this.code = code;
    }


    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
