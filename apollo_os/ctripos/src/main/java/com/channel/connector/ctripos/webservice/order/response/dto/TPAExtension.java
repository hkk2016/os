package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class TPAExtension {

    @XmlElement(name = "IsReserved", namespace = Namespace.NS)
    private Boolean isReserved;

    public Boolean getReserved() {
        return isReserved;
    }

    public void setReserved(Boolean reserved) {
        isReserved = reserved;
    }
}
