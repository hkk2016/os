package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.UniqueID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ReadRequest", namespace = Namespace.NS)
public class ReadRequest {

    @XmlElement(name = "UniqueID", namespace = Namespace.NS)
    private List<UniqueID> uniqueIDs;

    public List<UniqueID> getUniqueIDs() {
        if (null == uniqueIDs) {
            uniqueIDs = new ArrayList<>();
        }
        return uniqueIDs;
    }

    public void setUniqueIDs(List<UniqueID> uniqueIDs) {
        this.uniqueIDs = uniqueIDs;
    }


}
