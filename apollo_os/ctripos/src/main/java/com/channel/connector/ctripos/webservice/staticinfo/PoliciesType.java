
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>PoliciesType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PoliciesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Policy" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GuaranteePaymentPolicy" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.opentravel.org/OTA/2003/05}RequiredPaymentsType">
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PolicyInfoCodes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfPoliciesTypePolicyPolicyInfoCode" minOccurs="0"/>
 *                   &lt;element name="PolicyInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="CheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="LastCheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="EarlyCheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="CheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="MinGuestAge" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="UsualStayFreeChildPerAdult" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="UsualStayFreeCutoffAge" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="AcceptedGuestType">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="AdultOnly"/>
 *                                 &lt;enumeration value="CouplesOnly"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute name="KidsStayFree" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PetsPolicies" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PetsPolicy" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;attribute name="NonRefundableFee" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *                                     &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="PetsAllowedCode">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="Pets Allowed"/>
 *                                 &lt;enumeration value="Pets Not Allowed"/>
 *                                 &lt;enumeration value="Assistive Animals Only"/>
 *                                 &lt;enumeration value="Pets By Arrangements"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FeePolicies" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfFeeType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PoliciesType", propOrder = {
    "policy"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.HotelDescriptiveContentType.Policies.class
})
public class PoliciesType {

    @XmlElement(name = "Policy")
    protected PoliciesType.Policy policy;

    /**
     * 获取policy属性的值。
     *
     * @return
     *     possible object is
     *     {@link PoliciesType.Policy }
     *
     */
    public PoliciesType.Policy getPolicy() {
        return policy;
    }

    /**
     * 设置policy属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link PoliciesType.Policy }
     *
     */
    public void setPolicy(PoliciesType.Policy value) {
        this.policy = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     *
     * <p>以下模式片段指定包含在此类中的预期内容。
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GuaranteePaymentPolicy" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}RequiredPaymentsType">
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PolicyInfoCodes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfPoliciesTypePolicyPolicyInfoCode" minOccurs="0"/>
     *         &lt;element name="PolicyInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="CheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="LastCheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="EarlyCheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="CheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="MinGuestAge" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                 &lt;attribute name="UsualStayFreeChildPerAdult" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                 &lt;attribute name="UsualStayFreeCutoffAge" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                 &lt;attribute name="AcceptedGuestType">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                       &lt;enumeration value="AdultOnly"/>
     *                       &lt;enumeration value="CouplesOnly"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *                 &lt;attribute name="KidsStayFree" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PetsPolicies" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PetsPolicy" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;attribute name="NonRefundableFee" type="{http://www.w3.org/2001/XMLSchema}decimal" />
     *                           &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="PetsAllowedCode">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                       &lt;enumeration value="Pets Allowed"/>
     *                       &lt;enumeration value="Pets Not Allowed"/>
     *                       &lt;enumeration value="Assistive Animals Only"/>
     *                       &lt;enumeration value="Pets By Arrangements"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="FeePolicies" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfFeeType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guaranteePaymentPolicy",
        "policyInfoCodes",
        "policyInfo",
        "petsPolicies",
        "feePolicies"
    })
    public static class Policy {

        @XmlElement(name = "GuaranteePaymentPolicy")
        protected PoliciesType.Policy.GuaranteePaymentPolicy guaranteePaymentPolicy;
        @XmlElement(name = "PolicyInfoCodes")
        protected ArrayOfPoliciesTypePolicyPolicyInfoCode policyInfoCodes;
        @XmlElement(name = "PolicyInfo")
        protected PoliciesType.Policy.PolicyInfo policyInfo;
        @XmlElement(name = "PetsPolicies")
        protected PoliciesType.Policy.PetsPolicies petsPolicies;
        @XmlElement(name = "FeePolicies")
        protected ArrayOfFeeType feePolicies;

        /**
         * 获取guaranteePaymentPolicy属性的值。
         *
         * @return
         *     possible object is
         *     {@link PoliciesType.Policy.GuaranteePaymentPolicy }
         *
         */
        public PoliciesType.Policy.GuaranteePaymentPolicy getGuaranteePaymentPolicy() {
            return guaranteePaymentPolicy;
        }

        /**
         * 设置guaranteePaymentPolicy属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link PoliciesType.Policy.GuaranteePaymentPolicy }
         *
         */
        public void setGuaranteePaymentPolicy(PoliciesType.Policy.GuaranteePaymentPolicy value) {
            this.guaranteePaymentPolicy = value;
        }

        /**
         * 获取policyInfoCodes属性的值。
         *
         * @return
         *     possible object is
         *     {@link ArrayOfPoliciesTypePolicyPolicyInfoCode }
         *
         */
        public ArrayOfPoliciesTypePolicyPolicyInfoCode getPolicyInfoCodes() {
            return policyInfoCodes;
        }

        /**
         * 设置policyInfoCodes属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link ArrayOfPoliciesTypePolicyPolicyInfoCode }
         *
         */
        public void setPolicyInfoCodes(ArrayOfPoliciesTypePolicyPolicyInfoCode value) {
            this.policyInfoCodes = value;
        }

        /**
         * 获取policyInfo属性的值。
         *
         * @return
         *     possible object is
         *     {@link PoliciesType.Policy.PolicyInfo }
         *
         */
        public PoliciesType.Policy.PolicyInfo getPolicyInfo() {
            return policyInfo;
        }

        /**
         * 设置policyInfo属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link PoliciesType.Policy.PolicyInfo }
         *
         */
        public void setPolicyInfo(PoliciesType.Policy.PolicyInfo value) {
            this.policyInfo = value;
        }

        /**
         * 获取petsPolicies属性的值。
         *
         * @return
         *     possible object is
         *     {@link PoliciesType.Policy.PetsPolicies }
         *
         */
        public PoliciesType.Policy.PetsPolicies getPetsPolicies() {
            return petsPolicies;
        }

        /**
         * 设置petsPolicies属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link PoliciesType.Policy.PetsPolicies }
         *
         */
        public void setPetsPolicies(PoliciesType.Policy.PetsPolicies value) {
            this.petsPolicies = value;
        }

        /**
         * 获取feePolicies属性的值。
         *
         * @return
         *     possible object is
         *     {@link ArrayOfFeeType }
         *
         */
        public ArrayOfFeeType getFeePolicies() {
            return feePolicies;
        }

        /**
         * 设置feePolicies属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link ArrayOfFeeType }
         *
         */
        public void setFeePolicies(ArrayOfFeeType value) {
            this.feePolicies = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         *
         * <p>以下模式片段指定包含在此类中的预期内容。
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}RequiredPaymentsType">
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class GuaranteePaymentPolicy
            extends RequiredPaymentsType
        {


        }


        /**
         * <p>anonymous complex type的 Java 类。
         *
         * <p>以下模式片段指定包含在此类中的预期内容。
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PetsPolicy" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;attribute name="NonRefundableFee" type="{http://www.w3.org/2001/XMLSchema}decimal" />
         *                 &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *       &lt;attribute name="PetsAllowedCode">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *             &lt;enumeration value="Pets Allowed"/>
         *             &lt;enumeration value="Pets Not Allowed"/>
         *             &lt;enumeration value="Assistive Animals Only"/>
         *             &lt;enumeration value="Pets By Arrangements"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "petsPolicy"
        })
        public static class PetsPolicies {

            @XmlElement(name = "PetsPolicy")
            protected PoliciesType.Policy.PetsPolicies.PetsPolicy petsPolicy;

            /**
             * 允许宠物：Pets Allowed
             * 不允许宠物：Pets Not Allowed
             * 只允许工作或辅助性动物（如：导盲犬,缉毒犬）：Assistive Animals Only
             */
            @XmlAttribute(name = "PetsAllowedCode")
            protected String petsAllowedCode;

            /**
             * 获取petsPolicy属性的值。
             *
             * @return
             *     possible object is
             *     {@link PoliciesType.Policy.PetsPolicies.PetsPolicy }
             *
             */
            public PoliciesType.Policy.PetsPolicies.PetsPolicy getPetsPolicy() {
                return petsPolicy;
            }

            /**
             * 设置petsPolicy属性的值。
             *
             * @param value
             *     allowed object is
             *     {@link PoliciesType.Policy.PetsPolicies.PetsPolicy }
             *
             */
            public void setPetsPolicy(PoliciesType.Policy.PetsPolicies.PetsPolicy value) {
                this.petsPolicy = value;
            }

            /**
             * 获取petsAllowedCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPetsAllowedCode() {
                return petsAllowedCode;
            }

            /**
             * 设置petsAllowedCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPetsAllowedCode(String value) {
                this.petsAllowedCode = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;attribute name="NonRefundableFee" type="{http://www.w3.org/2001/XMLSchema}decimal" />
             *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class PetsPolicy {

                @XmlAttribute(name = "NonRefundableFee")
                protected BigDecimal nonRefundableFee;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;

                /**
                 * 获取nonRefundableFee属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getNonRefundableFee() {
                    return nonRefundableFee;
                }

                /**
                 * 设置nonRefundableFee属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setNonRefundableFee(BigDecimal value) {
                    this.nonRefundableFee = value;
                }

                /**
                 * 获取currencyCode属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * 设置currencyCode属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *       &lt;attribute name="CheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="LastCheckInTime" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="EarlyCheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="CheckOutTime" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="MinGuestAge" type="{http://www.w3.org/2001/XMLSchema}int" />
         *       &lt;attribute name="UsualStayFreeChildPerAdult" type="{http://www.w3.org/2001/XMLSchema}int" />
         *       &lt;attribute name="UsualStayFreeCutoffAge" type="{http://www.w3.org/2001/XMLSchema}int" />
         *       &lt;attribute name="AcceptedGuestType">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *             &lt;enumeration value="AdultOnly"/>
         *             &lt;enumeration value="CouplesOnly"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *       &lt;attribute name="KidsStayFree" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "description"
        })
        public static class PolicyInfo {

            @XmlElement(name = "Description")
            protected Object description;

            /**
             * 最早入住时间
             */
            @XmlAttribute(name = "CheckInTime")
            protected String checkInTime;

            /**
             * 最晚入住时间
             */
            @XmlAttribute(name = "LastCheckInTime")
            protected String lastCheckInTime;

            /**
             * 最早离店时间
             */
            @XmlAttribute(name = "EarlyCheckOutTime")
            protected String earlyCheckOutTime;

            /**
             * 最晚离店时间
             */
            @XmlAttribute(name = "CheckOutTime")
            protected String checkOutTime;

            /**
             * 最小客人年龄
             */
            @XmlAttribute(name = "MinGuestAge")
            protected Integer minGuestAge;

            /**
             * 每个成人允许带几个免费儿童
             */
            @XmlAttribute(name = "UsualStayFreeChildPerAdult")
            protected Integer usualStayFreeChildPerAdult;

            /**
             * 最大免费入住的儿童年龄
             */
            @XmlAttribute(name = "UsualStayFreeCutoffAge")
            protected Integer usualStayFreeCutoffAge;

            /**
             * 允许入住的客人类型，无要求请删除该字段
             * 仅成人入住：AdultOnly
             * 仅情侣入住：CouplesOnly
             */
            @XmlAttribute(name = "AcceptedGuestType")
            protected String acceptedGuestType;

            /**
             * 儿童是否免费
             */
            @XmlAttribute(name = "KidsStayFree")
            protected Boolean kidsStayFree;

            /**
             * 获取description属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getDescription() {
                return description;
            }

            /**
             * 设置description属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setDescription(Object value) {
                this.description = value;
            }

            /**
             * 获取checkInTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCheckInTime() {
                return checkInTime;
            }

            /**
             * 设置checkInTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCheckInTime(String value) {
                this.checkInTime = value;
            }

            /**
             * 获取lastCheckInTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastCheckInTime() {
                return lastCheckInTime;
            }

            /**
             * 设置lastCheckInTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastCheckInTime(String value) {
                this.lastCheckInTime = value;
            }

            /**
             * 获取earlyCheckOutTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEarlyCheckOutTime() {
                return earlyCheckOutTime;
            }

            /**
             * 设置earlyCheckOutTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEarlyCheckOutTime(String value) {
                this.earlyCheckOutTime = value;
            }

            /**
             * 获取checkOutTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCheckOutTime() {
                return checkOutTime;
            }

            /**
             * 设置checkOutTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCheckOutTime(String value) {
                this.checkOutTime = value;
            }

            /**
             * 获取minGuestAge属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getMinGuestAge() {
                return minGuestAge;
            }

            /**
             * 设置minGuestAge属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setMinGuestAge(Integer value) {
                this.minGuestAge = value;
            }

            /**
             * 获取usualStayFreeChildPerAdult属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getUsualStayFreeChildPerAdult() {
                return usualStayFreeChildPerAdult;
            }

            /**
             * 设置usualStayFreeChildPerAdult属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setUsualStayFreeChildPerAdult(Integer value) {
                this.usualStayFreeChildPerAdult = value;
            }

            /**
             * 获取usualStayFreeCutoffAge属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getUsualStayFreeCutoffAge() {
                return usualStayFreeCutoffAge;
            }

            /**
             * 设置usualStayFreeCutoffAge属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setUsualStayFreeCutoffAge(Integer value) {
                this.usualStayFreeCutoffAge = value;
            }

            /**
             * 获取acceptedGuestType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcceptedGuestType() {
                return acceptedGuestType;
            }

            /**
             * 设置acceptedGuestType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcceptedGuestType(String value) {
                this.acceptedGuestType = value;
            }

            /**
             * 获取kidsStayFree属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isKidsStayFree() {
                return kidsStayFree;
            }

            /**
             * 设置kidsStayFree属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setKidsStayFree(Boolean value) {
                this.kidsStayFree = value;
            }

        }

    }

}
