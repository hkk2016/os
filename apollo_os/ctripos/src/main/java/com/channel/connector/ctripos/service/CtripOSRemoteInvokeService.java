package com.channel.connector.ctripos.service;

import com.channel.connector.ctripos.webservice.confirmnum.OTAHotelResNumUpdateRQ;
import com.channel.connector.ctripos.webservice.confirmnum.OTAHotelResNumUpdateRS;
import com.channel.connector.ctripos.webservice.dynamic.OTAHotelAvailNotifRQ;
import com.channel.connector.ctripos.webservice.dynamic.OTAHotelAvailNotifRS;
import com.channel.connector.ctripos.webservice.dynamic.OTAHotelRateAmountNotifRQ;
import com.channel.connector.ctripos.webservice.dynamic.OTAHotelRateAmountNotifRS;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelDescriptiveContentNotifRQ;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelDescriptiveContentNotifRS;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelInvNotifRQ;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelInvNotifRS;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelRatePlanNotifRQ;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelRatePlanNotifRS;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelStatsNotifRQ;
import com.channel.connector.ctripos.webservice.staticinfo.OTAHotelStatsNotifRS;

import java.util.Map;

/**
 * Created by ASUS on 2018/11/15.
 */
public interface CtripOSRemoteInvokeService {

    /**
     * 推送酒店信息
     *
     * @param hotelDescriptiveContentNotifRQMap
     * @param shopId
     * @return
     */
    public Map<Long, OTAHotelDescriptiveContentNotifRS> pushHotelInfo(Map<Long, OTAHotelDescriptiveContentNotifRQ> hotelDescriptiveContentNotifRQMap, String shopId);

    /**
     * 推送房型信息
     *
     * @param hotelInvNotifRQMap
     * @param shopId
     * @return
     */
    public Map<Long, OTAHotelInvNotifRS> pushRoomTypeInfo(Map<Long, OTAHotelInvNotifRQ> hotelInvNotifRQMap, String shopId);

    /**
     * 推送子房型信息
     *
     * @param hotelRatePlanNotifRQMap
     * @param shopId
     * @return
     */
    public Map<Long, OTAHotelRatePlanNotifRS> pushRatePlanInfo(Map<Long, OTAHotelRatePlanNotifRQ> hotelRatePlanNotifRQMap, String shopId);

    /**
     * 同步房型信息
     *
     * @param otaHotelStatsNotifRQ
     * @return
     */
    public OTAHotelStatsNotifRS syncHotelInfo(OTAHotelStatsNotifRQ otaHotelStatsNotifRQ, String shopId);


    /**
     * 推送房态及库存信息
     *
     * @param otaHotelAvailNotifRQ
     * @param shopId
     * @return
     */
    public OTAHotelAvailNotifRS pushAvailabilityAndInventory(OTAHotelAvailNotifRQ otaHotelAvailNotifRQ, String shopId);

    /**
     * 推送价格信息
     *
     * @param otaHotelRateAmountNotifRQ
     * @param shopId
     * @return
     */
    OTAHotelRateAmountNotifRS pushRate(OTAHotelRateAmountNotifRQ otaHotelRateAmountNotifRQ, String shopId);

    /**
     * 更新订单确认号
     *
     * @param otaHotelResNumUpdateRQ
     * @param shopId
     * @return
     */
    OTAHotelResNumUpdateRS updateResConfirmationNum(OTAHotelResNumUpdateRQ otaHotelResNumUpdateRQ, String shopId);
}
