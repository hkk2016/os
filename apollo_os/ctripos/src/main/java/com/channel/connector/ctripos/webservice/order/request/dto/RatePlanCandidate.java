/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:49:54
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RatePlanCandidates", namespace = Namespace.NS)
public class RatePlanCandidate {

	@XmlAttribute(name = "RatePlanCode")
	private String RatePlanCode;

	public String getRatePlanCode() {
		return RatePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		RatePlanCode = ratePlanCode;
	}
}
