package com.channel.connector.ctripos.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.ctripos.domain.CtriposMapRoomtypeDO;

public interface CtriposMapRoomtypeMapper extends BaseMapper<CtriposMapRoomtypeDO> {
}