
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>MultimediaDescriptionsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="MultimediaDescriptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MultimediaDescription" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Renovation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="RenovationCompletionDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultimediaDescriptionsType", propOrder = {
    "multimediaDescription",
    "renovation"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.HotelInfoType.Descriptions.MultimediaDescriptions.class
})
public class MultimediaDescriptionsType {

    @XmlElement(name = "MultimediaDescription")
    protected List<MultimediaDescriptionType> multimediaDescription;
    @XmlElement(name = "Renovation")
    protected MultimediaDescriptionsType.Renovation renovation;

    /**
     * Gets the value of the multimediaDescription property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the multimediaDescription property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMultimediaDescription().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultimediaDescriptionType }
     *
     *
     */
    public List<MultimediaDescriptionType> getMultimediaDescription() {
        if (multimediaDescription == null) {
            multimediaDescription = new ArrayList<MultimediaDescriptionType>();
        }
        return this.multimediaDescription;
    }

    public void setMultimediaDescription(List<MultimediaDescriptionType> multimediaDescription) {
        this.multimediaDescription = multimediaDescription;
    }

    /**
     * 获取renovation属性的值。
     *
     * @return
     *     possible object is
     *     {@link MultimediaDescriptionsType.Renovation }
     *
     */
    public MultimediaDescriptionsType.Renovation getRenovation() {
        return renovation;
    }

    /**
     * 设置renovation属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link MultimediaDescriptionsType.Renovation }
     *
     */
    public void setRenovation(MultimediaDescriptionsType.Renovation value) {
        this.renovation = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="RenovationCompletionDate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Renovation {

        /**
         * 装修时间
         */
        @XmlAttribute(name = "RenovationCompletionDate")
        protected String renovationCompletionDate;

        /**
         * 获取renovationCompletionDate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRenovationCompletionDate() {
            return renovationCompletionDate;
        }

        /**
         * 设置renovationCompletionDate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRenovationCompletionDate(String value) {
            this.renovationCompletionDate = value;
        }

    }

}
