package com.channel.connector.ctripos.dto.response;

import java.io.Serializable;

/**
 * 客人人数信息
 */
public class HotelGuestCountDTO implements Serializable {

    private static final long serialVersionUID = 8198636606727222323L;

    /**
     * 成人数量
     */
    private Integer adultCount;

    /**
     * 儿童数量
     */
    private Integer childCount;

    public Integer getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(Integer adultCount) {
        this.adultCount = adultCount;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }
}
