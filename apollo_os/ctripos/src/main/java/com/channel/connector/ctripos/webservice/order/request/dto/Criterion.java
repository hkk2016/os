/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:42:57
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Criterion", namespace = Namespace.NS)
public class Criterion {

	@XmlElement(name = "HotelRef", namespace = Namespace.NS)
	private HotelRef hotelRef;
	
	@XmlElement(name = "StayDateRange", namespace = Namespace.NS)
	private StayDateRange stayDateRange;
	
	@XmlElementWrapper(name = "RatePlanCandidates", namespace = Namespace.NS)
	@XmlElement(name = "RatePlanCandidate", namespace = Namespace.NS)
	private List<RatePlanCandidate> ratePlanCandidates;
	
	@XmlElementWrapper(name = "RoomStayCandidates", namespace = Namespace.NS)
	@XmlElement(name = "RoomStayCandidate", namespace = Namespace.NS)
	private List<RoomStayCandidate> roomStayCandidates;

	public HotelRef getHotelRef() {
		return hotelRef;
	}

	public void setHotelRef(HotelRef hotelRef) {
		this.hotelRef = hotelRef;
	}

	public StayDateRange getStayDateRange() {
		return stayDateRange;
	}

	public void setStayDateRange(StayDateRange stayDateRange) {
		this.stayDateRange = stayDateRange;
	}

	public List<RatePlanCandidate> getRatePlanCandidates() {
		if (null == ratePlanCandidates) {
			ratePlanCandidates =  new ArrayList<RatePlanCandidate>();
		}
		return ratePlanCandidates;
	}

	public void setRatePlanCandidates(List<RatePlanCandidate> ratePlanCandidates) {
		this.ratePlanCandidates = ratePlanCandidates;
	}

	public List<RoomStayCandidate> getRoomStayCandidates() {
		if (null == roomStayCandidates) {
			roomStayCandidates =  new ArrayList<RoomStayCandidate>();
		}
		return roomStayCandidates;
	}

	public void setRoomStayCandidates(List<RoomStayCandidate> roomStayCandidates) {
		this.roomStayCandidates = roomStayCandidates;
	}

}
