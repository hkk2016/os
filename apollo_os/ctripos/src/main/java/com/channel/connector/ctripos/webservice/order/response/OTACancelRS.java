package com.channel.connector.ctripos.webservice.order.response;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.base.RSHeader;
import com.channel.connector.ctripos.webservice.order.response.dto.UniqueID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_CancelRS", namespace = Namespace.NS)
public class OTACancelRS extends RSHeader {
	
	@XmlElement(name="UniqueID", namespace = Namespace.NS)
	private List<UniqueID> uniqueIDs;

	public List<UniqueID> getUniqueIDs() {
		return uniqueIDs;
	}

	public void setUniqueIDs(List<UniqueID> uniqueIDs) {
		this.uniqueIDs = uniqueIDs;
	}
	

}
