/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:13:44
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Description {

	@XmlAttribute(name = "Text")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}
