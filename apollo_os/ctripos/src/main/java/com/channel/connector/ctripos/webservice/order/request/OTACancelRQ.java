package com.channel.connector.ctripos.webservice.order.request;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.request.base.RQHeader;
import com.channel.connector.ctripos.webservice.order.request.dto.Pos;
import com.channel.connector.ctripos.webservice.order.request.dto.Reason;
import com.channel.connector.ctripos.webservice.order.response.dto.UniqueID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_CancelRQ", namespace = Namespace.NS)
public class OTACancelRQ extends RQHeader {
	@XmlElement(name="POS", namespace = Namespace.NS)
	private Pos pos;
	
	@XmlElement(name = "UniqueID", namespace = Namespace.NS)
	private List<UniqueID>  uniqueIDs;
	
	@XmlElementWrapper(name = "Reasons", namespace = Namespace.NS)
	@XmlElement(name = "Reason", namespace = Namespace.NS)
	private List<Reason> reasons;

	public Pos getPos() {
		return pos;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public List<UniqueID> getUniqueIDs() {
		return uniqueIDs;
	}

	public void setUniqueIDs(List<UniqueID> uniqueIDs) {
		this.uniqueIDs = uniqueIDs;
	}

	public List<Reason> getReasons() {
		return reasons;
	}

	public void setReasons(List<Reason> reasons) {
		this.reasons = reasons;
	}
	

	
}
