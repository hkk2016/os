package com.channel.connector.product.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.PriceAndQuotaDO;
import com.channel.connector.product.dto.QueryPriceAndQuotaRequestDTO;

import java.util.List;

public interface PriceAndQuotaMapper extends BaseMapper<PriceAndQuotaDO> {

    /**
     *
     * @param queryPriceAndQuotaRequestDTO
     * @return
     */
    List<PriceAndQuotaDO> queryPriceAndQuota(QueryPriceAndQuotaRequestDTO queryPriceAndQuotaRequestDTO);
}