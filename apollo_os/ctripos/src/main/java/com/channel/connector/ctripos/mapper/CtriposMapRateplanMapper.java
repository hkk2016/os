package com.channel.connector.ctripos.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.ctripos.domain.CtriposMapRateplanDO;

public interface CtriposMapRateplanMapper extends BaseMapper<CtriposMapRateplanDO> {
}