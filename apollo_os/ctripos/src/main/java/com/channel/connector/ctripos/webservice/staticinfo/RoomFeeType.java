
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RoomFeeType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="RoomFeeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRoomTaxexTypeTax" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MandatoryInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomFeeType", propOrder = {
    "taxes"
})
public class RoomFeeType {

    @XmlElement(name = "Taxes")
    protected ArrayOfRoomTaxexTypeTax taxes;
    @XmlAttribute(name = "MandatoryInd", required = true)
    protected boolean mandatoryInd;

    /**
     * 获取taxes属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRoomTaxexTypeTax }
     *     
     */
    public ArrayOfRoomTaxexTypeTax getTaxes() {
        return taxes;
    }

    /**
     * 设置taxes属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRoomTaxexTypeTax }
     *     
     */
    public void setTaxes(ArrayOfRoomTaxexTypeTax value) {
        this.taxes = value;
    }

    /**
     * 获取mandatoryInd属性的值。
     * 
     */
    public boolean isMandatoryInd() {
        return mandatoryInd;
    }

    /**
     * 设置mandatoryInd属性的值。
     * 
     */
    public void setMandatoryInd(boolean value) {
        this.mandatoryInd = value;
    }

}
