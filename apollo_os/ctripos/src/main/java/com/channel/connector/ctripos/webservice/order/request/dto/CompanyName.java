package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CompanyName", namespace = Namespace.NS)
public class CompanyName {
	@XmlAttribute(name="Code")
	private String code;
	
	@XmlAttribute(name="CodeContext")
	private String codeContext;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeContext() {
		return codeContext;
	}
	public void setCodeContext(String codeContext) {
		this.codeContext = codeContext;
	}
	
	

}
