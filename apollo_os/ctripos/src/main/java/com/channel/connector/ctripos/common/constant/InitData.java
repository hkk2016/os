package com.channel.connector.ctripos.common.constant;

import com.channel.connector.ctripos.domain.CtriposShopInfoDO;
import com.channel.connector.product.domain.ChannelAgentDO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 40293 on 2018/12/3.
 */
public class InitData {

    /**
     * 商家的店铺信息:key=商家编码，value=店铺信息
     */
    public static Map<String,List<CtriposShopInfoDO>> merchantShopMap = new HashMap<>();

    /**
     * key:shopId,value=shopInfo
     */
    public static Map<String,CtriposShopInfoDO> shopIdShopMap = new HashMap<>();

    /**
     * key=merchantCode-channelcode
     */
    public static Map<String,ChannelAgentDO> channelAgentMap = new HashMap<>();

    /**
     * key=username-password
     */
    public static Map<String,CtriposShopInfoDO> userPwdShopMap = new HashMap<>();

}
