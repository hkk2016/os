/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:13:59
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.RoomDescription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RoomType", namespace = Namespace.NS)
public class RoomType {

	@XmlAttribute(name = "RoomTypeCode")
	private String roomTypeCode;
	
	@XmlAttribute(name = "NumberOfUnits")
	private Integer numberOfUnits;
	
	@XmlElement(name = "RoomDescription", namespace = Namespace.NS)
	private RoomDescription roomDescription;

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public RoomDescription getRoomDescription() {
		return roomDescription;
	}

	public void setRoomDescription(RoomDescription roomDescription) {
		this.roomDescription = roomDescription;
	}
	
}
