package com.channel.connector.product.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_htlpro_sale_state")
public class SaleStateDO {
    /**
     * 主键ID
     */
    @Id
    @Column(name = "salestate_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer salestateId;

    /**
     * 价格计划ID
     */
    @Column(name = "priceplan_id")
    private Integer priceplanId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * B2B售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "b2b_sale_state")
    private Byte b2bSaleState;

    /**
     * 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "ctrip_sale_state")
    private Byte ctripSaleState;

    /**
     * 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "qunar_sale_state")
    private Byte qunarSaleState;

    /**
     * 艺龙售卖状态(1上架  0下架  -1未开通)



     */
    @Column(name = "elong_sale_state")
    private Byte elongSaleState;

    /**
     * 同程售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "tongcheng_sale_state")
    private Byte tongchengSaleState;

    /**
     * 途牛售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "tuniu_sale_state")
    private Byte tuniuSaleState;

    /**
     * 新美大售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "xmd_sale_state")
    private Byte xmdSaleState;

    /**
     * 京东售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "jd_sale_state")
    private Byte jdSaleState;

    /**
     * 淘宝售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "taobao_sale_state")
    private Byte taobaoSaleState;

    /**
     * 去哪儿大B售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "qunar_b2b_sale_state")
    private Byte qunarB2bSaleState;

    /**
     * 去哪儿夜销售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "qunar_ngt_sale_state")
    private Byte qunarNgtSaleState;

    /**
     * 去哪儿美元售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "qunar_usd_sale_state")
    private Byte qunarUsdSaleState;

    /**
     * 携程海外售卖状态(1上架  0下架  -1未开通)
     */
    @Column(name = "ctripos_sale_state")
    private Byte ctriposSaleState;

    /**
     * 获取主键ID
     *
     * @return salestate_id - 主键ID
     */
    public Integer getSalestateId() {
        return salestateId;
    }

    /**
     * 设置主键ID
     *
     * @param salestateId 主键ID
     */
    public void setSalestateId(Integer salestateId) {
        this.salestateId = salestateId;
    }

    /**
     * 获取价格计划ID
     *
     * @return priceplan_id - 价格计划ID
     */
    public Integer getPriceplanId() {
        return priceplanId;
    }

    /**
     * 设置价格计划ID
     *
     * @param priceplanId 价格计划ID
     */
    public void setPriceplanId(Integer priceplanId) {
        this.priceplanId = priceplanId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取修改人
     *
     * @return modifier - 修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改人
     *
     * @param modifier 修改人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取B2B售卖状态(1上架  0下架  -1未开通)
     *
     * @return b2b_sale_state - B2B售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getB2bSaleState() {
        return b2bSaleState;
    }

    /**
     * 设置B2B售卖状态(1上架  0下架  -1未开通)
     *
     * @param b2bSaleState B2B售卖状态(1上架  0下架  -1未开通)
     */
    public void setB2bSaleState(Byte b2bSaleState) {
        this.b2bSaleState = b2bSaleState;
    }

    /**
     * 获取去哪儿售卖状态(1上架  0下架  -1未开通)
     *
     * @return ctrip_sale_state - 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getCtripSaleState() {
        return ctripSaleState;
    }

    /**
     * 设置去哪儿售卖状态(1上架  0下架  -1未开通)
     *
     * @param ctripSaleState 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    public void setCtripSaleState(Byte ctripSaleState) {
        this.ctripSaleState = ctripSaleState;
    }

    /**
     * 获取去哪儿售卖状态(1上架  0下架  -1未开通)
     *
     * @return qunar_sale_state - 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getQunarSaleState() {
        return qunarSaleState;
    }

    /**
     * 设置去哪儿售卖状态(1上架  0下架  -1未开通)
     *
     * @param qunarSaleState 去哪儿售卖状态(1上架  0下架  -1未开通)
     */
    public void setQunarSaleState(Byte qunarSaleState) {
        this.qunarSaleState = qunarSaleState;
    }

    /**
     * 获取艺龙售卖状态(1上架  0下架  -1未开通)



     *
     * @return elong_sale_state - 艺龙售卖状态(1上架  0下架  -1未开通)



     */
    public Byte getElongSaleState() {
        return elongSaleState;
    }

    /**
     * 设置艺龙售卖状态(1上架  0下架  -1未开通)



     *
     * @param elongSaleState 艺龙售卖状态(1上架  0下架  -1未开通)



     */
    public void setElongSaleState(Byte elongSaleState) {
        this.elongSaleState = elongSaleState;
    }

    /**
     * 获取同程售卖状态(1上架  0下架  -1未开通)
     *
     * @return tongcheng_sale_state - 同程售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getTongchengSaleState() {
        return tongchengSaleState;
    }

    /**
     * 设置同程售卖状态(1上架  0下架  -1未开通)
     *
     * @param tongchengSaleState 同程售卖状态(1上架  0下架  -1未开通)
     */
    public void setTongchengSaleState(Byte tongchengSaleState) {
        this.tongchengSaleState = tongchengSaleState;
    }

    /**
     * 获取途牛售卖状态(1上架  0下架  -1未开通)
     *
     * @return tuniu_sale_state - 途牛售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getTuniuSaleState() {
        return tuniuSaleState;
    }

    /**
     * 设置途牛售卖状态(1上架  0下架  -1未开通)
     *
     * @param tuniuSaleState 途牛售卖状态(1上架  0下架  -1未开通)
     */
    public void setTuniuSaleState(Byte tuniuSaleState) {
        this.tuniuSaleState = tuniuSaleState;
    }

    /**
     * 获取新美大售卖状态(1上架  0下架  -1未开通)
     *
     * @return xmd_sale_state - 新美大售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getXmdSaleState() {
        return xmdSaleState;
    }

    /**
     * 设置新美大售卖状态(1上架  0下架  -1未开通)
     *
     * @param xmdSaleState 新美大售卖状态(1上架  0下架  -1未开通)
     */
    public void setXmdSaleState(Byte xmdSaleState) {
        this.xmdSaleState = xmdSaleState;
    }

    /**
     * 获取京东售卖状态(1上架  0下架  -1未开通)
     *
     * @return jd_sale_state - 京东售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getJdSaleState() {
        return jdSaleState;
    }

    /**
     * 设置京东售卖状态(1上架  0下架  -1未开通)
     *
     * @param jdSaleState 京东售卖状态(1上架  0下架  -1未开通)
     */
    public void setJdSaleState(Byte jdSaleState) {
        this.jdSaleState = jdSaleState;
    }

    /**
     * 获取淘宝售卖状态(1上架  0下架  -1未开通)
     *
     * @return taobao_sale_state - 淘宝售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getTaobaoSaleState() {
        return taobaoSaleState;
    }

    /**
     * 设置淘宝售卖状态(1上架  0下架  -1未开通)
     *
     * @param taobaoSaleState 淘宝售卖状态(1上架  0下架  -1未开通)
     */
    public void setTaobaoSaleState(Byte taobaoSaleState) {
        this.taobaoSaleState = taobaoSaleState;
    }

    /**
     * 获取去哪儿大B售卖状态(1上架  0下架  -1未开通)
     *
     * @return qunar_b2b_sale_state - 去哪儿大B售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getQunarB2bSaleState() {
        return qunarB2bSaleState;
    }

    /**
     * 设置去哪儿大B售卖状态(1上架  0下架  -1未开通)
     *
     * @param qunarB2bSaleState 去哪儿大B售卖状态(1上架  0下架  -1未开通)
     */
    public void setQunarB2bSaleState(Byte qunarB2bSaleState) {
        this.qunarB2bSaleState = qunarB2bSaleState;
    }

    /**
     * 获取去哪儿夜销售卖状态(1上架  0下架  -1未开通)
     *
     * @return qunar_ngt_sale_state - 去哪儿夜销售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getQunarNgtSaleState() {
        return qunarNgtSaleState;
    }

    /**
     * 设置去哪儿夜销售卖状态(1上架  0下架  -1未开通)
     *
     * @param qunarNgtSaleState 去哪儿夜销售卖状态(1上架  0下架  -1未开通)
     */
    public void setQunarNgtSaleState(Byte qunarNgtSaleState) {
        this.qunarNgtSaleState = qunarNgtSaleState;
    }

    /**
     * 获取去哪儿美元售卖状态(1上架  0下架  -1未开通)
     *
     * @return qunar_usd_sale_state - 去哪儿美元售卖状态(1上架  0下架  -1未开通)
     */
    public Byte getQunarUsdSaleState() {
        return qunarUsdSaleState;
    }

    /**
     * 设置去哪儿美元售卖状态(1上架  0下架  -1未开通)
     *
     * @param qunarUsdSaleState 去哪儿美元售卖状态(1上架  0下架  -1未开通)
     */
    public void setQunarUsdSaleState(Byte qunarUsdSaleState) {
        this.qunarUsdSaleState = qunarUsdSaleState;
    }

    public Byte getCtriposSaleState() {
        return ctriposSaleState;
    }

    public void setCtriposSaleState(Byte ctriposSaleState) {
        this.ctriposSaleState = ctriposSaleState;
    }
}