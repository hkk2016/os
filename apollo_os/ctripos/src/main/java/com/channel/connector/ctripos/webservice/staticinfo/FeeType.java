
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>FeeType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FeeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfTaxesTypeTax" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfParagraphTypeText" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MandatoryInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeType", propOrder = {
    "taxes",
    "description"
})
public class FeeType {

    @XmlElement(name = "Taxes")
    protected ArrayOfTaxesTypeTax taxes;
    @XmlElement(name = "Description")
    protected ArrayOfParagraphTypeText description;

    /**
     * 入住后是否额外强制收取额外相关费用
     */
    @XmlAttribute(name = "MandatoryInd", required = true)
    protected boolean mandatoryInd;

    /**
     * 获取taxes属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTaxesTypeTax }
     *     
     */
    public ArrayOfTaxesTypeTax getTaxes() {
        return taxes;
    }

    /**
     * 设置taxes属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTaxesTypeTax }
     *     
     */
    public void setTaxes(ArrayOfTaxesTypeTax value) {
        this.taxes = value;
    }

    /**
     * 获取description属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public ArrayOfParagraphTypeText getDescription() {
        return description;
    }

    /**
     * 设置description属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParagraphTypeText }
     *     
     */
    public void setDescription(ArrayOfParagraphTypeText value) {
        this.description = value;
    }

    /**
     * 获取mandatoryInd属性的值。
     * 
     */
    public boolean isMandatoryInd() {
        return mandatoryInd;
    }

    /**
     * 设置mandatoryInd属性的值。
     * 
     */
    public void setMandatoryInd(boolean value) {
        this.mandatoryInd = value;
    }

}
