/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:38:27
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "HotelSearchCriteria", namespace = Namespace.NS)
public class HotelSearchCriteria {

	@XmlElement(name = "Criterion", namespace = Namespace.NS)
	private Criterion criterion;

	public Criterion getCriterion() {
		return criterion;
	}

	public void setCriterion(Criterion criterion) {
		this.criterion = criterion;
	}

	
}
