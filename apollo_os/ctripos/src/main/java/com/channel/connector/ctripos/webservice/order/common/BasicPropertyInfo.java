/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:20:08
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "BasicPropertyInfo", namespace = Namespace.NS)
public class BasicPropertyInfo {

	@XmlAttribute(name = "HotelCode")
	private String HotelCode;
	
	@XmlAttribute(name = "HotelName")
	private String HotelName;

	public String getHotelCode() {
		return HotelCode;
	}

	public void setHotelCode(String hotelCode) {
		HotelCode = hotelCode;
	}

	public String getHotelName() {
		return HotelName;
	}

	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
	
}
