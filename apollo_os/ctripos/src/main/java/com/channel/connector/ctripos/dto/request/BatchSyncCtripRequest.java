package com.channel.connector.ctripos.dto.request;

import com.channel.connector.ctripos.dto.CtriposBaseRequestDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BatchSyncCtripRequest extends CtriposBaseRequestDTO implements Serializable {

    private static final long serialVersionUID = -3988688040266516338L;

    private List<SyncCtripHotelRequest> hotelVOs;
}
