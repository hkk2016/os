package com.channel.connector.product.mapper;


import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.HotelDO;

public interface HotelMapper extends BaseMapper<HotelDO> {

	/**
	 * 查询酒店基本信息
	 */
	HotelDO queryHotelInfoByHotelId(Long hotelId);
	

}
