/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午02:38:30
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.common.RoomStay;
import com.channel.connector.ctripos.webservice.order.response.base.RSHeader;
import com.channel.connector.ctripos.webservice.order.response.dto.HotelStay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_HotelAvailRS", namespace = Namespace.NS)
public class OTAHotelAvailRS extends RSHeader {

	@XmlElementWrapper(name = "RoomStays", namespace = Namespace.NS)
	@XmlElement(name = "RoomStay", namespace = Namespace.NS)
	private List<RoomStay> roomStays;

	@XmlElementWrapper(name = "HotelStays", namespace = Namespace.NS)
	@XmlElement(name = "HotelStay", namespace = Namespace.NS)
	private List<HotelStay> hotelStays;

	public List<RoomStay> getRoomStays() {
		if (null == roomStays) {
			roomStays = new ArrayList<RoomStay>();
		}
		return roomStays;
	}

	public void setRoomStays(List<RoomStay> roomStays) {
		this.roomStays = roomStays;
	}

	public List<HotelStay> getHotelStays() {
		if (null == hotelStays) {
			hotelStays = new ArrayList<HotelStay>();
		}
		return hotelStays;
	}

	public void setHotelStays(List<HotelStay> hotelStays) {
		this.hotelStays = hotelStays;
	}

}
