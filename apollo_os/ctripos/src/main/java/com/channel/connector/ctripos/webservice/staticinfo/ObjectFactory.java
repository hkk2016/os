
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.channel.connector.ctripos.webservice.staticinfo package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OTAHotelRatePlanNotifRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelRatePlanNotifRS");
    private final static QName _OTAHotelStatsNotifRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelStatsNotifRS");
    private final static QName _OTAHotelInvNotifRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelInvNotifRS");
    private final static QName _OTAHotelDescriptiveContentNotifRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelDescriptiveContentNotifRS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.channel.connector.ctripos.webservice.staticinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OTAHotelInvNotifRQ }
     * 
     */
    public OTAHotelInvNotifRQ createOTAHotelInvNotifRQ() {
        return new OTAHotelInvNotifRQ();
    }

    /**
     * Create an instance of {@link OTAHotelRatePlanNotifRQ }
     * 
     */
    public OTAHotelRatePlanNotifRQ createOTAHotelRatePlanNotifRQ() {
        return new OTAHotelRatePlanNotifRQ();
    }

    /**
     * Create an instance of {@link OTAHotelDescriptiveContentNotifRQ }
     * 
     */
    public OTAHotelDescriptiveContentNotifRQ createOTAHotelDescriptiveContentNotifRQ() {
        return new OTAHotelDescriptiveContentNotifRQ();
    }

    /**
     * Create an instance of {@link HotelRatePlanType }
     * 
     */
    public HotelRatePlanType createHotelRatePlanType() {
        return new HotelRatePlanType();
    }

    /**
     * Create an instance of {@link BookingRulesType }
     * 
     */
    public BookingRulesType createBookingRulesType() {
        return new BookingRulesType();
    }

    /**
     * Create an instance of {@link ArrayOfCategoryCodesTypeGuestRoomInfo }
     * 
     */
    public ArrayOfCategoryCodesTypeGuestRoomInfo createArrayOfCategoryCodesTypeGuestRoomInfo() {
        return new ArrayOfCategoryCodesTypeGuestRoomInfo();
    }

    /**
     * Create an instance of {@link ArrayOfRoomTaxexTypeTax }
     * 
     */
    public ArrayOfRoomTaxexTypeTax createArrayOfRoomTaxexTypeTax() {
        return new ArrayOfRoomTaxexTypeTax();
    }

    /**
     * Create an instance of {@link HotelInfoType }
     * 
     */
    public HotelInfoType createHotelInfoType() {
        return new HotelInfoType();
    }

    /**
     * Create an instance of {@link HotelInfoType.Descriptions }
     * 
     */
    public HotelInfoType.Descriptions createHotelInfoTypeDescriptions() {
        return new HotelInfoType.Descriptions();
    }

    /**
     * Create an instance of {@link MultimediaDescriptionsType }
     * 
     */
    public MultimediaDescriptionsType createMultimediaDescriptionsType() {
        return new MultimediaDescriptionsType();
    }

    /**
     * Create an instance of {@link ArrayOfContactsTypeName }
     * 
     */
    public ArrayOfContactsTypeName createArrayOfContactsTypeName() {
        return new ArrayOfContactsTypeName();
    }

    /**
     * Create an instance of {@link HotelDescriptiveContentType }
     * 
     */
    public HotelDescriptiveContentType createHotelDescriptiveContentType() {
        return new HotelDescriptiveContentType();
    }

    /**
     * Create an instance of {@link PoliciesType }
     * 
     */
    public PoliciesType createPoliciesType() {
        return new PoliciesType();
    }

    /**
     * Create an instance of {@link PoliciesType.Policy }
     * 
     */
    public PoliciesType.Policy createPoliciesTypePolicy() {
        return new PoliciesType.Policy();
    }

    /**
     * Create an instance of {@link PoliciesType.Policy.PetsPolicies }
     * 
     */
    public PoliciesType.Policy.PetsPolicies createPoliciesTypePolicyPetsPolicies() {
        return new PoliciesType.Policy.PetsPolicies();
    }

    /**
     * Create an instance of {@link RequiredPaymentsType }
     * 
     */
    public RequiredPaymentsType createRequiredPaymentsType() {
        return new RequiredPaymentsType();
    }

    /**
     * Create an instance of {@link ArrayOfSellableProductsTypeSellableProduct }
     * 
     */
    public ArrayOfSellableProductsTypeSellableProduct createArrayOfSellableProductsTypeSellableProduct() {
        return new ArrayOfSellableProductsTypeSellableProduct();
    }

    /**
     * Create an instance of {@link ArrayOfRoomInfoTypeBedType }
     * 
     */
    public ArrayOfRoomInfoTypeBedType createArrayOfRoomInfoTypeBedType() {
        return new ArrayOfRoomInfoTypeBedType();
    }

    /**
     * Create an instance of {@link ArrayOfRatePlanShortTypeRatePlan }
     * 
     */
    public ArrayOfRatePlanShortTypeRatePlan createArrayOfRatePlanShortTypeRatePlan() {
        return new ArrayOfRatePlanShortTypeRatePlan();
    }

    /**
     * Create an instance of {@link ArrayOfBasicRoomTypeBasicRoom }
     * 
     */
    public ArrayOfBasicRoomTypeBasicRoom createArrayOfBasicRoomTypeBasicRoom() {
        return new ArrayOfBasicRoomTypeBasicRoom();
    }

    /**
     * Create an instance of {@link RateUploadType }
     * 
     */
    public RateUploadType createRateUploadType() {
        return new RateUploadType();
    }

    /**
     * Create an instance of {@link ArrayOfHotelRatePlanTypeRate }
     * 
     */
    public ArrayOfHotelRatePlanTypeRate createArrayOfHotelRatePlanTypeRate() {
        return new ArrayOfHotelRatePlanTypeRate();
    }

    /**
     * Create an instance of {@link GuestRoomType }
     * 
     */
    public GuestRoomType createGuestRoomType() {
        return new GuestRoomType();
    }

    /**
     * Create an instance of {@link ArrayOfPhonesTypePhone }
     * 
     */
    public ArrayOfPhonesTypePhone createArrayOfPhonesTypePhone() {
        return new ArrayOfPhonesTypePhone();
    }

    /**
     * Create an instance of {@link ArrayOfParagraphTypeText }
     * 
     */
    public ArrayOfParagraphTypeText createArrayOfParagraphTypeText() {
        return new ArrayOfParagraphTypeText();
    }

    /**
     * Create an instance of {@link ArrayOfAcceptedPaymentsTypeAcceptedPayment }
     * 
     */
    public ArrayOfAcceptedPaymentsTypeAcceptedPayment createArrayOfAcceptedPaymentsTypeAcceptedPayment() {
        return new ArrayOfAcceptedPaymentsTypeAcceptedPayment();
    }

    /**
     * Create an instance of {@link PaymentFormType }
     * 
     */
    public PaymentFormType createPaymentFormType() {
        return new PaymentFormType();
    }

    /**
     * Create an instance of {@link ImageItemsType }
     * 
     */
    public ImageItemsType createImageItemsType() {
        return new ImageItemsType();
    }

    /**
     * Create an instance of {@link ImageDescriptionType }
     * 
     */
    public ImageDescriptionType createImageDescriptionType() {
        return new ImageDescriptionType();
    }

    /**
     * Create an instance of {@link ArrayOfTaxesTypeTax }
     * 
     */
    public ArrayOfTaxesTypeTax createArrayOfTaxesTypeTax() {
        return new ArrayOfTaxesTypeTax();
    }

    /**
     * Create an instance of {@link ArrayOfImageItemsTypeImageItem }
     * 
     */
    public ArrayOfImageItemsTypeImageItem createArrayOfImageItemsTypeImageItem() {
        return new ArrayOfImageItemsTypeImageItem();
    }

    /**
     * Create an instance of {@link ArrayOfGuestRoomTypeAmenity }
     * 
     */
    public ArrayOfGuestRoomTypeAmenity createArrayOfGuestRoomTypeAmenity() {
        return new ArrayOfGuestRoomTypeAmenity();
    }

    /**
     * Create an instance of {@link RoomInfoType }
     * 
     */
    public RoomInfoType createRoomInfoType() {
        return new RoomInfoType();
    }

    /**
     * Create an instance of {@link RoomInfoType.Equipments }
     * 
     */
    public RoomInfoType.Equipments createRoomInfoTypeEquipments() {
        return new RoomInfoType.Equipments();
    }

    /**
     * Create an instance of {@link ArrayOfHotelStatsInfoTypeHotel }
     * 
     */
    public ArrayOfHotelStatsInfoTypeHotel createArrayOfHotelStatsInfoTypeHotel() {
        return new ArrayOfHotelStatsInfoTypeHotel();
    }

    /**
     * Create an instance of {@link TextItemsType }
     * 
     */
    public TextItemsType createTextItemsType() {
        return new TextItemsType();
    }

    /**
     * Create an instance of {@link ArrayOfAffiliationInfoTypeAward }
     * 
     */
    public ArrayOfAffiliationInfoTypeAward createArrayOfAffiliationInfoTypeAward() {
        return new ArrayOfAffiliationInfoTypeAward();
    }

    /**
     * Create an instance of {@link SellableProductsType }
     * 
     */
    public SellableProductsType createSellableProductsType() {
        return new SellableProductsType();
    }

    /**
     * Create an instance of {@link CommissionType }
     * 
     */
    public CommissionType createCommissionType() {
        return new CommissionType();
    }

    /**
     * Create an instance of {@link ArrayOfAddressesTypeAddress }
     * 
     */
    public ArrayOfAddressesTypeAddress createArrayOfAddressesTypeAddress() {
        return new ArrayOfAddressesTypeAddress();
    }

    /**
     * Create an instance of {@link CancelPenaltyType }
     * 
     */
    public CancelPenaltyType createCancelPenaltyType() {
        return new CancelPenaltyType();
    }

    /**
     * Create an instance of {@link ArrayOfPoliciesTypePolicyPolicyInfoCode }
     * 
     */
    public ArrayOfPoliciesTypePolicyPolicyInfoCode createArrayOfPoliciesTypePolicyPolicyInfoCode() {
        return new ArrayOfPoliciesTypePolicyPolicyInfoCode();
    }

    /**
     * Create an instance of {@link ArrayOfGDSInfoTypeGDSCodesGDSCode }
     * 
     */
    public ArrayOfGDSInfoTypeGDSCodesGDSCode createArrayOfGDSInfoTypeGDSCodesGDSCode() {
        return new ArrayOfGDSInfoTypeGDSCodesGDSCode();
    }

    /**
     * Create an instance of {@link SourceType }
     * 
     */
    public SourceType createSourceType() {
        return new SourceType();
    }

    /**
     * Create an instance of {@link ArrayOfNationalityTypeNationality }
     * 
     */
    public ArrayOfNationalityTypeNationality createArrayOfNationalityTypeNationality() {
        return new ArrayOfNationalityTypeNationality();
    }

    /**
     * Create an instance of {@link PaymentDetailType }
     * 
     */
    public PaymentDetailType createPaymentDetailType() {
        return new PaymentDetailType();
    }

    /**
     * Create an instance of {@link ArrayOfHotelInfoTypeService }
     * 
     */
    public ArrayOfHotelInfoTypeService createArrayOfHotelInfoTypeService() {
        return new ArrayOfHotelInfoTypeService();
    }

    /**
     * Create an instance of {@link OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents }
     * 
     */
    public OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents createOTAHotelDescriptiveContentNotifRQHotelDescriptiveContents() {
        return new OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents();
    }

    /**
     * Create an instance of {@link ArrayOfStatisticsTypeStatistic }
     * 
     */
    public ArrayOfStatisticsTypeStatistic createArrayOfStatisticsTypeStatistic() {
        return new ArrayOfStatisticsTypeStatistic();
    }

    /**
     * Create an instance of {@link OTAHotelRatePlanNotifRQ.RatePlans }
     * 
     */
    public OTAHotelRatePlanNotifRQ.RatePlans createOTAHotelRatePlanNotifRQRatePlans() {
        return new OTAHotelRatePlanNotifRQ.RatePlans();
    }

    /**
     * Create an instance of {@link OTAHotelStatsNotifRS }
     * 
     */
    public OTAHotelStatsNotifRS createOTAHotelStatsNotifRS() {
        return new OTAHotelStatsNotifRS();
    }

    /**
     * Create an instance of {@link POSType }
     * 
     */
    public POSType createPOSType() {
        return new POSType();
    }

    /**
     * Create an instance of {@link OTAHotelInvNotifRQ.SellableProducts }
     * 
     */
    public OTAHotelInvNotifRQ.SellableProducts createOTAHotelInvNotifRQSellableProducts() {
        return new OTAHotelInvNotifRQ.SellableProducts();
    }

    /**
     * Create an instance of {@link OTAHotelRatePlanNotifRS }
     * 
     */
    public OTAHotelRatePlanNotifRS createOTAHotelRatePlanNotifRS() {
        return new OTAHotelRatePlanNotifRS();
    }

    /**
     * Create an instance of {@link OTAHotelStatsNotifRQ }
     * 
     */
    public OTAHotelStatsNotifRQ createOTAHotelStatsNotifRQ() {
        return new OTAHotelStatsNotifRQ();
    }

    /**
     * Create an instance of {@link OTAHotelInvNotifRS }
     * 
     */
    public OTAHotelInvNotifRS createOTAHotelInvNotifRS() {
        return new OTAHotelInvNotifRS();
    }

    /**
     * Create an instance of {@link OTAHotelDescriptiveContentNotifRS }
     * 
     */
    public OTAHotelDescriptiveContentNotifRS createOTAHotelDescriptiveContentNotifRS() {
        return new OTAHotelDescriptiveContentNotifRS();
    }

    /**
     * Create an instance of {@link MultimediaDescriptionType }
     * 
     */
    public MultimediaDescriptionType createMultimediaDescriptionType() {
        return new MultimediaDescriptionType();
    }

    /**
     * Create an instance of {@link OperatingAirlineType }
     * 
     */
    public OperatingAirlineType createOperatingAirlineType() {
        return new OperatingAirlineType();
    }

    /**
     * Create an instance of {@link ArrayOfFeeType }
     * 
     */
    public ArrayOfFeeType createArrayOfFeeType() {
        return new ArrayOfFeeType();
    }

    /**
     * Create an instance of {@link ArrayOfContactInfoRootType }
     * 
     */
    public ArrayOfContactInfoRootType createArrayOfContactInfoRootType() {
        return new ArrayOfContactInfoRootType();
    }

    /**
     * Create an instance of {@link ContactInfoType }
     * 
     */
    public ContactInfoType createContactInfoType() {
        return new ContactInfoType();
    }

    /**
     * Create an instance of {@link FeeType }
     * 
     */
    public FeeType createFeeType() {
        return new FeeType();
    }

    /**
     * Create an instance of {@link ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode }
     * 
     */
    public ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode createArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode() {
        return new ArrayOfArrayOfGDSInfoTypeGDSCodesGDSCode();
    }

    /**
     * Create an instance of {@link TPAExtensionsType }
     * 
     */
    public TPAExtensionsType createTPAExtensionsType() {
        return new TPAExtensionsType();
    }

    /**
     * Create an instance of {@link TextDescriptionType }
     * 
     */
    public TextDescriptionType createTextDescriptionType() {
        return new TextDescriptionType();
    }

    /**
     * Create an instance of {@link RoomFeeType }
     * 
     */
    public RoomFeeType createRoomFeeType() {
        return new RoomFeeType();
    }

    /**
     * Create an instance of {@link ContactInfoRootType }
     * 
     */
    public ContactInfoRootType createContactInfoRootType() {
        return new ContactInfoRootType();
    }

    /**
     * Create an instance of {@link UniqueIDType }
     * 
     */
    public UniqueIDType createUniqueIDType() {
        return new UniqueIDType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link CompanyNameType }
     * 
     */
    public CompanyNameType createCompanyNameType() {
        return new CompanyNameType();
    }

    /**
     * Create an instance of {@link ArrayOfErrorType }
     * 
     */
    public ArrayOfErrorType createArrayOfErrorType() {
        return new ArrayOfErrorType();
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link SuccessType }
     * 
     */
    public SuccessType createSuccessType() {
        return new SuccessType();
    }

    /**
     * Create an instance of {@link AffiliationInfoType }
     * 
     */
    public AffiliationInfoType createAffiliationInfoType() {
        return new AffiliationInfoType();
    }

    /**
     * Create an instance of {@link CountryNameType }
     * 
     */
    public CountryNameType createCountryNameType() {
        return new CountryNameType();
    }

    /**
     * Create an instance of {@link AddressInfoType }
     * 
     */
    public AddressInfoType createAddressInfoType() {
        return new AddressInfoType();
    }

    /**
     * Create an instance of {@link CancelPenaltiesType }
     * 
     */
    public CancelPenaltiesType createCancelPenaltiesType() {
        return new CancelPenaltiesType();
    }

    /**
     * Create an instance of {@link ImageItemType }
     * 
     */
    public ImageItemType createImageItemType() {
        return new ImageItemType();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link HotelRatePlanType.BookingRules }
     * 
     */
    public HotelRatePlanType.BookingRules createHotelRatePlanTypeBookingRules() {
        return new HotelRatePlanType.BookingRules();
    }

    /**
     * Create an instance of {@link BookingRulesType.BookingRule }
     * 
     */
    public BookingRulesType.BookingRule createBookingRulesTypeBookingRule() {
        return new BookingRulesType.BookingRule();
    }

    /**
     * Create an instance of {@link ArrayOfCategoryCodesTypeGuestRoomInfo.GuestRoomInfo }
     * 
     */
    public ArrayOfCategoryCodesTypeGuestRoomInfo.GuestRoomInfo createArrayOfCategoryCodesTypeGuestRoomInfoGuestRoomInfo() {
        return new ArrayOfCategoryCodesTypeGuestRoomInfo.GuestRoomInfo();
    }

    /**
     * Create an instance of {@link ArrayOfRoomTaxexTypeTax.Tax }
     * 
     */
    public ArrayOfRoomTaxexTypeTax.Tax createArrayOfRoomTaxexTypeTaxTax() {
        return new ArrayOfRoomTaxexTypeTax.Tax();
    }

    /**
     * Create an instance of {@link HotelInfoType.Position }
     * 
     */
    public HotelInfoType.Position createHotelInfoTypePosition() {
        return new HotelInfoType.Position();
    }

    /**
     * Create an instance of {@link HotelInfoType.Descriptions.MultimediaDescriptions }
     * 
     */
    public HotelInfoType.Descriptions.MultimediaDescriptions createHotelInfoTypeDescriptionsMultimediaDescriptions() {
        return new HotelInfoType.Descriptions.MultimediaDescriptions();
    }

    /**
     * Create an instance of {@link MultimediaDescriptionsType.Renovation }
     * 
     */
    public MultimediaDescriptionsType.Renovation createMultimediaDescriptionsTypeRenovation() {
        return new MultimediaDescriptionsType.Renovation();
    }

    /**
     * Create an instance of {@link ArrayOfContactsTypeName.Name }
     * 
     */
    public ArrayOfContactsTypeName.Name createArrayOfContactsTypeNameName() {
        return new ArrayOfContactsTypeName.Name();
    }

    /**
     * Create an instance of {@link HotelDescriptiveContentType.Policies }
     * 
     */
    public HotelDescriptiveContentType.Policies createHotelDescriptiveContentTypePolicies() {
        return new HotelDescriptiveContentType.Policies();
    }

    /**
     * Create an instance of {@link PoliciesType.Policy.GuaranteePaymentPolicy }
     * 
     */
    public PoliciesType.Policy.GuaranteePaymentPolicy createPoliciesTypePolicyGuaranteePaymentPolicy() {
        return new PoliciesType.Policy.GuaranteePaymentPolicy();
    }

    /**
     * Create an instance of {@link PoliciesType.Policy.PolicyInfo }
     * 
     */
    public PoliciesType.Policy.PolicyInfo createPoliciesTypePolicyPolicyInfo() {
        return new PoliciesType.Policy.PolicyInfo();
    }

    /**
     * Create an instance of {@link PoliciesType.Policy.PetsPolicies.PetsPolicy }
     * 
     */
    public PoliciesType.Policy.PetsPolicies.PetsPolicy createPoliciesTypePolicyPetsPoliciesPetsPolicy() {
        return new PoliciesType.Policy.PetsPolicies.PetsPolicy();
    }

    /**
     * Create an instance of {@link RequiredPaymentsType.GuaranteePayment }
     * 
     */
    public RequiredPaymentsType.GuaranteePayment createRequiredPaymentsTypeGuaranteePayment() {
        return new RequiredPaymentsType.GuaranteePayment();
    }

    /**
     * Create an instance of {@link ArrayOfSellableProductsTypeSellableProduct.SellableProduct }
     * 
     */
    public ArrayOfSellableProductsTypeSellableProduct.SellableProduct createArrayOfSellableProductsTypeSellableProductSellableProduct() {
        return new ArrayOfSellableProductsTypeSellableProduct.SellableProduct();
    }

    /**
     * Create an instance of {@link ArrayOfRoomInfoTypeBedType.BedType }
     * 
     */
    public ArrayOfRoomInfoTypeBedType.BedType createArrayOfRoomInfoTypeBedTypeBedType() {
        return new ArrayOfRoomInfoTypeBedType.BedType();
    }

    /**
     * Create an instance of {@link ArrayOfRatePlanShortTypeRatePlan.RatePlan }
     * 
     */
    public ArrayOfRatePlanShortTypeRatePlan.RatePlan createArrayOfRatePlanShortTypeRatePlanRatePlan() {
        return new ArrayOfRatePlanShortTypeRatePlan.RatePlan();
    }

    /**
     * Create an instance of {@link ArrayOfBasicRoomTypeBasicRoom.BasicRoom }
     * 
     */
    public ArrayOfBasicRoomTypeBasicRoom.BasicRoom createArrayOfBasicRoomTypeBasicRoomBasicRoom() {
        return new ArrayOfBasicRoomTypeBasicRoom.BasicRoom();
    }

    /**
     * Create an instance of {@link RateUploadType.MealsIncluded }
     * 
     */
    public RateUploadType.MealsIncluded createRateUploadTypeMealsIncluded() {
        return new RateUploadType.MealsIncluded();
    }

    /**
     * Create an instance of {@link ArrayOfHotelRatePlanTypeRate.Rate }
     * 
     */
    public ArrayOfHotelRatePlanTypeRate.Rate createArrayOfHotelRatePlanTypeRateRate() {
        return new ArrayOfHotelRatePlanTypeRate.Rate();
    }

    /**
     * Create an instance of {@link GuestRoomType.Quantities }
     * 
     */
    public GuestRoomType.Quantities createGuestRoomTypeQuantities() {
        return new GuestRoomType.Quantities();
    }

    /**
     * Create an instance of {@link GuestRoomType.Occupancy }
     * 
     */
    public GuestRoomType.Occupancy createGuestRoomTypeOccupancy() {
        return new GuestRoomType.Occupancy();
    }

    /**
     * Create an instance of {@link GuestRoomType.Room }
     * 
     */
    public GuestRoomType.Room createGuestRoomTypeRoom() {
        return new GuestRoomType.Room();
    }

    /**
     * Create an instance of {@link GuestRoomType.RoomLevelFees }
     * 
     */
    public GuestRoomType.RoomLevelFees createGuestRoomTypeRoomLevelFees() {
        return new GuestRoomType.RoomLevelFees();
    }

    /**
     * Create an instance of {@link GuestRoomType.Currency }
     * 
     */
    public GuestRoomType.Currency createGuestRoomTypeCurrency() {
        return new GuestRoomType.Currency();
    }

    /**
     * Create an instance of {@link ArrayOfPhonesTypePhone.Phone }
     * 
     */
    public ArrayOfPhonesTypePhone.Phone createArrayOfPhonesTypePhonePhone() {
        return new ArrayOfPhonesTypePhone.Phone();
    }

    /**
     * Create an instance of {@link ArrayOfParagraphTypeText.Text }
     * 
     */
    public ArrayOfParagraphTypeText.Text createArrayOfParagraphTypeTextText() {
        return new ArrayOfParagraphTypeText.Text();
    }

    /**
     * Create an instance of {@link ArrayOfAcceptedPaymentsTypeAcceptedPayment.AcceptedPayment }
     * 
     */
    public ArrayOfAcceptedPaymentsTypeAcceptedPayment.AcceptedPayment createArrayOfAcceptedPaymentsTypeAcceptedPaymentAcceptedPayment() {
        return new ArrayOfAcceptedPaymentsTypeAcceptedPayment.AcceptedPayment();
    }

    /**
     * Create an instance of {@link PaymentFormType.PaymentCard }
     * 
     */
    public PaymentFormType.PaymentCard createPaymentFormTypePaymentCard() {
        return new PaymentFormType.PaymentCard();
    }

    /**
     * Create an instance of {@link ImageItemsType.ImageItem }
     * 
     */
    public ImageItemsType.ImageItem createImageItemsTypeImageItem() {
        return new ImageItemsType.ImageItem();
    }

    /**
     * Create an instance of {@link ImageDescriptionType.ImageFormat }
     * 
     */
    public ImageDescriptionType.ImageFormat createImageDescriptionTypeImageFormat() {
        return new ImageDescriptionType.ImageFormat();
    }

    /**
     * Create an instance of {@link ArrayOfTaxesTypeTax.Tax }
     * 
     */
    public ArrayOfTaxesTypeTax.Tax createArrayOfTaxesTypeTaxTax() {
        return new ArrayOfTaxesTypeTax.Tax();
    }

    /**
     * Create an instance of {@link ArrayOfImageItemsTypeImageItem.ImageItem }
     * 
     */
    public ArrayOfImageItemsTypeImageItem.ImageItem createArrayOfImageItemsTypeImageItemImageItem() {
        return new ArrayOfImageItemsTypeImageItem.ImageItem();
    }

    /**
     * Create an instance of {@link ArrayOfGuestRoomTypeAmenity.Amenity }
     * 
     */
    public ArrayOfGuestRoomTypeAmenity.Amenity createArrayOfGuestRoomTypeAmenityAmenity() {
        return new ArrayOfGuestRoomTypeAmenity.Amenity();
    }

    /**
     * Create an instance of {@link RoomInfoType.Ventilation }
     * 
     */
    public RoomInfoType.Ventilation createRoomInfoTypeVentilation() {
        return new RoomInfoType.Ventilation();
    }

    /**
     * Create an instance of {@link RoomInfoType.Equipments.WiFi }
     * 
     */
    public RoomInfoType.Equipments.WiFi createRoomInfoTypeEquipmentsWiFi() {
        return new RoomInfoType.Equipments.WiFi();
    }

    /**
     * Create an instance of {@link RoomInfoType.Equipments.LAN }
     * 
     */
    public RoomInfoType.Equipments.LAN createRoomInfoTypeEquipmentsLAN() {
        return new RoomInfoType.Equipments.LAN();
    }

    /**
     * Create an instance of {@link ArrayOfHotelStatsInfoTypeHotel.Hotel }
     * 
     */
    public ArrayOfHotelStatsInfoTypeHotel.Hotel createArrayOfHotelStatsInfoTypeHotelHotel() {
        return new ArrayOfHotelStatsInfoTypeHotel.Hotel();
    }

    /**
     * Create an instance of {@link TextItemsType.TextItem }
     * 
     */
    public TextItemsType.TextItem createTextItemsTypeTextItem() {
        return new TextItemsType.TextItem();
    }

    /**
     * Create an instance of {@link ArrayOfAffiliationInfoTypeAward.Award }
     * 
     */
    public ArrayOfAffiliationInfoTypeAward.Award createArrayOfAffiliationInfoTypeAwardAward() {
        return new ArrayOfAffiliationInfoTypeAward.Award();
    }

    /**
     * Create an instance of {@link SellableProductsType.SellableProduct }
     * 
     */
    public SellableProductsType.SellableProduct createSellableProductsTypeSellableProduct() {
        return new SellableProductsType.SellableProduct();
    }

    /**
     * Create an instance of {@link CommissionType.CommissionableAmount }
     * 
     */
    public CommissionType.CommissionableAmount createCommissionTypeCommissionableAmount() {
        return new CommissionType.CommissionableAmount();
    }

    /**
     * Create an instance of {@link CommissionType.PrepaidAmount }
     * 
     */
    public CommissionType.PrepaidAmount createCommissionTypePrepaidAmount() {
        return new CommissionType.PrepaidAmount();
    }

    /**
     * Create an instance of {@link CommissionType.FlatCommission }
     * 
     */
    public CommissionType.FlatCommission createCommissionTypeFlatCommission() {
        return new CommissionType.FlatCommission();
    }

    /**
     * Create an instance of {@link CommissionType.CommissionPayableAmount }
     * 
     */
    public CommissionType.CommissionPayableAmount createCommissionTypeCommissionPayableAmount() {
        return new CommissionType.CommissionPayableAmount();
    }

    /**
     * Create an instance of {@link ArrayOfAddressesTypeAddress.Address }
     * 
     */
    public ArrayOfAddressesTypeAddress.Address createArrayOfAddressesTypeAddressAddress() {
        return new ArrayOfAddressesTypeAddress.Address();
    }

    /**
     * Create an instance of {@link CancelPenaltyType.Deadline }
     * 
     */
    public CancelPenaltyType.Deadline createCancelPenaltyTypeDeadline() {
        return new CancelPenaltyType.Deadline();
    }

    /**
     * Create an instance of {@link CancelPenaltyType.AmountPercent }
     * 
     */
    public CancelPenaltyType.AmountPercent createCancelPenaltyTypeAmountPercent() {
        return new CancelPenaltyType.AmountPercent();
    }

    /**
     * Create an instance of {@link ArrayOfPoliciesTypePolicyPolicyInfoCode.PolicyInfoCode }
     * 
     */
    public ArrayOfPoliciesTypePolicyPolicyInfoCode.PolicyInfoCode createArrayOfPoliciesTypePolicyPolicyInfoCodePolicyInfoCode() {
        return new ArrayOfPoliciesTypePolicyPolicyInfoCode.PolicyInfoCode();
    }

    /**
     * Create an instance of {@link ArrayOfGDSInfoTypeGDSCodesGDSCode.GDSCode }
     * 
     */
    public ArrayOfGDSInfoTypeGDSCodesGDSCode.GDSCode createArrayOfGDSInfoTypeGDSCodesGDSCodeGDSCode() {
        return new ArrayOfGDSInfoTypeGDSCodesGDSCode.GDSCode();
    }

    /**
     * Create an instance of {@link SourceType.RequestorID }
     * 
     */
    public SourceType.RequestorID createSourceTypeRequestorID() {
        return new SourceType.RequestorID();
    }

    /**
     * Create an instance of {@link ArrayOfNationalityTypeNationality.Nationality }
     * 
     */
    public ArrayOfNationalityTypeNationality.Nationality createArrayOfNationalityTypeNationalityNationality() {
        return new ArrayOfNationalityTypeNationality.Nationality();
    }

    /**
     * Create an instance of {@link PaymentDetailType.PaymentAmount }
     * 
     */
    public PaymentDetailType.PaymentAmount createPaymentDetailTypePaymentAmount() {
        return new PaymentDetailType.PaymentAmount();
    }

    /**
     * Create an instance of {@link ArrayOfHotelInfoTypeService.Service }
     * 
     */
    public ArrayOfHotelInfoTypeService.Service createArrayOfHotelInfoTypeServiceService() {
        return new ArrayOfHotelInfoTypeService.Service();
    }

    /**
     * Create an instance of {@link OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents.HotelDescriptiveContent }
     * 
     */
    public OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents.HotelDescriptiveContent createOTAHotelDescriptiveContentNotifRQHotelDescriptiveContentsHotelDescriptiveContent() {
        return new OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents.HotelDescriptiveContent();
    }

    /**
     * Create an instance of {@link ArrayOfStatisticsTypeStatistic.Statistic }
     * 
     */
    public ArrayOfStatisticsTypeStatistic.Statistic createArrayOfStatisticsTypeStatisticStatistic() {
        return new ArrayOfStatisticsTypeStatistic.Statistic();
    }

    /**
     * Create an instance of {@link OTAHotelRatePlanNotifRQ.RatePlans.RatePlan }
     * 
     */
    public OTAHotelRatePlanNotifRQ.RatePlans.RatePlan createOTAHotelRatePlanNotifRQRatePlansRatePlan() {
        return new OTAHotelRatePlanNotifRQ.RatePlans.RatePlan();
    }

    /**
     * Create an instance of {@link OTAHotelStatsNotifRS.TPAExtensions }
     * 
     */
    public OTAHotelStatsNotifRS.TPAExtensions createOTAHotelStatsNotifRSTPAExtensions() {
        return new OTAHotelStatsNotifRS.TPAExtensions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelRatePlanNotifRS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelRatePlanNotifRS")
    public JAXBElement<OTAHotelRatePlanNotifRS> createOTAHotelRatePlanNotifRS(OTAHotelRatePlanNotifRS value) {
        return new JAXBElement<OTAHotelRatePlanNotifRS>(_OTAHotelRatePlanNotifRS_QNAME, OTAHotelRatePlanNotifRS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelStatsNotifRS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelStatsNotifRS")
    public JAXBElement<OTAHotelStatsNotifRS> createOTAHotelStatsNotifRS(OTAHotelStatsNotifRS value) {
        return new JAXBElement<OTAHotelStatsNotifRS>(_OTAHotelStatsNotifRS_QNAME, OTAHotelStatsNotifRS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelInvNotifRS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelInvNotifRS")
    public JAXBElement<OTAHotelInvNotifRS> createOTAHotelInvNotifRS(OTAHotelInvNotifRS value) {
        return new JAXBElement<OTAHotelInvNotifRS>(_OTAHotelInvNotifRS_QNAME, OTAHotelInvNotifRS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelDescriptiveContentNotifRS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelDescriptiveContentNotifRS")
    public JAXBElement<OTAHotelDescriptiveContentNotifRS> createOTAHotelDescriptiveContentNotifRS(OTAHotelDescriptiveContentNotifRS value) {
        return new JAXBElement<OTAHotelDescriptiveContentNotifRS>(_OTAHotelDescriptiveContentNotifRS_QNAME, OTAHotelDescriptiveContentNotifRS.class, null, value);
    }

}
