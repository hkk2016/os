package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ListItem", namespace = Namespace.NS)
public class ListItem {
	@XmlAttribute(name = "Listitem")
	String Listitem;
	@XmlValue
	String text;

	public String getListitem() {
		return Listitem;
	}

	public void setListitem(String listitem) {
		Listitem = listitem;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
