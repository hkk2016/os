package com.channel.connector.ctripos.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.ctripos.domain.CtriposShopInfoDO;

public interface CtriposShopInfoMapper extends BaseMapper<CtriposShopInfoDO> {
}