/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:15:49
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RoomDescription", namespace = Namespace.NS)
public class RoomDescription {

	@XmlElement(name = "Text", namespace = Namespace.NS)
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
