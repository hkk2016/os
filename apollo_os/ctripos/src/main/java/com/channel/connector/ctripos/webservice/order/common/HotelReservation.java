package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.request.dto.ResGuest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="HotelReservation", namespace = Namespace.NS)
public class HotelReservation {
	@XmlElementWrapper(name = "RoomStays", namespace = Namespace.NS)
	@XmlElement(name = "RoomStay", namespace = Namespace.NS)
	private List<RoomStay> roomStays;
	
	@XmlElementWrapper(name = "ResGuests", namespace = Namespace.NS)
	@XmlElement(name = "ResGuest", namespace = Namespace.NS)
	private List<ResGuest> resGuests;
	
	@XmlElement(name = "ResGlobalInfo", namespace = Namespace.NS)
	private ResGlobalInfo resGlobalInfo;

	@XmlAttribute(name = "ResStatus")
	private String resStatus;

	public List<RoomStay> getRoomStays() {
		return roomStays;
	}
	public void setRoomStays(List<RoomStay> roomStays) {
		this.roomStays = roomStays;
	}
	public List<ResGuest> getResGuests() {
		return resGuests;
	}
	public void setResGuests(List<ResGuest> resGuests) {
		this.resGuests = resGuests;
	}
	public ResGlobalInfo getResGlobalInfo() {
		return resGlobalInfo;
	}
	public void setResGlobalInfo(ResGlobalInfo resGlobalInfo) {
		this.resGlobalInfo = resGlobalInfo;
	}

	public String getResStatus() {
		return resStatus;
	}

	public void setResStatus(String resStatus) {
		this.resStatus = resStatus;
	}
}
