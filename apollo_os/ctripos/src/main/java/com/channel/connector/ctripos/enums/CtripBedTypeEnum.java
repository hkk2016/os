package com.channel.connector.ctripos.enums;

import java.io.Serializable;


public enum CtripBedTypeEnum implements Serializable {

	
	KING("5","大床"),TWIN("1","双床"),SINGLE("9","单床"),BUNK("14","上下铺"),BUNKHOUSE("12","通铺");

	public String key;
	public String value;

	private CtripBedTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public static String getKeyByValue(String value) {
		String key = null;
		for(CtripBedTypeEnum CtripBedTypeEnum : CtripBedTypeEnum.values()) {
			if(CtripBedTypeEnum.value.equals(value)) {
				key = CtripBedTypeEnum.key;
				break;
			}
		}
		return key;
	}
	
	public static String getValueByKey(String key) {
		String value = null;
		for(CtripBedTypeEnum CtripBedTypeEnum : CtripBedTypeEnum.values()) {
			if(CtripBedTypeEnum.key.equals(key)) {
				value = CtripBedTypeEnum.value;
				break;
			}
		}
		return value;
	}
	
	public static CtripBedTypeEnum  getEnumByKey(String key){
		CtripBedTypeEnum CtripBedTypeEnum = null;
		for(CtripBedTypeEnum bedType : CtripBedTypeEnum.values()) {
			if(bedType.key.equals(key)) {
				CtripBedTypeEnum = bedType;
				break;
			}
		}
		return CtripBedTypeEnum;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
