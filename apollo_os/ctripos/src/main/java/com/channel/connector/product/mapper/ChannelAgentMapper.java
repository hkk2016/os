package com.channel.connector.product.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.ChannelAgentDO;

public interface ChannelAgentMapper extends BaseMapper<ChannelAgentDO> {
}