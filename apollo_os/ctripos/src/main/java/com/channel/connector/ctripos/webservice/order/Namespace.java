/****************************************
 * 携程海外项目 @天下房仓 2016-1-15 下午05:26:56
 ****************************************/
package com.channel.connector.ctripos.webservice.order;

public class Namespace {

	public static final String NS = "http://www.opentravel.org/OTA/2003/05";
	public static final String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
	
}
