package com.channel.connector.ctripos.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.ctripos.domain.CtriposMapHotelDO;
import com.channel.connector.ctripos.domain.CtriposMapRateplanDO;
import com.channel.connector.ctripos.domain.CtriposMapRoomtypeDO;
import com.channel.connector.ctripos.domain.CtriposShopInfoDO;
import com.channel.connector.ctripos.dto.request.QueryRatePlanMappingRequestDTO;

import java.util.List;

public interface CtriposCommonMapper extends BaseMapper<CtriposShopInfoDO> {
    /**
     * 批量保存或更新酒店映射信息
     *
     * @param ctripOsHotelMappingPOS
     */
    void batchSaveOrUpdateCtripOsHotelMapping(List<CtriposMapHotelDO> ctripOsHotelMappingPOS);

    /**
     * 批量保存或更新房型映射信息
     *
     * @param ctripOsRoomTypeMappingPOS
     */
    void batchSaveOrUpdateCtripOsRoomTypeMapping(List<CtriposMapRoomtypeDO> ctripOsRoomTypeMappingPOS);

    /**
     * 批量保存或更新价格计划信息
     *
     * @param ctripOsRatePlanMappingPOS
     */
    void batchSaveOrUpdateCtripOsRatePlanMapping(List<CtriposMapRateplanDO> ctripOsRatePlanMappingPOS);

    /**
     * 批量更新携程酒店映射关系
     *
     * @param ctripOsHotelMappingPOS
     */
    void batchUpdateCtripOsHotelMapping(List<CtriposMapHotelDO> ctripOsHotelMappingPOS);

    /**
     * 批量更新携程房型映射关系
     *
     * @param ctripOsRoomTypeMappingPOS
     */
    void batchUpdateCtripOsRoomTypeMapping(List<CtriposMapRoomtypeDO> ctripOsRoomTypeMappingPOS);

    /**
     * 批量更新携程子房型映射关系
     *
     * @param ctripOsRatePlanMappingPOS
     */
    void batchUpdateCtripOsRatePlanMapping(List<CtriposMapRateplanDO> ctripOsRatePlanMappingPOS);

    /**
     * 查询映射的商品列表
     *
     * @param ratePlanMappingRequestDTO
     * @return
     */
    List<CtriposMapRateplanDO> queryRatePlanMapping(QueryRatePlanMappingRequestDTO ratePlanMappingRequestDTO);
}