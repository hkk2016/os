/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:24:59
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AmountPercent", namespace = Namespace.NS)
public class AmountPercent {

	@XmlAttribute(name = "NmbrOfNights")
	private Integer nmbrOfNights;
	
	@XmlAttribute(name = "Percentage")
	private String percentage;
	
	@XmlAttribute(name = "Amount")
	private Double amount;

	@XmlAttribute(name = "TaxInclusive")
	private Boolean raxInclusive;

	public Integer getNmbrOfNights() {
		return nmbrOfNights;
	}

	public void setNmbrOfNights(Integer nmbrOfNights) {
		this.nmbrOfNights = nmbrOfNights;
	}

	public Boolean isRaxInclusive() {
		return raxInclusive;
	}

	public void setRaxInclusive(Boolean raxInclusive) {
		this.raxInclusive = raxInclusive;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
}
