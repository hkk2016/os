package com.channel.connector.ctripos.service;

import com.channel.connector.ctripos.dto.ResponseDTO;
import com.channel.connector.ctripos.dto.request.BatchSyncCtripRequest;

/**
 * Created by 40293 on 2018/11/29.
 */
public interface CtriposMappingService {


    /**
     * 推送酒店信息到携程
     *
     * @param pushHotelRequestDTO
     * @return
     */
    ResponseDTO pushHotel(BatchSyncCtripRequest pushHotelRequestDTO);


    /**
     * 从携程同步酒店映射状态
     *
     * @param batchSyncCtripRequest
     * @return
     */
    ResponseDTO syncHotel(BatchSyncCtripRequest batchSyncCtripRequest);
}
