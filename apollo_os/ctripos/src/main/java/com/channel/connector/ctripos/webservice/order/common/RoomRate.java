/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:28:43
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RoomRate", namespace = Namespace.NS)
public class RoomRate {

	@XmlAttribute(name = "RoomTypeCode")
	private String roomTypeCode;

	@XmlAttribute(name = "RatePlanCode")
	private String ratePlanCode;

	@XmlAttribute(name = "RatePlanCategory")
	private String ratePlanCategory = "501";

	@XmlElementWrapper(name = "Rates", namespace = Namespace.NS)
	@XmlElement(name = "Rate", namespace = Namespace.NS)
	private List<Rate> rates;

	@XmlElement(name = "Total", namespace = Namespace.NS)
	private Total total;

	@XmlAttribute(name = "NumberOfUnits")
	private String numberOfUnits;

	public String getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(String numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public List<Rate> getRates() {
		if (null == rates) {
			rates = new ArrayList<Rate>();
		}
		return rates;
	}

	public void setRates(List<Rate> rates) {
		this.rates = rates;
	}

	public String getRatePlanCategory() {
		return ratePlanCategory;
	}

	public void setRatePlanCategory(String ratePlanCategory) {
		this.ratePlanCategory = ratePlanCategory;
	}

	public Total getTotal() {
		return total;
	}

	public void setTotal(Total total) {
		this.total = total;
	}
}
