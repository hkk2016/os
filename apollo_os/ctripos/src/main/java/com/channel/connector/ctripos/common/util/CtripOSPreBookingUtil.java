/****************************************
 * 携程海外项目 @天下房仓 2016-1-13 下午08:30:12
 ****************************************/
package com.channel.connector.ctripos.common.util;

import com.channel.connector.ctripos.dto.bms.response.SupplyProductPriceResponseDTO;
import com.channel.connector.product.domain.PriceAndQuotaDO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CtripOSPreBookingUtil {

    private static Log log = LogFactory.getLog(CtripOSPreBookingUtil.class);

    public static String validateForHotelMode(PriceAndQuotaDO priceAndQuotaDO, Long commodityId) {
        // 无底价||价格为待查||无加幅类型||无加幅||无售卖状态||无房态||房态待查||满房||有房配额小于1||预订条款||间数条款||连住大于1||限住||必住
        String result = "SUCCESS";
        if (null == priceAndQuotaDO) {
            result = "Commodity item is null";
        }
        if (null == priceAndQuotaDO.getCtriposSalePrice()) {
            result = "Commodity sale price is null";
        }
        if (null == priceAndQuotaDO.getCtriposSaleState() || 0 == priceAndQuotaDO.getCtriposSaleState()) {
            result = "Commodity sale state is null";
        }
        if (null == priceAndQuotaDO.getQuotaState()) {
            result = "Commodity no room state";
        } else {
            if ((1 == priceAndQuotaDO.getQuotaState() && (null == priceAndQuotaDO.getQuotaNum() ||
                    priceAndQuotaDO.getQuotaNum() < 1))) {
                result = "Commodity quota is 0";
            }
        }
        if (null == priceAndQuotaDO.getIsActive() || 0 == priceAndQuotaDO.getIsActive()) {
            result = "Commodity active is 0";
        }
        if (!"SUCCESS".equals(result)) {
            log.error("Commodity " + commodityId + " cannot booking,reason:" + result);
        }
        return result;
    }

    public static String validateForCommodityMode(SupplyProductPriceResponseDTO productPrice, Long commodityId) {

        String result = "SUCCESS";
        if (null == productPrice) {
            result = "Commodity item is null";
        }
        if (null == productPrice.getBasePrice()) {
            result = "Commodity base price is null";
        }
        if (null == productPrice.getSalePrice()) {
            result = "Commodity sale price is null";
        }
        if (null == productPrice.getQuotaState()) {
            result = "Commodity no room state";
        }
        if ((productPrice.getQuotaState() == 1 && (null == productPrice.getQuotaNum() || productPrice.getQuotaNum() < 1))) {
            result = "Commodity quota is 0";
        }
        if (!"SUCCESS".equals(result)) {
            log.error("Commodity " + commodityId + " cannot booking,reason:" + result);
        }
        return result;
    }

}
