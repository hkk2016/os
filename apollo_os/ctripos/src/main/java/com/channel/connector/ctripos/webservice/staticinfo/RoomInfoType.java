
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RoomInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="RoomInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ventilation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="HasWindow" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Equipments" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="WiFi" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
 *                           &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="LAN" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
 *                           &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BedTypes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRoomInfoTypeBedType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoomInfoType", propOrder = {
    "ventilation",
    "equipments",
    "bedTypes"
})
public class RoomInfoType {

    @XmlElement(name = "Ventilation")
    protected RoomInfoType.Ventilation ventilation;
    @XmlElement(name = "Equipments")
    protected RoomInfoType.Equipments equipments;
    @XmlElement(name = "BedTypes")
    protected ArrayOfRoomInfoTypeBedType bedTypes;

    /**
     * 获取ventilation属性的值。
     *
     * @return
     *     possible object is
     *     {@link RoomInfoType.Ventilation }
     *
     */
    public RoomInfoType.Ventilation getVentilation() {
        return ventilation;
    }

    /**
     * 设置ventilation属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link RoomInfoType.Ventilation }
     *
     */
    public void setVentilation(RoomInfoType.Ventilation value) {
        this.ventilation = value;
    }

    /**
     * 获取equipments属性的值。
     *
     * @return
     *     possible object is
     *     {@link RoomInfoType.Equipments }
     *
     */
    public RoomInfoType.Equipments getEquipments() {
        return equipments;
    }

    /**
     * 设置equipments属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link RoomInfoType.Equipments }
     *
     */
    public void setEquipments(RoomInfoType.Equipments value) {
        this.equipments = value;
    }

    /**
     * 获取bedTypes属性的值。
     *
     * @return
     *     possible object is
     *     {@link ArrayOfRoomInfoTypeBedType }
     *
     */
    public ArrayOfRoomInfoTypeBedType getBedTypes() {
        return bedTypes;
    }

    /**
     * 设置bedTypes属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfRoomInfoTypeBedType }
     *
     */
    public void setBedTypes(ArrayOfRoomInfoTypeBedType value) {
        this.bedTypes = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     *
     * <p>以下模式片段指定包含在此类中的预期内容。
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="WiFi" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
     *                 &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="LAN" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
     *                 &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "wiFi",
        "lan"
    })
    public static class Equipments {

        @XmlElement(name = "WiFi")
        protected RoomInfoType.Equipments.WiFi wiFi;
        @XmlElement(name = "LAN")
        protected RoomInfoType.Equipments.LAN lan;

        /**
         * 获取wiFi属性的值。
         *
         * @return
         *     possible object is
         *     {@link RoomInfoType.Equipments.WiFi }
         *
         */
        public RoomInfoType.Equipments.WiFi getWiFi() {
            return wiFi;
        }

        /**
         * 设置wiFi属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link RoomInfoType.Equipments.WiFi }
         *
         */
        public void setWiFi(RoomInfoType.Equipments.WiFi value) {
            this.wiFi = value;
        }

        /**
         * 获取lan属性的值。
         *
         * @return
         *     possible object is
         *     {@link RoomInfoType.Equipments.LAN }
         *
         */
        public RoomInfoType.Equipments.LAN getLAN() {
            return lan;
        }

        /**
         * 设置lan属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link RoomInfoType.Equipments.LAN }
         *
         */
        public void setLAN(RoomInfoType.Equipments.LAN value) {
            this.lan = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
         *       &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class LAN {

            @XmlAttribute(name = "Enable")
            protected JudgeEnum enable;
            @XmlAttribute(name = "isChargeable")
            protected JudgeEnum isChargeable;

            /**
             * 获取enable属性的值。
             * 
             * @return
             *     possible object is
             *     {@link JudgeEnum }
             *     
             */
            public JudgeEnum getEnable() {
                return enable;
            }

            /**
             * 设置enable属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link JudgeEnum }
             *     
             */
            public void setEnable(JudgeEnum value) {
                this.enable = value;
            }

            /**
             * 获取isChargeable属性的值。
             * 
             * @return
             *     possible object is
             *     {@link JudgeEnum }
             *     
             */
            public JudgeEnum getIsChargeable() {
                return isChargeable;
            }

            /**
             * 设置isChargeable属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link JudgeEnum }
             *     
             */
            public void setIsChargeable(JudgeEnum value) {
                this.isChargeable = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="Enable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
         *       &lt;attribute name="isChargeable" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class WiFi {

            @XmlAttribute(name = "Enable")
            protected JudgeEnum enable;
            @XmlAttribute(name = "isChargeable")
            protected JudgeEnum isChargeable;

            /**
             * 获取enable属性的值。
             * 
             * @return
             *     possible object is
             *     {@link JudgeEnum }
             *     
             */
            public JudgeEnum getEnable() {
                return enable;
            }

            /**
             * 设置enable属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link JudgeEnum }
             *     
             */
            public void setEnable(JudgeEnum value) {
                this.enable = value;
            }

            /**
             * 获取isChargeable属性的值。
             * 
             * @return
             *     possible object is
             *     {@link JudgeEnum }
             *     
             */
            public JudgeEnum getIsChargeable() {
                return isChargeable;
            }

            /**
             * 设置isChargeable属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link JudgeEnum }
             *     
             */
            public void setIsChargeable(JudgeEnum value) {
                this.isChargeable = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="HasWindow" type="{http://www.opentravel.org/OTA/2003/05}JudgeEnum" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Ventilation {

        @XmlAttribute(name = "HasWindow")
        protected JudgeEnum hasWindow;

        /**
         * 获取hasWindow属性的值。
         * 
         * @return
         *     possible object is
         *     {@link JudgeEnum }
         *     
         */
        public JudgeEnum getHasWindow() {
            return hasWindow;
        }

        /**
         * 设置hasWindow属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link JudgeEnum }
         *     
         */
        public void setHasWindow(JudgeEnum value) {
            this.hasWindow = value;
        }

    }

}
