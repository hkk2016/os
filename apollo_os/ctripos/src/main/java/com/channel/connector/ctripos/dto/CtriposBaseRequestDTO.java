package com.channel.connector.ctripos.dto;

import lombok.Data;

/**
 * Created by 40293 on 2018/12/3.
 */
@Data
public class CtriposBaseRequestDTO extends BaseRequestDTO{

    private static final long serialVersionUID = 8599846080639517136L;

    private String merchantCode;
    private String channelCode;
    private String shopId;

    public CtriposBaseRequestDTO() {
    }

    public CtriposBaseRequestDTO(String merchantCode, String channelCode, String shopId) {
        this.merchantCode = merchantCode;
        this.channelCode = channelCode;
        this.shopId = shopId;
    }
}
