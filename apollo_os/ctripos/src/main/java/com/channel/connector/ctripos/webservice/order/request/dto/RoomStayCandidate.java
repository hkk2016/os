/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:51:13
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.common.GuestCount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RoomStayCandidate", namespace = Namespace.NS)
public class RoomStayCandidate {

	@XmlAttribute(name = "RoomTypeCode")
	private String roomTypeCode;
	
	@XmlAttribute(name = "Quantity")
	private Integer qauantity;

	@XmlElementWrapper(name = "GuestCounts", namespace = Namespace.NS)
	@XmlElement(name = "GuestCount", namespace = Namespace.NS)
	private List<GuestCount> guestCounts;
	
	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public Integer getQauantity() {
		return qauantity;
	}

	public void setQauantity(Integer qauantity) {
		this.qauantity = qauantity;
	}

	public List<GuestCount> getGuestCounts() {
		if (null == guestCounts) {
			guestCounts = new ArrayList<GuestCount>();
		}
		return guestCounts;
	}

	public void setGuestCounts(List<GuestCount> guestCounts) {
		this.guestCounts = guestCounts;
	}
	
}
