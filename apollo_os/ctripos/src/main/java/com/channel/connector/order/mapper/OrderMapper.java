package com.channel.connector.order.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.order.domain.OrderDO;

public interface OrderMapper extends BaseMapper<OrderDO> {
}