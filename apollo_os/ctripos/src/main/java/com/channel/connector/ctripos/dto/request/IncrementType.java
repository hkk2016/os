package com.channel.connector.ctripos.dto.request;

import lombok.Data;

/**
 * Created by 402931207@qq.com on 2018/12/12.
 */
@Data
public class IncrementType {

    /**
     * 如果是ratePlan增量，则查询结果为isActive=0则调用删除接口，如果是isActive=1则调用update接口，同步其他的基本信息过去。
     */
    private boolean ratePlan;
    private boolean price;
    private boolean roomStatus;

    /**
     * 如果是saleState增量，则查询结果为onsale=0则调用update接口（status=2），如果是onsale=1则调用update接口（status=1）和房价接口。
     */
    private boolean saleState;
    private boolean restrict;

    public IncrementType() {
    }

    public IncrementType(boolean price, boolean roomStatus) {
        this.price = price;
        this.roomStatus = roomStatus;
    }

    public IncrementType(boolean ratePlan, boolean price, boolean roomStatus, boolean saleState, boolean restrict) {
        this.ratePlan = ratePlan;
        this.price = price;
        this.roomStatus = roomStatus;
        this.saleState = saleState;
        this.restrict = restrict;
    }

    public IncrementType andRatePlan(){
        this.ratePlan = true;
        return this;
    }

    public IncrementType andPrice(){
        this.price = true;
        return this;
    }

    public IncrementType andRoomStatus(){
        this.roomStatus = true;
        return this;
    }

    public IncrementType andSaleState(){
        this.saleState = true;
        return this;
    }

    public IncrementType andRestrict(){
        this.restrict = true;
        return this;
    }
}
