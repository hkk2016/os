package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Base", namespace = Namespace.NS)
public class Base {
	@XmlAttribute(name="AmountBeforeTax")
	private String amountBeforeTax;
	@XmlAttribute(name="AmountAfterTax")
	private String amountAfterTax;
	@XmlAttribute(name="CurrencyCode")
	private String currencyCode;

	public String getAmountBeforeTax() {
		return amountBeforeTax;
	}

	public void setAmountBeforeTax(String amountBeforeTax) {
		this.amountBeforeTax = amountBeforeTax;
	}

	public String getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(String amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
