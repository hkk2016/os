
package com.channel.connector.ctripos.webservice.confirmnum;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the test package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OTAHotelResNumUpdateRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelResNumUpdateRS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: test
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OTAHotelResNumUpdateRQ }
     * 
     */
    public OTAHotelResNumUpdateRQ createOTAHotelResNumUpdateRQ() {
        return new OTAHotelResNumUpdateRQ();
    }

    /**
     * Create an instance of {@link SourceType }
     * 
     */
    public SourceType createSourceType() {
        return new SourceType();
    }

    /**
     * Create an instance of {@link ErrorsType }
     * 
     */
    public ErrorsType createErrorsType() {
        return new ErrorsType();
    }

    /**
     * Create an instance of {@link OTAHotelResNumUpdateRS }
     * 
     */
    public OTAHotelResNumUpdateRS createOTAHotelResNumUpdateRS() {
        return new OTAHotelResNumUpdateRS();
    }

    /**
     * Create an instance of {@link POSType }
     * 
     */
    public POSType createPOSType() {
        return new POSType();
    }

    /**
     * Create an instance of {@link OTAHotelResNumUpdateRQ.HotelReservations }
     * 
     */
    public OTAHotelResNumUpdateRQ.HotelReservations createOTAHotelResNumUpdateRQHotelReservations() {
        return new OTAHotelResNumUpdateRQ.HotelReservations();
    }

    /**
     * Create an instance of {@link ResGlobalInfoType }
     * 
     */
    public ResGlobalInfoType createResGlobalInfoType() {
        return new ResGlobalInfoType();
    }

    /**
     * Create an instance of {@link UniqueIDType }
     * 
     */
    public UniqueIDType createUniqueIDType() {
        return new UniqueIDType();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link CompanyNameType }
     * 
     */
    public CompanyNameType createCompanyNameType() {
        return new CompanyNameType();
    }

    /**
     * Create an instance of {@link ArrayOfHotelReservationIDType }
     * 
     */
    public ArrayOfHotelReservationIDType createArrayOfHotelReservationIDType() {
        return new ArrayOfHotelReservationIDType();
    }

    /**
     * Create an instance of {@link HotelReservationType }
     * 
     */
    public HotelReservationType createHotelReservationType() {
        return new HotelReservationType();
    }

    /**
     * Create an instance of {@link SuccessType }
     * 
     */
    public SuccessType createSuccessType() {
        return new SuccessType();
    }

    /**
     * Create an instance of {@link HotelReservationIDType }
     * 
     */
    public HotelReservationIDType createHotelReservationIDType() {
        return new HotelReservationIDType();
    }

    /**
     * Create an instance of {@link SourceType.RequestorID }
     * 
     */
    public SourceType.RequestorID createSourceTypeRequestorID() {
        return new SourceType.RequestorID();
    }

    /**
     * Create an instance of {@link ErrorsType.Error }
     * 
     */
    public ErrorsType.Error createErrorsTypeError() {
        return new ErrorsType.Error();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelResNumUpdateRS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelResNumUpdateRS")
    public JAXBElement<OTAHotelResNumUpdateRS> createOTAHotelResNumUpdateRS(OTAHotelResNumUpdateRS value) {
        return new JAXBElement<OTAHotelResNumUpdateRS>(_OTAHotelResNumUpdateRS_QNAME, OTAHotelResNumUpdateRS.class, null, value);
    }

}
