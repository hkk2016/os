package com.channel.connector.ctripos.enums;

/**
 * Created by Ben on 2016/11/28.
 */

/**
 * 入住条款枚举类
 */
public enum OccupancyRestrictEnum {

    CONTINUE_OCCUPANCY(1, "连住条款"), LIMIT_OCCUPANCY(2, "限住条款"), GREATEST_OCCUPANCY(3, "最多住条款");

    public int key;
    public String value;

    private OccupancyRestrictEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValueByKey(int key) {
        for(OccupancyRestrictEnum occupancyRestrictEnum : OccupancyRestrictEnum.values()) {
            if(occupancyRestrictEnum.key == key) {
                return occupancyRestrictEnum.value;
            }
        }
        return null;
    }
}
