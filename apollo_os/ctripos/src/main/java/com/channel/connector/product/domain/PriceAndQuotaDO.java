package com.channel.connector.product.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PriceAndQuotaDO{


    private Integer hotelId;

    /**
     * 房型ID
     */
    private Integer roomTypeId;

    private Integer priceplanId;

    private String priceplanName;

    private Integer payMethod;

    private Integer isActive;

    /**
     * 配额账户ID
     */
    private Integer quotaaccountId;

    /**
     * 售卖日期
     */
    private Date saleDate;


    /**
     * 配额数量
     */
    private Integer quotaNum;

    /**
     * 房态(0关房  1开房)
     */
    private Integer quotaState;

    /**
     * 携程海外价格
     */
    private BigDecimal ctriposSalePrice;

    /**
     * 是否可超(0不可超，1可超)
     */
    private Integer overDraft;

    /**
     * 销售状态，1上架，0下架
     */
    private Integer ctriposSaleState;

    /**
     * 连住天数
     */
    private Integer occupancyOfDays;

    /**
     * 早餐类型
     * com.fangcang.common.enums.BreakFastTypeEnum
     */
    private Integer breakFastType;

}