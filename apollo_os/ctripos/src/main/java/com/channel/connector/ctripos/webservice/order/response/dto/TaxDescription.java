/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:06:48
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class TaxDescription {

	@XmlElement(name = "Text", namespace = Namespace.NS)
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
