package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ResGuest", namespace = Namespace.NS)
public class ResGuest {
	@XmlAttribute(name = "ArrivalTime")
	private String arrivalTime;

	@XmlElementWrapper(name = "Profiles", namespace = Namespace.NS)
	@XmlElement(name = "ProfileInfo", namespace = Namespace.NS)
	private List<ProfileInfo> profiles;

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public List<ProfileInfo> getProfiles() {
		if (null == profiles) {
			profiles = new ArrayList<ProfileInfo>();
		}
		return profiles;
	}

	public void setProfiles(List<ProfileInfo> profiles) {
		this.profiles = profiles;
	}

}
