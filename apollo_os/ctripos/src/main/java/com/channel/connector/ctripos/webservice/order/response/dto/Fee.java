/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:11:55
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Fee", namespace = Namespace.NS)
public class Fee {

	@XmlAttribute(name = "Type")
	private String type;

	@XmlAttribute(name = "Amount")
	private Double amount;

	@XmlAttribute(name = "CurrencyCode")
	private String currencyCode;

	@XmlAttribute(name = "ChargeUnit")
	private Integer chargeUnit;

	@XmlAttribute(name = "Code")
	private Integer code;

	@XmlElement(name = "Description", namespace = Namespace.NS)
	private Description Description;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getChargeUnit() {
		return chargeUnit;
	}

	public void setChargeUnit(Integer chargeUnit) {
		this.chargeUnit = chargeUnit;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Description getDescription() {
		return Description;
	}

	public void setDescription(Description description) {
		Description = description;
	}
	
}
