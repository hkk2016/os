package com.channel.connector.ctripos.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_areadata")
public class AreaDataDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer type;

    @Column(name = "data_code")
    private String dataCode;

    @Column(name = "data_name")
    private String dataName;

    private String pinyin;

    private String acronympinyin;

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "eng_dataname")
    private String engDataname;

    @Column(name = "country_code")
    private String countryCode;

    private String creator;

    @Column(name = "create_time")
    private Date createTime;

    private String modifier;

    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 洲编码
     */
    @Column(name = "continent_code")
    private String continentCode;

    /**
     * 洲名称
     */
    @Column(name = "continent_name")
    private String continentName;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return data_code
     */
    public String getDataCode() {
        return dataCode;
    }

    /**
     * @param dataCode
     */
    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }

    /**
     * @return data_name
     */
    public String getDataName() {
        return dataName;
    }

    /**
     * @param dataName
     */
    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    /**
     * @return pinyin
     */
    public String getPinyin() {
        return pinyin;
    }

    /**
     * @param pinyin
     */
    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    /**
     * @return acronympinyin
     */
    public String getAcronympinyin() {
        return acronympinyin;
    }

    /**
     * @param acronympinyin
     */
    public void setAcronympinyin(String acronympinyin) {
        this.acronympinyin = acronympinyin;
    }

    /**
     * @return parent_id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * @param parentId
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return eng_dataname
     */
    public String getEngDataname() {
        return engDataname;
    }

    /**
     * @param engDataname
     */
    public void setEngDataname(String engDataname) {
        this.engDataname = engDataname;
    }

    /**
     * @return country_code
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return modifier
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * @param modifier
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * @return modify_time
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * @param modifyTime
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取洲编码
     *
     * @return continent_code - 洲编码
     */
    public String getContinentCode() {
        return continentCode;
    }

    /**
     * 设置洲编码
     *
     * @param continentCode 洲编码
     */
    public void setContinentCode(String continentCode) {
        this.continentCode = continentCode;
    }

    /**
     * 获取洲名称
     *
     * @return continent_name - 洲名称
     */
    public String getContinentName() {
        return continentName;
    }

    /**
     * 设置洲名称
     *
     * @param continentName 洲名称
     */
    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }
}