package com.channel.connector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages={"com.channel.connector.*.mapper"})
public class CtriposApplication {

	public static void main(String[] args) {
		SpringApplication.run(CtriposApplication.class, args);
	}
}
