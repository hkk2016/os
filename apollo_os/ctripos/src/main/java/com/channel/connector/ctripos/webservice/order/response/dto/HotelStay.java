/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:19:26
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.common.BasicPropertyInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "HotelStay", namespace = Namespace.NS)
public class HotelStay {

	@XmlElement(name = "BasicPropertyInfo", namespace = Namespace.NS)
	private BasicPropertyInfo basicPropertyInfo;

	public BasicPropertyInfo getBasicPropertyInfo() {
		return basicPropertyInfo;
	}

	public void setBasicPropertyInfo(BasicPropertyInfo basicPropertyInfo) {
		this.basicPropertyInfo = basicPropertyInfo;
	}
	
}
