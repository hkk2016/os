package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="GuestCount", namespace = Namespace.NS)
public class GuestCount {

	@XmlAttribute(name = "AgeQualifyingCode")
	private Integer ageQualifyingCode;
	
	@XmlAttribute(name = "Count")
	private Integer count;

	public Integer getAgeQualifyingCode() {
		return ageQualifyingCode;
	}

	public void setAgeQualifyingCode(Integer ageQualifyingCode) {
		this.ageQualifyingCode = ageQualifyingCode;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
