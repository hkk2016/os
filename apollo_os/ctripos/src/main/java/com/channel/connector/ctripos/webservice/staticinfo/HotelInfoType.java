
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>HotelInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="HotelInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Descriptions" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MultimediaDescriptions" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType">
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CategoryCodes" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfCategoryCodesTypeGuestRoomInfo" minOccurs="0"/>
 *         &lt;element name="Position" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="Latitude" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Longitude" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Services" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfHotelInfoTypeService" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="WhenBuilt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="HotelStatus" use="required" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelInfoType", propOrder = {
    "descriptions",
    "categoryCodes",
    "position",
    "services"
})
public class HotelInfoType {

    @XmlElement(name = "Descriptions")
    protected HotelInfoType.Descriptions descriptions;
    @XmlElement(name = "CategoryCodes")
    protected ArrayOfCategoryCodesTypeGuestRoomInfo categoryCodes;
    @XmlElement(name = "Position")
    protected HotelInfoType.Position position;
    @XmlElement(name = "Services")
    protected ArrayOfHotelInfoTypeService services;

    /**
     * 开业时间
     */
    @XmlAttribute(name = "WhenBuilt")
    protected String whenBuilt;

    /**
     * 当前酒店在供应商系统中状态
     */
    @XmlAttribute(name = "HotelStatus", required = true)
    protected StatusEnum hotelStatus;

    /**
     * 获取descriptions属性的值。
     *
     * @return
     *     possible object is
     *     {@link HotelInfoType.Descriptions }
     *
     */
    public HotelInfoType.Descriptions getDescriptions() {
        return descriptions;
    }

    /**
     * 设置descriptions属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link HotelInfoType.Descriptions }
     *
     */
    public void setDescriptions(HotelInfoType.Descriptions value) {
        this.descriptions = value;
    }

    /**
     * 获取categoryCodes属性的值。
     *
     * @return
     *     possible object is
     *     {@link ArrayOfCategoryCodesTypeGuestRoomInfo }
     *
     */
    public ArrayOfCategoryCodesTypeGuestRoomInfo getCategoryCodes() {
        return categoryCodes;
    }

    /**
     * 设置categoryCodes属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfCategoryCodesTypeGuestRoomInfo }
     *
     */
    public void setCategoryCodes(ArrayOfCategoryCodesTypeGuestRoomInfo value) {
        this.categoryCodes = value;
    }

    /**
     * 获取position属性的值。
     *
     * @return
     *     possible object is
     *     {@link HotelInfoType.Position }
     *
     */
    public HotelInfoType.Position getPosition() {
        return position;
    }

    /**
     * 设置position属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link HotelInfoType.Position }
     *
     */
    public void setPosition(HotelInfoType.Position value) {
        this.position = value;
    }

    /**
     * 获取services属性的值。
     *
     * @return
     *     possible object is
     *     {@link ArrayOfHotelInfoTypeService }
     *
     */
    public ArrayOfHotelInfoTypeService getServices() {
        return services;
    }

    /**
     * 设置services属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelInfoTypeService }
     *
     */
    public void setServices(ArrayOfHotelInfoTypeService value) {
        this.services = value;
    }

    /**
     * 获取whenBuilt属性的值。
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getWhenBuilt() {
        return whenBuilt;
    }

    /**
     * 设置whenBuilt属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setWhenBuilt(String value) {
        this.whenBuilt = value;
    }

    /**
     * 获取hotelStatus属性的值。
     *
     * @return
     *     possible object is
     *     {@link StatusEnum }
     *
     */
    public StatusEnum getHotelStatus() {
        return hotelStatus;
    }

    /**
     * 设置hotelStatus属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link StatusEnum }
     *
     */
    public void setHotelStatus(StatusEnum value) {
        this.hotelStatus = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     *
     * <p>以下模式片段指定包含在此类中的预期内容。
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MultimediaDescriptions" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType">
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "multimediaDescriptions"
    })
    public static class Descriptions {

        @XmlElement(name = "MultimediaDescriptions")
        protected HotelInfoType.Descriptions.MultimediaDescriptions multimediaDescriptions;

        /**
         * 获取multimediaDescriptions属性的值。
         *
         * @return
         *     possible object is
         *     {@link HotelInfoType.Descriptions.MultimediaDescriptions }
         *
         */
        public HotelInfoType.Descriptions.MultimediaDescriptions getMultimediaDescriptions() {
            return multimediaDescriptions;
        }

        /**
         * 设置multimediaDescriptions属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link HotelInfoType.Descriptions.MultimediaDescriptions }
         *
         */
        public void setMultimediaDescriptions(HotelInfoType.Descriptions.MultimediaDescriptions value) {
            this.multimediaDescriptions = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType">
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class MultimediaDescriptions
            extends MultimediaDescriptionsType
        {


        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="Latitude" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Longitude" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Position {

        @XmlAttribute(name = "Latitude")
        protected String latitude;
        @XmlAttribute(name = "Longitude")
        protected String longitude;

        /**
         * 获取latitude属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLatitude() {
            return latitude;
        }

        /**
         * 设置latitude属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLatitude(String value) {
            this.latitude = value;
        }

        /**
         * 获取longitude属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLongitude() {
            return longitude;
        }

        /**
         * 设置longitude属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLongitude(String value) {
            this.longitude = value;
        }

    }

}
