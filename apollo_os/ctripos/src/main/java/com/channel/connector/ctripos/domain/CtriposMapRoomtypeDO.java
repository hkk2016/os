package com.channel.connector.ctripos.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_ctripos_map_roomtype")
public class CtriposMapRoomtypeDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 酒店编码
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 酒店中文名
     */
    @Column(name = "hotel_name")
    private String hotelName;

    /**
     * 酒店英文名
     */
    @Column(name = "hotel_name_eng")
    private String hotelNameEng;

    /**
     * 房型编码
     */
    @Column(name = "room_type_id")
    private Integer roomTypeId;

    /**
     * 房型中文名
     */
    @Column(name = "room_name")
    private String roomName;

    /**
     * 房型英文名
     */
    @Column(name = "room_name_eng")
    private String roomNameEng;

    /**
     * 携程房型编码
     */
    @Column(name = "ctrip_roomtype_id")
    private String ctripRoomtypeId;

    /**
     * 携程房型名称
     */
    @Column(name = "ctrip_roomtype_name")
    private String ctripRoomtypeName;

    /**
     * 携程房型英文名称
     */
    @Column(name = "ctrip_roomtype_name_eng")
    private String ctripRoomtypeNameEng;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 房型中文描述
     */
    @Column(name = "room_desc")
    private String roomDesc;

    /**
     * 房型英文描述
     */
    @Column(name = "room_desc_eng")
    private String roomDescEng;

    /**
     * 最大成人入住人数
     */
    private Integer maxperson;

    /**
     * 楼层
     */
    private String floor;

    /**
     * 床型
     */
    @Column(name = "bed_type")
    private String bedType;

    /**
     * 床宽
     */
    @Column(name = "bed_width")
    private String bedWidth;

    /**
     * 是/否可加床
     */
    @Column(name = "extra_bed")
    private Integer extraBed;

    /**
     * 加床费用
     */
    @Column(name = "extra_bed_fee")
    private BigDecimal extraBedFee;

    /**
     * 房间面积
     */
    @Column(name = "room_area")
    private BigDecimal roomArea;

    /**
     * 宽带
     */
    private Integer broadnet;

    /**
     * 有窗/无窗/部分有窗
     */
    @Column(name = "has_window")
    private Integer hasWindow;

    /**
     * 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    @Column(name = "map_status")
    private Integer mapStatus;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    private Date createdTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    @Column(name = "modified_time")
    private Date modifiedTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 错误信息
     */
    @Column(name = "error_info")
    private String errorInfo;

    /**
     * 店铺id
     */
    @Column(name = "shop_id")
    private Integer shopId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店编码
     *
     * @return hotel_id - 酒店编码
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店编码
     *
     * @param hotelId 酒店编码
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取酒店中文名
     *
     * @return hotel_name - 酒店中文名
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * 设置酒店中文名
     *
     * @param hotelName 酒店中文名
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     * 获取酒店英文名
     *
     * @return hotel_name_eng - 酒店英文名
     */
    public String getHotelNameEng() {
        return hotelNameEng;
    }

    /**
     * 设置酒店英文名
     *
     * @param hotelNameEng 酒店英文名
     */
    public void setHotelNameEng(String hotelNameEng) {
        this.hotelNameEng = hotelNameEng;
    }

    /**
     * 获取房型编码
     *
     * @return room_type_id - 房型编码
     */
    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    /**
     * 设置房型编码
     *
     * @param roomTypeId 房型编码
     */
    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    /**
     * 获取房型中文名
     *
     * @return room_name - 房型中文名
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * 设置房型中文名
     *
     * @param roomName 房型中文名
     */
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    /**
     * 获取房型英文名
     *
     * @return room_name_eng - 房型英文名
     */
    public String getRoomNameEng() {
        return roomNameEng;
    }

    /**
     * 设置房型英文名
     *
     * @param roomNameEng 房型英文名
     */
    public void setRoomNameEng(String roomNameEng) {
        this.roomNameEng = roomNameEng;
    }

    /**
     * 获取携程房型编码
     *
     * @return ctrip_roomtype_id - 携程房型编码
     */
    public String getCtripRoomtypeId() {
        return ctripRoomtypeId;
    }

    /**
     * 设置携程房型编码
     *
     * @param ctripRoomtypeId 携程房型编码
     */
    public void setCtripRoomtypeId(String ctripRoomtypeId) {
        this.ctripRoomtypeId = ctripRoomtypeId;
    }

    /**
     * 获取携程房型名称
     *
     * @return ctrip_roomtype_name - 携程房型名称
     */
    public String getCtripRoomtypeName() {
        return ctripRoomtypeName;
    }

    /**
     * 设置携程房型名称
     *
     * @param ctripRoomtypeName 携程房型名称
     */
    public void setCtripRoomtypeName(String ctripRoomtypeName) {
        this.ctripRoomtypeName = ctripRoomtypeName;
    }

    /**
     * 获取携程房型英文名称
     *
     * @return ctrip_roomtype_name_eng - 携程房型英文名称
     */
    public String getCtripRoomtypeNameEng() {
        return ctripRoomtypeNameEng;
    }

    /**
     * 设置携程房型英文名称
     *
     * @param ctripRoomtypeNameEng 携程房型英文名称
     */
    public void setCtripRoomtypeNameEng(String ctripRoomtypeNameEng) {
        this.ctripRoomtypeNameEng = ctripRoomtypeNameEng;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取房型中文描述
     *
     * @return room_desc - 房型中文描述
     */
    public String getRoomDesc() {
        return roomDesc;
    }

    /**
     * 设置房型中文描述
     *
     * @param roomDesc 房型中文描述
     */
    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    /**
     * 获取房型英文描述
     *
     * @return room_desc_eng - 房型英文描述
     */
    public String getRoomDescEng() {
        return roomDescEng;
    }

    /**
     * 设置房型英文描述
     *
     * @param roomDescEng 房型英文描述
     */
    public void setRoomDescEng(String roomDescEng) {
        this.roomDescEng = roomDescEng;
    }

    /**
     * 获取最大成人入住人数
     *
     * @return maxperson - 最大成人入住人数
     */
    public Integer getMaxperson() {
        return maxperson;
    }

    /**
     * 设置最大成人入住人数
     *
     * @param maxperson 最大成人入住人数
     */
    public void setMaxperson(Integer maxperson) {
        this.maxperson = maxperson;
    }

    /**
     * 获取楼层
     *
     * @return floor - 楼层
     */
    public String getFloor() {
        return floor;
    }

    /**
     * 设置楼层
     *
     * @param floor 楼层
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * 获取床型
     *
     * @return bed_type - 床型
     */
    public String getBedType() {
        return bedType;
    }

    /**
     * 设置床型
     *
     * @param bedType 床型
     */
    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    /**
     * 获取床宽
     *
     * @return bed_width - 床宽
     */
    public String getBedWidth() {
        return bedWidth;
    }

    /**
     * 设置床宽
     *
     * @param bedWidth 床宽
     */
    public void setBedWidth(String bedWidth) {
        this.bedWidth = bedWidth;
    }

    /**
     * 获取是/否可加床
     *
     * @return extra_bed - 是/否可加床
     */
    public Integer getExtraBed() {
        return extraBed;
    }

    /**
     * 设置是/否可加床
     *
     * @param extraBed 是/否可加床
     */
    public void setExtraBed(Integer extraBed) {
        this.extraBed = extraBed;
    }

    /**
     * 获取加床费用
     *
     * @return extra_bed_fee - 加床费用
     */
    public BigDecimal getExtraBedFee() {
        return extraBedFee;
    }

    /**
     * 设置加床费用
     *
     * @param extraBedFee 加床费用
     */
    public void setExtraBedFee(BigDecimal extraBedFee) {
        this.extraBedFee = extraBedFee;
    }

    /**
     * 获取房间面积
     *
     * @return room_area - 房间面积
     */
    public BigDecimal getRoomArea() {
        return roomArea;
    }

    /**
     * 设置房间面积
     *
     * @param roomArea 房间面积
     */
    public void setRoomArea(BigDecimal roomArea) {
        this.roomArea = roomArea;
    }

    /**
     * 获取宽带
     *
     * @return broadnet - 宽带
     */
    public Integer getBroadnet() {
        return broadnet;
    }

    /**
     * 设置宽带
     *
     * @param broadnet 宽带
     */
    public void setBroadnet(Integer broadnet) {
        this.broadnet = broadnet;
    }

    /**
     * 获取有窗/无窗/部分有窗
     *
     * @return has_window - 有窗/无窗/部分有窗
     */
    public Integer getHasWindow() {
        return hasWindow;
    }

    /**
     * 设置有窗/无窗/部分有窗
     *
     * @param hasWindow 有窗/无窗/部分有窗
     */
    public void setHasWindow(Integer hasWindow) {
        this.hasWindow = hasWindow;
    }

    /**
     * 获取映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @return map_status - 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public Integer getMapStatus() {
        return mapStatus;
    }

    /**
     * 设置映射状态 1 pending,2 active,3 failed,4 deactivated
     *
     * @param mapStatus 映射状态 1 pending,2 active,3 failed,4 deactivated
     */
    public void setMapStatus(Integer mapStatus) {
        this.mapStatus = mapStatus;
    }

    /**
     * 获取创建时间
     *
     * @return created_time - 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 设置创建时间
     *
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 获取创建人
     *
     * @return creater - 创建人
     */
    public String getCreater() {
        return creater;
    }

    /**
     * 设置创建人
     *
     * @param creater 创建人
     */
    public void setCreater(String creater) {
        this.creater = creater;
    }

    /**
     * 获取修改时间
     *
     * @return modified_time - 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 获取修改人
     *
     * @return modifier - 修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改人
     *
     * @param modifier 修改人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取错误信息
     *
     * @return error_info - 错误信息
     */
    public String getErrorInfo() {
        return errorInfo;
    }

    /**
     * 设置错误信息
     *
     * @param errorInfo 错误信息
     */
    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    /**
     * 获取店铺id
     *
     * @return shop_id - 店铺id
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 设置店铺id
     *
     * @param shopId 店铺id
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}