/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:26:02
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Deadline", namespace = Namespace.NS)
public class Deadline {

	@XmlAttribute(name = "OffsetTimeUnit")
	private String offsetTimeUnit;
	
	@XmlAttribute(name = "OffsetUnitMultiplier")
	private Integer offsetUnitMultiplier;
	
	@XmlAttribute(name = "OffsetDropTime")
	private String offsetDropTime;
	
	@XmlAttribute(name = "AbsoluteDeadline")
	private String absoluteDeadline;

	public String getOffsetTimeUnit() {
		return offsetTimeUnit;
	}

	public void setOffsetTimeUnit(String offsetTimeUnit) {
		this.offsetTimeUnit = offsetTimeUnit;
	}

	public Integer getOffsetUnitMultiplier() {
		return offsetUnitMultiplier;
	}

	public void setOffsetUnitMultiplier(Integer offsetUnitMultiplier) {
		this.offsetUnitMultiplier = offsetUnitMultiplier;
	}

	public String getOffsetDropTime() {
		return offsetDropTime;
	}

	public void setOffsetDropTime(String offsetDropTime) {
		this.offsetDropTime = offsetDropTime;
	}

	public String getAbsoluteDeadline() {
		return absoluteDeadline;
	}

	public void setAbsoluteDeadline(String absoluteDeadline) {
		this.absoluteDeadline = absoluteDeadline;
	}
	
}
