
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>AffiliationInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AffiliationInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Awards" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfAffiliationInfoTypeAward" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AffiliationInfoType", propOrder = {
    "awards"
})
public class AffiliationInfoType {

    @XmlElement(name = "Awards")
    protected ArrayOfAffiliationInfoTypeAward awards;

    /**
     * 获取awards属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAffiliationInfoTypeAward }
     *     
     */
    public ArrayOfAffiliationInfoTypeAward getAwards() {
        return awards;
    }

    /**
     * 设置awards属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAffiliationInfoTypeAward }
     *     
     */
    public void setAwards(ArrayOfAffiliationInfoTypeAward value) {
        this.awards = value;
    }

}
