/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午04:17:27
 ****************************************/
package com.channel.connector.ctripos.webservice.order.common;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.response.dto.CancelPenalty;
import com.channel.connector.ctripos.webservice.order.response.dto.Guarantee;
import com.channel.connector.ctripos.webservice.order.response.dto.MealsIncluded;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RatePlan", namespace = Namespace.NS)
public class RatePlan {

	@XmlAttribute(name = "RatePlanCode")
	private String ratePlanCode;

	@XmlAttribute(name = "PrepaidIndicator")
	private String prepaidIndicator = "true";

	@XmlElement(name = "RatePlanDescription", namespace = Namespace.NS)
	private RatePlanDescription ratePlanDescription;

	@XmlElement(name = "MealsIncluded", namespace = Namespace.NS)
	private MealsIncluded mealsIncluded;

	@XmlElement(name = "Guarantee", namespace = Namespace.NS)
	private Guarantee guarantee;

	@XmlElementWrapper(name = "CancelPenalties", namespace = Namespace.NS)
	@XmlElement(name = "CancelPenalty", namespace = Namespace.NS)
	private List<CancelPenalty> cancelPenalties;

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public String getPrepaidIndicator() {
		return prepaidIndicator;
	}

	public void setPrepaidIndicator(String prepaidIndicator) {
		this.prepaidIndicator = prepaidIndicator;
	}

	public RatePlanDescription getRatePlanDescription() {
		return ratePlanDescription;
	}

	public void setRatePlanDescription(RatePlanDescription ratePlanDescription) {
		this.ratePlanDescription = ratePlanDescription;
	}

	public Guarantee getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(Guarantee guarantee) {
		this.guarantee = guarantee;
	}

	public List<CancelPenalty> getCancelPenalties() {
		if (null == cancelPenalties) {
			cancelPenalties = new ArrayList<CancelPenalty>();
		}
		return cancelPenalties;
	}

	public void setCancelPenalties(List<CancelPenalty> cancelPenalties) {
		this.cancelPenalties = cancelPenalties;
	}

	public MealsIncluded getMealsIncluded() {
		return mealsIncluded;
	}

	public void setMealsIncluded(MealsIncluded mealsIncluded) {
		this.mealsIncluded = mealsIncluded;
	}

}
