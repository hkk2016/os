package com.channel.connector.ctripos.common.constant;

import java.math.BigDecimal;

public class CtripOSCommonConstants {

	public static final String  OTA_HotelAvailRQ="OTA_HotelAvailRQ";
	public static final String  OTA_HotelResRQ="OTA_HotelResRQ";
	public static final String  OTA_CancelRQ="OTA_CancelRQ";
	public static final String  OTA_ReadRQ ="OTA_ReadRQ";

	public static final BigDecimal CTRIP_OS_STATIC_INFO_VERSION = new BigDecimal(1.0);

	public static final BigDecimal  CTRIP_OS_IF_VERSION = new BigDecimal("2.2");

	public static final String CTRIP_OS_PUSH_INVENTORY_JOB_RUNNER = "ctrip_os_push_inventory_job_runner";
	
	public static final String CTRIP_OS_PUSH_PRICE_JOB_RUNNER = "ctrip_os_push_price_job_runner";
	
	public static final String REDIS_JOIN_CHAR = ":";
	
	public static final String DAY_INDEX_07 = "07";
	
	public static final String DAY_INDEX_30 = "30";
	
	public static final String DAY_INDEX_90 = "90";
	
	public static final String CTRIP_OS_PRIMARY_LANG_ID_CN = "zh-cn";

	public static final String CTRIP_OS_PRIMARY_LANG_ID_EN ="en-us";

	public static final String CTRIP_OS_STATIC_INFO_CURRENCY_CN = "CNY";
	
	/** 预付 **/
	public static final String PAY_METHOD_PREPAY = "501";
	/** 面付 **/
	public static final String PAY_METHOD_PAY_AT_HOTEL = "16";
	
	/*库存接口常量开始*/
	/** 库存AvailStatusMessage节点 BookingLimitMessageType属性值SetLimit **/
	public static final String INVENTORY_BOOKING_LIMIT_MESSAGE_TYPE = "SetLimit";
	
	/*库存接口常量结束*/
	
	
	/*报价接口常量开始*/
	/** 报价 BaseByGuestAmt节点Code熟悉值Sell**/
	public static final String RATE_BASE_BY_GUEST_AMT_CODE = "Sell";
	/*报价接口常量结束*/
	
	
	/*试预订接口常量开始*/
	
	/*试预订接口常量结束*/
	
	
	/*下单接口常量开始*/
	
	/*下单接口常量结束*/
	
	
	/*取消单接口常量开始*/
	
	/*取消单接口常量结束*/
}
