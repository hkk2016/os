package com.channel.connector.ctripos.common.runner;

import com.alibaba.fastjson.JSON;
import com.channel.connector.ctripos.common.constant.InitData;
import com.channel.connector.ctripos.service.CtriposShopService;
import com.channel.connector.product.domain.ChannelAgentDO;
import com.channel.connector.product.mapper.ChannelAgentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 40293 on 2018/12/3.
 */
@Service
@Slf4j
public class InitDataService implements CommandLineRunner {

    @Autowired
    private CtriposShopService shopService;

    @Autowired
    private ChannelAgentMapper channelAgentMapper;

    @Override
    public void run(String... strings) throws Exception {
        shopService.initShopInfoToMap();
        log.info("初始化店铺信息按商家：{}", JSON.toJSONString(InitData.merchantShopMap));
        log.info("初始化店铺信息按店铺：{}", JSON.toJSONString(InitData.shopIdShopMap));
        log.info("初始化店铺信息：{}", JSON.toJSONString(InitData.userPwdShopMap));

        initChannelAgent();
        log.info("初始化渠道分销商信息：{}", JSON.toJSONString(InitData.channelAgentMap));

    }

    private void initChannelAgent() {
        List<ChannelAgentDO> channelAgentDOList = channelAgentMapper.selectAll();
        for (ChannelAgentDO channelAgentDO : channelAgentDOList) {
            String channelKey = channelAgentDO.getMerchantCode() + "-" + channelAgentDO.getChannelCode();
            if (!InitData.channelAgentMap.containsKey(channelKey)) {
                InitData.channelAgentMap.put(channelKey, channelAgentDO);
            }
        }
    }
}
