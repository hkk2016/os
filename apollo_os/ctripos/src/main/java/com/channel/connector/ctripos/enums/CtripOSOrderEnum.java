package com.channel.connector.ctripos.enums;


public enum CtripOSOrderEnum {

    STATUS_0("", "未知状态", "NO"),
    STATUS_1("1", "下单失败", "NO"),
    STATUS_2("2", "订单处理中", "Pending"),
    STATUS_3("3", "已确认", "OK"),
    STATUS_4("4", "已取消", "Cancel"),
    STATUS_5("5", "不确认", "NO"),;

    public String status;
    public String desc;
    public String ctripOSStatus;

    private CtripOSOrderEnum(String status, String desc, String ctripOSStatus) {
        this.status = status;
        this.desc = desc;
        this.ctripOSStatus = ctripOSStatus;
    }

    public static CtripOSOrderEnum getEnumByStatus(String status) {
        if (null != status && !"".equals(status)) {
            for (CtripOSOrderEnum enum0 : CtripOSOrderEnum.values()) {
                if (enum0.status.equals(status)) {
                    return enum0;
                }
            }

        }
        return STATUS_0;
    }


}
