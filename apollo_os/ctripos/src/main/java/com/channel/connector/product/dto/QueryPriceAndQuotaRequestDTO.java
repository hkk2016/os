package com.channel.connector.product.dto;

import com.channel.connector.ctripos.dto.request.IncrementDTO;
import lombok.Data;

/**
 * Created by 402931207@qq.com on 2018/12/24.
 */
@Data
public class QueryPriceAndQuotaRequestDTO extends IncrementDTO {

    private String merchantCode;
}
