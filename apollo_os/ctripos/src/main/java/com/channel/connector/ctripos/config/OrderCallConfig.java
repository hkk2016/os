package com.channel.connector.ctripos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by 402931207@qq.com on 2018/12/25.
 */
@Component
@ConfigurationProperties(prefix = "bms.order")
@Data
public class OrderCallConfig {
    public String createUrl;

    public String preBookingUrl;

    public String cancelUrl;

    public String addOrderRequestUrl;
}
