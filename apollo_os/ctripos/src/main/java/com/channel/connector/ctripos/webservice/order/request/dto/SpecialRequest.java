package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SpecialRequest", namespace = Namespace.NS)
public class SpecialRequest {
	@XmlElement(name="Text", namespace = Namespace.NS)
	private Text text;
	@XmlElement(name = "ListItem", namespace = Namespace.NS)
	private List<ListItem> itemLists=new ArrayList<ListItem>();

	public Text getText() {
		return text;
	}

	public List<ListItem> getItemLists() {
		return itemLists;
	}

	public void setItemLists(List<ListItem> itemLists) {
		this.itemLists = itemLists;
	}



	public void setText(Text text) {
		this.text = text;
	}

	

}
