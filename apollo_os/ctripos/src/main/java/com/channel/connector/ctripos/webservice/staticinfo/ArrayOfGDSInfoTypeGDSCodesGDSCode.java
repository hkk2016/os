
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfGDS_InfoTypeGDS_CodesGDS_Code complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGDS_InfoTypeGDS_CodesGDS_Code">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GDS_Code" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="GDS_PropertyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="GDS_Name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGDS_InfoTypeGDS_CodesGDS_Code", propOrder = {
    "gdsCode"
})
public class ArrayOfGDSInfoTypeGDSCodesGDSCode {

    @XmlElement(name = "GDS_Code")
    protected List<GDSCode> gdsCode;

    /**
     * Gets the value of the gdsCode property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gdsCode property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGDSCode().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfGDSInfoTypeGDSCodesGDSCode.GDSCode }
     *
     *
     */
    public List<GDSCode> getGDSCode() {
        if (gdsCode == null) {
            gdsCode = new ArrayList<GDSCode>();
        }
        return this.gdsCode;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="GDS_PropertyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="GDS_Name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class GDSCode {

        @XmlAttribute(name = "GDS_PropertyCode")
        protected String gdsPropertyCode;
        @XmlAttribute(name = "GDS_Name")
        protected String gdsName;

        /**
         * 获取gdsPropertyCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGDSPropertyCode() {
            return gdsPropertyCode;
        }

        /**
         * 设置gdsPropertyCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGDSPropertyCode(String value) {
            this.gdsPropertyCode = value;
        }

        /**
         * 获取gdsName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGDSName() {
            return gdsName;
        }

        /**
         * 设置gdsName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGDSName(String value) {
            this.gdsName = value;
        }

    }

}
