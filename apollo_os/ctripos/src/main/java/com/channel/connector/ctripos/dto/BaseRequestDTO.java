package com.channel.connector.ctripos.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 40293 on 2018/11/29.
 */
@Data
public class BaseRequestDTO implements Serializable {

    private static final long serialVersionUID = -1752110258995750777L;
    /**
     * 创建者
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改者
     */
    private String modifier;

    /**
     * 修改时间
     */
    private Date modifyTime;
}
