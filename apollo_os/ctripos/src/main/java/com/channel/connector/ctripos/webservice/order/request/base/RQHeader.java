package com.channel.connector.ctripos.webservice.order.request.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;


@XmlAccessorType(XmlAccessType.FIELD)
public class RQHeader {

	@XmlAttribute(name = "Version")
	private String version;

	@XmlAttribute(name = "EchoToken")
	private String echoToken;

	@XmlAttribute(name = "PrimaryLangID")
	private String primaryLangID;

	@XmlAttribute(name = "RetransmissionIndicator")
	private String retransmissionIndicator;

//	@XmlAttribute(name = "xmlns")
//	private String xmlns;
//
//	@XmlAttribute(name = "xmlns:xsi")
//	private String xmlns_xsi;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getEchoToken() {
		return echoToken;
	}

	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}

//	public String getXmlns() {
//		return xmlns;
//	}
//
//	public void setXmlns(String xmlns) {
//		this.xmlns = xmlns;
//	}
//
//	public String getXmlns_xsi() {
//		return xmlns_xsi;
//	}
//
//	public void setXmlns_xsi(String xmlns_xsi) {
//		this.xmlns_xsi = xmlns_xsi;
//	}

	public String getPrimaryLangID() {
		return primaryLangID;
	}

	public void setPrimaryLangID(String primaryLangID) {
		this.primaryLangID = primaryLangID;
	}

	public String getRetransmissionIndicator() {
		return retransmissionIndicator;
	}

	public void setRetransmissionIndicator(String retransmissionIndicator) {
		this.retransmissionIndicator = retransmissionIndicator;
	}

}
