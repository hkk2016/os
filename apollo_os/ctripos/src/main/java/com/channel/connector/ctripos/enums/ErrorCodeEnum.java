package com.channel.connector.ctripos.enums;

public enum ErrorCodeEnum {

    INVALID_INPUTPARAM("001","INVALID_INPUTPARAM","无效的入参对象"),

    /****** 成功返回 ***/
    SUCCESS("200","SUCCESS","成功"),


    /******* 酒店接口调用异常 **************/
    CALL_CTRIPOS_RETURN_FAILED("199","CALL_CTRIPOS_RETURN_FAILED","调用携程返回失败"),

    CALL_CTRIPOS_RATESINCREMENT_API_EXCEPTION("401","CALL_CTRIPOS_RATESINCREMENT_API_EXCEPTION","推送房价和房态携程接口异常"),

    /******* 调用BMS错误码 ******************************/
    EBK_ORDER_CANCELED("502","EBK_ORDER_CANCELED","商家已取消订单"),
    PRE_BOOKING_PRODUCT_NOT_EXIST("503","PRE_BOOKING_PRODUCT_NOT_EXIST","无此产品"),
    PRE_BOOKING_PRODUCT_NOT_ON_SALE("504","PRE_BOOKING_PRODUCT_NOT_ON_SALE","产品未上架"),
    PRE_BOOKING_PRODUCT_PRICE_INVALID("505","PRE_BOOKING_PRODUCT_PRICE_INVALID","部分日期无价格"),
    PRE_BOOKING_PRODUCT_FULL_ROOM("506","PRE_BOOKING_PRODUCT_FULL_ROOM","满房"),
    PRE_BOOKING_PRODUCT_QUOTA_NOT_ENOUGH("506","PRE_BOOKING_PRODUCT_QUOTA_NOT_ENOUGH","配额不足"),

    BMS_ORDER_URL_INVALID("601","BMS_ORDER_URL_INVALID","URL无效"),


    /********业务错误************************************/
    PUSH_PRICE_AND_QUOTA_NOT_FOUND_DATA("701","PUSH_PRICE_AND_QUOTA_NOT_FOUND_DATA","未查到数据"),

    /***********系统异常类错误代码 以9开头***********/
    SYSTEM_EXCEPTION("900","SYSTEM_EXCEPTION","系统异常"),
    OUTER_IF_EXCEPTION("901","OUTER_IF_EXCEPTION","外部接口调用异常"),
    UNKNOWN_EXCEPTION("902","UNKNOWN_EXCEPTION","未知异常"),
    CONNECT_TIME_OUT("903","CONNECT_TIME_OUT","连接超时"),
    READ_TIME_OUT("904","READ_TIME_OUT","访问超时"),
    IO_EXCEPTION("905","IO_EXCEPTION","IO异常");

    public String errorNo;
    public String errorCode;//业务场景编号
    public String errorDesc;//业务场景描述

    private ErrorCodeEnum(String errorNo, String errorCode, String errorDesc)
    {
        this.errorNo = errorNo;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public static String getKeyByValue(String errorCode) {
        String key = "000";
        for(ErrorCodeEnum errorCodeEnum : ErrorCodeEnum.values()) {
            if(errorCodeEnum.errorCode.equals(errorCode)) {
                key = errorCodeEnum.errorNo;
                break;
            }
        }
        return key;
    }

    public static String getDescByValue(String errorCode) {
        String desc = "";
        for(ErrorCodeEnum errorCodeEnum : ErrorCodeEnum.values()) {
            if(errorCodeEnum.errorCode.equals(errorCode)) {
                desc = errorCodeEnum.errorDesc;
                break;
            }
        }
        return desc;
    }

    public static String getErrorCodeByKey(String key) {
        String errorCode = "";
        for(ErrorCodeEnum errorCodeEnum : ErrorCodeEnum.values()) {
            if(errorCodeEnum.errorNo.equals(key)) {
                errorCode = errorCodeEnum.errorCode;
                break;
            }
        }
        return errorCode;
    }
}
