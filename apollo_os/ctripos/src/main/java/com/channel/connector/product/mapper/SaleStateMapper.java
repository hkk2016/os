package com.channel.connector.product.mapper;

import com.channel.connector.ctripos.common.BaseMapper;
import com.channel.connector.product.domain.SaleStateDO;

public interface SaleStateMapper extends BaseMapper<SaleStateDO> {
}