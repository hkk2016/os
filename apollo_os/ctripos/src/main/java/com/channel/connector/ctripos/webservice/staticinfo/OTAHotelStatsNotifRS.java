
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="TPA_Extensions" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Hotels" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfHotelStatsInfoTypeHotel" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType" minOccurs="0"/>
 *           &lt;element name="Error" type="{http://www.opentravel.org/OTA/2003/05}ErrorType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="TimeStamp" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="Target" use="required" type="{http://www.opentravel.org/OTA/2003/05}TargetEnum" />
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PrimaryLangID" type="{http://www.w3.org/2001/XMLSchema}language" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tpaExtensionsOrSuccessOrError"
})
@XmlRootElement(name = "OTA_HotelStatsNotifRS")
public class OTAHotelStatsNotifRS {

    @XmlElements({
        @XmlElement(name = "TPA_Extensions", type = OTAHotelStatsNotifRS.TPAExtensions.class),
        @XmlElement(name = "Success", type = SuccessType.class),
        @XmlElement(name = "Error", type = ErrorType.class)
    })
    protected List<Object> tpaExtensionsOrSuccessOrError;
    @XmlAttribute(name = "TimeStamp", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target", required = true)
    protected TargetEnum target;
    @XmlAttribute(name = "Version")
    protected String version;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * Gets the value of the tpaExtensionsOrSuccessOrError property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tpaExtensionsOrSuccessOrError property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTPAExtensionsOrSuccessOrError().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OTAHotelStatsNotifRS.TPAExtensions }
     * {@link SuccessType }
     * {@link ErrorType }
     * 
     * 
     */
    public List<Object> getTPAExtensionsOrSuccessOrError() {
        if (tpaExtensionsOrSuccessOrError == null) {
            tpaExtensionsOrSuccessOrError = new ArrayList<Object>();
        }
        return this.tpaExtensionsOrSuccessOrError;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TargetEnum }
     *     
     */
    public TargetEnum getTarget() {
        return target;
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TargetEnum }
     *     
     */
    public void setTarget(TargetEnum value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Hotels" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfHotelStatsInfoTypeHotel" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotels"
    })
    public static class TPAExtensions {

        @XmlElement(name = "Hotels")
        protected ArrayOfHotelStatsInfoTypeHotel hotels;

        /**
         * 获取hotels属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfHotelStatsInfoTypeHotel }
         *     
         */
        public ArrayOfHotelStatsInfoTypeHotel getHotels() {
            return hotels;
        }

        /**
         * 设置hotels属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfHotelStatsInfoTypeHotel }
         *     
         */
        public void setHotels(ArrayOfHotelStatsInfoTypeHotel value) {
            this.hotels = value;
        }

    }

    public void setTpaExtensionsOrSuccessOrError(List<Object> tpaExtensionsOrSuccessOrError) {
        this.tpaExtensionsOrSuccessOrError = tpaExtensionsOrSuccessOrError;
    }
}
