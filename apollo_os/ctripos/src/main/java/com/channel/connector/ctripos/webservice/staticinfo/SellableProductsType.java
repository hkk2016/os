
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>SellableProductsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SellableProductsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SellableProduct" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GuestRoom" type="{http://www.opentravel.org/OTA/2003/05}GuestRoomType" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="InvCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="InvStatusType" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
 *                 &lt;attribute name="InvTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SellableProductsType", propOrder = {
    "sellableProduct"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.OTAHotelInvNotifRQ.SellableProducts.class
})
public class SellableProductsType {

    @XmlElement(name = "SellableProduct")
    protected List<SellableProduct> sellableProduct;

    /**
     * Gets the value of the sellableProduct property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellableProduct property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellableProduct().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SellableProductsType.SellableProduct }
     *
     *
     */
    public List<SellableProduct> getSellableProduct() {
        if (sellableProduct == null) {
            sellableProduct = new ArrayList<SellableProduct>();
        }
        return this.sellableProduct;
    }

    public void setSellableProduct(List<SellableProduct> sellableProduct) {
        this.sellableProduct = sellableProduct;
    }

    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GuestRoom" type="{http://www.opentravel.org/OTA/2003/05}GuestRoomType" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="InvCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="InvStatusType" type="{http://www.opentravel.org/OTA/2003/05}StatusEnum" />
     *       &lt;attribute name="InvTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guestRoom"
    })
    public static class SellableProduct {

        @XmlElement(name = "GuestRoom")
        protected GuestRoomType guestRoom;


        @XmlAttribute(name = "InvCode")
        protected String invCode;

        /**
         * 当前酒店在供应商系统中状态
         */
        @XmlAttribute(name = "InvStatusType")
        protected StatusEnum invStatusType;

        /**
         * 集团或供应商的房型代码
         */
        @XmlAttribute(name = "InvTypeCode")
        protected String invTypeCode;

        /**
         * 获取guestRoom属性的值。
         * 
         * @return
         *     possible object is
         *     {@link GuestRoomType }
         *     
         */
        public GuestRoomType getGuestRoom() {
            return guestRoom;
        }

        /**
         * 设置guestRoom属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link GuestRoomType }
         *     
         */
        public void setGuestRoom(GuestRoomType value) {
            this.guestRoom = value;
        }

        /**
         * 获取invCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvCode() {
            return invCode;
        }

        /**
         * 设置invCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvCode(String value) {
            this.invCode = value;
        }

        /**
         * 获取invStatusType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link StatusEnum }
         *     
         */
        public StatusEnum getInvStatusType() {
            return invStatusType;
        }

        /**
         * 设置invStatusType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link StatusEnum }
         *     
         */
        public void setInvStatusType(StatusEnum value) {
            this.invStatusType = value;
        }

        /**
         * 获取invTypeCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvTypeCode() {
            return invTypeCode;
        }

        /**
         * 设置invTypeCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvTypeCode(String value) {
            this.invTypeCode = value;
        }

    }

}
