package com.channel.connector.ctripos.dto.request;

import com.channel.connector.ctripos.dto.BaseRequestDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 价格计划查询请求对象
 * @author zhanwang
 */
@Data
public class QueryRatePlanMappingRequestDTO extends BaseRequestDTO implements Serializable {

    /**
     * 商品ID
     */
    private Long commodityId;

    /**
     * 商品IDs
     */
    private List<Long> commodityIds;

    /**
     * 商家编码
     */
    private String merchantCode;

    /**
     * 酒店ID
     */
    private Long hotelId;

    /**
     * 房型ID
     */
    private Long roomTypeId;

    /**
     * 携程酒店映射状态
     */
    private Integer mapStatus;

    /**
     * 上下架状态
     */
    private Integer isOnSale;

}