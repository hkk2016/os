
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RequiredPaymentsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="RequiredPaymentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GuaranteePayment" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcceptedPayments" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfAcceptedPaymentsTypeAcceptedPayment" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequiredPaymentsType", propOrder = {
    "guaranteePayment"
})
@XmlSeeAlso({
    com.channel.connector.ctripos.webservice.staticinfo.PoliciesType.Policy.GuaranteePaymentPolicy.class
})
public class RequiredPaymentsType {

    @XmlElement(name = "GuaranteePayment")
    protected RequiredPaymentsType.GuaranteePayment guaranteePayment;

    /**
     * 获取guaranteePayment属性的值。
     *
     * @return
     *     possible object is
     *     {@link RequiredPaymentsType.GuaranteePayment }
     *
     */
    public RequiredPaymentsType.GuaranteePayment getGuaranteePayment() {
        return guaranteePayment;
    }

    /**
     * 设置guaranteePayment属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link RequiredPaymentsType.GuaranteePayment }
     *
     */
    public void setGuaranteePayment(RequiredPaymentsType.GuaranteePayment value) {
        this.guaranteePayment = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcceptedPayments" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfAcceptedPaymentsTypeAcceptedPayment" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acceptedPayments"
    })
    public static class GuaranteePayment {

        @XmlElement(name = "AcceptedPayments")
        protected ArrayOfAcceptedPaymentsTypeAcceptedPayment acceptedPayments;

        /**
         * 获取acceptedPayments属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfAcceptedPaymentsTypeAcceptedPayment }
         *     
         */
        public ArrayOfAcceptedPaymentsTypeAcceptedPayment getAcceptedPayments() {
            return acceptedPayments;
        }

        /**
         * 设置acceptedPayments属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfAcceptedPaymentsTypeAcceptedPayment }
         *     
         */
        public void setAcceptedPayments(ArrayOfAcceptedPaymentsTypeAcceptedPayment value) {
            this.acceptedPayments = value;
        }

    }

}
