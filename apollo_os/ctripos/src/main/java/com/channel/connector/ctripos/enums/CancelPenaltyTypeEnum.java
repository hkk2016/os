package com.channel.connector.ctripos.enums;
/**
 * @Description : 
 * @author : chihping
 * @date : 2019年1月11日 上午9:19:48
 */
public enum CancelPenaltyTypeEnum {
	
	BEFOREARRIVAL("BeforeArrival", "入住前"), AFTERBOOKING("AfterBooking", "入住后"), AFTERCONFIRMATION("AfterConfirmation", "订单确认后");

	public String key;
	
	public String value;

	private CancelPenaltyTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public static CancelPenaltyTypeEnum getEnumByKey(String key) {
		CancelPenaltyTypeEnum cancel = null;
		if (null == key || "".equals(key)) {
			return null;
		}
		for (CancelPenaltyTypeEnum enu : CancelPenaltyTypeEnum.values()) {
			if (enu.key.equals(key)) {
				cancel = enu;
				break;
			}
		}
		return cancel;
	}
}
