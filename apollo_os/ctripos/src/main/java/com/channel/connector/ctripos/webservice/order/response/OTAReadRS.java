package com.channel.connector.ctripos.webservice.order.response;

import com.channel.connector.ctripos.webservice.order.Namespace;
import com.channel.connector.ctripos.webservice.order.common.HotelReservation;
import com.channel.connector.ctripos.webservice.order.response.base.RSHeader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OTA_ReadRS", namespace = Namespace.NS)
public class OTAReadRS extends RSHeader {

    @XmlElementWrapper(name = "HotelReservations", namespace = Namespace.NS)
    @XmlElement(name = "HotelReservation", namespace = Namespace.NS)
    private List<HotelReservation> hotelReservations;

    public List<HotelReservation> getHotelReservations() {
        if (null == hotelReservations) {
            hotelReservations = new ArrayList<>();
        }
        return hotelReservations;
    }

    public void setHotelReservations(List<HotelReservation> hotelReservations) {
        this.hotelReservations = hotelReservations;
    }
}
