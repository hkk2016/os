
package com.channel.connector.ctripos.webservice.staticinfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>LimitationEnum的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="LimitationEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Permission"/>
 *     &lt;enumeration value="Forbidden"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LimitationEnum")
@XmlEnum
public enum LimitationEnum {

    @XmlEnumValue("Permission")
    PERMISSION("Permission"),
    @XmlEnumValue("Forbidden")
    FORBIDDEN("Forbidden");
    private final String value;

    LimitationEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LimitationEnum fromValue(String v) {
        for (LimitationEnum c: LimitationEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
