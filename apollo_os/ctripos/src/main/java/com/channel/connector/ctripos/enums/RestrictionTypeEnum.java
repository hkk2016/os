package com.channel.connector.ctripos.enums;
/**
 * @Description : 
 * @author : Zhiping Sun
 * @date : 2018年12月5日 下午4:07:40
 */
public enum RestrictionTypeEnum {

	MASTER("Master"), DEPARTURE("Departure"), ARRIVAL("Arrival");
	
	public String key;

	private RestrictionTypeEnum(String key) {
		this.key = key;
	}
}
