/****************************************
 * 携程海外项目 @天下房仓 2015-12-1 下午05:23:45
 ****************************************/
package com.channel.connector.ctripos.webservice.order.request.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AvailRequestSegment", namespace = Namespace.NS)
public class AvailRequestSegment {

	@XmlElement(name = "HotelSearchCriteria", namespace = Namespace.NS)
	private HotelSearchCriteria hotelSearchCriteria;

	public HotelSearchCriteria getHotelSearchCriteria() {
		return hotelSearchCriteria;
	}

	public void setHotelSearchCriteria(HotelSearchCriteria hotelSearchCriteria) {
		this.hotelSearchCriteria = hotelSearchCriteria;
	}

}
