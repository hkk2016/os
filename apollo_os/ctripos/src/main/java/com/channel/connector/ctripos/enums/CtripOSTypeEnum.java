package com.channel.connector.ctripos.enums;

/**
 * 错误和警告类型
 */
public enum CtripOSTypeEnum {

    CtripOSType_1("1", "Unknown", "Indicates an unknown error"),
    CtripOSType_2("2", "No implementation", "Indicates that the target business system has no implementation for the intended request"),
    CtripOSType_3("3", "Biz rule", "Indicates that the XML message has passed a low-level validation check, but that the business rules for the request message were not met"),
    CtripOSType_4("4", "Authentication", "Indicates the message lacks adequate security credentials"),
    CtripOSType_5("5", "Authentication timeout", "Indicates that the security credentials in the message have expired"),
    CtripOSType_6("6", "Authorization", "Indicates the message lacks adequate security credentials"),
    CtripOSType_7("7", "Protocol violation", "Indicates that a request was sent within a message exchange that does not align to the message"),
    CtripOSType_8("8", "Transaction model", "Indicates that the target business system does not support the intended transaction-oriented operation"),
    CtripOSType_9("9", "Authentical model", "Indicates the type of authentication requested is not recognized"),
    CtripOSType_10("10", "Required field missing", "Indicates that an element or attribute that is required in by the schema (or required by agreement between trading partners) is missing from the message"),
    CtripOSType_11("11", "Advisory", ""),
    CtripOSType_12("12", "Processing exception", "Indicates that during processing of the request that a not further defined exception occurred"),
    CtripOSType_13("13", "Application error", "Indicates that an involved backend application returned an error or warning, which is passed back in the response message"),
    CtripOSType_14("14", "MD 5 Check Failed", "MD5 string check fails in reservation"),;

    private String type;
    private String name;
    private String typeDesc;


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getTypeDesc() {
        return typeDesc;
    }


    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    private CtripOSTypeEnum(String type, String name, String typeDesc) {
        this.type = type;
        this.name = name;
        this.typeDesc = typeDesc;
    }

}
