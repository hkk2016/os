
package com.channel.connector.ctripos.webservice.dynamic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>ArrayOfHotelRatePlanTypeRate complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ArrayOfHotelRatePlanTypeRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Rate" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateUploadType">
 *                 &lt;sequence>
 *                   &lt;element name="LengthsOfStay" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanLengthsOfStayType" minOccurs="0"/>
 *                   &lt;element name="AvailStatusMessage" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanRestrictionStatus" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfHotelRatePlanTypeRate", propOrder = {
    "rate"
})
public class ArrayOfHotelRatePlanTypeRate {

    @XmlElement(name = "Rate")
    protected List<Rate> rate;

    /**
     * Gets the value of the rate property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rate property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRate().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfHotelRatePlanTypeRate.Rate }
     *
     *
     */
    public List<Rate> getRate() {
        if (rate == null) {
            rate = new ArrayList<Rate>();
        }
        return this.rate;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateUploadType">
     *       &lt;sequence>
     *         &lt;element name="LengthsOfStay" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanLengthsOfStayType" minOccurs="0"/>
     *         &lt;element name="AvailStatusMessage" type="{http://www.opentravel.org/OTA/2003/05}ArrayOfRatePlanRestrictionStatus" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}AvailabilityStatusType" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lengthsOfStay",
        "availStatusMessage"
    })
    public static class Rate
        extends RateUploadType
    {

        @XmlElement(name = "LengthsOfStay")
        protected ArrayOfRatePlanLengthsOfStayType lengthsOfStay;
        @XmlElement(name = "AvailStatusMessage")
        protected ArrayOfRatePlanRestrictionStatus availStatusMessage;
        @XmlAttribute(name = "Status")
        protected AvailabilityStatusType status;

        /**
         * 获取lengthsOfStay属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfRatePlanLengthsOfStayType }
         *     
         */
        public ArrayOfRatePlanLengthsOfStayType getLengthsOfStay() {
            return lengthsOfStay;
        }

        /**
         * 设置lengthsOfStay属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfRatePlanLengthsOfStayType }
         *     
         */
        public void setLengthsOfStay(ArrayOfRatePlanLengthsOfStayType value) {
            this.lengthsOfStay = value;
        }

        /**
         * 获取availStatusMessage属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ArrayOfRatePlanRestrictionStatus }
         *     
         */
        public ArrayOfRatePlanRestrictionStatus getAvailStatusMessage() {
            return availStatusMessage;
        }

        /**
         * 设置availStatusMessage属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ArrayOfRatePlanRestrictionStatus }
         *     
         */
        public void setAvailStatusMessage(ArrayOfRatePlanRestrictionStatus value) {
            this.availStatusMessage = value;
        }

        /**
         * 获取status属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AvailabilityStatusType }
         *     
         */
        public AvailabilityStatusType getStatus() {
            return status;
        }

        /**
         * 设置status属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AvailabilityStatusType }
         *     
         */
        public void setStatus(AvailabilityStatusType value) {
            this.status = value;
        }

    }

}
