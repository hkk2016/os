/****************************************
 * 携程海外项目 @天下房仓 2015-12-11 上午10:36:00
 ****************************************/
package com.channel.connector.ctripos.webservice.order.response.dto;

import com.channel.connector.ctripos.webservice.order.Namespace;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MealsIncluded", namespace = Namespace.NS)
public class MealsIncluded {

	@XmlAttribute(name = "Breakfast")
	private String breakfast;
	
	@XmlAttribute(name = "NumberOfBreakfast")
	private Integer numberOfBreakfast;

	public String getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(String breakfast) {
		this.breakfast = breakfast;
	}

	public Integer getNumberOfBreakfast() {
		return numberOfBreakfast;
	}

	public void setNumberOfBreakfast(Integer numberOfBreakfast) {
		this.numberOfBreakfast = numberOfBreakfast;
	}
	
}
