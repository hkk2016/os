package com.supply.hsa.link.hotelinfo.server;

import com.supply.hsa.link.hotelinfo.dto.SupplyHotelDTO;
import com.supply.hsa.link.hotelinfo.dto.SupplyRoomDTO;

import java.util.List;

public interface SupplyHotelStaticServer {
    /**
     * 新增供应商酒店数据信息
     * @param supplyHotelList
     */
    public void addSupplyHotel(List<SupplyHotelDTO> supplyHotelList);

    /**
     * 新增供应商房型数据信息
     * @param supplyRoomList
     */
    public void addSupplyRoom(List<SupplyRoomDTO> supplyRoomList);
}
