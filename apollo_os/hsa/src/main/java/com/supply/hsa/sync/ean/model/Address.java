package com.supply.hsa.sync.ean.model;

public class Address {
	/**
	 * 地址行 1
	 */
	private String line_1;
	/**
	 * 地址行 2
	 */
	private String line_2;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 2 字母代码，适用于澳大利亚、加拿大和美国
	 */
	private String state_province_code;
	/**
	 * 州/省的文本名称 - 对于附加文本更常见 countries
	 */
	private String state_province_name;
	/**
	 * 邮政编码
	 */
	private String postal_code;
	/**
	 * 2 字母国家/地区代码，格式为 ISO 3166-1 alpha-2
	 */
	private String country_code;
	public String getLine_1() {
		return line_1;
	}
	public void setLine_1(String line_1) {
		this.line_1 = line_1;
	}
	public String getLine_2() {
		return line_2;
	}
	public void setLine_2(String line_2) {
		this.line_2 = line_2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState_province_code() {
		return state_province_code;
	}
	public void setState_province_code(String state_province_code) {
		this.state_province_code = state_province_code;
	}
	public String getState_province_name() {
		return state_province_name;
	}
	public void setState_province_name(String state_province_name) {
		this.state_province_name = state_province_name;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	
}
