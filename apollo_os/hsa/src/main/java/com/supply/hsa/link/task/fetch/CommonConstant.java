package com.supply.hsa.link.task.fetch;

public final class CommonConstant {
	
	public static final String SPRING_APPLICATION_CONTEXT_NAME = "Root WebApplicationContext";
	
	public static class TaskConstant{
		
		/**任务ID*/
		public static final String TASK_ID = "TaskId";
		
		/**父任务ID*/
		public static final String TASK_PARENT_ID = "TaskParentId";
		
		/**任务类型*/
		public static final String TASK_TYPE = "TaskType";
		
		/**任务执行结果*/
		public static final String TASK_RESULT = "TaskResult";
		
		/**任务状态*/
		public static final String TASK_WORKER_STATUS = "WorkerStatus";
		
		/**任务执行失败原因*/
		public static final String TASK_FAIL_REASON = "TaskFailReason";
		
		/**任务加入时间*/
		public static final String TASK_JOIN_DATE = "TaskJoinDate";
		
		/**任务执行开始时间*/
		public static final String TASK_START_DATE = "TaskStartDate";
		
		/**任务执行结束时间*/
		public static final String TASK_END_DATE = "TaskEndDate";
		
		/**任务总数*/
		public static final String TASK_COUNT = "TaskCount";
		
		/**执行成功的任务总数*/
		public static final String TASK_SUCCESS_COUNT = "TaskSuccessCount";
		
		/**执行失败的任务总数*/
		public static final String TASK_FAILURE_COUNT = "TaskFailureCount";
		
		/**任务超时时间*/
		public static final String TASK_TIMEOUT_TIME = "TaskTimeout";
		
		/**任务重试次数*/
		public static final String TASK_RETRY_COUNT = "TaskRetryCount";
	}
	
	/**任务类型*/
	public static class TaskType{
		public static final String TIMER_TASK = "TimerTask";
		
		public static final String LTS_TASK = "LtsTask";
		
		public static final String FETCH_HOTEL_TASK = "FetchHotelTask";
		
		public static final String FETCH_HOTEL_DAY_TASK = "FetchHotelDayTask";
		
		public static final String ANALYZE_HOTEL_TASK = "AnalyzeHotelTask";
	}
	
	public static class FetchSupplyDataConstant{
		
		/**供应商成功抓取的每日数据*/
		public static final String SUPPLY_SUCCESS_DAY_DATA = "SupplySuccessDayData";
		
		/**供应商失败抓取的每日数据*/
		public static final String SUPPLY_FAILURE_DAY_DATA = "SupplyFailureDayData";
		
		public static final String TASK_ID = "TaskId";
		/**供应商编码*/
		public static final String SUPPLY_CODE = "SupplyCode";
		
		/**供应商类型*/
		public static final String SUPPLIER_CLASS = "SupplierClass";
		
		/**酒店ID*/
		public static final String HOTEL_ID = "HotelId";
		
		/**查询开始日期*/
		public static final String CHECK_IN_DATE = "CheckInDate";
		
		/**查询结束日期*/
		public static final String CHECK_OUT_DATE = "CheckOutDate";
		
	}
	
	public static class AnalyzeHotelTaskDataConstant{
		
		/**解析酒店数据任务数量*/
		public static final String ANALYZE_HOTEL_TASK_SIZE = "AnalyzeHotelTaskSize";
		
		/**解析的供应商数据汇总内容*/
		public static final String ANALYZE_HOTEL_TASK_DATA = "AnalyzeHotelTaskData";
		
		/**解析任务执行结果*/
		public static final String ANALYZE_TASK_RESULT = "AnalyzeTaskResult";
		
		/**解析任务执行失败原因*/
		public static final String ANALYZE_TASK_FAIL_REASON = "AnalyzeTaskFailReason";
		
		/**解析的中间价格计划数据*/
		public static final String ANALYZE_PRICE_PLAN_MIDDLE = "AnalyzePricePlanMiddle";
		
		/**数据同步入库*/
		public static final String ANALYZE_SYNC_PRODUCT_TO_DB = "AnalyzeSyncProductToDB";
		
	}

	public static final String Fetch_Hotel_Info_Exception="抓取酒店基本信息异常,supplyClass:";
	public static final String Fetch_Product_Fail="抓取产品信息失败!";
}
