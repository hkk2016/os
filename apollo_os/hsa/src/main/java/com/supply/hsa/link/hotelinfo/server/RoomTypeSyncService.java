package com.supply.hsa.link.hotelinfo.server;

import com.fangcang.common.ResponseDTO;
import com.fangcang.hotelinfo.request.RoomTypeRequestDTO;

public interface RoomTypeSyncService {
    /**
     * 新增或者修改房型
     * @param roomTypeRequestDTO
     * @return
     */
    public ResponseDTO saveOrUpdateRoomType(RoomTypeRequestDTO roomTypeRequestDTO);
}
