package com.supply.hsa.link.hotelinfo.server.impl;

import com.supply.hsa.link.domain.SupplyHotelDO;
import com.supply.hsa.link.domain.SupplyRoomDO;
import com.supply.hsa.link.hotelinfo.dto.SupplyHotelDTO;
import com.supply.hsa.link.hotelinfo.dto.SupplyRoomDTO;
import com.supply.hsa.link.hotelinfo.server.SupplyHotelStaticServer;
import com.supply.hsa.link.mapper.SupplyHotelMapper;
import com.supply.hsa.link.mapper.SupplyRoomMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SupplyHotelStaticServerImpl implements SupplyHotelStaticServer {
    @Autowired
    private SupplyHotelMapper supplyHotelMapper;

    @Autowired
    private SupplyRoomMapper supplyRoomMapper;

    public void addSupplyHotel(List<SupplyHotelDTO> supplyHotelList) {
        if(CollectionUtils.isNotEmpty(supplyHotelList)){
            List<SupplyHotelDO> supplyHotelDOS = new ArrayList<>();
            for(SupplyHotelDTO supplyHotel:supplyHotelList){
                SupplyHotelDO supplyHotelDO = new SupplyHotelDO();
                supplyHotelDO.setCoCity(supplyHotel.getCoCity());
                if(supplyHotel.getCoCnAddress1() !=null && supplyHotel.getCoCnAddress1().length() > 250){
                    supplyHotelDO.setCoCnAddress1(supplyHotel.getCoCnAddress1().substring(0,250));
                } else {
                    supplyHotelDO.setCoCnAddress1(supplyHotel.getCoCnAddress1());
                }
                if(supplyHotel.getCoCnAddress2() != null && supplyHotel.getCoCnAddress2().length() > 250){
                    supplyHotelDO.setCoCnAddress2(supplyHotel.getCoCnAddress2().substring(0,250));
                } else {
                    supplyHotelDO.setCoCnAddress2(supplyHotel.getCoCnAddress2());
                }

                if(supplyHotel.getCoEngAddress1() != null && supplyHotel.getCoEngAddress1().length() > 250){
                    supplyHotelDO.setCoEngAddress1(supplyHotel.getCoEngAddress1().substring(0,250));
                } else {
                    supplyHotelDO.setCoEngAddress1(supplyHotel.getCoEngAddress1());
                }

                if (supplyHotel.getCoEngAddress2() != null && supplyHotel.getCoEngAddress2().length() > 250){
                    supplyHotelDO.setCoEngAddress2(supplyHotel.getCoEngAddress2().substring(0,250));
                } else {
                    supplyHotelDO.setCoEngAddress2(supplyHotel.getCoEngAddress2());
                }

                if(supplyHotel.getCoHotelCnname() != null && supplyHotel.getCoHotelCnname().length() > 250){
                    supplyHotelDO.setCoHotelCnName(supplyHotel.getCoHotelCnname().substring(0,250));
                } else {
                    supplyHotelDO.setCoHotelCnName(supplyHotel.getCoHotelCnname());
                }

                if(supplyHotel.getCoHotelEngname() != null && supplyHotel.getCoHotelEngname().length() > 250){
                    supplyHotelDO.setCoHotelEngName(supplyHotel.getCoHotelEngname().substring(0,250));
                } else {
                    supplyHotelDO.setCoHotelEngName(supplyHotel.getCoHotelEngname());
                }

                supplyHotelDO.setCoCountry(supplyHotel.getCoCountry());
                supplyHotelDO.setCoFax(supplyHotel.getCoFax());
                supplyHotelDO.setCoHotelId(supplyHotel.getCoHotelId());
                supplyHotelDO.setCoLatitude(supplyHotel.getCoLatitude());
                supplyHotelDO.setCoLongitude(supplyHotel.getCoLongitude());
                supplyHotelDO.setCoProvince(supplyHotel.getCoProvince());
                supplyHotelDO.setCoStart(supplyHotel.getCoStart());
                supplyHotelDO.setCoTel(supplyHotel.getCoTel());
                supplyHotelDO.setSupplierClass(supplyHotel.getSupplierClass());
                supplyHotelDO.setCreateTime(supplyHotel.getCreateTime());
                supplyHotelDO.setRank(supplyHotel.getRank());
                supplyHotelDO.setCoProvinceCode(supplyHotel.getCoProvinceCode());
                supplyHotelDO.setCoCityCode(supplyHotel.getCoCityCode());
                supplyHotelDOS.add(supplyHotelDO);

                if(CollectionUtils.isNotEmpty(supplyHotel.getSupplyRoomList())){
                    addSupplyRoom(supplyHotel.getSupplyRoomList());
                }
            }

            if(CollectionUtils.isNotEmpty(supplyHotelDOS)){
                supplyHotelMapper.batchSaveOrUpdateSupplyHotel(supplyHotelDOS);
            }
        }
    }

    @Override
    public void addSupplyRoom(List<SupplyRoomDTO> supplyRoomList) {
        List<SupplyRoomDO> supplyRoomDOS = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(supplyRoomList)){
            for(SupplyRoomDTO supplyRoomDTO:supplyRoomList){
                SupplyRoomDO supplyRoomDO = new SupplyRoomDO();
                if(supplyRoomDTO.getCoBedType() != null && supplyRoomDTO.getCoBedType().length() >100){
                    supplyRoomDO.setCoBedType(supplyRoomDTO.getCoBedType().substring(0,99));
                } else {
                    supplyRoomDO.setCoBedType(supplyRoomDTO.getCoBedType());
                }
                supplyRoomDO.setCoHotelId(supplyRoomDTO.getCoHotelId());
                supplyRoomDO.setCoRoomId(supplyRoomDTO.getCoRoomId());
                if(supplyRoomDTO.getCoRoomNameCn() != null && supplyRoomDTO.getCoRoomNameCn().length() >250){
                    supplyRoomDO.setCoRoomNameCn(supplyRoomDTO.getCoRoomNameCn().substring(0,250));
                } else{
                    supplyRoomDO.setCoRoomNameCn(supplyRoomDTO.getCoRoomNameCn());
                }
                if(supplyRoomDTO.getCoRoomNameEn() != null && supplyRoomDTO.getCoRoomNameEn().length() > 250){
                    supplyRoomDO.setCoRoomNameEn(supplyRoomDTO.getCoRoomNameEn().substring(0,250));
                } else {
                    supplyRoomDO.setCoRoomNameEn(supplyRoomDTO.getCoRoomNameEn());
                }

                supplyRoomDO.setSupplierClass(supplyRoomDTO.getSupplierClass());
                supplyRoomDO.setCreateTime(supplyRoomDTO.getCreateTime());
                supplyRoomDOS.add(supplyRoomDO);
            }
        }
        if(CollectionUtils.isNotEmpty(supplyRoomDOS)){
//            supplyRoomMapper.batchSaveOrUpdateSupplyRoom(supplyRoomDOS);
        }
    }
}
