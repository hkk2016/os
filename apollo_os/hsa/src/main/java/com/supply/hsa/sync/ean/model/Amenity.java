package com.supply.hsa.sync.ean.model;

public class Amenity {
	/**
	 * 服务/设施的数字标识符
	 */
	private Integer id;
	/**
	 * 服务/设施名称
	 */
	private String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
