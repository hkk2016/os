package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_supply_room")
public class SupplyRoomDO {
    /**
     * 供应商酒店编码
     */
    @Column(name = "co_hotel_id")
    private String coHotelId;

    /**
     * 供应商房型id
     */
    @Column(name = "co_room_id")
    private String coRoomId;

    /**
     * 供应商房型中文名
     */
    @Column(name = "co_room_name_cn")
    private String coRoomNameCn;

    /**
     * 供应商房型英文名
     */
    @Column(name = "co_room_name_en")
    private String coRoomNameEn;

    /**
     * 供应商床型
     */
    @Column(name = "co_bed_type")
    private String coBedType;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 获取供应商酒店编码
     *
     * @return co_hotel_id - 供应商酒店编码
     */
    public String getCoHotelId() {
        return coHotelId;
    }

    /**
     * 设置供应商酒店编码
     *
     * @param coHotelId 供应商酒店编码
     */
    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    /**
     * 获取供应商房型id
     *
     * @return co_room_id - 供应商房型id
     */
    public String getCoRoomId() {
        return coRoomId;
    }

    /**
     * 设置供应商房型id
     *
     * @param coRoomId 供应商房型id
     */
    public void setCoRoomId(String coRoomId) {
        this.coRoomId = coRoomId;
    }

    /**
     * 获取供应商房型中文名
     *
     * @return co_room_name_cn - 供应商房型中文名
     */
    public String getCoRoomNameCn() {
        return coRoomNameCn;
    }

    /**
     * 设置供应商房型中文名
     *
     * @param coRoomNameCn 供应商房型中文名
     */
    public void setCoRoomNameCn(String coRoomNameCn) {
        this.coRoomNameCn = coRoomNameCn;
    }

    /**
     * 获取供应商房型英文名
     *
     * @return co_room_name_en - 供应商房型英文名
     */
    public String getCoRoomNameEn() {
        return coRoomNameEn;
    }

    /**
     * 设置供应商房型英文名
     *
     * @param coRoomNameEn 供应商房型英文名
     */
    public void setCoRoomNameEn(String coRoomNameEn) {
        this.coRoomNameEn = coRoomNameEn;
    }

    /**
     * 获取供应商床型
     *
     * @return co_bed_type - 供应商床型
     */
    public String getCoBedType() {
        return coBedType;
    }

    /**
     * 设置供应商床型
     *
     * @param coBedType 供应商床型
     */
    public void setCoBedType(String coBedType) {
        this.coBedType = coBedType;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}