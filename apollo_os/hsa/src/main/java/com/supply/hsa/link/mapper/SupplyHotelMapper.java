package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.SupplyHotelDO;

import java.util.List;

public interface SupplyHotelMapper extends BaseMapper<SupplyHotelDO> {
    /**
     * 批量新增或修改供应商酒店信息
     * @param supplyHotelDOS
     */
    public void batchSaveOrUpdateSupplyHotel(List<SupplyHotelDO> supplyHotelDOS);
}