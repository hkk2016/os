package com.supply.hsa.link.task.fetch.hotel.container;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.TaskHandler;
import org.springframework.stereotype.Service;

@Service
public class TaskHandlerContainer {

	private Map<SupplierClass, TaskHandler> taskHandlerMap = new ConcurrentHashMap<SupplierClass, TaskHandler>();

	/**
	 * 注册TaskHandler
	 */
	public void register(SupplierClass supplierClass,
			TaskHandler taskHandler) {
		if(supplierClass == null || taskHandler == null){
			throw new IllegalArgumentException("注册TaskHandler服务异常,[参数不能为空]");
		}
		
		if (taskHandlerMap.get(supplierClass) == null) {
			this.taskHandlerMap.put(supplierClass, taskHandler);
		}else{
			throw new IllegalArgumentException("注册TaskHandler服务异常,[重复注册服务], supplierClass = " + supplierClass.getSupplierClass());
		}
	}
	
	/**
	 * 获取已注册的供应商
	 */
	public Set<SupplierClass> getRegisteredSupplier() {
		return this.taskHandlerMap.keySet();
	}

	/**
	 * 获取TaskHandler
	 */
	public TaskHandler getTaskHandler(SupplierClass supplierClass) {
		if (supplierClass == null) {
			return null;
		}

		return taskHandlerMap.get(supplierClass);
	}
}
