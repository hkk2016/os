package com.supply.hsa.link.task.fetch.hotel.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CommonSupplyFetchResultWrapper implements Serializable{

	private static final long serialVersionUID = 1L;

	private CommonSyncRequest request;
	
	private Date queryStartTime;
	
	private Date queryEndTime;
	
	private List<Object> resultList;
	
	private boolean isSuccess;

	public CommonSyncRequest getRequest() {
		return request;
	}

	public void setRequest(CommonSyncRequest request) {
		this.request = request;
	}

	public List<Object> getResultList() {
		return resultList;
	}

	public void setResultList(List<Object> resultList) {
		this.resultList = resultList;
	}

	public Date getQueryStartTime() {
		return queryStartTime;
	}

	public void setQueryStartTime(Date queryStartTime) {
		this.queryStartTime = queryStartTime;
	}

	public Date getQueryEndTime() {
		return queryEndTime;
	}

	public void setQueryEndTime(Date queryEndTime) {
		this.queryEndTime = queryEndTime;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
}
