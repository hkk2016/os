package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.HotelSyncFreqDO;

public interface HotelSyncFreqMapper extends BaseMapper<HotelSyncFreqDO> {
}