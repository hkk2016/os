package com.supply.hsa.link.task.fetch.process;

import javax.annotation.PostConstruct;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.hotel.container.SupplyFetchDataServiceContainer;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractSupplyFetchDataService implements SupplyFetchDataService {
	
	@Autowired
	protected SupplyFetchDataServiceContainer supplyFetchDataServiceContainer;

	/**
	 * 注册查询数据服务
	 */
	@PostConstruct
	@Override
	public void register(){
		supplyFetchDataServiceContainer.register(this.registerSupplierClass(), this);
	}
	
	public abstract SupplierClass registerSupplierClass();
}
