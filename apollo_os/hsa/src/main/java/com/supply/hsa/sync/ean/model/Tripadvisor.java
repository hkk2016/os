package com.supply.hsa.sync.ean.model;

public class Tripadvisor {
	/**
	 * 酒店的 TripAdvisor 评分
	 */
	private String rating;
	/**
	 * TripAdvisor 评分的数量
	 */
	private Integer count;
	
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
}
