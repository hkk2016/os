package com.supply.hsa.link.task.fetch.enums;

public enum HotelDataTypeEnum {
	/**
	 * 酒店基本信息
	 */
	BasicHotelInfo, 
	/**
	 * 酒店信息（房型、价格计划等）
	 */
	HotelInfo,
	
	ChangeHotelInfo
}
