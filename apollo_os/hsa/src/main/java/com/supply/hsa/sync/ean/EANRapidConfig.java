package com.supply.hsa.sync.ean;

public class EANRapidConfig {
	private String rapidApikay;
	
	private String rapidSignature;
	
	private String rapidURL;
	
	private String language;
	
	private String customerIpAddress;

	public String getRapidApikay() {
		return rapidApikay;
	}

	public void setRapidApikay(String rapidApikay) {
		this.rapidApikay = rapidApikay;
	}

	public String getRapidSignature() {
		return rapidSignature;
	}

	public void setRapidSignature(String rapidSignature) {
		this.rapidSignature = rapidSignature;
	}

	public String getRapidURL() {
		return rapidURL;
	}

	public void setRapidURL(String rapidURL) {
		this.rapidURL = rapidURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCustomerIpAddress() {
		return customerIpAddress;
	}

	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}
}
