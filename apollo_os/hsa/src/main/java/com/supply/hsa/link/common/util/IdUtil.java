package com.supply.hsa.link.common.util;

import java.util.UUID;

public final class IdUtil {

	public static String getTaskId(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
