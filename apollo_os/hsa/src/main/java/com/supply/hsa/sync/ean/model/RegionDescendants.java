package com.supply.hsa.sync.ean.model;

import java.util.Map;

public class RegionDescendants extends ErrorObj {
	/**
	 * 第一个key是类别名，第二个key是区域id
	 */
	Map<String, Map<String, Region>> regionMap;

	public Map<String, Map<String, Region>> getRegionMap() {
		return regionMap;
	}

	public void setRegionMap(Map<String, Map<String, Region>> regionMap) {
		this.regionMap = regionMap;
	}
	
}
