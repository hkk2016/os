package com.supply.hsa.link.task.fetch.hotel;

import java.util.Date;

import com.supply.hsa.link.task.fetch.CommonConstant;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.TaskManager;
import com.supply.hsa.link.task.fetch.WorkerStatus;
import com.supply.hsa.link.task.fetch.enums.HotelDataTypeEnum;
import com.supply.hsa.link.task.fetch.exception.SupplyFetchDataException;
import com.supply.hsa.link.task.fetch.hotel.FetchHotelDayTask;
import com.supply.hsa.link.task.fetch.process.SupplyFetchDataService;
import com.supply.hsa.link.task.fetch.hotel.container.SupplyFetchDataServiceContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FetchHotelDayTaskExecutor implements TaskExecutor<FetchHotelDayTask> {

	@Autowired
    private SupplyFetchDataServiceContainer supplyDataServiceContainer;
	
	private boolean validTask(FetchHotelDayTask task){
		if(task == null || task.getRequest() == null){
			return false;
		}
		
		return true;
	}
	
	@Override
	public void add(FetchHotelDayTask task) {
		
	}

	@Override
	public void execute(FetchHotelDayTask task) {
		if (!this.validTask(task)) {
			return;
		}

		// 初始化任务
		task.initTask();
		//任务类型
		task.setTaskName(CommonConstant.TaskType.FETCH_HOTEL_DAY_TASK);
		// 注册任务
		TaskManager.getInstance().register(task);

		SupplyFetchDataService supplyDataService = supplyDataServiceContainer
				.getSupplyFetchDataService(task.getRequest().getSupplierClass());

		HotelDataTypeEnum hotelDataType = task.getHotelDataType();
		Object response = null;

		try {
			if (hotelDataType == HotelDataTypeEnum.HotelInfo) {
				response = supplyDataService.fetchHotelInfo(task);
			} else if (hotelDataType == HotelDataTypeEnum.BasicHotelInfo) {
				response = supplyDataService.fetchHotelBasicInfo(task);
			}

			task.setResult(true);
		} catch (SupplyFetchDataException e) {
			task.setResult(false);
			task.setFailReason(e.getCause().getMessage());

			task.getTaskResult().setError(e.getErrorCode());
		}

		task.setResponse(response);
		
		this.finish(task);
	}
	
	@Override
	public void stop(FetchHotelDayTask task) {
	}
	
	@Override
	public void pause(FetchHotelDayTask task) {
	}

	@Override
	public void resume(FetchHotelDayTask task) {
	}

	@Override
	public void finish(FetchHotelDayTask task) {
		if (!this.validTask(task)) {
			return;
		}
		
		task.setWorkStatus(WorkerStatus.FINISH);
		task.setEndDate(new Date());
		
		task.getTaskResult().setTaskJoinDate(task.getJoinDate());
		task.getTaskResult().setTaskStartDate(task.getStartDate());
		task.getTaskResult().setTaskEndDate(task.getEndDate());
		task.getTaskResult().setCheckInDate(task.getRequest().getBeginDate());
		task.getTaskResult().setCheckOutDate(task.getRequest().getEndDate());
		
		TaskManager taskManager = TaskManager.getInstance();
		
		taskManager.unregister(task);
	}

}
