package com.supply.hsa.link.common.dto;

import java.util.Date;

public class PricePlanMappingDTO {
    /**
     * 价格计划映射主键id
     */
    private Long id;
    /**
     * 价格计划id
     */
    private Long priceplanId;
    /**
     * 供应商酒店id
     */
    private String coHotelId;
    /**
     * 供应商房型id
     */
    private String coRoomtypeId;
    /**
     * 供应商价格计划id
     */
    private String coPriceplanId;
    /**
     * 供应商价格计划名称
     */
    private String coPriceplanName;
    /**
     * 0:无效 1:有效
     */
    private Boolean isActive;
    /**
     * 供应商编号
     */
    private String supplierCode;
    /**
     * 供应商简称
     */
    private String supplierClass;
    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新时间
     */
    private Date modifyDate;
    /**
     * 落地的价格计划名称
     */
    private String priceplanName;
    /**
     * 落地时的床型编码
     */
    private String bedType;

    private String uniqueKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPriceplanId() {
        return priceplanId;
    }

    public void setPriceplanId(Long priceplanId) {
        this.priceplanId = priceplanId;
    }

    public String getCoHotelId() {
        return coHotelId;
    }

    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    public String getCoRoomtypeId() {
        return coRoomtypeId;
    }

    public void setCoRoomtypeId(String coRoomtypeId) {
        this.coRoomtypeId = coRoomtypeId;
    }

    public String getCoPriceplanId() {
        return coPriceplanId;
    }

    public void setCoPriceplanId(String coPriceplanId) {
        this.coPriceplanId = coPriceplanId;
    }

    public String getCoPriceplanName() {
        return coPriceplanName;
    }

    public void setCoPriceplanName(String coPriceplanName) {
        this.coPriceplanName = coPriceplanName;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        isActive = isActive;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getPriceplanName() {
        return priceplanName;
    }

    public void setPriceplanName(String priceplanName) {
        this.priceplanName = priceplanName;
    }

    public String getBedType() {
        return bedType;
    }

    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }
}
