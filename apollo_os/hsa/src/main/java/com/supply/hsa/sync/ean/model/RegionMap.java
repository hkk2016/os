package com.supply.hsa.sync.ean.model;

import java.util.Map;

public class RegionMap {
	private  Map<String, Region> regionMap;

	public Map<String, Region> getRegionMap() {
		return regionMap;
	}

	public void setRegionMap(Map<String, Region> regionMap) {
		this.regionMap = regionMap;
	}
	
}
