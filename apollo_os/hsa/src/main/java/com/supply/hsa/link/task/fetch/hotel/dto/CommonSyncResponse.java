package com.supply.hsa.link.task.fetch.hotel.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.supply.hsa.link.task.fetch.enums.HotelDataTypeEnum;

public class CommonSyncResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
     * 响应码 参见 ResponseCodeEnum
     */
    private Integer resultCode;

    /**
     * 失败原因
     */
    private String failReson;
    
    /**
     * 查询开始时间
     */
    private Date queryStartTime;
    
    /**
     * 查询结束时间
     */
    private Date queryEndTime;
    
    /**
     * 数据类型
     */
    private HotelDataTypeEnum hotelDataType;

    /**
     * 结果
     */
	private List<CommonSupplyFetchResultWrapper> resultList;
	
	public CommonSyncResponse() {}

    public CommonSyncResponse(Integer resultCode, String failReson) {
		this.resultCode = resultCode;
		this.failReson = failReson;
    }

	public Integer getResultCode() {
		return resultCode;
	}

	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}

	public String getFailReson() {
		return failReson;
	}

	public void setFailReson(String failReson) {
		this.failReson = failReson;
	}

	public List<CommonSupplyFetchResultWrapper> getResultList() {
		return resultList;
	}

	public void setResultList(List<CommonSupplyFetchResultWrapper> resultList) {
		this.resultList = resultList;
	}

	public Date getQueryStartTime() {
		return queryStartTime;
	}

	public void setQueryStartTime(Date queryStartTime) {
		this.queryStartTime = queryStartTime;
	}

	public Date getQueryEndTime() {
		return queryEndTime;
	}

	public void setQueryEndTime(Date queryEndTime) {
		this.queryEndTime = queryEndTime;
	}

	public HotelDataTypeEnum getHotelDataType() {
		return hotelDataType;
	}

	public void setHotelDataType(HotelDataTypeEnum hotelDataType) {
		this.hotelDataType = hotelDataType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommonSyncResponse [resultCode=").append(resultCode)
				.append(", failReson=").append(failReson)
				.append(", queryStartTime=").append(queryStartTime)
				.append(", queryEndTime=").append(queryEndTime)
				.append(", hotelDataType=").append(hotelDataType)
				.append(", resultList=").append(resultList).append("]");
		return builder.toString();
	}
}
