package com.supply.hsa.sync.ean.model;

public class Rating {
	/**
	 * 酒店的评分信息
	 */
	private Property property;
	/**
	 * 酒店的 TripAdvisor 信息
	 */
	private Tripadvisor tripadvisor;
	public Property getProperty() {
		return property;
	}
	public void setProperty(Property property) {
		this.property = property;
	}
	public Tripadvisor getTripadvisor() {
		return tripadvisor;
	}
	public void setTripadvisor(Tripadvisor tripadvisor) {
		this.tripadvisor = tripadvisor;
	}
	
}
