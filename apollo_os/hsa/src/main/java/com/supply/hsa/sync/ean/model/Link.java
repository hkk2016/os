package com.supply.hsa.sync.ean.model;

public class Link {
	/**
	 * 用于访问链接的请求方法
	 */
	private String method;
	/**
	 * 绝对 URL
	 */
	private String href;
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	
}
