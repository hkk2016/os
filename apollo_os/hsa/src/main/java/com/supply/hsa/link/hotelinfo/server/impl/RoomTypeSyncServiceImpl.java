package com.supply.hsa.link.hotelinfo.server.impl;

import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.BedTypeEnum;
import com.fangcang.common.enums.ImageTypeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.exception.ServiceException;
import com.fangcang.common.util.PropertyCopyUtil;
import com.fangcang.hotelinfo.domain.HotelDO;
import com.fangcang.hotelinfo.domain.ImageDO;
import com.fangcang.hotelinfo.domain.RoomTypeDO;
import com.fangcang.hotelinfo.dto.BedTypeDTO;
import com.fangcang.hotelinfo.mapper.ImageMapper;
import com.fangcang.hotelinfo.mapper.RoomTypeMapper;
import com.fangcang.hotelinfo.request.RoomTypeRequestDTO;
import com.supply.hsa.link.common.constant.SystemConstants;
import com.supply.hsa.link.hotelinfo.server.RoomTypeSyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@Slf4j
public class RoomTypeSyncServiceImpl implements RoomTypeSyncService {
    @Autowired
    private RoomTypeMapper roomTypeMapper;

    @Autowired
    private ImageMapper imageMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ResponseDTO saveOrUpdateRoomType(RoomTypeRequestDTO roomTypeRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            // 房型基本信息
            RoomTypeDO roomTypeDO = null;
            roomTypeDO = PropertyCopyUtil.transfer(roomTypeRequestDTO, RoomTypeDO.class);
            roomTypeDO.setIsActive(1);
            if (!CollectionUtils.isEmpty(roomTypeRequestDTO.getBedTypeList())) {
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder stringBuilderBed = new StringBuilder();
                for (BedTypeDTO bedTypeDTO : roomTypeRequestDTO.getBedTypeList()) {
                    stringBuilder.append(BedTypeEnum.getValueByKey(bedTypeDTO.getBedType())).append("(")
                            .append(bedTypeDTO.getLength()).append("m").append("*").append(bedTypeDTO.getWide())
                            .append("m").append(";").append(bedTypeDTO.getNum()).append("张").append(")").append(",");
                    stringBuilderBed.append(bedTypeDTO.getBedType()).append(",");
                }
                String bedDescription = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
                String bedType = stringBuilderBed.toString().substring(0, stringBuilderBed.toString().length() - 1);
                roomTypeDO.setBedDescription(bedDescription);
                roomTypeDO.setBedType(bedType);
            }
            if (null == roomTypeRequestDTO.getSort()) {
                roomTypeDO.setSort(99);
            }
            // 房型主图
            ImageDO imageDO = new ImageDO();
            imageDO.setImageId(roomTypeRequestDTO.getImageId());
            imageDO.setImageUrl(roomTypeRequestDTO.getImageUrl());
            imageDO.setRealPath(roomTypeRequestDTO.getRealPath());
            imageDO.setHotelId(roomTypeRequestDTO.getHotelId());
            imageDO.setIsMainImage(1);
            imageDO.setImageType(ImageTypeEnum.ROOMVIEW.key);
            imageDO.setCreator(roomTypeRequestDTO.getCreator());
            imageDO.setModifier(roomTypeRequestDTO.getModifier());

            if(roomTypeDO.getRoomTypeId() == null && roomTypeDO.getCreator() != null && roomTypeDO.getCreator().startsWith(SystemConstants.EAN_CREATER)){
                Example roomQueryExample = new Example(RoomTypeDO.class);
                Example.Criteria roomQueryCriteria = roomQueryExample.createCriteria();
                roomQueryCriteria.andEqualTo("isActive",1);
                roomQueryCriteria.andEqualTo("creator",roomTypeDO.getCreator());
                List<RoomTypeDO> roomDOList = roomTypeMapper.selectByExample(roomQueryExample);
                if(!CollectionUtils.isEmpty(roomDOList)){
                    roomTypeDO.setRoomTypeId(roomDOList.get(0).getRoomTypeId());
                }
            }

            if (null == roomTypeDO.getRoomTypeId()) {

                roomTypeMapper.insertRoomType(roomTypeDO);
                imageDO.setExtId(roomTypeDO.getRoomTypeId());

                imageMapper.insertHotelImage(imageDO);
            } else if (null != roomTypeDO.getRoomTypeId()) {
                // 修改房型基本信息
                roomTypeMapper.updateRoomType(roomTypeDO);
                // 修改房型图片主图和真实路径
                if (null != imageDO && imageDO.getImageId() != null) {
                    imageMapper.updateHotelImage(imageDO);
                }else if(null != imageDO && imageDO.getImageId() == null){
                    imageDO.setExtId(roomTypeRequestDTO.getRoomTypeId());
                    imageMapper.insertHotelImage(imageDO);
                }
            }
            responseDTO.setModel(roomTypeDO.getRoomTypeId());
            responseDTO.setResult(ResultCodeEnum.SUCCESS.code);
        } catch (Exception e) {
            log.error("saveOrUpdateRoomType has error", e);
            throw new ServiceException("saveOrUpdateRoomType has error", e);
        }
        return responseDTO;
    }
}
