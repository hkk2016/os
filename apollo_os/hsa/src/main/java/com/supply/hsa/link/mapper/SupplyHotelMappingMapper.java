package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.common.dto.HotelAndRoomMapDTO;
import com.supply.hsa.link.common.dto.QueryHotelAndRoomMapDTO;
import com.supply.hsa.link.domain.SupplyHotelMappingDO;

import java.util.List;

public interface SupplyHotelMappingMapper extends BaseMapper<SupplyHotelMappingDO> {
    /**
     * 查询酒店房型映射组合
     * @param queryHotelAndRoomMap
     * @return
     */
    public List<HotelAndRoomMapDTO> queryHotelAndRoomMapping(QueryHotelAndRoomMapDTO queryHotelAndRoomMap);
}