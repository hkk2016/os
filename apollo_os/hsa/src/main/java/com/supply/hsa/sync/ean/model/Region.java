package com.supply.hsa.sync.ean.model;

import java.util.List;

public class Region {
	/**
	 * 地区 ID
	 */
	private String id;
	/**
	 * 地区类型
	 */
	private String type;
	/**
	 * 地区名称
	 */
	private String name;
	/**
	 * 完整的地区名称
	 */
	private String name_full;
	/**
	 * 地区的国家/地区代码 (ISO-3166 ALPHA-2)
	 */
	private String country_code;
	/**
	 * 该地区的一系列关联酒店 ID
	 */
	private List<String> property_ids;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName_full() {
		return name_full;
	}
	public void setName_full(String name_full) {
		this.name_full = name_full;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public List<String> getProperty_ids() {
		return property_ids;
	}
	public void setProperty_ids(List<String> property_ids) {
		this.property_ids = property_ids;
	}
	
}
