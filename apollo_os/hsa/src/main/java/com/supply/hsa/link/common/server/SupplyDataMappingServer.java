package com.supply.hsa.link.common.server;

import com.fangcang.common.ResponseDTO;
import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.common.dto.HotelAndRoomMapDTO;
import com.supply.hsa.link.common.dto.HotelMappingDTO;
import com.supply.hsa.link.common.dto.PricePlanMappingDTO;
import com.supply.hsa.link.common.dto.RoomMappingDTO;

import java.util.List;

public interface SupplyDataMappingServer {
    /**
     * 增加酒店映射映射
     * @param request
     * @return
     */
    public ResponseDTO addHotelMapping(HotelMappingDTO request);

    /**
     * 增加房型映射
     * @param request
     * @return
     */
    public ResponseDTO addRoomMapping(RoomMappingDTO request);

    /**
     * 增加价格计划映射
     * @param request
     * @return
     */
    public ResponseDTO addPricePlanMapping(PricePlanMappingDTO request);

    /**
     * 从缓存中获取指定供应商酒店
     */
    List<HotelAndRoomMapDTO> querySupplyHotelMappingCache(SupplierClass supplierClass, String coHotelId);

    /**
     * 查询价格计划映射
     * @param supplierClass
     * @param supplyCode
     * @param coHotelId
     * @param enableMappingCache
     * @return
     */
    List<PricePlanMappingDTO> queryPricePlanMapping( SupplierClass supplierClass, String supplyCode, String coHotelId, boolean enableMappingCache);
}
