package com.supply.hsa.link.domain;

import javax.persistence.*;

@Table(name = "t_htlsync_freq")
public class HotelSyncFreqDO {
    private Integer id;

    @Column(name = "freq_code")
    private String freqCode;

    @Column(name = "freq_name")
    private String freqName;

    @Column(name = "cron_expression")
    private String cronExpression;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return freq_code
     */
    public String getFreqCode() {
        return freqCode;
    }

    /**
     * @param freqCode
     */
    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    /**
     * @return freq_name
     */
    public String getFreqName() {
        return freqName;
    }

    /**
     * @param freqName
     */
    public void setFreqName(String freqName) {
        this.freqName = freqName;
    }

    /**
     * @return cron_expression
     */
    public String getCronExpression() {
        return cronExpression;
    }

    /**
     * @param cronExpression
     */
    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }
}