package com.supply.hsa.link.common.dto;

import java.util.Date;

public class RoomMappingDTO {
    /**
     * 房型映射主键id
     */
    private Integer id;
    /**
     * 房型id
     */
    private Long roomId;
    /**
     * 供应商房型编码
     */
    private String coRoomId;
    /**
     * 供应商房型名称
     */
    private String coRoomName;
    /**
     * 房型名横
     */
    private String roomName;
    /**
     * 供应商酒店id
     */
    private String coHotelId;
    /**
     * 0:无效 1:有效
     */
    private Integer isActive;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 更新时间
     */
    private Date modifyDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getCoRoomId() {
        return coRoomId;
    }

    public void setCoRoomId(String coRoomId) {
        this.coRoomId = coRoomId;
    }

    public String getCoRoomName() {
        return coRoomName;
    }

    public void setCoRoomName(String coRoomName) {
        this.coRoomName = coRoomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getCoHotelId() {
        return coHotelId;
    }

    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}
