package com.supply.hsa.sync.ean.model;

public class Description {
	/**
	 * 说明酒店的一般建筑服务/设施
	 */
	private String amenities;
	/**
	 * 说明酒店的餐饮以及住宿信息
	 */
	private String dining;
	/**
	 * 说明客房或酒店的任何近期装修
	 */
	private String renovations;
	/**
	 * 说明对酒店颁发星级的单位（例如地区或国家旅游局）以及获得的其他评分
	 */
	private String national_ratings;
	/**
	 * 说明酒店提供的商务专用服务/设施，例如会议室
	 */
	private String business_amenities;
	/**
	 * 说明典型客房服务/设施
	 */
	private String rooms;
	/**
	 * 附近景点/景区（一般包括与酒店的距离）
	 */
	private String attractions;
	/**
	 * 酒店输入的一般位置
	 */
	private String location;
	public String getAmenities() {
		return amenities;
	}
	public void setAmenities(String amenities) {
		this.amenities = amenities;
	}
	public String getDining() {
		return dining;
	}
	public void setDining(String dining) {
		this.dining = dining;
	}
	public String getRenovations() {
		return renovations;
	}
	public void setRenovations(String renovations) {
		this.renovations = renovations;
	}
	public String getNational_ratings() {
		return national_ratings;
	}
	public void setNational_ratings(String national_ratings) {
		this.national_ratings = national_ratings;
	}
	public String getBusiness_amenities() {
		return business_amenities;
	}
	public void setBusiness_amenities(String business_amenities) {
		this.business_amenities = business_amenities;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getAttractions() {
		return attractions;
	}
	public void setAttractions(String attractions) {
		this.attractions = attractions;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
}
