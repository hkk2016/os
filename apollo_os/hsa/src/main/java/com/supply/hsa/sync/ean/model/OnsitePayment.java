package com.supply.hsa.sync.ean.model;

import java.util.Map;

public class OnsitePayment {
    private String currency;

    private Map<String,RefObj> types;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Map<String, RefObj> getTypes() {
        return types;
    }

    public void setTypes(Map<String, RefObj> types) {
        this.types = types;
    }
}
