package com.supply.hsa.link.task.fetch.hotel.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fangcang.common.util.DateUtil;
import com.supply.hsa.link.common.constant.SupplierClass;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;

public class CommonSyncRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 供应商类型
	 */
	@NotNull
	private SupplierClass supplierClass;

	/**
	 * 供应商编码
	 */
	@NotNull
	private String supplyCode;
	
	/**
	 * 查询酒店ID
	 */
	@NotNull
	private String hotelId;
	
	/**
	 * 批量查询酒店ID分隔符
	 */
	private String hotelIdSplitChar;
	
	/**
	 * 商家编码
	 */
	private String merchantCode;
	
	/**
	 * 查询开始时间
	 */
	@JSONField(format = "yyyy-MM-dd")
	private Date beginDate;

	/**
	 * 查询结束时间
	 * 
	 */
	@JSONField(format = "yyyy-MM-dd")
	private Date endDate;
	
	/**
	 * 将checkInDate、checkOutDate 按照指定的分隔天数进行分隔，并发查询该时间段的数据;<p>
	 * 当concurrenyQueryOfSplitDay为Null或0时，将不支持并发查询;<p>
	 */
	private Integer concurrenyQueryOfSplitDay;
	
	/**
	 * 将checkInDate、checkOutDate 查询出来的数据，按照指定的分隔天数分段存储;<p>
	 * 当saveSupplyResultOfSplitDay为Null或0时，将所有的数据存储到一块;<p>
	 * 当saveSupplyResultOfSplitDay不为空时，按saveSupplyResultOfSplitDay*concurrenyQueryOfSplitDay分割
	 * 
	 * 用JSON转对象时，如果字符串较大，内存不够时，会报OOM；<p>
	 */
	private Integer saveSupplyResultOfSplitDay;
	
	private String requestUrl;
	
	public boolean check() {
		if(this.supplierClass == null){
			return false;
		}
		
		if(StringUtils.isBlank(this.getSupplyCode())){
			return false;
		}

		if(StringUtils.isBlank(this.hotelId)){
			return false;
		}
		
		if(this.beginDate == null){
			return false;
		}
		
		if(this.endDate == null){
			return false;
		}
		
		if(this.beginDate.compareTo(this.endDate)>0){
			return false;
		}
		
		return true;
	}

	public SupplierClass getSupplierClass() {
		return supplierClass;
	}

	public void setSupplierClass(SupplierClass supplierClass) {
		this.supplierClass = supplierClass;
	}

	public String getSupplyCode() {
		return supplyCode;
	}

	public void setSupplyCode(String supplyCode) {
		this.supplyCode = supplyCode;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public Integer getConcurrenyQueryOfSplitDay() {
		return concurrenyQueryOfSplitDay;
	}

	public void setConcurrenyQueryOfSplitDay(Integer concurrenyQueryOfSplitDay) {
		this.concurrenyQueryOfSplitDay = concurrenyQueryOfSplitDay;
	}

	public Integer getSaveSupplyResultOfSplitDay() {
		return saveSupplyResultOfSplitDay;
	}

	public void setSaveSupplyResultOfSplitDay(Integer saveSupplyResultOfSplitDay) {
		this.saveSupplyResultOfSplitDay = saveSupplyResultOfSplitDay;
	}

	public String getHotelIdSplitChar() {
		return hotelIdSplitChar;
	}

	public void setHotelIdSplitChar(String hotelIdSplitChar) {
		this.hotelIdSplitChar = hotelIdSplitChar;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommonSyncRequest [supplierClass=").append(supplierClass)
				.append(", supplyCode=").append(supplyCode)
				.append(", hotelId=").append(hotelId).append(", merchantCode=")
				.append(merchantCode).append(", beginDate=")
				.append(DateUtil.dateToString(beginDate)).append(", endDate=")
				.append(DateUtil.dateToString(endDate)).append(", concurrenyQueryOfSplitDay=")
				.append(concurrenyQueryOfSplitDay).append(", requestUrl=")
				.append(requestUrl).append("]");
		return builder.toString();
	}

}
