package com.supply.hsa.sync.ean;

public class BaseRequestRapid {
	/**
	 * 供应商编码
	 */
	private String supplyCode;
	/**
	 * 具体的请求后面URL
	 */
	private String paramURL;
	/**
	 * 超时时间（ms）
	 */
	private int timeOut=30000;

	public String getSupplyCode() {
		return supplyCode;
	}

	public void setSupplyCode(String supplyCode) {
		this.supplyCode = supplyCode;
	}

	public String getParamURL() {
		return paramURL;
	}

	public void setParamURL(String paramURL) {
		this.paramURL = paramURL;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
}
