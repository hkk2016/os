package com.supply.hsa.sync.ean;

import java.util.HashMap;
import java.util.Map;

public class EanMapConstants {
    /**
     * Map<EAN供应商星级,本地星级>
     */
    public static final Map<String,Integer> HOTEL_START = new HashMap<>();
    /**
     * 酒店类别
     */
    public static final Map<Integer,Integer> HOTEL_CATEGORY = new HashMap<>();
    /**
     * 支付款类别
     */
    public static final Map<String,String> PAYMENTS = new HashMap<>();

    static{
        HOTEL_START.put("5.0",50);
        HOTEL_START.put("4.5",45);
        HOTEL_START.put("4.0",40);
        HOTEL_START.put("3.5",35);
        HOTEL_START.put("3.0",30);
        HOTEL_START.put("2.5",25);
        HOTEL_START.put("2.0",20);
        HOTEL_START.put("1.5",15);
        HOTEL_START.put("1.0",10);

        HOTEL_CATEGORY.put(0,16);
        HOTEL_CATEGORY.put(1,14);
        HOTEL_CATEGORY.put(2,16);
        HOTEL_CATEGORY.put(3,14);
        HOTEL_CATEGORY.put(4,1);
        HOTEL_CATEGORY.put(5,4);
        HOTEL_CATEGORY.put(6,1);
        HOTEL_CATEGORY.put(7,2);
        HOTEL_CATEGORY.put(8,16);
        HOTEL_CATEGORY.put(9,16);
        HOTEL_CATEGORY.put(10,16);
        HOTEL_CATEGORY.put(11,13);
        HOTEL_CATEGORY.put(12,1);
        HOTEL_CATEGORY.put(13,11);
        HOTEL_CATEGORY.put(14,13);
        HOTEL_CATEGORY.put(15,16);
        HOTEL_CATEGORY.put(16,2);
        HOTEL_CATEGORY.put(17,16);
        HOTEL_CATEGORY.put(18,16);
        HOTEL_CATEGORY.put(20,1);
        HOTEL_CATEGORY.put(21,16);
        HOTEL_CATEGORY.put(22,2);
        HOTEL_CATEGORY.put(23,7);
        HOTEL_CATEGORY.put(24,10);
        HOTEL_CATEGORY.put(25,1);
        HOTEL_CATEGORY.put(26,1);
        HOTEL_CATEGORY.put(29,11);
        HOTEL_CATEGORY.put(30,1);
        HOTEL_CATEGORY.put(31,16);
        HOTEL_CATEGORY.put(32,16);
        HOTEL_CATEGORY.put(33,2);
        HOTEL_CATEGORY.put(34,16);
        HOTEL_CATEGORY.put(36,16);
        HOTEL_CATEGORY.put(37,9);
        HOTEL_CATEGORY.put(39,16);
        HOTEL_CATEGORY.put(40,11);
        HOTEL_CATEGORY.put(41,16);
        HOTEL_CATEGORY.put(42,16);

        PAYMENTS.put("179","5");
        PAYMENTS.put("183","10");
        PAYMENTS.put("184","3");
        PAYMENTS.put("171","4");
        PAYMENTS.put("189","8");
        PAYMENTS.put("1073743307","1");
    }
}
