package com.supply.hsa.sync.ean.model;

import java.util.List;

public class BedGroup {
	private String id;
	private String description;
	private List<Configuration> configuration;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Configuration> getConfiguration() {
		return configuration;
	}
	public void setConfiguration(List<Configuration> configuration) {
		this.configuration = configuration;
	}
	
}
