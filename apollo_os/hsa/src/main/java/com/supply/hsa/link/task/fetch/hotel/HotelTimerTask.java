package com.supply.hsa.link.task.fetch.hotel;


import com.supply.hsa.link.task.fetch.Task;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.hotel.result.HotelTimerTaskResult;

/**
 * 抓取全部产品定时器任务
 */
public class HotelTimerTask extends Task {
	
	private HotelTimerTaskResult taskResult = new HotelTimerTaskResult();

	public HotelTimerTaskResult getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(HotelTimerTaskResult taskResult) {
		this.taskResult = taskResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class<? extends TaskExecutor> getTaskExecutor() {
		return HotelTimerTaskExecutor.class;
	}

}
