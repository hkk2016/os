package com.supply.hsa.link.common.server.impl;

import com.fangcang.common.util.PropertyCopyUtil;
import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;
import com.supply.hsa.link.common.server.HotelSyncPreDataServer;
import com.supply.hsa.link.domain.HotelSyncPreDataDO;
import com.supply.hsa.link.mapper.HotelSyncPreDataMapper;
import com.supply.hsa.link.task.fetch.hotel.dto.SupplyRelation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class HotelSyncPreDataServerImpl implements HotelSyncPreDataServer {
    @Autowired
    private HotelSyncPreDataMapper hotelSyncPreDataMapper;
    @Override
    public List<HtlSyncPreDataDTO> queryPreDate(String supplierClass, String supplyCode, Integer frepId) {
        Example queryExample = new Example(HotelSyncPreDataDO.class);
        Example.Criteria mappingQueryCriteria = queryExample.createCriteria();
        mappingQueryCriteria.andEqualTo("isActive",1);
        mappingQueryCriteria.andEqualTo("supplyCode", supplyCode);
        mappingQueryCriteria.andEqualTo("supplierClass", supplierClass);
        mappingQueryCriteria.andEqualTo("freqId",frepId);
        List<HotelSyncPreDataDO> hotelSyncPreDataDOS = hotelSyncPreDataMapper.selectByExample(queryExample);
        if(CollectionUtils.isNotEmpty(hotelSyncPreDataDOS)){
            List<HtlSyncPreDataDTO> htlSyncPreDataDTOS = PropertyCopyUtil.transferArray(hotelSyncPreDataDOS, HtlSyncPreDataDTO.class);
            return htlSyncPreDataDTOS;
        }
        return null;
    }

    @Override
    public List<SupplyRelation> querySupplyRelationList(SupplyRelation request) {
        List<SupplyRelation> supplyRelationList = hotelSyncPreDataMapper.querySupplyRelationList(request);
        return supplyRelationList;
    }
}
