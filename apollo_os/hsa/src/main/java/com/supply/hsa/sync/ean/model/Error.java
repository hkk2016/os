package com.supply.hsa.sync.ean.model;

import java.util.List;

public class Error {
	/**
	 * 错误类型
	 */
	private String type;
	/**
	 * 无效的包含消息
	 */
	private String message;
	/**
	 * 有关包含错误的特定字段的详细信息
	 */
	private List<Field> fields;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Field> getFields() {
		return fields;
	}
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
}
