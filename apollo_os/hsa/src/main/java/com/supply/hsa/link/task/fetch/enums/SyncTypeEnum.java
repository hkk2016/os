package com.supply.hsa.link.task.fetch.enums;

public enum SyncTypeEnum {
    type_1(1,"指定天数"),
    type_2(2,"指定日期");
    private Integer value;
    private String desc;

    SyncTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
