package com.supply.hsa.link.task.fetch.hotel.result;

import com.alibaba.fastjson.annotation.JSONField;

public abstract class AbstractTaskResult {

	/**任务总数*/
	@JSONField(serialize = false)
	private long taskCount;
	
	/**执行成功的任务总数*/
	@JSONField(serialize = false)
	private long taskSuccessCount;
	
	/**执行失败的任务总数*/
	@JSONField(serialize = false)
	private long taskFailureCount;

	public long getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(long taskCount) {
		this.taskCount = taskCount;
	}

	public long getTaskSuccessCount() {
		return taskSuccessCount;
	}

	public void setTaskSuccessCount(long taskSuccessCount) {
		this.taskSuccessCount = taskSuccessCount;
	}

	public long getTaskFailureCount() {
		return taskFailureCount;
	}

	public void setTaskFailureCount(long taskFailureCount) {
		this.taskFailureCount = taskFailureCount;
	}
}
