package com.supply.hsa.sync.ean.model;

import java.util.List;
import java.util.Map;

public class RoomContent {
	private String id;
	private String name;
	private Map<String, String> descriptions;
	private Map<String, Amenity> amenities;
	private List<Image> images;
	private Map<String, BedGroup> bed_groups;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, String> getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(Map<String, String> descriptions) {
		this.descriptions = descriptions;
	}
	public Map<String, Amenity> getAmenities() {
		return amenities;
	}
	public void setAmenities(Map<String, Amenity> amenities) {
		this.amenities = amenities;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public Map<String, BedGroup> getBed_groups() {
		return bed_groups;
	}
	public void setBed_groups(Map<String, BedGroup> bed_groups) {
		this.bed_groups = bed_groups;
	}
	
}
