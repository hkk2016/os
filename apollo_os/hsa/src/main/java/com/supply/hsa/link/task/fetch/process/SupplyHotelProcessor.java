package com.supply.hsa.link.task.fetch.process;

import com.supply.hsa.link.task.fetch.hotel.analyze.AnalyzeHotelTask;
import com.supply.hsa.link.task.fetch.hotel.dto.PricePlanMiddleDto;

/**
 * 酒店信息转换、保存处理服务
 */
public interface SupplyHotelProcessor<T extends PricePlanMiddleDto> {
	
	/**
	 * 注册服务
	 */
	void register();

	/**
     *	将供应商房价房态每日信息转换为使用于房仓的中间对象,便于后续处理并落地到房仓
     *
     */
	void convertSupplyHotelInfo(AnalyzeHotelTask<T> task);
    
    /**
     *	批量将房价房态每日基本信息中间对象落地到房仓
     */
    void processSupplyHotelInfo(AnalyzeHotelTask<T> task);
}
