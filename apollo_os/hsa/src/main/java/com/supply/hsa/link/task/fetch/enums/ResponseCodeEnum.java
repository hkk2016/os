/**
 * Copyright (c) 2006-2015 Fangcang Ltd. All Rights Reserved. 
 *  
 * This code is the confidential and proprietary information of   
 * Fangcang. You shall not disclose such Confidential Information   
 * and shall use it only in accordance with the terms of the agreements   
 * you entered into with Fangcang,http://www.fangcang.com.
 *  
 */   
package com.supply.hsa.link.task.fetch.enums;

/**
 * <p>
 * 
 *
 *  服务响应码
 * </p>
 */
public enum ResponseCodeEnum {
    Sucess(0000,"SUCCESS"),
    Fail(1111,"ERROR"),
    SaveSupplyHotelAndRoomFail(1001,"保存供应商酒店，房型数据到DB失败"),
    FetchSupplyHotelAndRoomFail(1002,"从供应商抓取酒店，房型数据失败"),
    AnalyzeSupplyHotelAndRoomFail(1003,"从Reids抓取酒店房型数据落地失败"),
    FetchProductFail(1004,"从供应商抓住产品数据失败"),
    AnalyzeProductFail(1005,"从Reids抓取产品数据落地失败"),
    FetchParamError(1006,"请求供应商参数不正确"),
    SaveInActiveRecordFail(1007,"保存失效记录失败"),
    GetSupplyAuthoFail(1008,"获取供应商授权信息失败"),
    SyncProductToDbFail(1009,"同步产品信息到DB失败"),
    InactiveProductFail(1010,"失效产品失败");
    public Integer resultCode;
    public String message;
    ResponseCodeEnum(Integer resultCode,String message){
	this.resultCode=resultCode;
	this.message=message;
    }
}
 