package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.SupplyRoomMappingDO;

public interface SupplyRoomMappingMapper extends BaseMapper<SupplyRoomMappingDO> {
}