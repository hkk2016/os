package com.supply.hsa.link.task.fetch;

public interface TaskExecutor<T extends Task> {
	
	/**
	 * 添加任务
	 */
	void add(T task);
	
	/**
	 * 执行任务
	 */
	void execute(T task);
	
	/**
	 * 停止任务
	 */
	void stop(T task);
	
	/**
	 * 暂停任务
	 */
	void pause(T task);
	
	/**
	 * 恢复任务
	 */
	void resume(T task);
	
	/**
	 * 完成任务
	 */
	void finish(T task);
}
