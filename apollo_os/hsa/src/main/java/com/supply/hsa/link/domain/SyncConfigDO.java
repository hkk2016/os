package com.supply.hsa.link.domain;

import javax.persistence.*;

@Table(name = "t_htlsync_config")
public class SyncConfigDO {
    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 供应商编码
     */
    @Column(name = "supplier_code")
    private String supplierCode;

    /**
     * 商家编码
     */
    @Column(name = "merchant_code")
    private String merchantCode;

    /**
     * 配置对应名
     */
    @Column(name = "field_name")
    private String fieldName;

    /**
     * 配置值
     */
    @Column(name = "field_value")
    private String fieldValue;

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取供应商编码
     *
     * @return supplier_code - 供应商编码
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * 设置供应商编码
     *
     * @param supplierCode 供应商编码
     */
    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    /**
     * 获取商家编码
     *
     * @return merchant_code - 商家编码
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * 设置商家编码
     *
     * @param merchantCode 商家编码
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * 获取配置对应名
     *
     * @return field_name - 配置对应名
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * 设置配置对应名
     *
     * @param fieldName 配置对应名
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * 获取配置值
     *
     * @return field_value - 配置值
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * 设置配置值
     *
     * @param fieldValue 配置值
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}