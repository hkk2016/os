package com.supply.hsa.sync.util;

import com.google.gson.Gson;

public class GsonUtil {

	/**
	 * object 转json
	 * 
	 * @param obj
	 * @return
	 */
	public static String toJson(Object obj) {
		Gson gson = new Gson();
		return gson.toJson(obj);
	}

	/**
	 * json 转object
	 * @param json
	 * @param type
	 * @param <T>
	 * @return
	 */
	public static <T> T fromJson(String json, Class<T> type) {
		Gson gson = new Gson();
		return gson.fromJson(json, type);
	}

}
