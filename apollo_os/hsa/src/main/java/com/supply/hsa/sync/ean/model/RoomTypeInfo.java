package com.supply.hsa.sync.ean.model;

public class RoomTypeInfo {
	private String roomTypeId;
	private String name;
	private String bedDesc;
	public String getRoomTypeId() {
		return roomTypeId;
	}
	public void setRoomTypeId(String roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBedDesc() {
		return bedDesc;
	}
	public void setBedDesc(String bedDesc) {
		this.bedDesc = bedDesc;
	}
	
}
