package com.supply.hsa.link.common.constant;

public enum SupplierClass {
    EAN("EAN");

    private String supplierClass;

    private SupplierClass(String supplierClass) {
        this.setSupplierClass(supplierClass);
    }

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public static SupplierClass getSupplierClass(String supplierClass){
        if(supplierClass == null){
            return null;
        }

        for(SupplierClass tSupplierClass : SupplierClass.values()){
            if(supplierClass.equalsIgnoreCase(tSupplierClass.getSupplierClass())){
                return tSupplierClass;
            }
        }

        return null;
    }
}
