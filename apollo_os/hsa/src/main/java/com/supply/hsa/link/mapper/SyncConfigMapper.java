package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.SyncConfigDO;

public interface SyncConfigMapper extends BaseMapper<SyncConfigDO> {
}