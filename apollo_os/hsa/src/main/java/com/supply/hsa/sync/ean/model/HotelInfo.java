package com.supply.hsa.sync.ean.model;

import java.util.List;

public class HotelInfo {
	private String hotelId;
	
	private String hotelName;
	
	private String address1;
	
	private String address2;
	
	private String rating;
	
	private String latitude;
	
	private String longitude;
	
	private String phone;
	
	private String fax;
	
	private String checkin;
	
	private String checkout;
	
	private String city;
	
	private String countryCode;

	private String provinceCode;

	private String provinceName;
	
	private List<RoomTypeInfo> rooms;

	private Integer rank;

	/**
	 * 详细节点数据
	 */
	private PropertyContent propertyContent;

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<RoomTypeInfo> getRooms() {
		return rooms;
	}

	public void setRooms(List<RoomTypeInfo> rooms) {
		this.rooms = rooms;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public PropertyContent getPropertyContent() {
		return propertyContent;
	}

	public void setPropertyContent(PropertyContent propertyContent) {
		this.propertyContent = propertyContent;
	}
}
