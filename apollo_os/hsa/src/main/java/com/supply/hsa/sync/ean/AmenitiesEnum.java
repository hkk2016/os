package com.supply.hsa.sync.ean;

public enum AmenitiesEnum {
    _1(1,"Air conditioning",5,"可自调空调"),
    _3(3,"Bar/lounge",3,"酒吧"),
    _6(6,"Babysitting or childcare",4,"看护小孩"),
    _8(8,"Elevator/lift",4,"升降梯"),
    _9(9,"Fitness facilities",4,"健身设施"),
    _10(10,"Free airport transportation",4,"机场巴士"),
    _14(14,"Indoor pool",4,"室内游泳池"),
    _19(19,"Restaurant",3,"餐厅"),
    _24(24,"Outdoor pool",4,"室外游泳池"),
    _38(38,"Supervised childcare/activities",4,"看护小孩"),
    _41(41,"ATM/banking",4,"自动柜员机"),
    _43(43,"Concierge services",4,"礼宾服务"),
    _44(44,"Gift shops or newsstand",4,"礼品店或报摊"),
    _45(45,"Shopping on site",4,"商店"),
    _56(56,"Airport transportation (surcharge)",4,"免费接机/接站服务"),
    _81(81,"Conference space",4,"会议室"),
    _200(200,"Business services",4,"商务服务"),
    _317(317,"Fireplace in lobby",4,"大堂壁炉"),
    _321(321,"Barbecue grill(s)",4,"烧烤炉"),
    _324(324,"Coffee/tea in common areas",4,"公共区供应咖啡/茶"),
    _361(361,"Breakfast available (surcharge)",6,"早餐"),
    _369(369,"Laundry facilities",4,"洗衣服务"),
    _371(371,"Spa tub",4,"SPA 浴缸"),
    _372(372,"Television in common areas",4,"公共区设有电视"),
    _375(375,"Arcade/game room",4,"游乐厅/游戏室"),
    _378(378,"Garden",4,"花园"),
    _385(385,"Library",4,"图书馆"),
    _400(400,"Poolside bar",3,"酒吧"),
    _2000(2000,"Swim-up bar",3,"泳池水上吧台"),
    _2001(2001,"Free breakfast",6,"早餐"),
    _2004(2004,"Coffee shop or café",3,"咖啡厅"),
    _2008(2008,"Health club",4,"健身俱乐部"),
    _2010(2010,"Nightclub",4,"夜总会"),
    _2014(2014,"Children's pool",4,"儿童游泳池"),
    _2016(2016,"Safe-deposit box at front desk",4,"行李寄存"),
    _2017(2017,"Spa services on site",4,"酒店内提供 SPA 服务"),
    _2043(2043,"Multilingual staff",4,"多语种工作人员"),
    _2047(2047,"Free newspapers in lobby",4,"免费大堂报纸"),
    _2048(2048,"Free use of nearby fitness center",4,"免费使用附近的健身中心"),
    _2063(2063,"24-hour front desk",4,"24小时前台接待"),
    _2065(2065,"Business center",4,"商务中心"),
    _2070(2070,"Dry cleaning/laundry service",4,"洗衣服务"),
    _2072(2072,"Limo or Town Car service available",4,"车送车接服务"),
    _2074(2074,"Swimming pool",4,"游泳池"),
    _2112(2112,"Casino",4,"娱乐场"),
    _2123(2123,"Full-service spa",4,"全套 SPA 服务"),
    _2129(2129,"Spa treatment room(s)",4,"SPA 护理室"),
    _2131(2131,"Meeting rooms",4,"会议室"),
    _2134(2134,"Steam room",4,"蒸汽房"),
    _2135(2135,"Sauna",4,"桑拿"),
    _2137(2137,"Smoke-free property",4,"无烟酒店"),
    _2138(2138,"Outdoor seasonal pool",4,"按季节开放的室外游泳池"),
    _2167(2167,"Wedding services",4,"婚庆服务"),
    _2349(2349,"Designated smoking areas",4,"指定吸烟区"),
    _2352(2352,"Pool umbrellas",4,"游泳池遮阳伞"),
    _2353(2353,"Airport transportation",4,"机场巴士"),
    _2385(2385,"Billiards or pool table",4,"台球桌或撞球台"),
    _2386(2386,"Grocery/convenience store",4,"杂货店/便利店"),
    _2387(2387,"Tours/ticket assistance",4,"旅游/票务服务"),
    _2390(2390,"Free WiFi",1,"公共区域提供WiF"),
    _2391(2391,"WiFi (surcharge)",1,"WiFi（收费）"),
    _2392(2392,"Free wired Internet",1,"公共区域提供WiF"),
    _2393(2393,"Wired Internet access - surcharge",1,"有线高速上网 - 收费"),
    _2416(2416,"Free train station pickup",4,"免费接机/接站服务"),
    _2420(2420,"Accessible bathroom",4,"残疾人设施"),
    _2421(2421,"Roll-in shower",4,"残疾人设施"),
    _2423(2423,"In-room accessibility",4,"残疾人设施"),
    _2617(2617,"Golf course on site",4,"酒店内设高尔夫球场"),
    _2618(2618,"Tennis on site",4,"酒店内设网球场"),
    _2819(2819,"Water park",4,"水上乐园"),
    _3372(3372,"Conference center",4,"会议室"),
    _3373(3373,"Indoor tennis court",4,"室内网球场"),
    _3375(3375,"Outdoor tennis court",4,"室外网球场"),
    _3500(3500,"Snack bar/deli",4,"商店"),
    _3761(3761,"Parking (limited spaces)",2,"免费停车场"),
    _3777(3777,"Beach bar",4,"海滩酒吧"),
    _3912(3912,"Rooftop terrace",4,"屋顶露台"),
    _4003(4003,"Luggage storage",4,"行李寄存"),
    _4004(4004,"Free airport transportation - pickup",4,"免费接机/接站服务"),
    _4005(4005,"Free airport transportation - drop-off",4,"免费接机/接站服务"),
    _4006(4006,"Airport transportation - pickup (surcharge)",4,"收费接机/接站服务"),
    _4007(4007,"Airport transportation - drop-off (surcharge)",4,"收费接机/接站服务"),
    _4010(4010,"Train station pickup (surcharge)",4,"收费接机/接站服务"),
    _4432(4432,"Beach umbrellas",4,"海滩遮阳伞"),
    _4437(4437,"Beach sun loungers",4,"沙滩躺椅"),
    _4438(4438,"Pool sun loungers",4,"池畔躺椅"),
    _4467(4467,"Beach towels",4,"沙滩毛巾"),
    _4468(4468,"24-hour fitness facilities",4,"24 小时健身设施"),
    _4514(4514,"Terrace",4,"露台"),
    _4695(4695,"Babysitting or childcare (free)",4,"看护小孩"),
    _4696(4696,"Babysitting or childcare (surcharge)",4,"看护小孩"),
    _4699(4699,"Supervised childcare/activities (free)",4,"看护小孩"),
    _4700(4700,"Supervised childcare/activities (surcharge)",4,"看护小孩"),
    _5054(5054,"24-hour health club",4,"24 小时健身俱乐部"),
    _1073742765(1073742765,"Fitness facilities (surcharge)",4,"健身设施（收费）"),
    _1073742766(1073742766,"Sauna (surcharge)",4,"桑拿（收费）"),
    _1073742767(1073742767,"Spa tub (surcharge)",4,"SPA 浴缸（收费）"),
    _1073742768(1073742768,"Indoor pool (surcharge)",4,"室内游泳池（收费）"),
    _1073742769(1073742769,"Outdoor pool (surcharge)",4,"室外游泳池（收费）"),
    _1073742791(1073742791,"Newspapers in lobby (surcharge)",4,"大堂报纸（收费）"),
    _1073743304(1073743304,"Karaoke",4,"卡拉 OK"),
    _1073743305(1073743305,"Free train station drop-off",4,"收费接机/接站服务"),
    _1073743306(1073743306,"Train station drop-off (surcharge)",4,"收费接机/接站服务"),
    _1073743384(1073743384,"Video library",4,"视频图书馆"),
    _1073743386(1073743386,"Ping pong table",4,"乒乓桌"),
    _1073743387(1073743387,"Music library",4,"音乐图书馆"),
    _1073743388(1073743388,"Foosball",4,"桌上足球"),
    _1073743392(1073743392,"Free WiFi (limited) time",1,"公共区域提供WiF"),
    _1073743393(1073743393,"Free WiFi (limited) duration",1,"公共区域提供WiF"),
    _1073743394(1073743394,"Free WiFi (limited) device count",1,"公共区域提供WiF"),
    _1073743395(1073743395,"Limited WiFi (surcharge) device count",1,"公共区域提供WiF"),
    _1073743404(1073743404,"Shared microwave",4,"公用微波炉"),
    _1073743405(1073743405,"Shared refrigerator",4,"公用冰箱"),
    _1073743877(1073743877,"Water dispenser",4,"饮水机"),
    _1073743884(1073743884,"Lockers available",4,"提供寄存柜"),
    _1073744064(1073744064,"Clubhouse",4,"会所"),
    _1073744070(1073744070,"Shopping mall on site",4,"购物商城"),
    _1073744110(1073744110,"Ballroom",4,"舞厅"),
    _1073744111(1073744111,"Banquet hall",4,"宴会厅"),
    _1073744112(1073744112,"Reception hall",4,"接待厅"),
    _1073744117(1073744117,"Indoor/outdoor pool",4,"室内/室外游泳池"),
    _1073744141(1073744141,"Free self-serve breakfast",6,"自助早餐"),
    _1073744275(1073744275,"Covered parking",4,"市内停车场"),
    _1073744276(1073744276,"Uncovered parking",4,"露天停车场"),
    _1073744278(1073744278,"Covered parking",4,"市内停车场"),
    _1073744279(1073744279,"Uncovered parking",4,"露天停车场"),
    _26(26,"Television",5,"闭路电视"),
    _121(121,"Wheelchair accessible",5,"轮椅无障碍设施"),
    _130(130,"Refrigerator",5,"冰箱"),
    _131(131,"Minibar",5,"迷你吧"),
    _132(132,"Coffee/tea maker",5,"泡茶机/咖啡机"),
    _136(136,"Phone",5,"电话机"),
    _141(141,"Private bathroom",5,"浴室"),
    _142(142,"Bathrobes",5,"浴袍"),
    _143(143,"Free toiletries",5,"洗漱用品"),
    _144(144,"Hair dryer",5,"吹风机"),
    _145(145,"Iron/ironing board",5,"熨衣套件"),
    _146(146,"In-room safe",5,"保险柜"),
    _147(147,"Fireplace",5,"壁炉"),
    _148(148,"Jetted bathtub",5,"浴缸"),
    _312(312,"Kitchenette",5,"厨房"),
    _318(318,"Balcony",5,"阳台"),
    _331(331,"Ceiling fan",5,"风扇"),
    _399(399,"Espresso maker",5,"泡茶机/咖啡机"),
    _2026(2026,"Desk",5,"书桌"),
    _2030(2030,"In-room climate control (air conditioning)",5,"中央空调"),
    _2032(2032,"Patio",5,"露台"),
    _2035(2035,"Second bathroom",5,"浴室"),
    _2036(2036,"Shared bathroom",5,"浴室"),
    _2038(2038,"Sofa bed",5,"沙发床"),
    _2039(2039,"Pay movies",5,"付费电影"),
    _2044(2044,"Rollaway/extra beds available",5,"加床按需提供"),
    _2045(2045,"Cribs/infant beds available",5,"加床按需提供"),
    _2055(2055,"Iron/ironing board (on request)",5,"熨衣套件"),
    _2058(2058,"Microwave",5,"微波炉"),
    _2081(2081,"Premium TV channels",5,"付费电视频道"),
    _2086(2086,"DVD player",5,"DVD影碟机"),
    _2158(2158,"Kitchen",5,"厨房"),
    _2162(2162,"Slippers",5,"拖鞋"),
    _2170(2170,"Separate bathtub and shower",5,"淋浴"),
    _2183(2183,"Shower/tub combination",5,"淋浴"),
    _2389(2389,"In-room safe (laptop compatible)",5,"保险柜"),
    _2396(2396,"Satellite TV service",5,"卫星电视"),
    _2398(2398,"Cable TV service",5,"闭路电视"),
    _2402(2402,"HDTV",5,"高清电视"),
    _2403(2403,"Free WiFi",1,"房间提供WiFi"),
    _2575(2575,"LCD TV",5,"液晶电视"),
    _2859(2859,"Private pool",5,"私人游泳池"),
    _2860(2860,"Private plunge pool",5,"私人深水泳池"),
    _3857(3857,"Separate living room",5,"独立客厅"),
    _3858(3858,"Bathtub or shower",5,"浴缸或淋浴"),
    _3923(3923,"Free long-distance calls",5,"免费长途电话"),
    _3924(3924,"Free international calls",5,"免费国际电话"),
    _4118(4118,"Balcony or patio",5,"阳台"),
    _5013(5013,"Spring water bathtub",5,"温泉浴缸"),
    _5093(5093,"Shared/communal kitchen",5,"共享/公共厨房"),
    _5106(5106,"Tablet computer",5,"平板电脑"),
    _5178(5178,"Day bed",5,"沙发床"),
    _6142(6142,"Minibar (stocked, free items)",5,"迷你吧"),
    _6143(6143,"Minibar (stocked, some free items)",5,"迷你吧"),
    _1073742815(1073742815,"Rollaway/extra beds (free)",5,"折叠床按需提供"),
    _1073743289(1073743289,"Electric kettle",5,"电热水壶"),
    _1073743291(1073743291,"Oven",5,"烤箱"),
    _1073743311(1073743311,"Washing machine",5,"洗衣机"),
    _1073743312(1073743312,"Dryer",5,"干衣机"),
    _1073743316(1073743316,"Rice cooker",5,"电饭煲"),
    _1073743372(1073743372,"Highchair",5,"高脚椅"),
    _1073743569(1073743569,"Smart TV",5,"智能电视"),
    _1073743880(1073743880,"Fan",5,"风扇"),
    _1073743948(1073743948,"Free tea bags/instant coffee",5,"免费茶包/速溶咖啡"),
    _1073743957(1073743957,"Outdoor bathroom",5,"室外浴室"),
    _1073743958(1073743958,"Outdoor shower",5,"室外淋浴"),
    _1073744077(1073744077,"Champagne service",5,"香槟服务"),
    _1073744185(1073744185,"Blender",5,"搅拌机"),
    _1073744189(1073744189,"Spices",5,"香料"),
    _1073744190(1073744190,"Toaster oven",5,"小烤箱"),
    _1073744217(1073744217,"Electrical adapters/chargers",5,"电气适配器/充电器"),
    _1073744219(1073744219,"Children's dinnerware",5,"儿童餐具"),
    _1073744220(1073744220,"Baby bath",5,"婴儿浴"),
    _1073744221(1073744221,"Baby monitor",5,"婴儿监护仪"),
    _1073744224(1073744224,"Children's books",5,"儿童读物"),
    _1073744227(1073744227,"CD player",5,"光盘播放机"),
    _1073744244(1073744244,"Reusable coffee/tea filters",5,"可重复使用的咖啡/茶过滤器"),
    _1073744282(1073744282,"Child-size robes",5,"儿童长袍"),
    _1073744283(1073744283,"Child-size slippers",5,"儿童拖鞋"),
    _1073744325(1073744325,"Unlimited free calls",5,"无限免费通话"),
    _1073744327(1073744327,"Smartphone",5,"智能手机");

    /**
     * EAN属性ID
     */
    private Integer attributeID;
    /**
     * EAN属性描述
     */
    private String attributeDesc;
    /**
     * 本地酒店设施类型
     */
    private Integer facilityType;
    /**
     * 本地酒店设施描述
     */
    private String facilityName;


    private AmenitiesEnum(Integer attributeID, String attributeDesc, Integer facilityType, String facilityName) {
        this.attributeID=attributeID;
        this.attributeDesc=attributeDesc;
        this.facilityType=facilityType;
        this.facilityName=facilityName;
    }

    public static AmenitiesEnum getEnumNameyByID(Integer attributeID) {
        AmenitiesEnum amenitiesEnum = null;
        for(AmenitiesEnum amenitie : AmenitiesEnum.values()) {
            if(amenitie.attributeID.equals(attributeID)) {
                amenitiesEnum = amenitie;
                break;
            }
        }
        return amenitiesEnum;
    }

    public Integer getAttributeID() {
        return attributeID;
    }

    public void setAttributeID(Integer attributeID) {
        this.attributeID = attributeID;
    }

    public String getAttributeDesc() {
        return attributeDesc;
    }

    public void setAttributeDesc(String attributeDesc) {
        this.attributeDesc = attributeDesc;
    }

    public Integer getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(Integer facilityType) {
        this.facilityType = facilityType;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }
}
