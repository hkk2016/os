package com.supply.hsa.link.common.server.impl;

import com.fangcang.common.util.PropertyCopyUtil;
import com.fangcang.order.domain.SupplyOrderDO;
import com.supply.hsa.link.common.dto.HtlSyncFreqDTO;
import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;
import com.supply.hsa.link.common.dto.SupplyConfigDTO;
import com.supply.hsa.link.common.server.HotelPreSyncServer;
import com.supply.hsa.link.domain.HotelSyncFreqDO;
import com.supply.hsa.link.domain.HotelSyncPreDataDO;
import com.supply.hsa.link.mapper.HotelSyncFreqMapper;
import com.supply.hsa.link.mapper.HotelSyncPreDataMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service("hotelPreSyncServer")
public class HotelPreSyncServerImpl implements HotelPreSyncServer {
    @Autowired
    private HotelSyncFreqMapper hotelSyncFreqMapper;

    @Autowired
    private HotelSyncPreDataMapper hotelSyncPreDataMapper;

    @Override
    public List<HtlSyncFreqDTO> queryAllFreq() {
        List<HotelSyncFreqDO> dos = hotelSyncFreqMapper.selectAll();
        if(CollectionUtils.isNotEmpty(dos)) {
            List<HtlSyncFreqDTO> htlSyncFreqDTOs = PropertyCopyUtil.transferArray(dos, HtlSyncFreqDTO.class);
            return htlSyncFreqDTOs;
        }
        return null;
    }

    @Override
    public List<HtlSyncPreDataDTO> queryHotelSyncPreData() {
        Example example = new Example(HotelSyncPreDataDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("isActive", 1);
        List<HotelSyncPreDataDO> dos = hotelSyncPreDataMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(dos)){
            List<HtlSyncPreDataDTO> htlSyncPreDataDTOs = PropertyCopyUtil.transferArray(dos, HtlSyncPreDataDTO.class);
            return htlSyncPreDataDTOs;
        }
        return null;
    }

    @Override
    public List<HtlSyncPreDataDTO> queryALlFreqForSupplyCode() {
        List<HotelSyncPreDataDO> dos = hotelSyncPreDataMapper.queryALlFreqForSupplyCode();
        if(CollectionUtils.isNotEmpty(dos)) {
            List<HtlSyncPreDataDTO> htlSyncFreqDTOs = PropertyCopyUtil.transferArray(dos, HtlSyncPreDataDTO.class);
            return htlSyncFreqDTOs;
        }
        return null;
    }
}
