package com.supply.hsa.link.task.fetch.hotel.container;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.process.SupplyFetchDataService;
import org.springframework.stereotype.Service;


/**
 * 供应商数据查询服务注册管理器
 */
@Service
public class SupplyFetchDataServiceContainer {

	private Map<SupplierClass, SupplyFetchDataService> supplyFetchDataServiceMap = new ConcurrentHashMap<SupplierClass, SupplyFetchDataService>();

	/**
	 * 注册供应商服务
	 */
	public void register(SupplierClass supplierClass,
			SupplyFetchDataService supplyFetchDataService) {
		if(supplierClass == null || supplyFetchDataService == null){
			throw new IllegalArgumentException("注册抓取供应商数据服务异常,[参数不能为空]");
		}
		
		if (supplyFetchDataServiceMap.get(supplierClass) == null) {
			this.supplyFetchDataServiceMap.put(supplierClass, supplyFetchDataService);
		}else{
			throw new IllegalArgumentException("注册抓取供应商数据服务异常,[重复注册服务], supplierClass = " + supplierClass.getSupplierClass());
		}
	}

	/**
	 * 获取已注册的供应商
	 */
	public Set<SupplierClass> getRegisteredSupplier() {
		return this.supplyFetchDataServiceMap.keySet();
	}

	/**
	 * 获取供应商服务
	 */
	public SupplyFetchDataService getSupplyFetchDataService(
			SupplierClass supplierClass) {
		if (supplierClass == null) {
			return null;
		}

		return supplyFetchDataServiceMap.get(supplierClass);
	}
}
