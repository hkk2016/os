package com.supply.hsa.sync.ean.model;

public class Property {
	/**
	 * 酒店星级 (0-5)。值为 0.0 或为空表示没有星级
	 */
	private String rating;
	/**
	 *  返回“Star”或“Alternate”值。Star 表示评分是由酒店当地的星级评定机构提供的。
	 *  Alternate 表示该评分是 Expedia 分配的值；不提供官方评分。在面向消费者的网站上，澳大利亚销售网站必须形象化地区分这两种不同的评分类型
	 */
	private String type;
	
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
