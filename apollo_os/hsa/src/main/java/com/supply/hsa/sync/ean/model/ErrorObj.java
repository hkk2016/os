package com.supply.hsa.sync.ean.model;

import java.util.List;

public class ErrorObj {
	/**
	 * 错误类型
	 */
	private String type;
	/**
	 * 人类可读消息，提供有关此错误的详细信息
	 */
	private String message;
	
	private List<Error> errors;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}
	
}
