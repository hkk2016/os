package com.supply.hsa.link.common.server;

import com.supply.hsa.link.common.dto.HtlSyncFreqDTO;
import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;

import java.util.List;

public interface HotelPreSyncServer {
    /**
     * 查询所有同步频率
     * @return
     */
    public List<HtlSyncFreqDTO> queryAllFreq();

    /**
     * 查询所有待同步酒店
     * @return
     */
    public List<HtlSyncPreDataDTO> queryHotelSyncPreData();

    /**
     * 查找频率id和供应商编码（仅这两项数据，要去重）
     * @return
     */
    public List<HtlSyncPreDataDTO> queryALlFreqForSupplyCode();
}
