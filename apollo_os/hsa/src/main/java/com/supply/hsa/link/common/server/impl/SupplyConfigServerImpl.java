package com.supply.hsa.link.common.server.impl;

import com.fangcang.common.util.PropertyCopyUtil;
import com.supply.hsa.link.common.dto.SupplyConfigDTO;
import com.supply.hsa.link.common.server.SupplyConfigServer;
import com.supply.hsa.link.domain.SyncConfigDO;
import com.supply.hsa.link.mapper.SyncConfigMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class SupplyConfigServerImpl implements SupplyConfigServer {

    private Map<String, List<SupplyConfigDTO>> configExtendMap = new HashMap<String, List<SupplyConfigDTO>>();

    @Autowired
    private SyncConfigMapper syncConfigMapper;

    @Override
    public List<SupplyConfigDTO> queryAllConfigList() {
        List<SyncConfigDO> syncConfigDOs= syncConfigMapper.selectAll();
        if(CollectionUtils.isNotEmpty(syncConfigDOs)){
            List<SupplyConfigDTO> supplyConfigDTOS = PropertyCopyUtil.transferArray(syncConfigDOs, SupplyConfigDTO.class);
            return supplyConfigDTOS;
        }
        return null;
    }
}
