package com.supply.hsa.link.task.fetch.server;


import com.supply.hsa.link.task.fetch.hotel.FetchHotelTask;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncResponse;

public interface CommonSupplyFetchService {
//
//	/**
//	 * 拉取供应商酒店、房型基本信息
//	 */
//	CommonSyncResponse fetchHotelBasicInfo(FetchHotelTask task);

	/**
	 * 拉取酒店产品（价格计划）信息
	 */
	CommonSyncResponse fetchHotelProductInfo(FetchHotelTask task);

}
