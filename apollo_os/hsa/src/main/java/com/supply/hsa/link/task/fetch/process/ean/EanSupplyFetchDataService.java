package com.supply.hsa.link.task.fetch.process.ean;


import java.util.Map;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.exception.SupplyFetchDataException;
import com.supply.hsa.link.task.fetch.hotel.FetchHotelDayTask;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;
import com.supply.hsa.link.task.fetch.process.AbstractSupplyFetchDataService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class EanSupplyFetchDataService extends AbstractSupplyFetchDataService {
	
	private static final Log logger = LogFactory.getLog(EanSupplyFetchDataService.class);
	

	
	@Override
	public SupplierClass registerSupplierClass() {
		return SupplierClass.EAN;
	}
	
	@Override
	public void fillingParamBeforeFetchData(CommonSyncRequest request, Map<String,String> lstConfigExtend) throws Exception{
		System.out.println("进来了fillingParamBeforeFetchData");
	}

	@Override
	public Object fetchHotelInfo(FetchHotelDayTask task) throws SupplyFetchDataException {
		System.out.println("进来了fetchHotelInfo");
		return null;
	}


	@Override
	public Object fetchHotelBasicInfo(FetchHotelDayTask task) {
		return null;
	}
	

}
