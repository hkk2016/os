package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.SupplierClassDO;

public interface SupplierClassMapper extends BaseMapper<SupplierClassDO> {
}