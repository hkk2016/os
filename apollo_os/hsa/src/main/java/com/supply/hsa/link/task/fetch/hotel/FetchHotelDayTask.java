package com.supply.hsa.link.task.fetch.hotel;

import com.supply.hsa.link.task.fetch.Task;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.enums.HotelDataTypeEnum;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;
import com.supply.hsa.link.task.fetch.hotel.result.FetchHotelDayTaskResult;

public class FetchHotelDayTask extends Task {
	
	private CommonSyncRequest request;
	
	private HotelDataTypeEnum hotelDataType;
	
	private Object response;
	
	private FetchHotelDayTaskResult taskResult = new FetchHotelDayTaskResult();

	public CommonSyncRequest getRequest() {
		return request;
	}

	public void setRequest(CommonSyncRequest request) {
		this.request = request;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public HotelDataTypeEnum getHotelDataType() {
		return hotelDataType;
	}

	public void setHotelDataType(HotelDataTypeEnum hotelDataType) {
		this.hotelDataType = hotelDataType;
	}

	public FetchHotelDayTaskResult getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(FetchHotelDayTaskResult taskResult) {
		this.taskResult = taskResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class<? extends TaskExecutor> getTaskExecutor() {
		return FetchHotelDayTaskExecutor.class;
	}
}
