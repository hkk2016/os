package com.supply.hsa.link.task.fetch.hotel.result;

import java.util.ArrayList;
import java.util.List;

public class FetchHotelTaskResult extends AbstractTaskResult{
	
	/**抓取失败的每日数据情况*/
	private List<FetchHotelDayTaskResult> fetchHttpFailureList = new ArrayList<FetchHotelDayTaskResult>();
	
	/**http连接失败数量*/
	private long fetchHttpFailureCount;
	
	private AnalyzeHotelTaskResult analyzeHotelTaskResult = new AnalyzeHotelTaskResult();

	public AnalyzeHotelTaskResult getAnalyzeHotelTaskResult() {
		return analyzeHotelTaskResult;
	}

	public void setAnalyzeHotelTaskResult(
			AnalyzeHotelTaskResult analyzeHotelTaskResult) {
		this.analyzeHotelTaskResult = analyzeHotelTaskResult;
	}

	public List<FetchHotelDayTaskResult> getFetchHttpFailureList() {
		return fetchHttpFailureList;
	}

	public void setFetchHttpFailureList(
			List<FetchHotelDayTaskResult> fetchHttpFailureList) {
		this.fetchHttpFailureList = fetchHttpFailureList;
	}

	public long getFetchHttpFailureCount() {
		return fetchHttpFailureCount;
	}

	public void setFetchHttpFailureCount(long fetchHttpFailureCount) {
		this.fetchHttpFailureCount = fetchHttpFailureCount;
	}
}
