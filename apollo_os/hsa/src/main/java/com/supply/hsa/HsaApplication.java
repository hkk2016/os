package com.supply.hsa;

import com.supply.hsa.link.common.util.ApplicationContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
//@MapperScan(basePackages={"com.supply.hsa.*.mapper"})
@MapperScan(basePackages={"com.supply.hsa.*.mapper","com.fangcang.*.mapper","com.fangcang.*.*.mapper","com.fangcang.*.*.*.mapper"})
public class HsaApplication {
	public static void main(String[] args) {
		SpringApplication.run(HsaApplication.class, args);
//		ApplicationContext applicationContext = SpringApplication.run(HsaApplication.class, args);
//		ApplicationContextUtil.setApplicationContext(applicationContext);
	}
}
