package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.SupplyHotelDO;
import com.supply.hsa.link.domain.SupplyRoomDO;

import java.util.List;

public interface SupplyRoomMapper extends BaseMapper<SupplyRoomDO> {
    /**
     * 批量新增或修改供应商酒店信息
     * @param supplyRoomDOS
     */
    public void batchSaveOrUpdateSupplyRoom(List<SupplyHotelDO> supplyRoomDOS);
}