package com.supply.hsa.link.hotelinfo.server;

import com.fangcang.common.ResponseDTO;
import com.fangcang.hotelinfo.request.HotelInfoRequestDTO;
import com.fangcang.hotelinfo.response.HotelInfoResponseDTO;

public interface HotelInfoSyncService {
    /**
     * 保存修改酒店基本信息
     */
    public ResponseDTO<HotelInfoResponseDTO> saveOrUpdateHotel(HotelInfoRequestDTO HotelInfoRequestDTO);
}
