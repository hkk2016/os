package com.supply.hsa.link.common.dto;

public class SupplyConfigDTO {
    /**
     * 供应商简称
     */
    private String supplierClass;
    /**
     * 供应商编码
     */
    private String supplierCode;
    /**
     * 商家编码
     */
    private String merchantCode;
    /**
     * 名称
     */
    private String fieldName;
    /**
     * 值
     */
    private String fieldValue;

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
