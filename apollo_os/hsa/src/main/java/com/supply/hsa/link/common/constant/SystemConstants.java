package com.supply.hsa.link.common.constant;

public class SystemConstants {
    public static final String SYSTEM_OPERATOR = "system";//系统操作用system操作人

    public static final String EAN_CREATER = "ean";

    /**
     * 同步酒店产品任务类
     */
    public static final String FETCHHOTELTASK_JOBCLASS = "com.supply.hsa.link.task.job.FetchHotelJob";

    public static final String FETCHHOTELTASK_JOBNAME = "FetchHotelJob";
    public static final String FETCHHOTELTASK_JOBGROUP = "HSA";
    public static final String FETCHHOTELTASK_TRIGGER = "FetchHotelGroup";
    public static final String FETCHHOTELTASK_MAP_KEY = "FetchHotelData";

    /**
     * 同步产品天数
     */
    public static final Integer FETCH_DAY90 = 90;

    /**
     * 构建价格计划key分隔符
     */
    public static final String PRICE_PLAN_KEY_SPLITOR = "|";
    public static final String SLASH="/";
    public static final String COMMA=",";
    public static final String HYPHEN = "-";

    public static final String QUOTA_ACCOUNT_NAME = "账户";
}
