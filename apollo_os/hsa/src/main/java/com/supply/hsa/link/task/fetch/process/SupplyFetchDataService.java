package com.supply.hsa.link.task.fetch.process;

import com.supply.hsa.link.task.fetch.hotel.FetchHotelDayTask;
import com.supply.hsa.link.task.fetch.exception.SupplyFetchDataException;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;

import java.util.Map;


/**
 * 供应商数据查询服务
 */
public interface SupplyFetchDataService {

	/**
	 * 注册服务
	 */
	void register();
	
	/**
	 * 在获取数据 之前 设置请求参数;
	 * 
	 * @param lstConfigExtend 供应商针对所有商家的公共配置(key=shareProduct) + 指定商家配置
	 */
	void fillingParamBeforeFetchData(CommonSyncRequest request, Map<String,String> lstConfigExtend) throws Exception;
	
	/**
	 * 获取酒店信息数据
	 */
	Object fetchHotelInfo(FetchHotelDayTask task) throws SupplyFetchDataException;

	/**
	 * 获取酒店基本信息数据
	 */
	Object fetchHotelBasicInfo(FetchHotelDayTask task) throws SupplyFetchDataException;
}
