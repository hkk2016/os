package com.supply.hsa.link.task.fetch;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.common.util.IdUtil;

public abstract class Task{

	/**
	 * 任务ID
	 */
	private String taskId;
	
	/**
	 * 父任务ID
	 */
	private String parentTaskId;
	
	/**
	 * 任务名称
	 */
	private String taskName;
	
	/**
	 * 供应商类型
	 */
	private SupplierClass supplierClass;

	/**
	 * 供应商编码
	 */
	private String supplierCode;

	/**
	 * 频率编码
	 */
	private Integer freqId;
	
	/**
	 * 任务执行结果,true成功，false失败
	 */
	private boolean result;
	
	/**
	 * 失败原因
	 */
	private String failReason;
	
	/**
	 * 任务状态
	 */
	private WorkerStatus workStatus;
	
	/**
	 * 任务加入时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date joinDate;
	
	/**
	 * 任务开始时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date startDate;
	
	/**
	 * 任务结束时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date endDate;
	
	private boolean enableLog;
	
	/**
	 * 获取关联的任务执行器
	 */
	@SuppressWarnings("rawtypes")
	public abstract Class<? extends TaskExecutor> getTaskExecutor();
	
	/**
	 * 初始化任务
	 */
	public void initTask(){
		this.taskId = IdUtil.getTaskId();
		this.workStatus = WorkerStatus.RUNNING;
		this.startDate = new Date();
	}
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public SupplierClass getSupplierClass() {
		return supplierClass;
	}

	public void setSupplierClass(SupplierClass supplierClass) {
		this.supplierClass = supplierClass;
	}

	public WorkerStatus getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(WorkerStatus workStatus) {
		this.workStatus = workStatus;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public boolean isEnableLog() {
		return enableLog;
	}

	public void setEnableLog(boolean enableLog) {
		this.enableLog = enableLog;
	}

	public Integer getFreqId() {
		return freqId;
	}

	public void setFreqId(Integer freqId) {
		this.freqId = freqId;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
}
