package com.supply.hsa.sync.ean.model;

public class Fee {
	/**
	 * 说明度假费和其他应缴税款或费用。可说明包含哪些服务，例如健身中心或上网接入
	 */
	private String mandatory;
	/**
	 * 针对早餐、WiFi、停车、宠物等项目说明其他可选费用
	 */
	private String optional;
	
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getOptional() {
		return optional;
	}
	public void setOptional(String optional) {
		this.optional = optional;
	}
	
}
