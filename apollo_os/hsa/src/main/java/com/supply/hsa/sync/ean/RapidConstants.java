package com.supply.hsa.sync.ean;

public class RapidConstants {
	public static final String RAPIDAPIKEY = "RAPIDAPIKEY";
	
	public static final String RAPIDSIGNATURE="RAPIDSIGNATURE";
	
	public static final String RAPIDURL="RAPIDURL";
	
	public static final String LANGUAGE="LANGUAGE";
	
	public static final String CUSTOMERIPADDRESS="CUSTOMERIPADDRESS";

	public static final String LANG_EN="en-US";

	public static final String LANG_CN="zh-CN";
	
}
