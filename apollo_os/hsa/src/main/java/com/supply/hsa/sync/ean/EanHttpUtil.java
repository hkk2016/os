package com.supply.hsa.sync.ean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class EanHttpUtil {
	
	private static final Log logger = LogFactory.getLog(EanHttpUtil.class);

	public static String postForGzip(String url, String param,int timeOut){
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            //设置请求超时与请求方式  
            conn.setReadTimeout(timeOut);
            // 设置通用的请求属性
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            //请求数据格式
            conn.setRequestProperty("Content-Type", "application/xml");
            //返回数据格式
            conn.setRequestProperty("Accept", "application/xml");
            //返回压缩的数据
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            GZIPInputStream gzip = new GZIPInputStream(conn.getInputStream());
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(gzip,"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
        	logger.error("发送 POST-GZIP 请求出现异常！"+e);
            e.printStackTrace();
        }finally{
        	//使用finally块来关闭输出流、输入流
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
	}
	
	public static String getForJsonGzip(String url, Map<String,String> headers, int timeOut){
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = (URLConnection) realUrl.openConnection();
            //设置请求超时与请求方式  
            conn.setReadTimeout(timeOut);
            // 设置通用的请求属性
//            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("User-Agent","chrome");
            //请求数据格式
            conn.setRequestProperty("Accept", "application/json");
            //返回压缩的数据
            conn.setRequestProperty("Accept-Encoding", "gzip");
            
            if(!headers.isEmpty()){
            	for(String key:headers.keySet()){
            		conn.setRequestProperty(key, headers.get(key));
            	}
            }
            conn.setDoOutput(true);

            // 发送POST请求必须设置如下两行
            GZIPInputStream gzip = new GZIPInputStream(conn.getInputStream());
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(gzip,"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
        	logger.error("发送GET-GZIP 请求出现异常！"+e);
            e.printStackTrace();
        }finally{
        	//使用finally块来关闭输出流、输入流
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
	}

}
