package com.supply.hsa.link.task.fetch.hotel;

import com.fangcang.common.util.DateUtil;
import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.common.constant.SystemConstants;
import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;
import com.supply.hsa.link.common.server.HotelSyncPreDataServer;
import com.supply.hsa.link.task.fetch.TaskHandler;
import com.supply.hsa.link.task.fetch.hotel.container.TaskHandlerContainer;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;
import com.supply.hsa.link.task.fetch.hotel.dto.SupplyRelation;
import com.supply.hsa.link.task.fetch.enums.SyncTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

public abstract class AbstractTaskHandler implements TaskHandler {
	@Autowired
	private TaskHandlerContainer taskHandlerContainer;
	@Autowired
	private HotelSyncPreDataServer hotelSyncPreDataServer;
	
	/**
	 * 注册服务
	 */
	@PostConstruct
	@Override
	public void register(){
		this.taskHandlerContainer.register(this.registerSupplierClass(), this);
	}
	
	public abstract SupplierClass registerSupplierClass();
	
	@Override
	public List<CommonSyncRequest> getSyncRequestOfTimerTask(SupplierClass supplierClass, String supplierCode, Integer freqId) {
		/**
		 * 1、查询是否有效的供应商，一个直连供应商可能有多个账号就可能存在多个本地供应商
		 */
		SupplyRelation request = new SupplyRelation();
		request.setSupplierClass(supplierClass.getSupplierClass());
		request.setSupplierCode(supplierCode);
		List<SupplyRelation> supplyRelationList = hotelSyncPreDataServer.querySupplyRelationList(request);

		if(CollectionUtils.isEmpty(supplyRelationList)){
			return Collections.emptyList();
		}

		Set<String> supplyCodeSet = new HashSet<>(supplyRelationList.size());
		for(SupplyRelation supply:supplyRelationList){
			supplyCodeSet.add(supply.getSupplierCode());
		}

		/**
		 * 2、查询供应商下的预同步数据
		 */
		List<HtlSyncPreDataDTO> htlSyncPreDataDTOS = hotelSyncPreDataServer.queryPreDate(supplierClass.name(), supplierCode, freqId);
		if(CollectionUtils.isEmpty(htlSyncPreDataDTOS)){
			return Collections.emptyList();
		}

		/**
		 * 3、筛选封装待同步数据
		 */
		List<CommonSyncRequest> requests = new ArrayList<>();
		Date today = DateUtil.stringToDate(DateUtil.dateToString(new Date()));
		for(HtlSyncPreDataDTO dto:htlSyncPreDataDTOS){
			CommonSyncRequest syncRequest = new CommonSyncRequest();
			if(!supplyCodeSet.contains(dto.getSupplyCode())){
				continue;
			}
			if(SyncTypeEnum.type_1.getValue().equals(dto.getSyncType())){
				/**
				 * 将未来beginDay TO endDay 转换为具体的date
				 */
				syncRequest.setBeginDate(dto.getBeginDay()!=null?DateUtil.getDate(today,dto.getBeginDay(),0):today);
				syncRequest.setEndDate(dto.getEndDay()!=null?DateUtil.getDate(today,dto.getEndDay(),0):DateUtil.getDate(today, SystemConstants.FETCH_DAY90));
			}else {
				if(dto.getBeginDate() == null || dto.getEndDate() == null){
					continue;
				}
				syncRequest.setBeginDate(dto.getBeginDate());
				syncRequest.setEndDate(dto.getEndDate());
			}
			syncRequest.setHotelId(dto.getCoDataCode());
			syncRequest.setSupplierClass(SupplierClass.getSupplierClass(dto.getSupplierClass()));
			syncRequest.setSupplyCode(dto.getSupplyCode());
			requests.add(syncRequest);
		}
        
		return this.convertCommonSyncRequestOfTimerTask(requests);
	}

	public abstract List<CommonSyncRequest> convertCommonSyncRequestOfTimerTask(List<CommonSyncRequest> coHotels);
}
