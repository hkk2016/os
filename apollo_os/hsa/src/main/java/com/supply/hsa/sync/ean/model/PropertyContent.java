package com.supply.hsa.sync.ean.model;

import java.util.List;
import java.util.Map;

public class PropertyContent {
	/**
	 * 唯一的 Expedia 酒店 ID
	 */
	private String property_id;
	/**
	 * 酒店名称
	 */
	private String name;
	/**
	 * 酒店地址信息
	 */
	private Address address;
	/**
	 * 有关酒店评分的信息
	 */
	private Rating ratings;
	/**
	 * 酒店的位置信息
	 */
	private Map<String, Coordinate> location;
	/**
	 * 酒店的电话号码
	 */
	private String phone;
	/**
	 * 酒店的传真号码
	 */
	private String fax;
	/**
	 * 酒店的类别。有关当前已知的类别 ID 和名称
	 */
	private Category category;
	/**
	 * 酒店的排名。此值会根据过去 30 天的 EAN 交易数据对酒店进行排序，其中 1 表示业绩最好的酒店，后面的数字按照升序进行排列
	 */
	private Integer rank;
	/**
	 * 酒店的入住信息
	 */
	private Checkin checkin;
	/**
	 * 酒店的退房信息
	 */
	private Checkout checkout;
	/**
	 * 与酒店费用相关的信息
	 */
	private Fee fees;
	/**
	 * 有关住客需要注意的酒店政策的信息
	 */
	private Map<String, String> policies;
	/**
	 * 出面向所有住客提供的酒店服务/设施。有关当前已知的服务/设施 ID 和名称值
	 */
	private Map<Integer, Amenity> amenities;
	/**
	 * 包含所有可用酒店图片
	 */
	private List<Image> images;
	/**
	 * 有关所有客房的信息
	 */
	private Map<String, RoomContent> rooms;
	/**
	 * 有关酒店提供的房价的其他内容。 此内容应该与价格以及购买时使用的其他房价相关信息一起使用
	 */
	private Map<String, RateContent> rates;
	/**
	 * 酒店描述
	 */
	private Map<String,String> descriptions;
	/**
	 * 酒店主题相关
	 */
	private Map<String,RefObj> themes;
	/**
	 * Statistics represent
	 */
	private Map<String,RefObj> statistics;
	/**
	 * 酒店内接受的付款方式
	 */
	private OnsitePayment onsite_payments;
	public String getProperty_id() {
		return property_id;
	}
	public void setProperty_id(String property_id) {
		this.property_id = property_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Rating getRatings() {
		return ratings;
	}
	public void setRatings(Rating ratings) {
		this.ratings = ratings;
	}
	public Map<String, Coordinate> getLocation() {
		return location;
	}
	public void setLocation(Map<String, Coordinate> location) {
		this.location = location;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Checkin getCheckin() {
		return checkin;
	}
	public void setCheckin(Checkin checkin) {
		this.checkin = checkin;
	}
	public Checkout getCheckout() {
		return checkout;
	}
	public void setCheckout(Checkout checkout) {
		this.checkout = checkout;
	}
	public Fee getFees() {
		return fees;
	}
	public void setFees(Fee fees) {
		this.fees = fees;
	}
	public Map<String, String> getPolicies() {
		return policies;
	}
	public void setPolicies(Map<String, String> policies) {
		this.policies = policies;
	}
	public Map<Integer, Amenity> getAmenities() {
		return amenities;
	}
	public void setAmenities(Map<Integer, Amenity> amenities) {
		this.amenities = amenities;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public Map<String, RoomContent> getRooms() {
		return rooms;
	}
	public void setRooms(Map<String, RoomContent> rooms) {
		this.rooms = rooms;
	}
	public Map<String, RateContent> getRates() {
		return rates;
	}
	public void setRates(Map<String, RateContent> rates) {
		this.rates = rates;
	}

	public Map<String, String> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(Map<String, String> descriptions) {
		this.descriptions = descriptions;
	}

	public Map<String, RefObj> getThemes() {
		return themes;
	}

	public void setThemes(Map<String, RefObj> themes) {
		this.themes = themes;
	}

	public Map<String, RefObj> getStatistics() {
		return statistics;
	}

	public void setStatistics(Map<String, RefObj> statistics) {
		this.statistics = statistics;
	}

	public OnsitePayment getOnsite_payments() {
		return onsite_payments;
	}

	public void setOnsite_payments(OnsitePayment onsite_payments) {
		this.onsite_payments = onsite_payments;
	}
}
