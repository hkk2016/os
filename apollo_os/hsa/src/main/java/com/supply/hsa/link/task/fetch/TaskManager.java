package com.supply.hsa.link.task.fetch;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.common.util.ApplicationContextUtil;
import com.supply.hsa.link.task.fetch.hotel.HotelTimerTask;
import org.apache.commons.lang3.StringUtils;



/**
 * 全局任务管理器;
 */
public final class TaskManager {

	//全局任务管理
	private ConcurrentMap<String, Task> taskMap = new ConcurrentHashMap<String, Task>(1000);
	
	//定时器任务
	private ConcurrentHashMap<SupplierClass, HotelTimerTask> timerTaskMap = new ConcurrentHashMap<SupplierClass, HotelTimerTask>();
	
	@SuppressWarnings("rawtypes")
	private Map<String, ? extends TaskExecutor> taskExecutorMap;
	
	@SuppressWarnings("rawtypes")
//	private Map<String, ? extends TaskCounter> countExecutorMap;
	
	private static TaskManager instance = new TaskManager();
	
	private TaskManager(){
		taskExecutorMap = ApplicationContextUtil.getApplicationContext().getBeansOfType(TaskExecutor.class);
//		countExecutorMap  = ApplicationContextUtil.getApplicationContext().getBeansOfType(TaskCounter.class);
	}
	
	public static TaskManager getInstance(){
		return instance;
	}
	
	private boolean validTask(Task task){
		if(task == null || StringUtils.isBlank(task.getTaskId())){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 注册任务
	 */
	public void register(Task task){
		if(!this.validTask(task)){
			return;
		}
		
		taskMap.put(task.getTaskId(), task);
		
		if(task instanceof HotelTimerTask){
			HotelTimerTask timerTask = (HotelTimerTask)task;
			timerTaskMap.put(timerTask.getSupplierClass(), timerTask);
		}
		
		//注册任务时就写日志
//		if(task.isEnableLog() && (task instanceof HotelTimerTask || task instanceof HotelLtsTask)){
//			TaskEventManager.getInstance().post(task);
//		}
	}
	
	public void unregister(Task task){
		if(!this.validTask(task)){
			return;
		}
		
		taskMap.remove(task.getTaskId());
		
		if(task instanceof HotelTimerTask){
			HotelTimerTask timerTask = (HotelTimerTask)task;
			timerTaskMap.remove(timerTask.getSupplierClass());
		}
		
		//触发日志统计任务事件
//		if(task.isEnableLog()){
//			TaskEventManager.getInstance().post(task);
//		}
		
	}
	
	public Task getTask(String taskId){
		if(StringUtils.isBlank(taskId)){
			return null;
		}
		
		return taskMap.get(taskId);
	}
	
	public HotelTimerTask getTimerTask(SupplierClass supplierClass){
		if(supplierClass == null){
			return null;
		}
		
		return timerTaskMap.get(supplierClass);
	}
	
	/**
	 * 调用任务执行器完成任务
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void finish(Task task){
		if(!this.validTask(task)){
			return;
		}
		
		TaskExecutor taskExecutor = this.getTaskExecutor(task);
		if(taskExecutor!=null){
			taskExecutor.finish(task);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private TaskExecutor getTaskExecutor(Task task){
		for(Map.Entry<String, ? extends TaskExecutor> entry : taskExecutorMap.entrySet()){
			if(task.getTaskExecutor().equals(entry.getValue().getClass())){
				return entry.getValue();
			}
		}
		
		return null;
	}
	
//	@SuppressWarnings("rawtypes")
//	private TaskCounter getCountExecutor(Task task){
//		for(Map.Entry<String, ? extends TaskCounter> entry : countExecutorMap.entrySet()){
//			if(task.getTaskExecutor().equals(entry.getValue().getClass())){
//				return entry.getValue();
//			}
//		}
//
//		return null;
//	}
	
	/**
	 * 任务计数
	 */
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public void increment(Task task, Task childTask){
//		if(!this.validTask(task)){
//			return;
//		}
//
//		if(!this.validTask(childTask)){
//			return;
//		}
//
//		TaskCounter counter = this.getCountExecutor(task);
//		if(counter!=null){
//			counter.increment(task, childTask);
//		}
//	}

}
