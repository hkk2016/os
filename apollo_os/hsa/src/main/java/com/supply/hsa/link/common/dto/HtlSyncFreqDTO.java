package com.supply.hsa.link.common.dto;

import lombok.Data;

@Data
public class HtlSyncFreqDTO {
    private Integer id;
    private String freqCode;
    private String freqName;
    private String cronExpression;
}
