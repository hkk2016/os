package com.supply.hsa.link.task.fetch;

public enum WorkerStatus {
	RUNNING("running"),
	PAUSING("pausing"),
    STOPPED("stopped"), 
    FINISH("finish");

    private String value;

    WorkerStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
