package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_mp_hotel")
public class SupplyHotelMappingDO {
    /**
     * 酒店映射主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 供应商酒店id
     */
    @Column(name = "co_hotel_id")
    private String coHotelId;

    /**
     * 供应商名称
     */
    @Column(name = "co_hotel_name")
    private String coHotelName;

    /**
     * 供应商城市编码
     */
    @Column(name = "co_city_code")
    private String coCityCode;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 0:无效 1:有效
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    @Column(name = "modify_date")
    private Date modifyDate;

    /**
     * 获取酒店映射主键id
     *
     * @return id - 酒店映射主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置酒店映射主键id
     *
     * @param id 酒店映射主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取供应商酒店id
     *
     * @return co_hotel_id - 供应商酒店id
     */
    public String getCoHotelId() {
        return coHotelId;
    }

    /**
     * 设置供应商酒店id
     *
     * @param coHotelId 供应商酒店id
     */
    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    /**
     * 获取供应商名称
     *
     * @return co_hotel_name - 供应商名称
     */
    public String getCoHotelName() {
        return coHotelName;
    }

    /**
     * 设置供应商名称
     *
     * @param coHotelName 供应商名称
     */
    public void setCoHotelName(String coHotelName) {
        this.coHotelName = coHotelName;
    }

    /**
     * 获取供应商城市编码
     *
     * @return co_city_code - 供应商城市编码
     */
    public String getCoCityCode() {
        return coCityCode;
    }

    /**
     * 设置供应商城市编码
     *
     * @param coCityCode 供应商城市编码
     */
    public void setCoCityCode(String coCityCode) {
        this.coCityCode = coCityCode;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取0:无效 1:有效
     *
     * @return is_active - 0:无效 1:有效
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 设置0:无效 1:有效
     *
     * @param isActive 0:无效 1:有效
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return modifier - 修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改人
     *
     * @param modifier 修改人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取修改时间
     *
     * @return modify_date - 修改时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifyDate 修改时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}