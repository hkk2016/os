package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.domain.HotelSyncFreqDO;
import com.supply.hsa.link.domain.HotelSyncPreDataDO;
import com.supply.hsa.link.task.fetch.hotel.dto.SupplyRelation;

import java.util.List;

public interface HotelSyncPreDataMapper extends BaseMapper<HotelSyncPreDataDO> {
    /**
     * 查询指定供应商关联数据
     * @param request
     * @return
     */
    public List<SupplyRelation> querySupplyRelationList(SupplyRelation request);

    public List<HotelSyncPreDataDO>  queryALlFreqForSupplyCode();
}