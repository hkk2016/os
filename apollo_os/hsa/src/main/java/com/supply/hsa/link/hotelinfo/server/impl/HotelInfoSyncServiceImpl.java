package com.supply.hsa.link.hotelinfo.server.impl;

import com.fangcang.common.ResponseDTO;
import com.fangcang.common.enums.ImageTypeEnum;
import com.fangcang.common.enums.ResultCodeEnum;
import com.fangcang.common.util.PropertyCopyUtil;
import com.fangcang.hotelinfo.domain.*;
import com.fangcang.hotelinfo.dto.HotelAdditionalDTO;
import com.fangcang.hotelinfo.dto.HotelFacilityDTO;
import com.fangcang.hotelinfo.mapper.*;
import com.fangcang.hotelinfo.request.HotelInfoRequestDTO;
import com.fangcang.hotelinfo.response.HotelInfoResponseDTO;
import com.fangcang.hotelinfo.service.RoomTypeService;
import com.fangcang.supplier.domain.SupplyCostDO;
import com.supply.hsa.link.common.constant.SystemConstants;
import com.supply.hsa.link.domain.HotelInfoPO;
import com.supply.hsa.link.hotelinfo.server.HotelInfoSyncService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class HotelInfoSyncServiceImpl implements HotelInfoSyncService {
    @Autowired
    private HotelMapper hotelMapper;
    @Autowired
    private HotelAdditionalMapper hotelAdditionalMapper;

    @Autowired
    private HotelPolicyMapper hotelPolicyMapper;

    @Autowired
    private HotelFacilityMapper hotelFacilityMapper;

    @Autowired
    private ImageMapper imageMapper;
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ResponseDTO<HotelInfoResponseDTO> saveOrUpdateHotel(HotelInfoRequestDTO hotelInfoRequestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        HotelInfoResponseDTO hotelInfoResponseDTO = new HotelInfoResponseDTO();
        try {
            //已存在的酒店判断
            if(hotelInfoRequestDTO.getHotelId() == null && hotelInfoRequestDTO.getCreator() != null && hotelInfoRequestDTO.getCreator().startsWith(SystemConstants.EAN_CREATER)){
                Example hotelQueryExample = new Example(HotelDO.class);
                Example.Criteria hotelQueryCriteria = hotelQueryExample.createCriteria();
                hotelQueryCriteria.andEqualTo("isActive",1);
                hotelQueryCriteria.andEqualTo("creator",hotelInfoRequestDTO.getCreator());
                List<HotelDO> hotelDOList = hotelMapper.selectByExample(hotelQueryExample);
                if(!CollectionUtils.isEmpty(hotelDOList)){
                    hotelInfoRequestDTO.setHotelId(hotelDOList.get(0).getHotelId());
                }
            }
            // 酒店基础信息
            HotelDO hotelDO = PropertyCopyUtil.transfer(hotelInfoRequestDTO, HotelDO.class);
            // 酒店主题
            if (null != hotelInfoRequestDTO.getTheme()) {
                String[] strTheme = hotelInfoRequestDTO.getTheme();
                StringBuilder themBuffer = new StringBuilder();
                for (int i = 0; i < strTheme.length; i++) {
                    if (strTheme.length - 1 == i) {
                        themBuffer.append(strTheme[i]);
                    } else {
                        themBuffer.append(strTheme[i]).append(",");
                    }
                }
                hotelDO.setTheme(themBuffer.toString());
            }
            // 免房政策
            if (null != hotelInfoRequestDTO.getFreeRoomPolicy()) {
                String[] intFreeRoomPolicy = hotelInfoRequestDTO.getFreeRoomPolicy();
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < intFreeRoomPolicy.length; i++) {
                    if (intFreeRoomPolicy.length - 1 == i) {
                        sb.append(intFreeRoomPolicy[i]);
                    } else {
                        sb.append(intFreeRoomPolicy[i]).append(",");
                    }
                }
                hotelDO.setFreeRoomPolicy(sb.toString());
            }
            // 酒店主图
            ImageDO imageDO = new ImageDO();
            imageDO.setImageId(hotelInfoRequestDTO.getImageId());
            imageDO.setHotelId(hotelDO.getHotelId());
            imageDO.setImageType(ImageTypeEnum.OUTVIEW.key);
            imageDO.setExtId(hotelDO.getHotelId());
            imageDO.setIsMainImage(1);
            imageDO.setImageUrl(hotelInfoRequestDTO.getImageUrl());
            imageDO.setRealPath(hotelInfoRequestDTO.getRealPath());
            imageDO.setCreator(hotelInfoRequestDTO.getCreator());
            imageDO.setModifier(hotelInfoRequestDTO.getModifier());

            // 酒店附加项
            List<HotelAdditionalDO> hotelAdditionalDOList = new ArrayList<>();
            List<HotelAdditionalDTO> additionalList = hotelInfoRequestDTO.getAdditionalList();
            if (!CollectionUtils.isEmpty(additionalList)) {
                HotelAdditionalDO hotelAdditionalDO = null;
                for (HotelAdditionalDTO additional : additionalList) {
                    if (null != additional.getIsSelected() && 1 == additional.getIsSelected()) {
                        hotelAdditionalDO = PropertyCopyUtil.transfer(additional, HotelAdditionalDO.class);
                        hotelAdditionalDO.setHotelId(hotelDO.getHotelId());
                        hotelAdditionalDO.setMerchantCode(hotelInfoRequestDTO.getMerchantCode());
                        hotelAdditionalDO.setCreator(hotelInfoRequestDTO.getCreator());
                        hotelAdditionalDOList.add(hotelAdditionalDO);
                    }
                }
            }

            // 酒店政策
            HotelPolicyDO hotelPolicyDO = PropertyCopyUtil.transfer(hotelInfoRequestDTO, HotelPolicyDO.class);
            if (null != hotelInfoRequestDTO.getCreditCard()) {
                String[] strCreditCard = hotelInfoRequestDTO.getCreditCard();
                StringBuilder creditCardBuffer = new StringBuilder();
                for (int i = 0; i < strCreditCard.length; i++) {
                    if (strCreditCard.length - 1 == i) {
                        creditCardBuffer.append(strCreditCard[i]);
                    } else {
                        creditCardBuffer.append(strCreditCard[i] + ",");
                    }
                }
                hotelPolicyDO.setCreditCard(creditCardBuffer.toString());
                hotelPolicyDO.setHotelId(hotelDO.getHotelId());
            }

            // 酒店设施
            List<HotelFacilityDTO> facilityList = hotelInfoRequestDTO.getFacilityList();
            List<HotelFacilityDO> hotelFacilityDOList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(facilityList)) {
                HotelFacilityDO hotelFacilityDO = null;
                for (HotelFacilityDTO hotelFacility : facilityList) {
                    hotelFacilityDO = PropertyCopyUtil.transfer(hotelFacility, HotelFacilityDO.class);
                    hotelFacilityDO.setHotelId(hotelDO.getHotelId());
                    hotelFacilityDO.setCreator(hotelInfoRequestDTO.getCreator());
                    hotelFacilityDOList.add(hotelFacilityDO);
                }
            }

            if (hotelInfoRequestDTO.getHotelId() == null) {
                // 新增
                hotelDO.setIsActive(1);
                hotelDO.setCreateTime(new Date());
                hotelMapper.insertHotel(hotelDO);
                // 主图
                if (null != imageDO) {
                    imageDO.setHotelId(hotelDO.getHotelId());
                    imageDO.setExtId(hotelDO.getHotelId());
                    imageMapper.insertHotelImage(imageDO);
                }
                // 附加项
                if (!CollectionUtils.isEmpty(hotelAdditionalDOList)) {
                    for (HotelAdditionalDO hotelAdditionalDO : hotelAdditionalDOList) {
                        hotelAdditionalDO.setHotelId(hotelDO.getHotelId());
                    }
                    hotelAdditionalMapper.inserHotelAdditional(hotelAdditionalDOList);
                }
                // 酒店政策
                if (null != hotelPolicyDO) {
                    hotelPolicyDO.setHotelId(hotelDO.getHotelId());
                    hotelPolicyMapper.insertHotelPolicy(hotelPolicyDO);
                }
                // 酒店设施
                if (!CollectionUtils.isEmpty(hotelFacilityDOList)) {
                    for (HotelFacilityDO hotelFacilityDO : hotelFacilityDOList) {
                        hotelFacilityDO.setHotelId(hotelDO.getHotelId());
                    }
                    hotelFacilityMapper.inserHotelFacility(hotelFacilityDOList);
                }

            } else if (null != hotelDO.getHotelId()) {
                // 修改酒店基本信息
                hotelMapper.updateHotel(hotelDO);

                // 修改酒店主图
                if (null != imageDO && null != imageDO.getImageId()) {
                    imageMapper.updateHotelImage(imageDO);// 根据ID修改
                }else if(null != imageDO && null == imageDO.getImageId()){
                    imageMapper.insertHotelImage(imageDO);
                }

                // 修改酒店附加项
                HotelAdditionalDO hotelAdditionalDO = new HotelAdditionalDO();
                hotelAdditionalDO.setHotelId(hotelInfoRequestDTO.getHotelId());
                hotelAdditionalDO.setMerchantCode(hotelInfoRequestDTO.getMerchantCode());
                hotelAdditionalMapper.deleteHotelAdditional(hotelAdditionalDO);
                if (!CollectionUtils.isEmpty(hotelAdditionalDOList)) {
                    hotelAdditionalMapper.inserHotelAdditional(hotelAdditionalDOList);
                }
                // 修改酒店政策
                if (null != hotelPolicyDO) {
                    hotelPolicyMapper.updateHotelPolicy(hotelPolicyDO);
                }
                // 酒店设施
                hotelFacilityMapper.deleteHotelFacByHotelId(hotelInfoRequestDTO.getHotelId());

                if (!CollectionUtils.isEmpty(facilityList)) {
                    hotelFacilityMapper.inserHotelFacility(hotelFacilityDOList);
                }
            }
            hotelInfoResponseDTO.setHotelId(hotelDO.getHotelId());
            responseDTO.setModel(hotelInfoResponseDTO);
            responseDTO.setResult(ResultCodeEnum.SUCCESS.code);
        } catch (Exception e) {
            log.error("saveOrUpdateHotel has error", e);
            throw new ServiceException("saveOrUpdateHotel has error", e);
        }
        return responseDTO;
    }
}
