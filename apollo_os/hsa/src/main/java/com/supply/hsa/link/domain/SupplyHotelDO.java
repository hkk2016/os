package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_supply_hotel")
public class SupplyHotelDO {
    /**
     * 供应商酒店id
     */
    @Column(name = "co_hotel_id")
    private String coHotelId;

    /**
     * 供应商酒店名称
     */
    @Column(name = "co_hotel_cn_name")
    private String coHotelCnName;

    /**
     * 供应商酒店英文名
     */
    @Column(name = "co_hotel_eng_name")
    private String coHotelEngName;

    /**
     * 供应商国家
     */
    @Column(name = "co_country")
    private String coCountry;

    /**
     * 供应商省份
     */
    @Column(name = "co_province")
    private String coProvince;

    /**
     * 供应商城市
     */
    @Column(name = "co_city")
    private String coCity;

    /**
     * 供应商中文地址1
     */
    @Column(name = "co_cn_address1")
    private String coCnAddress1;

    /**
     * 供应商中文地址2
     */
    @Column(name = "co_cn_address2")
    private String coCnAddress2;

    /**
     * 供应商英文地址1
     */
    @Column(name = "co_eng_address1")
    private String coEngAddress1;

    /**
     * 供应商英文地址2
     */
    @Column(name = "co_eng_address2")
    private String coEngAddress2;

    /**
     * 供应商电话
     */
    @Column(name = "co_tel")
    private String coTel;

    /**
     * 供应商传真
     */
    @Column(name = "co_fax")
    private String coFax;

    /**
     * 供应商星级
     */
    @Column(name = "co_start")
    private String coStart;

    /**
     * 供应商经度
     */
    @Column(name = "co_longitude")
    private String coLongitude;

    /**
     * 供应商纬度
     */
    @Column(name = "co_latitude")
    private String coLatitude;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 供应商酒店的业绩排名
     */
    private Integer rank;

    /**
     * 供应商省份编码
     */
    @Column(name = "co_province_code")
    private String coProvinceCode;

    /**
     * 供应商城市编码
     */
    @Column(name = "co_city_code")
    private String coCityCode;

    /**
     * 获取供应商酒店id
     *
     * @return co_hotel_id - 供应商酒店id
     */
    public String getCoHotelId() {
        return coHotelId;
    }

    /**
     * 设置供应商酒店id
     *
     * @param coHotelId 供应商酒店id
     */
    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    /**
     * 获取供应商酒店名称
     *
     * @return co_hotel_cn_name - 供应商酒店名称
     */
    public String getCoHotelCnName() {
        return coHotelCnName;
    }

    /**
     * 设置供应商酒店名称
     *
     * @param coHotelCnName 供应商酒店名称
     */
    public void setCoHotelCnName(String coHotelCnName) {
        this.coHotelCnName = coHotelCnName;
    }

    /**
     * 获取供应商酒店英文名
     *
     * @return co_hotel_eng_name - 供应商酒店英文名
     */
    public String getCoHotelEngName() {
        return coHotelEngName;
    }

    /**
     * 设置供应商酒店英文名
     *
     * @param coHotelEngName 供应商酒店英文名
     */
    public void setCoHotelEngName(String coHotelEngName) {
        this.coHotelEngName = coHotelEngName;
    }

    /**
     * 获取供应商国家
     *
     * @return co_country - 供应商国家
     */
    public String getCoCountry() {
        return coCountry;
    }

    /**
     * 设置供应商国家
     *
     * @param coCountry 供应商国家
     */
    public void setCoCountry(String coCountry) {
        this.coCountry = coCountry;
    }

    /**
     * 获取供应商省份
     *
     * @return co_province - 供应商省份
     */
    public String getCoProvince() {
        return coProvince;
    }

    /**
     * 设置供应商省份
     *
     * @param coProvince 供应商省份
     */
    public void setCoProvince(String coProvince) {
        this.coProvince = coProvince;
    }

    /**
     * 获取供应商城市
     *
     * @return co_city - 供应商城市
     */
    public String getCoCity() {
        return coCity;
    }

    /**
     * 设置供应商城市
     *
     * @param coCity 供应商城市
     */
    public void setCoCity(String coCity) {
        this.coCity = coCity;
    }

    /**
     * 获取供应商中文地址1
     *
     * @return co_cn_address1 - 供应商中文地址1
     */
    public String getCoCnAddress1() {
        return coCnAddress1;
    }

    /**
     * 设置供应商中文地址1
     *
     * @param coCnAddress1 供应商中文地址1
     */
    public void setCoCnAddress1(String coCnAddress1) {
        this.coCnAddress1 = coCnAddress1;
    }

    /**
     * 获取供应商中文地址2
     *
     * @return co_cn_address2 - 供应商中文地址2
     */
    public String getCoCnAddress2() {
        return coCnAddress2;
    }

    /**
     * 设置供应商中文地址2
     *
     * @param coCnAddress2 供应商中文地址2
     */
    public void setCoCnAddress2(String coCnAddress2) {
        this.coCnAddress2 = coCnAddress2;
    }

    /**
     * 获取供应商英文地址1
     *
     * @return co_eng_address1 - 供应商英文地址1
     */
    public String getCoEngAddress1() {
        return coEngAddress1;
    }

    /**
     * 设置供应商英文地址1
     *
     * @param coEngAddress1 供应商英文地址1
     */
    public void setCoEngAddress1(String coEngAddress1) {
        this.coEngAddress1 = coEngAddress1;
    }

    /**
     * 获取供应商英文地址2
     *
     * @return co_eng_address2 - 供应商英文地址2
     */
    public String getCoEngAddress2() {
        return coEngAddress2;
    }

    /**
     * 设置供应商英文地址2
     *
     * @param coEngAddress2 供应商英文地址2
     */
    public void setCoEngAddress2(String coEngAddress2) {
        this.coEngAddress2 = coEngAddress2;
    }

    /**
     * 获取供应商电话
     *
     * @return co_tel - 供应商电话
     */
    public String getCoTel() {
        return coTel;
    }

    /**
     * 设置供应商电话
     *
     * @param coTel 供应商电话
     */
    public void setCoTel(String coTel) {
        this.coTel = coTel;
    }

    /**
     * 获取供应商传真
     *
     * @return co_fax - 供应商传真
     */
    public String getCoFax() {
        return coFax;
    }

    /**
     * 设置供应商传真
     *
     * @param coFax 供应商传真
     */
    public void setCoFax(String coFax) {
        this.coFax = coFax;
    }

    /**
     * 获取供应商星级
     *
     * @return co_start - 供应商星级
     */
    public String getCoStart() {
        return coStart;
    }

    /**
     * 设置供应商星级
     *
     * @param coStart 供应商星级
     */
    public void setCoStart(String coStart) {
        this.coStart = coStart;
    }

    /**
     * 获取供应商经度
     *
     * @return co_longitude - 供应商经度
     */
    public String getCoLongitude() {
        return coLongitude;
    }

    /**
     * 设置供应商经度
     *
     * @param coLongitude 供应商经度
     */
    public void setCoLongitude(String coLongitude) {
        this.coLongitude = coLongitude;
    }

    /**
     * 获取供应商纬度
     *
     * @return co_latitude - 供应商纬度
     */
    public String getCoLatitude() {
        return coLatitude;
    }

    /**
     * 设置供应商纬度
     *
     * @param coLatitude 供应商纬度
     */
    public void setCoLatitude(String coLatitude) {
        this.coLatitude = coLatitude;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取供应商酒店的业绩排名
     *
     * @return rank - 供应商酒店的业绩排名
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * 设置供应商酒店的业绩排名
     *
     * @param rank 供应商酒店的业绩排名
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * 获取供应商省份编码
     *
     * @return co_province_code - 供应商省份编码
     */
    public String getCoProvinceCode() {
        return coProvinceCode;
    }

    /**
     * 设置供应商省份编码
     *
     * @param coProvinceCode 供应商省份编码
     */
    public void setCoProvinceCode(String coProvinceCode) {
        this.coProvinceCode = coProvinceCode;
    }

    /**
     * 获取供应商城市编码
     *
     * @return co_city_code - 供应商城市编码
     */
    public String getCoCityCode() {
        return coCityCode;
    }

    /**
     * 设置供应商城市编码
     *
     * @param coCityCode 供应商城市编码
     */
    public void setCoCityCode(String coCityCode) {
        this.coCityCode = coCityCode;
    }
}