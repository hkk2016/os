package com.supply.hsa.link.mapper;

import com.supply.hsa.link.common.BaseMapper;
import com.supply.hsa.link.common.dto.PricePlanMappingDTO;
import com.supply.hsa.link.common.dto.PricePlanMappingQueryDTO;
import com.supply.hsa.link.domain.PricePlanMappingDO;

import java.util.List;

public interface PricePlanMappingMapper extends BaseMapper<PricePlanMappingDO> {
    /**
     * 查询有房型映射的酒店映射
     * @param pricePlanMappingQuery
     * @return
     */
    public List<PricePlanMappingDTO> queryPricePlanMappingList(PricePlanMappingQueryDTO pricePlanMappingQuery);
}