package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_mp_priceplan")
public class PricePlanMappingDO {
    /**
     * 价格计划映射主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 价格计划id
     */
    @Column(name = "priceplan_id")
    private Integer priceplanId;

    /**
     * 供应商酒店id
     */
    @Column(name = "co_hotel_id")
    private String coHotelId;

    /**
     * 供应商房型id
     */
    @Column(name = "co_roomtype_id")
    private String coRoomtypeId;

    /**
     * 供应商价格计划id
     */
    @Column(name = "co_priceplan_id")
    private String coPriceplanId;

    /**
     * 供应商价格计划名称
     */
    @Column(name = "co_priceplan_name")
    private String coPriceplanName;

    /**
     * 0:无效 1:有效
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 供应商编号
     */
    @Column(name = "supplier_code")
    private String supplierCode;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 更新时间
     */
    @Column(name = "modify_date")
    private Date modifyDate;

    /**
     * 落地的价格计划名称
     */
    @Column(name = "priceplan_name")
    private String priceplanName;

    /**
     * 落地时的床型编码
     */
    @Column(name = "bed_type")
    private String bedType;

    /**
     * 获取价格计划映射主键id
     *
     * @return id - 价格计划映射主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置价格计划映射主键id
     *
     * @param id 价格计划映射主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取价格计划id
     *
     * @return priceplan_id - 价格计划id
     */
    public Integer getPriceplanId() {
        return priceplanId;
    }

    /**
     * 设置价格计划id
     *
     * @param priceplanId 价格计划id
     */
    public void setPriceplanId(Integer priceplanId) {
        this.priceplanId = priceplanId;
    }

    /**
     * 获取供应商酒店id
     *
     * @return co_hotel_id - 供应商酒店id
     */
    public String getCoHotelId() {
        return coHotelId;
    }

    /**
     * 设置供应商酒店id
     *
     * @param coHotelId 供应商酒店id
     */
    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    /**
     * 获取供应商房型id
     *
     * @return co_roomtype_id - 供应商房型id
     */
    public String getCoRoomtypeId() {
        return coRoomtypeId;
    }

    /**
     * 设置供应商房型id
     *
     * @param coRoomtypeId 供应商房型id
     */
    public void setCoRoomtypeId(String coRoomtypeId) {
        this.coRoomtypeId = coRoomtypeId;
    }

    /**
     * 获取供应商价格计划id
     *
     * @return co_priceplan_id - 供应商价格计划id
     */
    public String getCoPriceplanId() {
        return coPriceplanId;
    }

    /**
     * 设置供应商价格计划id
     *
     * @param coPriceplanId 供应商价格计划id
     */
    public void setCoPriceplanId(String coPriceplanId) {
        this.coPriceplanId = coPriceplanId;
    }

    /**
     * 获取供应商价格计划名称
     *
     * @return co_priceplan_name - 供应商价格计划名称
     */
    public String getCoPriceplanName() {
        return coPriceplanName;
    }

    /**
     * 设置供应商价格计划名称
     *
     * @param coPriceplanName 供应商价格计划名称
     */
    public void setCoPriceplanName(String coPriceplanName) {
        this.coPriceplanName = coPriceplanName;
    }

    /**
     * 获取0:无效 1:有效
     *
     * @return is_active - 0:无效 1:有效
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 设置0:无效 1:有效
     *
     * @param isActive 0:无效 1:有效
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * 获取供应商编号
     *
     * @return supplier_code - 供应商编号
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * 设置供应商编号
     *
     * @param supplierCode 供应商编号
     */
    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return modifier - 更新人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置更新人
     *
     * @param modifier 更新人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取更新时间
     *
     * @return modify_date - 更新时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置更新时间
     *
     * @param modifyDate 更新时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取落地的价格计划名称
     *
     * @return priceplan_name - 落地的价格计划名称
     */
    public String getPriceplanName() {
        return priceplanName;
    }

    /**
     * 设置落地的价格计划名称
     *
     * @param priceplanName 落地的价格计划名称
     */
    public void setPriceplanName(String priceplanName) {
        this.priceplanName = priceplanName;
    }

    /**
     * 获取落地时的床型编码
     *
     * @return bed_type - 落地时的床型编码
     */
    public String getBedType() {
        return bedType;
    }

    /**
     * 设置落地时的床型编码
     *
     * @param bedType 落地时的床型编码
     */
    public void setBedType(String bedType) {
        this.bedType = bedType;
    }
}