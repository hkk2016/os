package com.supply.hsa.link.hotelinfo.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SupplyHotelDTO implements Serializable {
    /**
     * 供应商酒店id
     */
    private String coHotelId;

    /**
     * 供应商酒店名称
     */
    private String coHotelCnname;

    /**
     * 供应商酒店英文名
     */
    private String coHotelEngname;

    /**
     * 供应商国家
     */
    private String coCountry;

    /**
     * 供应商省份
     */
    @Column(name = "co_province")
    private String coProvince;

    /**
     * 供应商城市
     */
    private String coCity;

    /**
     * 供应商中文地址1
     */
    private String coCnAddress1;

    /**
     * 供应商中文地址2
     */
    private String coCnAddress2;

    /**
     * 供应商英文地址1
     */
    private String coEngAddress1;

    /**
     * 供应商英文地址2
     */
    private String coEngAddress2;

    /**
     * 供应商电话
     */
    private String coTel;

    /**
     * 供应商传真
     */
    private String coFax;

    /**
     * 供应商星级
     */
    @Column(name = "co_start")
    private String coStart;

    /**
     * 供应商经度
     */
    private String coLongitude;

    /**
     * 供应商纬度
     */
    private String coLatitude;

    /**
     * 供应商简称
     */
    private String supplierClass;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 供应商房型
     */
    private List<SupplyRoomDTO> supplyRoomList;
    /**
     * 酒店业绩排名
     */
    private Integer rank;
    /**
     * 供应商省份编码
     */
    private String coProvinceCode;
    /**
     * 供应商城市编码
     */
    private String coCityCode;

    public String getCoHotelId() {
        return coHotelId;
    }

    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    public String getCoHotelCnname() {
        return coHotelCnname;
    }

    public void setCoHotelCnname(String coHotelCnname) {
        this.coHotelCnname = coHotelCnname;
    }

    public String getCoHotelEngname() {
        return coHotelEngname;
    }

    public void setCoHotelEngname(String coHotelEngname) {
        this.coHotelEngname = coHotelEngname;
    }

    public String getCoCountry() {
        return coCountry;
    }

    public void setCoCountry(String coCountry) {
        this.coCountry = coCountry;
    }

    public String getCoProvince() {
        return coProvince;
    }

    public void setCoProvince(String coProvince) {
        this.coProvince = coProvince;
    }

    public String getCoCity() {
        return coCity;
    }

    public void setCoCity(String coCity) {
        this.coCity = coCity;
    }

    public String getCoCnAddress1() {
        return coCnAddress1;
    }

    public void setCoCnAddress1(String coCnAddress1) {
        this.coCnAddress1 = coCnAddress1;
    }

    public String getCoCnAddress2() {
        return coCnAddress2;
    }

    public void setCoCnAddress2(String coCnAddress2) {
        this.coCnAddress2 = coCnAddress2;
    }

    public String getCoEngAddress1() {
        return coEngAddress1;
    }

    public void setCoEngAddress1(String coEngAddress1) {
        this.coEngAddress1 = coEngAddress1;
    }

    public String getCoEngAddress2() {
        return coEngAddress2;
    }

    public void setCoEngAddress2(String coEngAddress2) {
        this.coEngAddress2 = coEngAddress2;
    }

    public String getCoTel() {
        return coTel;
    }

    public void setCoTel(String coTel) {
        this.coTel = coTel;
    }

    public String getCoFax() {
        return coFax;
    }

    public void setCoFax(String coFax) {
        this.coFax = coFax;
    }

    public String getCoStart() {
        return coStart;
    }

    public void setCoStart(String coStart) {
        this.coStart = coStart;
    }

    public String getCoLongitude() {
        return coLongitude;
    }

    public void setCoLongitude(String coLongitude) {
        this.coLongitude = coLongitude;
    }

    public String getCoLatitude() {
        return coLatitude;
    }

    public void setCoLatitude(String coLatitude) {
        this.coLatitude = coLatitude;
    }

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<SupplyRoomDTO> getSupplyRoomList() {
        return supplyRoomList;
    }

    public void setSupplyRoomList(List<SupplyRoomDTO> supplyRoomList) {
        this.supplyRoomList = supplyRoomList;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getCoProvinceCode() {
        return coProvinceCode;
    }

    public void setCoProvinceCode(String coProvinceCode) {
        this.coProvinceCode = coProvinceCode;
    }

    public String getCoCityCode() {
        return coCityCode;
    }

    public void setCoCityCode(String coCityCode) {
        this.coCityCode = coCityCode;
    }
}
