package com.supply.hsa.link.common.dto;

import java.util.Date;

public class HotelMappingDTO {
    /**
     * 酒店映射主键id
     */
    private Integer id;
    /**
     * 酒店id
     */
    private Long hotelId;
    /**
     * 供应商酒店id
     */
    private String coHotelId;
    /**
     * 供应商名称
     */
    private String coHotelName;
    /**
     * 供应商城市编码
     */
    private String coCityCode;
    /**
     * 供应商简称
     */
    private String supplierClass;
    /**
     * 0:无效 1:有效
     */
    private Integer isActive;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改人
     */
    private String modifier;
    /**
     * 修改时间
     */
    private Date modifyDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public String getCoHotelId() {
        return coHotelId;
    }

    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    public String getCoHotelName() {
        return coHotelName;
    }

    public void setCoHotelName(String coHotelName) {
        this.coHotelName = coHotelName;
    }

    public String getCoCityCode() {
        return coCityCode;
    }

    public void setCoCityCode(String coCityCode) {
        this.coCityCode = coCityCode;
    }

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}
