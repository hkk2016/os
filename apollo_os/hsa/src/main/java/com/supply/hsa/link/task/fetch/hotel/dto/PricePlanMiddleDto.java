/**
 * Copyright (c) 2013-2016 fangcang Ltd. All Rights Reserved. 
 *  
 * This code is the confidential and proprietary information of   
 * fangcang. You shall not disclose such Confidential Information   
 * and shall use it only in accordance with the terms of the agreements   
 * you entered into with fangcang,http://www.fangcang.com.
 *  
 */
package com.supply.hsa.link.task.fetch.hotel.dto;

import com.fangcang.common.enums.CurrencyEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * 将供应商房价房态每日信息转换落地到房仓时的中间对象
 * 
 * </p>
 * 
 * @author zhangwendeng
 * @date 2016年3月8日 下午3:57:54
 * @version
 */
public class PricePlanMiddleDto {

    /** 保存供应商每日信息中对应房仓价格计划的几个关键属性 */
//    private StringHashMap pricePlanExPropertiesMap = new StringHashMap();
//
//    /** 保存供应商每日信息中其他需要用到的属性 */
//    private StringHashMap otherPropertiesMap = new StringHashMap();
//
//    /** 每日配额，房态 */
//    private List<RoomStatusDTO> roomStatusDTOs = new ArrayList<RoomStatusDTO>();
//
//    /** 每日条款 */
//    private List<RestrictDTO> restrictDTOList = new ArrayList<RestrictDTO>();
//
//    /** 促销公告 */
//    private List<PricePlanDeclareDTO> pricePlanDeclareDTOList = new ArrayList<PricePlanDeclareDTO>();
//
    /** 每日房价、房态、配额、条款封装DTO */
    private List<OverSeaProductInfoDetailDTO> productInfoDetailDTOs = new ArrayList<OverSeaProductInfoDetailDTO>();

    /**
     * 供应商类别
     */
    private String supplierClass;

    /** 供应商编码 */
    private String supplierCode;

    /** 供应商酒店id */
    private String hotelid;

    /** 供应商房型id */
    private String roomtypeid;
    
    /** 供应商房型名称 */
    private String roomtypeName;

    /** 供应商价格计划编码 */
    private String pricePlanCode;

    /** 供应商价格计划名称 */
    private String pricePlanName;
    
    /** 供应商价格计划币种 */
    private String currency;

    /** 价格计划有效性 */
    private boolean isActive = true;
    
    /** 对应房仓价格计划名称 */
    private String fcPriceplanName;

    /** 价格计划开始时间 */
    private Date startDate;

    /** 价格计划结束时间 */
    private Date endDate;
    
    /** 床型编码 */
    private String roomBedType;
    
    /** 首日满房标示 */
	private boolean firstDayFull=true;
    
    // 对应的本地币种
    public Integer getFcCurrency() {
        return CurrencyEnum.getKeyByValue(this.getCurrency());
    }

//    public StringHashMap getPricePlanExPropertiesMap() {
//	return pricePlanExPropertiesMap;
//    }
//
//    public void setPricePlanExPropertiesMap(StringHashMap pricePlanExPropertiesMap) {
//	this.pricePlanExPropertiesMap = pricePlanExPropertiesMap;
//    }
//
//    public StringHashMap getOtherPropertiesMap() {
//	return otherPropertiesMap;
//    }
//
//    public void setOtherPropertiesMap(StringHashMap otherPropertiesMap) {
//	this.otherPropertiesMap = otherPropertiesMap;
//    }
//
//    public List<RestrictDTO> getRestrictDTOList() {
//	return restrictDTOList;
//    }
//
//    public void setRestrictDTOList(List<RestrictDTO> restrictDTOList) {
//	this.restrictDTOList = restrictDTOList;
//    }
//
    public List<OverSeaProductInfoDetailDTO> getProductInfoDetailDTOs() {
		return productInfoDetailDTOs;
	}

	public void setProductInfoDetailDTOs(
			List<OverSeaProductInfoDetailDTO> productInfoDetailDTOs) {
		this.productInfoDetailDTOs = productInfoDetailDTOs;
	}

//	public List<PricePlanDeclareDTO> getPricePlanDeclareDTOList() {
//	return pricePlanDeclareDTOList;
//    }
//
//    public void setPricePlanDeclareDTOList(List<PricePlanDeclareDTO> pricePlanDeclareDTOList) {
//	this.pricePlanDeclareDTOList = pricePlanDeclareDTOList;
//    }
//
//    public String getSupplierClass() {
//	return supplierClass;
//    }
//
//    public void setSupplierClass(String supplierClass) {
//	this.supplierClass = supplierClass;
//    }
//
//    public String getSupplierCode() {
//	return supplierCode;
//    }
//
//    public void setSupplierCode(String supplierCode) {
//	this.supplierCode = supplierCode;
//    }
//
//    public List<RoomStatusDTO> getRoomStatusDTOs() {
//	return roomStatusDTOs;
//    }
//
//    public void setRoomStatusDTOs(List<RoomStatusDTO> roomStatusDTOs) {
//	this.roomStatusDTOs = roomStatusDTOs;
//    }

    public String getHotelid() {
	return hotelid;
    }

    public void setHotelid(String hotelid) {
	this.hotelid = hotelid;
    }

    public String getRoomtypeid() {
	return roomtypeid;
    }

    public void setRoomtypeid(String roomtypeid) {
	this.roomtypeid = roomtypeid;
    }

    public String getPricePlanCode() {
	return pricePlanCode;
    }

    public void setPricePlanCode(String pricePlanCode) {
	this.pricePlanCode = pricePlanCode;
    }

    public boolean isActive() {
	return isActive;
    }

    public void setActive(boolean isActive) {
	this.isActive = isActive;
    }

    public String getPricePlanName() {
	return pricePlanName;
    }

    public void setPricePlanName(String pricePlanName) {
	this.pricePlanName = pricePlanName;
    }

	public String getFcPriceplanName() {
		return fcPriceplanName;
	}

	public void setFcPriceplanName(String fcPriceplanName) {
		this.fcPriceplanName = fcPriceplanName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRoomBedType() {
		return roomBedType;
	}

	public void setRoomBedType(String roomBedType) {
		this.roomBedType = roomBedType;
	}

	public String getRoomtypeName() {
		return roomtypeName;
	}

	public void setRoomtypeName(String roomtypeName) {
		this.roomtypeName = roomtypeName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public boolean getFirstDayFull() {
		return firstDayFull;
	}

	public void setFirstDayFull(boolean firstDayFull) {
		this.firstDayFull = firstDayFull;
	}

	public String getSupplierClass() {
		return supplierClass;
	}

	public void setSupplierClass(String supplierClass) {
		this.supplierClass = supplierClass;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public boolean isFirstDayFull() {
		return firstDayFull;
	}
}
