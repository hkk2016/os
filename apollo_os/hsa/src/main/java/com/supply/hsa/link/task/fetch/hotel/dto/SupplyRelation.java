package com.supply.hsa.link.task.fetch.hotel.dto;

import lombok.Data;

@Data
public class SupplyRelation {
    private static final long serialVersionUID = 1L;
    private String supplierCode;

    private String merchantCode;

    private String supplierClass;

}
