package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_presync_data")
public class HotelSyncPreDataDO {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 供应商编码
     */
    @Column(name = "supply_code")
    private String supplyCode;

    /**
     * 供应商数据编码（酒店编码）
     */
    @Column(name = "co_data_code")
    private String coDataCode;

    /**
     * 本地数据编码
     */
    @Column(name = "data_code")
    private String dataCode;

    /**
     * 本地数据名称
     */
    @Column(name = "data_name")
    private String dataName;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 1:全量天数 2:指定日期
     */
    @Column(name = "sync_type")
    private Boolean syncType;

    /**
     * 指定日期起
     */
    @Column(name = "begin_date")
    private Date beginDate;

    /**
     * 指定日期止
     */
    @Column(name = "end_date")
    private Date endDate;

    /**
     * 同步频率对应id
     */
    @Column(name = "freq_id")
    private Byte freqId;

    /**
     * 是否有效:0无效 1有效
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取供应商编码
     *
     * @return supply_code - 供应商编码
     */
    public String getSupplyCode() {
        return supplyCode;
    }

    /**
     * 设置供应商编码
     *
     * @param supplyCode 供应商编码
     */
    public void setSupplyCode(String supplyCode) {
        this.supplyCode = supplyCode;
    }

    /**
     * 获取供应商数据编码（酒店编码）
     *
     * @return co_data_code - 供应商数据编码（酒店编码）
     */
    public String getCoDataCode() {
        return coDataCode;
    }

    /**
     * 设置供应商数据编码（酒店编码）
     *
     * @param coDataCode 供应商数据编码（酒店编码）
     */
    public void setCoDataCode(String coDataCode) {
        this.coDataCode = coDataCode;
    }

    /**
     * 获取本地数据编码
     *
     * @return data_code - 本地数据编码
     */
    public String getDataCode() {
        return dataCode;
    }

    /**
     * 设置本地数据编码
     *
     * @param dataCode 本地数据编码
     */
    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }

    /**
     * 获取本地数据名称
     *
     * @return data_name - 本地数据名称
     */
    public String getDataName() {
        return dataName;
    }

    /**
     * 设置本地数据名称
     *
     * @param dataName 本地数据名称
     */
    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取1:全量天数 2:指定日期
     *
     * @return sync_type - 1:全量天数 2:指定日期
     */
    public Boolean getSyncType() {
        return syncType;
    }

    /**
     * 设置1:全量天数 2:指定日期
     *
     * @param syncType 1:全量天数 2:指定日期
     */
    public void setSyncType(Boolean syncType) {
        this.syncType = syncType;
    }

    /**
     * 获取指定日期起
     *
     * @return begin_date - 指定日期起
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     * 设置指定日期起
     *
     * @param beginDate 指定日期起
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     * 获取指定日期止
     *
     * @return end_date - 指定日期止
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * 设置指定日期止
     *
     * @param endDate 指定日期止
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取同步频率对应id
     *
     * @return freq_id - 同步频率对应id
     */
    public Byte getFreqId() {
        return freqId;
    }

    /**
     * 设置同步频率对应id
     *
     * @param freqId 同步频率对应id
     */
    public void setFreqId(Byte freqId) {
        this.freqId = freqId;
    }

    /**
     * 获取是否有效:0无效 1有效
     *
     * @return is_active - 是否有效:0无效 1有效
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 设置是否有效:0无效 1有效
     *
     * @param isActive 是否有效:0无效 1有效
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}