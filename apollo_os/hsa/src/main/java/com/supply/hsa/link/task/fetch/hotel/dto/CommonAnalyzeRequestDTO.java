package com.supply.hsa.link.task.fetch.hotel.dto;

import java.io.Serializable;
import java.util.Date;

import com.supply.hsa.link.task.fetch.enums.SyncTypeEnum;

/**
 * 查询的供应商原始数据,用于解析成房仓数据结构
 */
public class CommonAnalyzeRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private SyncTypeEnum taskType;
	
	private String supplyClass;

    private String supplyCode;
    
    /** 请求开始时间 */
    private Date checkInDate;
    
    /** 请求结束时间 */
    private Date checkOutDate;
    
    /**
     * 不用json处理请求、响应
     */
    private boolean withoutJson;

    /** 该次同步请求的参数 */
    private Object request;

    /** 
     * 该次同步请求的返回内容 ;
     * 集合中每个元素表示一段时间内的酒店详情数据；
     * */
    private Object responseList;

	public SyncTypeEnum getTaskType() {
		return taskType;
	}

	public void setTaskType(SyncTypeEnum taskType) {
		this.taskType = taskType;
	}

	public String getSupplyClass() {
		return supplyClass;
	}

	public void setSupplyClass(String supplyClass) {
		this.supplyClass = supplyClass;
	}

	public String getSupplyCode() {
		return supplyCode;
	}

	public void setSupplyCode(String supplyCode) {
		this.supplyCode = supplyCode;
	}

	public Object getRequest() {
		return request;
	}

	public void setRequest(Object request) {
		this.request = request;
	}

	public Object getResponseList() {
		return responseList;
	}

	public void setResponseList(Object responseList) {
		this.responseList = responseList;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public boolean isWithoutJson() {
		return withoutJson;
	}

	public void setWithoutJson(boolean withoutJson) {
		this.withoutJson = withoutJson;
	}

}
