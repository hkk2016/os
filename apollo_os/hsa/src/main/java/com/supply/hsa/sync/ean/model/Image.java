package com.supply.hsa.sync.ean.model;

import java.util.Map;

public class Image {
	private String caption;
	private String hero_image;
	private String category;
	private Map<String, Link> links;
	
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getHero_image() {
		return hero_image;
	}
	public void setHero_image(String hero_image) {
		this.hero_image = hero_image;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Map<String, Link> getLinks() {
		return links;
	}
	public void setLinks(Map<String, Link> links) {
		this.links = links;
	}
	
}
