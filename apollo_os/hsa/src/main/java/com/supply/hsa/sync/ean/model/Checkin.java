package com.supply.hsa.sync.ean.model;

public class Checkin {
	/**
	 * 酒店开始允许入住的时间
	 */
	private String begin_time;
	/**
	 * 酒店停止允许入住的时间
	 */
	private String end_time;
	/**
	 * 酒店的入住政策
	 */
	private String instructions;
	/**
	 * 有关入住此酒店的任何特别说明，该说明可能是特定于这家酒店的
	 */
	private String special_instructions;
	/**
	 * 能够入住酒店的客户的最小年龄
	 */
	private Integer min_age;
	
	public String getBegin_time() {
		return begin_time;
	}
	public void setBegin_time(String begin_time) {
		this.begin_time = begin_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getSpecial_instructions() {
		return special_instructions;
	}
	public void setSpecial_instructions(String special_instructions) {
		this.special_instructions = special_instructions;
	}
	public Integer getMin_age() {
		return min_age;
	}
	public void setMin_age(Integer min_age) {
		this.min_age = min_age;
	}
	
}
