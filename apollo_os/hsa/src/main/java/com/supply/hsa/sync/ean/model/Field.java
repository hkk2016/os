package com.supply.hsa.sync.ean.model;

public class Field {
	/**
	 * 包含错误的字段
	 */
	private String name;
	/**
	 * 包含错误的字段的类型
	 */
	private String type;
	/**
	 * 包含错误的字段的值
	 */
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
