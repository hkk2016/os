package com.supply.hsa.link.domain;

import javax.persistence.*;

@Table(name = "t_htlsync_supplierclass")
public class SupplierClassDO {
    /**
     * 供应商编码
     */
    @Column(name = "supply_code")
    private String supplyCode;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 0：无效 1：有效
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 获取供应商编码
     *
     * @return supply_code - 供应商编码
     */
    public String getSupplyCode() {
        return supplyCode;
    }

    /**
     * 设置供应商编码
     *
     * @param supplyCode 供应商编码
     */
    public void setSupplyCode(String supplyCode) {
        this.supplyCode = supplyCode;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取0：无效 1：有效
     *
     * @return is_active - 0：无效 1：有效
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 设置0：无效 1：有效
     *
     * @param isActive 0：无效 1：有效
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}