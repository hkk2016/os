package com.supply.hsa.link.task.fetch.hotel.container;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.process.SupplyHotelProcessor;
import org.springframework.stereotype.Service;


/**
 * 酒店基本信息\酒店信息 服务 注册管理器
 */
@Service
@SuppressWarnings("rawtypes")
public class SupplyHotelProcessorContainer {

	private Map<SupplierClass, SupplyHotelProcessor> supplyHotelInfoProcessorMap = new ConcurrentHashMap<SupplierClass, SupplyHotelProcessor>();

	/**
	 * 注册酒店信息服务
	 */
	public void registerHotelInfoProcessor(SupplierClass supplierClass,
			SupplyHotelProcessor supplyHotelInfoProcessor) {
		if(supplierClass == null || supplyHotelInfoProcessor == null){
			throw new IllegalArgumentException("注册酒店信息服务异常,[参数不能为空]");
		}
		
		if (supplyHotelInfoProcessorMap.get(supplierClass) == null) {
			this.supplyHotelInfoProcessorMap.put(supplierClass, supplyHotelInfoProcessor);
		}else{
			throw new IllegalArgumentException("注册酒店信息服务异常,[重复注册服务], supplierClass = " + supplierClass.getSupplierClass());
		}
	}

	/**
	 * 获取已注册的酒店基本信息服务供应商
	 */
	public Set<SupplierClass> getRegisteredHotelInfoProcessorSupplier() {
		return this.supplyHotelInfoProcessorMap.keySet();
	}

	/**
	 * 获取酒店信息服务
	 */
	public SupplyHotelProcessor getHotelInfoProcessor(
			SupplierClass supplierClass) {
		if (supplierClass == null) {
			return null;
		}

		return supplyHotelInfoProcessorMap.get(supplierClass);
	}
}
