package com.supply.hsa.link.task.fetch.hotel.result;

import java.util.Map;

public class AnalyzeHotelTaskResult {
	
	private long pricePlanMiddleCount;
	
	private long syncProductDbCount;

	/**解析的中间价格计划数据*/
	private Map<String, Integer> pricePlanMiddle;
	
	/**同步入库的数据*/
	private Map<String, Integer> syncProductDB;
	
	/**coHotel - fcHotelId*/
	private Map<String, Long> hotelIdMapping; 

	public Map<String, Integer> getPricePlanMiddle() {
		return pricePlanMiddle;
	}

	public void setPricePlanMiddle(Map<String, Integer> pricePlanMiddle) {
		this.pricePlanMiddle = pricePlanMiddle;
	}

	public Map<String, Integer> getSyncProductDB() {
		return syncProductDB;
	}

	public void setSyncProductDB(Map<String, Integer> syncProductDB) {
		this.syncProductDB = syncProductDB;
	}

	public long getPricePlanMiddleCount() {
		return pricePlanMiddleCount;
	}

	public void setPricePlanMiddleCount(long pricePlanMiddleCount) {
		this.pricePlanMiddleCount = pricePlanMiddleCount;
	}

	public long getSyncProductDbCount() {
		return syncProductDbCount;
	}

	public void setSyncProductDbCount(long syncProductDbCount) {
		this.syncProductDbCount = syncProductDbCount;
	}

	public Map<String, Long> getHotelIdMapping() {
		return hotelIdMapping;
	}

	public void setHotelIdMapping(Map<String, Long> hotelIdMapping) {
		this.hotelIdMapping = hotelIdMapping;
	}

}
