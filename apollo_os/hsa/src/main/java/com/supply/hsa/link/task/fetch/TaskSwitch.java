package com.supply.hsa.link.task.fetch;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.TaskManager;
import com.supply.hsa.link.task.fetch.WorkerStatus;
import com.supply.hsa.link.task.fetch.hotel.HotelTimerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;



/**
 * 任务开关
 */
@Service
public class TaskSwitch {
	
	@Autowired
	@Qualifier("hotelTimerTaskExecutor")
	private TaskExecutor<HotelTimerTask> hotelTimerTaskExecutor;

	/**
	 * 获取定时任务状态
	 */
	public WorkerStatus getTimerTaskStatus(SupplierClass supplierClass){
		if(supplierClass == null){
			return null;
		}
		HotelTimerTask task = TaskManager.getInstance().getTimerTask(supplierClass);
		return task!=null ? task.getWorkStatus() : null;
	}
	
	/**
	 * 停止定时任务
	 */
	public void stopTimerTask(SupplierClass supplierClass){
		if(supplierClass == null){
			return;
		}
		HotelTimerTask task = TaskManager.getInstance().getTimerTask(supplierClass);
		hotelTimerTaskExecutor.stop(task);
	}
	
	/**
	 * 暂停定时任务
	 */
	public void pauseTimerTask(SupplierClass supplierClass){
		if(supplierClass == null){
			return;
		}
		HotelTimerTask task = TaskManager.getInstance().getTimerTask(supplierClass);
		hotelTimerTaskExecutor.pause(task);
	}
	
	/**
	 * 恢复定时任务
	 */
	public void resumeTimerTask(SupplierClass supplierClass){
		if(supplierClass == null){
			return;
		}
		HotelTimerTask task = TaskManager.getInstance().getTimerTask(supplierClass);
		hotelTimerTaskExecutor.resume(task);
	}
	
}
