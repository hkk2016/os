package com.supply.hsa.link.task.fetch;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;

import java.util.List;

public interface TaskHandler {
	
	/**
	 * 注册服务
	 */
	void register();

	/**
	 * 获取定时器任务 请求对象
	 * 
	 * @param supplierClass 供应商类型
	 */
	List<CommonSyncRequest> getSyncRequestOfTimerTask(SupplierClass supplierClass, String supplierCode, Integer freqId);
}
