package com.supply.hsa.link.common.dto;

import lombok.Data;

import java.util.Date;

@Data
public class HtlSyncPreDataDTO {
    private Integer id;

    /**
     * 供应商编码
     */
    private String supplyCode;

    /**
     * 供应商数据编码（酒店编码）
     */
    private String coDataCode;

    /**
     * 本地数据编码
     */
    private String dataCode;

    /**
     * 本地数据名称
     */
    private String dataName;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 供应商简称
     */
    private String supplierClass;

    /**
     * 1:全量天数 2:指定日期
     */
    private Boolean syncType;

    /**
     * 开始天
     */
    private Integer beginDay;

    /**
     * 结束天
     */
    private Integer endDay;

    /**
     * 指定日期起
     */
    private Date beginDate;

    /**
     * 指定日期止
     */
    private Date endDate;

    /**
     * 同步频率对应id
     */
    private Integer freqId;

}
