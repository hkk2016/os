package com.supply.hsa.link.task.fetch.hotel.analyze;

import java.util.List;
import java.util.Map;


import com.supply.hsa.link.task.fetch.Task;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonAnalyzeRequestDTO;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncResponse;
import com.supply.hsa.link.task.fetch.hotel.dto.PricePlanMiddleDto;
import com.supply.hsa.link.task.fetch.enums.SyncTypeEnum;
import com.supply.hsa.link.task.fetch.hotel.result.AnalyzeHotelTaskResult;

public class AnalyzeHotelTask<T extends PricePlanMiddleDto> extends Task {
	
	private SyncTypeEnum taskType;
	
	/**
	 * 抓取的请求数据
	 */
	private CommonSyncRequest fetchRequest;
	
	/**
	 * 抓取的响应数据
	 */
	private CommonSyncResponse fetchResponse;
	
	/**
	 * 解析的请求数据
	 */
	private CommonAnalyzeRequestDTO analyzeRequest;
	
	/**
	 * 解析的价格计划数据; key=供应商酒店ID,value=供应商数据
	 */
	private Map<String, List<T>> pricePlanMiddleListMap;
	
	private AnalyzeHotelTaskResult taskResult = new AnalyzeHotelTaskResult();

	public CommonSyncRequest getFetchRequest() {
		return fetchRequest;
	}

	public void setFetchRequest(CommonSyncRequest fetchRequest) {
		this.fetchRequest = fetchRequest;
	}

	public CommonSyncResponse getFetchResponse() {
		return fetchResponse;
	}

	public void setFetchResponse(CommonSyncResponse fetchResponse) {
		this.fetchResponse = fetchResponse;
	}

	public Map<String, List<T>> getPricePlanMiddleListMap() {
		return pricePlanMiddleListMap;
	}

	public void setPricePlanMiddleListMap(
			Map<String, List<T>> pricePlanMiddleListMap) {
		this.pricePlanMiddleListMap = pricePlanMiddleListMap;
	}

	public CommonAnalyzeRequestDTO getAnalyzeRequest() {
		return analyzeRequest;
	}

	public void setAnalyzeRequest(CommonAnalyzeRequestDTO analyzeRequest) {
		this.analyzeRequest = analyzeRequest;
	}

	public SyncTypeEnum getTaskType() {
		return taskType;
	}

	public void setTaskType(SyncTypeEnum taskType) {
		this.taskType = taskType;
	}

	public AnalyzeHotelTaskResult getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(AnalyzeHotelTaskResult taskResult) {
		this.taskResult = taskResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class<? extends TaskExecutor> getTaskExecutor() {
		return AnalyzeHotelTaskExecutor.class;
	}

}
