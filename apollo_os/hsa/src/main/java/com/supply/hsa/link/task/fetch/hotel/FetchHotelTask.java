package com.supply.hsa.link.task.fetch.hotel;


import com.supply.hsa.link.task.fetch.Task;
import com.supply.hsa.link.task.fetch.TaskExecutor;
import com.supply.hsa.link.task.fetch.hotel.dto.CommonSyncRequest;
import com.supply.hsa.link.task.fetch.enums.SyncTypeEnum;
import com.supply.hsa.link.task.fetch.hotel.result.FetchHotelTaskResult;

/**
 * 远程获取产品信息任务
 *
 */
public class FetchHotelTask extends Task {
	
	private CommonSyncRequest request;

	private FetchHotelTaskResult taskResult = new FetchHotelTaskResult();

	public CommonSyncRequest getRequest() {
		return request;
	}

	public void setRequest(CommonSyncRequest request) {
		this.request = request;
	}

	public FetchHotelTaskResult getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(FetchHotelTaskResult taskResult) {
		this.taskResult = taskResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class<? extends TaskExecutor> getTaskExecutor() {
		return FetchHotelTaskExecutor.class;
	}
}
