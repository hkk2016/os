package com.supply.hsa.link.task.job;

import com.supply.hsa.link.common.constant.SupplierClass;
import com.supply.hsa.link.common.constant.SystemConstants;
import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;
import com.supply.hsa.link.common.util.ApplicationContextUtil;
import com.supply.hsa.link.common.util.IdUtil;
import com.supply.hsa.link.hotelinfo.server.HotelInfoSyncService;
import com.supply.hsa.link.hotelinfo.server.impl.HotelInfoSyncServiceImpl;
import com.supply.hsa.link.task.fetch.hotel.FetchHotelTask;
import com.supply.hsa.link.task.fetch.server.CommonSupplyFetchService;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * 酒店数据任务调度
 */
public class FetchHotelJob implements Job {
    private final Logger log = LoggerFactory.getLogger(FetchHotelJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
            String jobName = jobDataMap.get("jobName").toString();
            log.info(" job "+jobName+"is start");
            /**
             * 获取到到的参数集合是个多供应商数据的，需要按供应商分割数据处理，
             * 一个直连供应商可能有多套价格体系，用supplyCode区分
             *
             */
            List<HtlSyncPreDataDTO> dtos = (List<HtlSyncPreDataDTO>) jobDataMap.get(SystemConstants.FETCHHOTELTASK_MAP_KEY);

            System.out.println("job "+jobName+">>>>>>>>>>>"+new Date());
            CommonSupplyFetchService commonSupplyFetchService = ApplicationContextUtil.getBean("commonSupplyFetchService", CommonSupplyFetchService.class);
            if(CollectionUtils.isNotEmpty(dtos)){
                for(HtlSyncPreDataDTO dto:dtos) {
                    FetchHotelTask task = new FetchHotelTask();
                    task.setSupplierClass(SupplierClass.getSupplierClass(dto.getSupplierClass()));
                    task.setTaskId(IdUtil.getTaskId());
                    task.setTaskName(jobName);
                    task.setRequest(null);
                    commonSupplyFetchService.fetchHotelProductInfo(task);
                }
            }

            log.info(" job "+jobName+"is end");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("酒店数据任务调度异常",e);
        }

    }
}
