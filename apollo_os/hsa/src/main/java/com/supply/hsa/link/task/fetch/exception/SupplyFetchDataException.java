package com.supply.hsa.link.task.fetch.exception;

import com.supply.hsa.link.task.fetch.enums.SupplyFetchErrorEnum;

/**
 * 抓取数据异常
 */
public class SupplyFetchDataException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private SupplyFetchErrorEnum errorCode;
	
	public SupplyFetchDataException(SupplyFetchErrorEnum errorCode, Exception e){
		super(errorCode.name(), e);
		
		this.errorCode = errorCode;
	}

	public SupplyFetchErrorEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(SupplyFetchErrorEnum errorCode) {
		this.errorCode = errorCode;
	}
}
