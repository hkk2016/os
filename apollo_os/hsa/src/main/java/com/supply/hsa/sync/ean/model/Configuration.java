package com.supply.hsa.sync.ean.model;

public class Configuration {
	/**
	 * 客房中此床配置的类型
	 */
	private String type;
	/**
	 * 客房中此床配置的大小
	 */
	private String size;
	/**
	 * 客房中此床配置的数量
	 */
	private Integer quantity;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
