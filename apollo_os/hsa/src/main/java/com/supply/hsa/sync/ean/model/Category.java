package com.supply.hsa.sync.ean.model;

public class Category {
	/**
	 * 酒店类别 ID
	 */
	private Integer id;
	/**
	 * 酒店的类别
	 */
	private String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
