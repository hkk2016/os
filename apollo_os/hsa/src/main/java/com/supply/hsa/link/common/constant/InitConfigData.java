package com.supply.hsa.link.common.constant;

import java.util.HashMap;
import java.util.Map;

public class InitConfigData {
    /**
     * 配置商家switch供应商Map<supplierClass,Map<merchantCode,Map<FieldName,FieldValue>>>
     */
    public static   Map<String,Map<String, Map<String, String>>> configMerchantMap = new HashMap<String, Map<String,Map<String,String>>>();
    /**
     * 配置共享供应商配置Map<SupplierCode,Map<fieldName,fieldValue>>  supplierCode:共享供应商编码
     */
    public static Map<String,Map<String,String>> shareSupplierMap = new HashMap<String,Map<String,String>>();
}
