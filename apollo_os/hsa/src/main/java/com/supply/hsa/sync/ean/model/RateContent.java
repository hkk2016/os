package com.supply.hsa.sync.ean.model;

import java.util.Map;

public class RateContent {
	private String id;
	private Map<String, Amenity> amenities;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Map<String, Amenity> getAmenities() {
		return amenities;
	}
	public void setAmenities(Map<String, Amenity> amenities) {
		this.amenities = amenities;
	}
	
}
