package com.supply.hsa.sync.ean.model;

public class Checkout {
	/**
	 * 住客必须在此之前退房的时间
	 */
	private String time;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
}
