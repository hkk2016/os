package com.supply.hsa.link.common.server;

import com.supply.hsa.link.common.dto.HtlSyncPreDataDTO;
import com.supply.hsa.link.task.fetch.hotel.dto.SupplyRelation;

import java.util.List;

public interface HotelSyncPreDataServer {
    /**
     * 查询预同步数据
     * @param supplierClass 供应商
     * @param frepId 同步频率ID
     * @return
     */
    public List<HtlSyncPreDataDTO> queryPreDate(String supplierClass, String supplyCode, Integer frepId);
    /**
     * 查询指定供应商关联数据
     * @param request
     * @return
     */
    public List<SupplyRelation> querySupplyRelationList(SupplyRelation request);
}
