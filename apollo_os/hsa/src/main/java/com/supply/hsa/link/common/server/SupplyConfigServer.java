package com.supply.hsa.link.common.server;

import com.supply.hsa.link.common.dto.SupplyConfigDTO;

import java.util.List;

public interface SupplyConfigServer {
    public List<SupplyConfigDTO> queryAllConfigList();

}
