package com.supply.hsa.link.hotelinfo.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

public class SupplyRoomDTO implements Serializable {
    /**
     * 供应商酒店编码
     */
    private String coHotelId;

    /**
     * 供应商房型id
     */
    private String coRoomId;

    /**
     * 供应商房型中文名
     */
    private String coRoomNameCn;

    /**
     * 供应商房型英文名
     */
    private String coRoomNameEn;

    /**
     * 供应商床型
     */
    private String coBedType;

    /**
     * 供应商简称
     */
    private String supplierClass;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    public String getCoHotelId() {
        return coHotelId;
    }

    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    public String getCoRoomId() {
        return coRoomId;
    }

    public void setCoRoomId(String coRoomId) {
        this.coRoomId = coRoomId;
    }

    public String getCoRoomNameCn() {
        return coRoomNameCn;
    }

    public void setCoRoomNameCn(String coRoomNameCn) {
        this.coRoomNameCn = coRoomNameCn;
    }

    public String getCoRoomNameEn() {
        return coRoomNameEn;
    }

    public void setCoRoomNameEn(String coRoomNameEn) {
        this.coRoomNameEn = coRoomNameEn;
    }

    public String getCoBedType() {
        return coBedType;
    }

    public void setCoBedType(String coBedType) {
        this.coBedType = coBedType;
    }

    public String getSupplierClass() {
        return supplierClass;
    }

    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
