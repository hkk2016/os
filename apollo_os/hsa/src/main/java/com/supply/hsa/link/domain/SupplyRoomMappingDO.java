package com.supply.hsa.link.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_htlsync_mp_room")
public class SupplyRoomMappingDO {
    /**
     * 房型映射主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 房型id
     */
    @Column(name = "room_id")
    private Integer roomId;

    /**
     * 供应商房型编码
     */
    @Column(name = "co_room_id")
    private String coRoomId;

    /**
     * 供应商房型名称
     */
    @Column(name = "co_room_name")
    private String coRoomName;

    /**
     * 房型名横
     */
    @Column(name = "room_name")
    private String roomName;

    /**
     * 供应商酒店id
     */
    @Column(name = "co_hotel_id")
    private String coHotelId;

    /**
     * 0:无效 1:有效
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 供应商简称
     */
    @Column(name = "supplier_class")
    private String supplierClass;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 更新时间
     */
    @Column(name = "modify_date")
    private Date modifyDate;

    /**
     * 获取房型映射主键id
     *
     * @return id - 房型映射主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置房型映射主键id
     *
     * @param id 房型映射主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取房型id
     *
     * @return room_id - 房型id
     */
    public Integer getRoomId() {
        return roomId;
    }

    /**
     * 设置房型id
     *
     * @param roomId 房型id
     */
    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    /**
     * 获取供应商房型编码
     *
     * @return co_room_id - 供应商房型编码
     */
    public String getCoRoomId() {
        return coRoomId;
    }

    /**
     * 设置供应商房型编码
     *
     * @param coRoomId 供应商房型编码
     */
    public void setCoRoomId(String coRoomId) {
        this.coRoomId = coRoomId;
    }

    /**
     * 获取供应商房型名称
     *
     * @return co_room_name - 供应商房型名称
     */
    public String getCoRoomName() {
        return coRoomName;
    }

    /**
     * 设置供应商房型名称
     *
     * @param coRoomName 供应商房型名称
     */
    public void setCoRoomName(String coRoomName) {
        this.coRoomName = coRoomName;
    }

    /**
     * 获取房型名横
     *
     * @return room_name - 房型名横
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * 设置房型名横
     *
     * @param roomName 房型名横
     */
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    /**
     * 获取供应商酒店id
     *
     * @return co_hotel_id - 供应商酒店id
     */
    public String getCoHotelId() {
        return coHotelId;
    }

    /**
     * 设置供应商酒店id
     *
     * @param coHotelId 供应商酒店id
     */
    public void setCoHotelId(String coHotelId) {
        this.coHotelId = coHotelId;
    }

    /**
     * 获取0:无效 1:有效
     *
     * @return is_active - 0:无效 1:有效
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 设置0:无效 1:有效
     *
     * @param isActive 0:无效 1:有效
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * 获取供应商简称
     *
     * @return supplier_class - 供应商简称
     */
    public String getSupplierClass() {
        return supplierClass;
    }

    /**
     * 设置供应商简称
     *
     * @param supplierClass 供应商简称
     */
    public void setSupplierClass(String supplierClass) {
        this.supplierClass = supplierClass;
    }

    /**
     * 获取创建人
     *
     * @return creator - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return modifier - 更新人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置更新人
     *
     * @param modifier 更新人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取更新时间
     *
     * @return modify_date - 更新时间
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置更新时间
     *
     * @param modifyDate 更新时间
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}