package com.supply.hsa.link.task.fetch.enums;

public enum SupplyFetchErrorEnum {
	HTTP_EXCEPTION(""),
	
	SERVICE_UNAVAILABLE(""),
	
	DATA_EXCEPTION("");
	
	String msg;
	
	private SupplyFetchErrorEnum(String msg){
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
