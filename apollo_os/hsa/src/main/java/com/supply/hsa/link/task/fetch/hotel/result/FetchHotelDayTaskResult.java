package com.supply.hsa.link.task.fetch.hotel.result;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.supply.hsa.link.task.fetch.enums.SupplyFetchErrorEnum;

public class FetchHotelDayTaskResult{
	
	@JSONField(serialize=false)
	private SupplyFetchErrorEnum error;

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date taskJoinDate;
	
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date taskStartDate;
	
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date taskEndDate;
	
	@JSONField(format = "yyyy-MM-dd")
	private Date checkInDate;
	
	@JSONField(format = "yyyy-MM-dd")
	private Date checkOutDate;

	public Date getTaskStartDate() {
		return taskStartDate;
	}

	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	public Date getTaskEndDate() {
		return taskEndDate;
	}

	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public Date getTaskJoinDate() {
		return taskJoinDate;
	}

	public void setTaskJoinDate(Date taskJoinDate) {
		this.taskJoinDate = taskJoinDate;
	}

	public SupplyFetchErrorEnum getError() {
		return error;
	}

	public void setError(SupplyFetchErrorEnum error) {
		this.error = error;
	}
}
