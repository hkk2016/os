package com.supply.hsa.sync.ean;

public enum EanBedTypeEnum {
    FULL("FullBed",1,"大床"),
    KING("KingBed",1,"大床"),
    QUEEN("QueenBed",1,"大床"),
    SINGLE("TwinBed",0,"单床"),
    SINGLEXL("TwinXLBed",0,"单床"),
    SOFA("SofaBed",0,"单床"),
    FUTON("Futon",0,"单床"),
    BUNK("BunkBed",7,"单床");

    /**
     * EAN床型类型
     */
    private String eanType;
    /**
     * 本地床型key
     */
    private Integer key;
    /**
     * 本地床型
     */
    private String bedType;

    EanBedTypeEnum(String eanType, Integer key, String bedType) {
        this.eanType = eanType;
        this.key = key;
        this.bedType = bedType;
    }

    public static Integer getKeyByEanType(String eanType) {
        for(EanBedTypeEnum bedTypeEnum : EanBedTypeEnum.values()) {
            if(bedTypeEnum.eanType.equals(eanType)) {
                return bedTypeEnum.key;
            }
        }
        return null;
    }
}
