package com.supply.hsa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.supply.hsa.sync.ean.EanHttpUtil;
import com.supply.hsa.sync.ean.model.PropertyContent;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EanRapidTest {
	String apiKey="iv05hip6l5d81qcm2fmr3dj8g";
	String sharedSecret="epbm0bhrfq8ce";
	String ip="192.168.0.145";
	int timeOut=100000;
	
	Gson gson = new Gson();
	
	private String getSignature(){
		Date date= new Date();
		Long timestamp = (date.getTime() / 1000);
		
		String signature = null;
		try {
		   String toBeHashed = apiKey + sharedSecret + timestamp;
		   MessageDigest md = MessageDigest.getInstance("SHA-512");
		   byte[] bytes = md.digest(toBeHashed.getBytes("UTF-8"));
		   StringBuilder sb = new StringBuilder();
		   for(int i=0; i< bytes.length ;i++){
		       sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		   }
		   signature = sb.toString();
		} catch (NoSuchAlgorithmException e) {
		   e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
		   e.printStackTrace();
		}
		String authHeaderValue = "EAN APIKey=" + apiKey +  ",Signature=" + signature + ",timestamp=" + timestamp;
		return authHeaderValue;
	}
	
	@Test
	public void testQueryHotel() throws Exception{
		String url="https://test.ean.com/2.2/properties/content?language=zh-CN&property_id=523388";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", getSignature());
		headers.put("Customer-Ip", ip);
		headers.put("Customer-Session-Id", UUID.randomUUID().toString());
		
		String result= EanHttpUtil.getForJsonGzip(url, headers, timeOut);
		System.out.println(result);
		Map<String, PropertyContent> hotelMap= gson.fromJson(result, new TypeToken<Map<String, PropertyContent>>(){}.getType());
//		
//		PropertyContent propertyContent = JSON.parseObject(result, PropertyContent.class);
		for(String key:hotelMap.keySet()){
			PropertyContent hotel=hotelMap.get(key);
			System.out.println("ok");
		}
		System.out.println("ok");
	}
	
	/**
	 * 查询区域数据调供应商是接口经常失败，得设置调用失败的id重试几次
	 */
	@Test
	public void testQueryRegion(){
		String countrys="37";
		String[] ids=countrys.split(",");
		String leftIDs="";
		for(String country:ids){
			String url="https://test.ean.com/2.2/regions/"+country+"/descendants?language=en-US&include=standard";
//			String url="https://test.ean.com/2.2/regions/"+country+"/descendants?language=zh-CN&include=standard";
			
//			String url="https://test.ean.com/2.2/regions/"+country+"/descendants?language=zh-CN&include=details&include=property_ids";
			
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", getSignature());
			headers.put("Customer-Ip", ip);
			headers.put("Customer-Session-Id", UUID.randomUUID().toString());
			
			try{
				String result=EanHttpUtil.getForJsonGzip(url, headers, timeOut);
//				System.out.println(result);
//				RegionDescendants listMap= gson.fromJson(result, new TypeToken<RegionDescendants>(){}.getType());
//				Map<String, Map<String, Region>> listMap = (Map<String, Map<String, Region>>) JsonUtil.jsonToMap(result);
				System.out.println("ooo");
//				for(String key:listMap.getRegionMap().keySet()){
//					Map<String, Region> regions=listMap.getRegionMap().get(key);
//					for(String temp:regions.keySet()){
//						Region region=regions.get(temp);
//						String tempString=country+"|"+region.getId()+"|"+region.getName()+"|"+region.getName_full()+"|"+region.getType()
//						+"|"+region.getCountry_code()+"|"+region.getProperty_ids();
//						System.out.println(tempString);
//						
////						if(CollectionUtils.isNotEmpty(region.getProperty_ids())){
////							String tempStr="";
////							for(String str:region.getProperty_ids()){
////								tempStr=tempStr+str+",";
////							}
////							writeIDsToFile(tempStr);
////							System.out.println(tempStr);
////						}
//					}
//				}
			}catch (Exception e) {
				e.printStackTrace();
				leftIDs=leftIDs+country+",";
			}
		}
		System.out.println(leftIDs);
	}
	
	@Test
	public void testAvailability(){
		String url="https://test.ean.com/2.2/properties/availability?checkin=2019-04-03&checkout=2019-04-04&currency=HKD"
				+"&language=en-US&country_code=HK&occupancy=2&property_id=558012&sales_channel=cache&sales_environment=hotel_only"
						+ "&sort_type=preferred&include=all_rates";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", getSignature());
		headers.put("Customer-Ip", ip);
		headers.put("Customer-Session-Id", UUID.randomUUID().toString());
		
		url="https://test.ean.com/2.2/properties/availability/558012/rooms/200749719/rates/209660372/price-check?token=Ql1WAERHXV1QOxULUAtVUVxSBgMACE8BUlNYFAEDAg9LU1RVVhQCAgwGCFIHX1cDDQMTCQFHAAlQVxZrXQBpQFFVWEFSDnVtZydwLHUSEl9MVGpBTRQAD10AVldAVBIeXwdVRkcAXQBBCVZHWUtCPlQHRw5UQ1ALVDxQXlUIXQFVEF1SQl5WEF5WAT1SBFUOCUdVUAxfFFkGUFxXWks2YRNHUxEDag1EQQheCAkVRlMVVW4WG0YHXmJgIyt1dGF9FVIDRw1SBAhHV1FWVw1cVwQCUgQBHANXGQQDQkJVF0cLB0A7FRdfBwpUBm5dAQxWCQYOVAIVVxZGFFALBU5bcC4gElkCQF4GQw9fAWtaDVQPXwRUWAcQW19XBggNQkxZAlUGWhkCBE8FVR5bUQFsWQUMAABXVAMeEkYPXQwMVwQDQltYDRBVVDxFFl1GPlMXDUYWXANSXkBdQktdBAFCE1FfBT1LVxYUVFEOFFUUW1VPEg4KQUQFFhYSDVpSED1BVUtDDFlba1pTWVNXAwBWB14DGFpUDVQUUgcMDBhWW1lWHFAGVgpcVQcADgcFVxVFXBZGDQtbPAhVBFJWClRSAwIBGgQDAVIYA1dWARlVWQVeHAdRUgJSAw1cCAcEAEBLUQhTQGgCWwJWWgENCVtTAlEDEwJUWVwXUhdWUmsSWglcCwIDAQweVAQVVlRgUQMWCiALBkcLIA1WG1AEBjxFQVRZVxY5UAxCXBNeCFlWWkZcWF4WB1o9DF9YG0NcRkNmR0cLU0JQUFQVUUsOEgVaTFdEEExnUlwHUQl4LxRWAFc6BlcXBhdZERdRDF8JVBoyTVFdChxxUQcSBFABOVQJVgMNU0ERU0EKWAgMQwF7QAIjFFMKFEJXVkNbERpHBQpBAyQGRgZxFVAHElFDUUABAkQCchJWV2JNA1MIHAMLQAZ1QQoGFhpAVUYCVx0HIBFXUGITBABdcFZcQQsKR1N0RA11RBFNSBMKXlFBOV0ADwNWRxYXWAEDPBEJFlZbX1cEAAgeVQUTd1tSBwoRQVsNVgtcDFcCCFEJUw==";
		
		String result=EanHttpUtil.getForJsonGzip(url, headers, timeOut);
		System.out.println(result);
	}
	
	private void writeStringToFile(String str) {
        try {
            FileWriter fw = new FileWriter("F:/region.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(str+"\r\n ");// 往已有的文件上添加字符串
            bw.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	private void writeIDsToFile(String str) {
        try {
            FileWriter fw = new FileWriter("F:/eanids.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(str);// 往已有的文件上添加字符串
            bw.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
